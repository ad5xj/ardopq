# ARDOPQ

ARDOPQ is a program for receiving and transmitting the custom protocol **A**mateur **R**adio **O**pen **P**rotocol (ARDOP) by Rick Muething, KN6KB and John Wiseman, G8BPQ (originally written in VB.NET and 'C'). It is implemented as a virtual TNC, using the computer soundcard or an external computer interface that contains a soundcard. It has a TCP/IP interface for connection to a host application. The user interface is an abstract simulation of a hardware terminal node controller (TNC), hince the term "virtual".

**Current Version:** v0.9.0 pre-Alpha

## Contribution guidelines

Contact AD5XJ Ken ad5xj@arrl.net 
project owner and repo admin for instructions on how you can contribute to this project.

## Getting Started

The program will remember the last position and the size of each window on restart.

Before use, ARDOPQ will need some configuration.

The Setup tool is accessed through the "Setup" menu in the Virtual TNC window.
The following selections are available:

- Radio Settings (optional)  
- TNC Setup  
  All settings to the computer interface and audio source and sink are done with TNC setup.
  All settings for CAT/CI-V interface to your rig (where available) are done with the Radio setup.
- INI file  

You may wish to edit the .ini file directly. Please use a plain text editor like Wordpad, Bluefish, etc.
to change the .ini file. Word processing formatting is not allowed in this file. On Windows, each line
must end with a carriage return (cr) and line feed (lf). On Linux the system standard is (lf) only.


There are several sections which may be edited. It is recommended that you only change the settings
you are absolutely sure of as some can cause the TNC to cease to function.

A typical .ini file from a Microsoft Windows installation may look like this:

~~~~
[STATION]
Callsign=AD5XJ
EnableCWID=false
LoginPassword=\x1
SecureHostLogin=false

[SOUND]
CaptureDevice=Microphone (USB Audio CODEC)
PlaybackDevice=Speakers (USB Audio CODEC)
Stereo=true
RxChannels=2
RxSampleRate=8000
TxSampleRate=8000
RxVolume=80
DriveLevel=80
Squelch=5
CaptureClock=0
PlaybackClock=0

[BLUETOOTH]
HostPairing=
HostParing=

[NETWORK]
DefaultNetworkConfiguration=287176528
TCPIPAddress=127.0.0.1
TCPIPPort=8515

[SERIAL]
SerialBaud=0
SerialCOMPort=

[ARDOP]
ARQConRepReq=2
ARQTimeout=30
AccumStats=true
FECId=false
FECMode=Negotiated
FECRepeats=0
LeaderLenth=100
ProtocolMode=8FSK.200.25
SlowCPU=false
TrailerLength=0
TuningRange=0
Version=v0.8.2

[SPECTRUM]
avg=0.9
maxdb=-25
range=35

[TNC]
AccumStats=false
CommandTrace=true
DebugLog=false
DispSpectrum=false
DispWaterfall=true
EnableCWID=false
EnblRadioCont=false
HostBlueTooth=false
HostPairing=
HostSerial=false
HostTCPIP=true
LeaderLenth=100
SlowCPU=false
StartMinimized=true
TrailerLength=0
TuningRange=100

[TNC_PANEL]
WindowGeometry=@ByteArray(\x1\xd9\xd0\xcb\0\x2\0\0\0\0\x1\xd1\0\0\x1_\0\0\x3\xfd\0\0\x2\x35\0\0\x1\xd2\0\0\x1v\0\0\x3\xfc\0\0\x2\x34\0\0\0\0\0\0\0\0\x5\0)
WindowState=@ByteArray(\0\0\0\xff\0\0\0\0\xfd\0\0\0\0\0\0\x2+\0\0\0\x8b\0\0\0\x4\0\0\0\x4\0\0\0\b\0\0\0\b\xfc\0\0\0\x1\0\0\0\x2\0\0\0\x2\0\0\0\x16\0m\0\x61\0i\0n\0T\0o\0o\0l\0\x42\0\x61\0r\x1\0\0\0\0\xff\xff\xff\xff\0\0\0\0\0\0\0\0\0\0\0\xe\0t\0o\0o\0l\0\x42\0\x61\0r\x1\0\0\0\xe\xff\xff\xff\xff\0\0\0\0\0\0\0\0)
~~~~


## Programmer Application Interface

The ARDOPQ Virtual TNC has only one interface for programmers.
That interface is to allow a host application to connect through TCP/IP with the TNC.
The Command Structure page in the user documentation (Doxygen) will list all allowable 
commands that operate the TNC and return information from the modem.

Programmers may also be interested in the internals of the TNC application. The
classes are listed and documented for your convenience. Please see the copyright
notice on the Main Page and the theory text when making internal changes for release
to the public. Be sure to contact the repo maintainers to publish your changes or become
a active developer in this project. Contact ad5xj@arrl.net for information and prerequisites
to develop for ARDOPQ.

### Step 1: Prerequisites

Before you can compile ARDOPQ make sure that you have installed the following software.

**LINUX:**

* g++ 
* libfftw3-dev 
* Qt SDK 5.7.0 or later (not compatible with v4) and dependencies  
* hamlib-dev 
* libasound2-dev 
* libpulse-dev
* build-essential 
* libgl1-mesa-dev
* Also see the Qt Docs for platform requirements: http://doc.qt.io/qt-5/linux.html
ARDOPQ is *NOT* compatible with Qt 4.8.6. Developed with and for Qt 5.5.x or higher (current version at this writing 5.7.1). 


**WINDOWS (XP, Vista, 7, 8, 10):**

* Qt 5.7.0 (or later) SDK with MinGW  
* FFTW3 library for Windows
* Python for Windows
* Git for windows (if you want to use the repo from QtCreator)
* Also see the Qt Docs for platform requirements: http://doc.qt.io/qt-5/windows-support.html
There is some information on the page referenced above on deployment issues. Please read carefully.
~~~~
**Note:**
Digia has made a Visual Studio integration product for Qt 5 available as a development environment.
**Use with caution!** The use of the .NET interface is NOT recommended.
VS.NET C++ is not GCC++ compatible nor is it transportable to other platforms (like Linux or Mac OS X)
without considerable platform specific coding.
~~~~


**MAC OS X:**

* g++  
* libfftw3-dev  
* Qt SDK 5.7.0 or later for MAC (not compatible with v4) and dependencies  
* hamlib-dev  
* libasound2-dev  
* libpulse-dev  
See also the deployment issues in the Qt Documentation: http://doc.qt.io/qt-5/osx-issues.html
ARDOPQ is *NOT* compatible with Qt 4.8.6. Developed with and for Qt 5.5.x or higher (current version at this writing 5.7.1)


### Step 2: Getting the ARDOPQ software

The latest versions of ARDOPQ is always available at https//bitbucket.org/ardopq.
Download the latest source bundle to a local directory (e.g. ~/Downloads)
Binary install applications will be made available as time allows.

The documentation for the program classes and some theory is provided. You should run the Doxygen or the Doxywizard GUI against the Doxyfile in the docs folder. If you are unfamiliar with Doxygen or the Doxywizard, consult the Doxygen web site for instructions: www.doxygen.org

Module Groups for ARDOPQ Development

* MODEM (including host interface commands)  
* AUDIO  (Qt provided interface to the platform available audio devices)
* NETWORK  (TCP/IP interface and host command structure)
* SERIAL   (Serial Port interface for host interface or PTT/CI-V)
* BLUETOOTH (Bluetooth paring for host interface connection - unused so far)  
* PROTOCOL (The ARDOP protocol layer ) 
* FFTW    (The MIT Fast Fourier Transform library interface and routines)
* WIDGETS (custom widgets designed for the GUI)

## Host Interface Commands

The command interface is via ASCII commands sent to the TCPIP address/port, the serial port or to the
Bluetooth paired device identified in the host .ini file. It is designed for a computerized host
interface (not keyboard commands as in 1980’s type Packet TNCs). For robustness all serial or
Bluetooth commands and data are protected by a 16 bit CRC. TCPIP commands are not CRC protected as
the TCPIP protocol itself has built in CRC and auto repeat if necessary. All commands are plain
(readable) text using 7 bit ASCII character encoding.

See the docs for theory behind the interface and the class implementation.