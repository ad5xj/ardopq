#include <QtCore/QDebug>
#include <QtWidgets/QWidget>
#include <QtGui/QPainter>
#include <QtGui/QPaintEvent>
#include <QtGui/QPainter>

QT_CHARTS_USE_NAMESPACE

#include <QtCore/qmath.h>
#include <QtGui/QPainter>
#include <QtGui/QPaintEvent>
#include <QtWidgets/QWidget>

#include "waterfallwidget.hpp"

WaterfallWidget::WaterfallWidget(QWidget *parent) : QWidget(parent)
{
    m_displayTypeFlag = WaterFall;
    m_bandWidth       = MAX200;
    m_displayHeight   = 0;
    m_fftMax          = 0;
    m_fftMin          = 0;
    m_displayHeight   = 100;
    m_maxSlots        = 206;
    m_windowSize      = 0;
    m_fftLength       = 0;
    m_samplingRate    = 0;

    m_range           = 0.0;
    m_avg             = 0.0;
    m_hertzPerSlot    = 11.719;   /* 12 hz per slot in frequency domain */
    m_tuningRange     = 200.0;

    m_colorOrange.reserve(3);
    m_colorKhaki.reserve(3);
    m_colorCyan.reserve(3);
    m_colorDeepSkyBlue.reserve(3);
    m_colorRoyalBlue.reserve(3);
    m_colorNavyBlue.reserve(3);
    m_colorBlack.reserve(3);
    m_colorTomato.reserve(3);
    m_colorChartreuse.reserve(3);

    m_displayHeight = 100;
    m_maxSlots      = 206;
    m_tuningRange   = 200;
    m_hertzPerSlot  = 11.719;       /* 12 hz per slot in frequency domain */

    m_displayTypeFlag = displayType::WaterFall;
    m_ARQBandWidth = ARQBandWidthType::MAX200;
    m_displayTypeChanged = false;

    m_displayImage = QImage(m_maxSlots, m_displayHeight, QImage::Format_RGB888);
    m_displayImage.fill(Qt::black);
    QPalette palette;
    palette.setBrush(QPalette::Background, m_displayImage);
    this->setPalette(palette);
}

WaterfallWidget::~WaterfallWidget() {  }

// -------------------------------------------------
// PROPERTY SETS AND GETS
// -------------------------------------------------
void WaterfallWidget::setDisplayType(displayType val)
{
    if ( val == Disabled )
    {
        this->hide();
        m_displayTypeFlag = val;
        return;
    }

    if ( val != m_displayTypeFlag )
    {
        this->show();
        m_displayTypeFlag = val;
        m_displayImage.fill(Qt::black);

    }
}

// ------------------------------------------------- //

void WaterfallWidget::init(qint32 iWaterfallWindowSize, qint32 iBlockCount, qint64 iSampleingRate)
{
    quint32 i;

    Q_ASSERT_X(iSamplingRate != 0, "WaterfallWidget::init", "Sampling Rate is zero");

    // fix so that VS2013 will compile, it has an error that it won't accept
    // initializers for arrays
    m_colorOrange[0] = 255;
    m_colorOrange[1] = 165;
    m_colorOrange[2] =  0;
    m_colorKhaki[0]  = 95;
    m_colorKhaki[1]  = 176;
    m_colorKhaki[2] = 145;
    m_colorCyan[0]   = 0;
    m_colorCyan[1]   = 255;
    m_colorCyan[2]   = 255;
    m_colorDeepSkyBlue[0] =   0;
    m_colorDeepSkyBlue[1] = 191;
    m_colorDeepSkyBlue[2] = 255;
    m_colorRoyalBlue[0]   =   0;
    m_colorRoyalBlue[1]   =  35;
    m_colorRoyalBlue[2]   = 102;
    m_colorNavyBlue[0]    =   0;
    m_colorNavyBlue[1]    =   0;
    m_colorNavyBlue[2]    = 128;
    m_colorBlack[0]       =   0;
    m_colorBlack[1]       =   0;
    m_colorBlack[2]       =   0;
    m_colorTomato[0]      = 255;
    m_colorTomato[1]      =  99;
    m_colorTomato[2]      = 71;
    m_colorChartreuse[0]  = 223;
    m_colorChartreuse[1]  = 255;
    m_colorChartreuse[2]  = 0;
}

void WaterfallWidget::updateDisplay(QVector<qreal> *dRealFftValues, QVector<qreal> *dImaginaryFftValues)
{

    if ( m_displayTypeChanged == true)
    {
        m_displayTypeChanged = false;
        this->show();
    }

    if ( m_displayTypeFlag == WaterFall)
    {
        UpdateWaterFallDisplay(dRealFftValues, dImaginaryFftValues);
    }
    else
    {
        if ( m_displayTypeFlag == Spectrum)
        {
            UpdateSpectrumDisplay(dRealFftValues, dImaginaryFftValues);
        }
    }
}

void WaterfallWidget::UpdateWaterFallDisplay(QVector<real> dRealFftValues, QVector<real> dImaginaryFftValues)
{
    quint8 *iScanLineData  =   0;
    quint8 *iColorLevels   = m_colorBlack;
    qint32 i               =   0;
    qint32 iBandWidth      = 200;
    qint32 iDelta          =   0;
    qint32 iTuneLineLow    =   0;
    qint32 iTuneLineHigh   =   0;
    qreal dMagAvg          = 0.0;
    qreal dSignalMagnitude = 0.0;
    qreal dWork            = 0.0;
    QVector<qreal> dMag;
    dMag.resize(m_maxSlots);

    for ( i = 0; i < m_maxSlots; ++i )
    {
        dMag[i] = pow(dRealFftValues->at(i + iOffset), 2) + pow(dImaginaryFftValues->at(i + iOffset), 2);
        dMagAvg += dMag.at(i);
    }


    iDelta = ((m_bandWidth / 2) + m_tuningRange) / m_hertzPerSlot;
    iTuneLineLow = qMax(((m_maxSlots / 2) - iDelta), 3);
    iTuneLineHigh = qMin(((m_maxSlots / 2) + iDelta), m_maxSlots - 3);
    dMagAvg = LOG10(dMagAvg / 50000);
    for ( i = m_displayHeight - 1; i > 0; --i )
    {
        // scroll image down one scan line
        iScanLineData = m_displayImage.scanLine(i - 1);
        memcpy(m_displayImage.scanLine(i), iScanLineData, m_displayImage.bytesPerLine());
    }
    iScanLineData = m_displayImage.scanLine(0);     // get top line data

    // For each frequency band to be displayed, determine the color of the pixel used to display
    // the intensity of the signal at that frequency.

    dMag.reserve(206);
    dMag.clear();

    for ( i = 0; i < m_maxSlots; ++i )
    {
        dMag[i] = qPow(dRealFftValues.at(i + 25), 2) + qPow(dImaginaryFftValues.at(i + 25), 2);
        dMagAvg += dMag.at(i);
    }

    switch(m_ARQBandWidth)
    {
    case ARQBandWidthType::MAX200:
        break;
    case ARQBandWidthType::FORCED200:
        iBandWidth = 200;
        break;
    case ARQBandWidthType::MAX500:
        break;
    case ARQBandWidthType::FORCED500:
        iBandWidth = 500;
        break;
    case ARQBandWidthType::MAX1000:
        break;
    case ARQBandWidthType::FORCED1000:
        iBandWidth = 1000;
        break;
    case ARQBandWidthType::MAX2000:
        break;
    case ARQBandWidthType::FORCED2000:
        iBandWidth = 2000;
        break;
    default:
        iBandWidth = 200;
    }


    iDelta = ((iBandWidth / 2) + m_tuningRange) / m_hertzPerSlot;
    iTuneLineLow = qMax(((m_maxSlots / 2) - iDelta), 3);
    iTuneLineHigh = qMin(((m_maxSlots / 2) + iDelta), m_maxSlots - 3);
    dMagAvg = qLn(dMagAvg / 50000) / qLn(10);  /* log base 10 dMagAve / 50000 */

    for ( i = m_displayHeight - 1; i > 0; --i )
    {
        // scroll image down one scan line
        iScanLineData = m_displayImage.scanLine.at(i - 1);
        memcpy(m_displayImage.scanLine.at(i), iScanLineData, m_displayImage.bytesPerLine());
    }
    iScanLineData = m_displayImage.scanLine.at(0);     /* get top line data */

    for ( i = 0; i < m_maxSlots; ++i )
    {
        dWork = qLn(dMag.at(i) + 0.01) / qLn(10);
        dSignalMagnitude = (0.25 + (2.5 / dMagAvg)) + dWork;
        if ( dSignalMagnitude > 6.5 )
        {
            iColorLevels = m_colorOrange.data();
        }
        else
        {
            if (dSignalMagnitude > 6.0 )
            {
                iColorLevels = m_colorKhaki.data();
        }
        else
        {
            if ( dSignalMagnitude > 6.0 )
            {
                iColorLevels = m_colorKhaki;
            }
            else
            {
                if ( dSignalMagnitude > 5.5 )
                {
                    iColorLevels = m_colorCyan.data();
                }
                else
                {
                    if ( dSignalMagnitude > 5.0 )
                    {
                        iColorLevels = m_colorDeepSkyBlue.data();
                    }
                    else
                    {
                        if (dSignalMagnitude > 4.5)
                        {
                            iColorLevels = m_colorRoyalBlue.data();
                        }
                    }
                    else
                    {
                        if ( dSignalMagnitude > 4.5 )
                        {
                            iColorLevels = m_colorRoyalBlue;
                        }
                        else
                        {
                            if ( dSignalMagnitude > 4.0 )
                            {
                                iColorLevels = m_colorNavyBlue.data();
                            }
                            else
                            {
                                iColorLevels = m_colorBlack.data();
                            }
                            else
                            {
                                iColorLevels = m_colorBlack;
                            }
                        }
                    }
                }
            }
        }

        if ( i == (m_maxSlots / 2) )
        {
            iColorLevels = m_colorTomato.data();
        }
        else
        {
            if ( (i == iTuneLineLow) ||
                 (i == iTuneLineLow - 1)
               )
            {
                iColorLevels = m_colorChartreuse.data();
            }
            else
            {
                if ( (i == iTuneLineHigh) ||
                     (i == (iTuneLineHigh + 1))
                   )
                {
                    iColorLevels = m_colorChartreuse.data();
                }
            }
        }

        iScanLineData[(i * 3) + 0] = iColorLevels[0];
        iScanLineData[(i * 3) + 1] = iColorLevels[1];
        iScanLineData[(i * 3) + 2] = iColorLevels[2];

        quint8 x = (i * 3);

        iScanLineData[x + 0] = iColorLevels.at(0);
        iScanLineData[x + 1] = iColorLevels.at(1);
        iScanLineData[x + 2] = iColorLevels.at(2);
    }

    QPalette palette;
    palette.setBrush(QPalette::Background, m_displayImage);
    setPalette(palette);
}

void WaterfallWidget::UpdateSpectrumDisplay(QVector<qreal> *dRealFftValues, QVector<qreal> *dImaginaryFftValues)
{
    static qreal dMaxScale = 0.0;
    static QVector<qreal> dMag(m_maxSlots);

    quint8 *iScanLineData;
    qint32 iDelta = 0;
    qint32 iTuneLineLow = 0;;
    qint32 iTuneLineHigh = 0;
    qint32 iIntWork = 0;
    quint32 i = 0;
    qreal dMagMax = 0.0;
    qreal dMagMin = 0.0;
    QVector<qreal> dMagBusy;
    dMagBusy.resize(m_maxSlots);
    dMag.resize(m_maxSlots);

    for ( i = 0; i < m_maxSlots; ++i )
    {
        dMagBusy[i] = pow(dRealFftValues->at(i + 25), 2) + pow(dImaginaryFftValues->at(i + 25), 2);
    }
}

void WaterfallWidget::setDisplayTypeFlag(qint8 value)
{
    if ( value == displayType::Disabled )
    {
        this->hide();
        m_displayTypeChanged = true;
        return;
    }

    if ( value != m_displayTypeFlag ) m_displayTypeChanged = true;
}

