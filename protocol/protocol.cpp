#include "utils.hpp"
#include "demodulator.hpp"
#include "tncpanel.hpp"

#include "protocol.hpp"

ARDOPprotocol::ARDOPprotocol(TNCPanel *parent) : QObject(parent)
{
    objMain = parent;

    intFrameRepeatInterval = 2000; //!< delay time in ms between rpeated frames
    intNegotiatedARQBW     = 200;
    intARQRTmeasuredMs     = 0;

    dttStartRTMeasure      = QDateTime::currentDateTime();
}

void ARDOPprotocol::setIsListening(bool value)
{
    // do other things if session in progress
    // (e.g Dirty disconnect etc)
    blnListen = value;
    if ( blnListen )
    {
        intRcvdSamples.clear(); // clear out any old samples
        intRcvdSamplesRPtr = 0;
        // clear out any old samples
        objMain->objDemod->intMixedSamples.clear();
        // this will force Search for leader tone to use
        // full tuning range until good frame type decode is made.
        objMain->objDemod->setLastGoodTypeDecode(QDateTime::currentDateTime().addSecs(-30));
//        objMain->objBusy.ClearBusy();
    }

    objMain->RxState = SearchingForLeader;
    ARDOPState = DISC;
}

void ARDOPprotocol::setMCB(ModemControlBlock *mcb)
{
    MCB = mcb;
}

// -------------------------------------------------------
//                 PUBLIC METHODS AND FUNCTIONS
// -------------------------------------------------------
void ARDOPprotocol::Break()
{
    if ( !(ARDOPState == IRS ) )
    {
        if ( MCB->DebugLog )
        {
            qDebug() << "BREAK command received in Prototocol state="
                     << ARDOPStateToString()
                     << ", ARQState="
                     << ARQStateToString()
                     << ", Command ignored";
        }

        if ( MCB->DebugLog && MCB->AutoBreak )
        {
            qDebug() << "BREAK command received with AutoBreak=true";
        }
        blnBREAKCmd = true; // Set flag to process pending BREAK
    }
} // Break()

