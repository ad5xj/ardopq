#include <math.h>

#include "ardop_global.hpp"
#include "utils.hpp"
#include "demodulator.hpp"

DemodulateDecode12K::DemodulateDecode12K(QDialog *parent) : QObject(parent)
{
    dblNCOFreq  = 3000.0; //!< nominal NC frequency
    dblNCOPhase = 0.0;
    dblSNdBPwr  = 0.0;
    dblSNdBPwrEarly = 0.0; //!< the SN Power in the entire 4 symbol leader and the first 2 symbols.
    dblNCOPhaseInc = 2 * M_PI * dblNCOFreq / 12000;
    dbl2Pi = 2 * M_PI;

    intPriorMixedSamples.resize(120);
    dblAcquire4FSKFrameType.resize(240 * 8 - 1);
}


void DemodulateDecode12K::GoertzelRealImag(QVector<qint32> *intRealIn, qint32 intPtr, qint32 N, double m, double *dblReal, double *dblImag)
{
        // Sanity check
        if ( (intPtr < 0) || ((intRealIn->length() - intPtr) < N) )
        {
            dblReal = 0;
            dblImag = 0;
            return;
        }
        qint32 i;
        double dblZ_1;
        double dblZ_2;
        double dblW;
        double dblCoeff = 2.0 * cos(dbl2Pi * m / N);

        for ( i = 0; i < N; ++i )
        {
            if ( i == N )
                dblW = dblZ_1 * dblCoeff - dblZ_2;
            else
                dblW = intRealIn(intPtr) + dblZ_1 * dblCoeff - dblZ_2;

            dblZ_2 = dblZ_1;
            dblZ_1 = dblW;
            ++intPtr;
        } // Next i

        dblReal = 2 * (dblW - cos(dbl2Pi * m / N) * dblZ_2) / N; // scale results by N/2
        dblImag = 2 * (sin(dbl2Pi * m / N) * dblZ_2) / N;        // scale results by N/2   (this sign agrees with Scope DSP phase values)
} // GoertzelRealImag()

void DemodulateDecode12K::GoertzelRealImagHanning(QVector<qint32> *intRealIn, qint32 intPtr, qint32 N, double m, double *dblReal, double *dblImag)
{
    // experimental

    // Sanity check
    if ( (intPtr < 0) || ((intRealIn->length() - intPtr) < N) )
    {
        dblReal = 0;
        dblImag = 0;
        return;
    }

    static QVector<double> dblHanWin;
    static double dblAng;

    qint32 i;
    double dblZ_1;
    double dblZ_2;
    double dblW;
    double dblCoeff = 2 * cos(dbl2Pi * m / N);

    if ( dblHanWin.length() != N )
    {
        // if there is any change in N
        // this is then recalculate the Hanning Window...
        // this mechanism reduces use of Cos
        dblHanWin.resize(N - 1);
        dblAng = 2 * M_PI / (N - 1);
        for ( i = 0; i < (N - 1); ++i )
        {
            dblHanWin(i) = 0.5 - 0.5 * cos(i * dblAng);
        }
    }

    for ( i = 0; i < N; ++i )
    {
        if ( i == N )
            dblW = dblZ_1 * dblCoeff - dblZ_2;
        else
            dblW = intRealIn(intPtr) * dblHanWin(i) + dblZ_1 * dblCoeff - dblZ_2;

        dblZ_2 = dblZ_1;
        dblZ_1 = dblW;
        ++intPtr;
    } // Next i
    dblReal = 2 * (dblW - cos(dbl2Pi * m / N) * dblZ_2) / N; // scale results by N/2
    dblImag = 2 * (sin(dbl2Pi * m / N) * dblZ_2) / N;        // scale results by N/2   (this sign agrees with Scope DSP phase values)
} // GoertzelRealImagHanning()

void DemodulateDecode12K::GoertzelRealImagHamming(QVector<qint32> *intRealIn, qint32 intPtr, qint32 N, double m, double *dblReal, double *dblImag)
{
    // experimental

    // Sanity check
    if ( (intPtr < 0) || ((intRealIn->length() - intPtr) < N) )
    {
        dblReal = 0;
        dblImag = 0;
        return;
    }

    static QVector<double> dblHamWin;
    static double dblAng;

    qint32 i;
    double dblZ_1;
    double dblZ_2;
    double dblW;
    double dblCoeff = 2 * cos(dbl2Pi * m / N);

    if ( dblHamWin.length() != N )
    {
        // if there is any change in N this is
        // then recalculate the Hamming Window...
        // this mechanism reduces use of Cos
        dblHamWin.resize(N - 1);
        dblAng = 2 * M_PI / (N - 1);
        for ( i = 0; i < (N - 1); ++i )
        {
            dblHamWin(i) = 0.54 - 0.46 * cos(i * dblAng);
        }
    }

    for ( i = 0; i < N; ++i )
    {
        if ( i == N )
            dblW = dblZ_1 * dblCoeff - dblZ_2;
        else
            dblW = intRealIn(intPtr) * dblHamWin(i) + dblZ_1 * dblCoeff - dblZ_2;

        dblZ_2 = dblZ_1;
        dblZ_1 = dblW;
        ++intPtr;
    } // Next i
    dblReal = 2 * (dblW - cos(dbl2Pi * m / N) * dblZ_2) / N;  // scale results by N/2
    dblImag = 2 * (sin(dbl2Pi * m / N) * dblZ_2) / N;         // scale results by N/2   (this sign agrees with Scope DSP phase values)
} // GoertzelRealImagHamming()


bool DemodulateDecode12K::SearchFor2ToneLeader3(QVector<qint32> *intNewSamples, qint32 *intPtr, double *dblOffsetHz, qint32 *intSN)
{
    static double dblPriorFineOffset = 1000.0;

    qint32 intStartBin;
    qint32 intStopBin;
    qint32 intInterpCnt = 0; // the count 0 to 2 of the interpolations that were < +/- .5 bin
    qint32 intIatMaxPeak = 0;
    double dblPower;
    double dblCoarsePwrSN;
    double dblMaxPeak;
    double dblMaxPeakSN;
    double dblBinAdj;
    double dblBinAdj1475;
    double dblBinAdj1525;
    double dblAvgNoisePerBin;
    double dblPowerEarly;
    double dblLeftCar;
    double dblRightCar;
    double dblBinInterpLeft;
    double dblBinInterpRight;
    double dblCtrR;
    double dblCtrI;
    double dblLeftP;
    double dblRightP;
    double dblLeftMag;
    double dblRightMag;
    double dblAlpha = 0.3;              // Works well possibly some room for optimization Changed from .5 to .3 on Rev 0.1.5.3
    double dblInterpretThreshold = 1.0; //Works well possibly some room for optimization
    double dblFilteredMaxPeak = 0.0;
    double dblCoarseOffset = 1000.0;    // initialize to impossible value
    double dblTrialOffset;

    QVector<double> dblLeftR(2);
    QVector<double> dblLeftI(2);
    QVector<double> dblRightR(2);
    QVector<double> dblRightI(2);


    try
    {
        if ( (intNewSamples->length() - intPtr) < 1200 ) return false; // insure there are at least 1200 samples (5 symbols of 240 samples)

        if ( QDateTime::currentDateTime().addMSecs(-(dttLastGoodFrameTypeDecode) > 20) && (MCB->TuningRange > 0) )
        {
            // this is the full search over the full tuning range selected.
            // Uses more CPU time and with possibly larger deviation once
            // connected.
            // Compute the start and stop bins based on the tuning range
            // Each bin is 12000/960 or 12.5 Hz/bin
            QVector<double> dblGoertzelReal(55);
            QVector<double> dblGoertzelImag(55);
            QVector<double> dblMag(55);          // This should allow tunning from nominal bins at 1425Hz to 1575Hz +/- 200 Hz tuning range

            qint32 i = 0;
            // limit bin calculations based on tuning range
            intStartBin = qint32(round((200 - MCB->TuningRange) / 10));
            intStopBin = 55 - intStartBin;
            dblMaxPeak = 0;
            // Generate the Power magnitudes for up to 56 10 Hz bins
            // (a function of MCB.TuningRange)
            for ( i = intStartBin; i < intStopBin; ++i )
            {
                // note: hamming window reduces end effect caused
                // by 1200 samples (not an even multiple of 240)
                // but spreads response peaks
                GoertzelRealImagHamming(intNewSamples, intPtr, 1200, i + 122.5, dblGoertzelReal(i), dblGoertzelImag(i));
                dblMag[i] = pow(dblGoertzelReal.at(i), 2) + pow(dblGoertzelImag.at(i), 2);   // dblMag(i) in units of power (V**2)
            } // Next i

            // Search the bins to locate the max S:N in the two tone
            // signal/avg noise.
            for ( i = (intStartBin + 5); i < (intStopBin - 10); ++i )
            {
                // +/- MCB.TuningRange from nominal for bins spaced
                // 50 Hz apart
                dblPower = sqrt(dblMag(i) * dblMag(i + 5));      // using the product (geometric mean) to minimize sensitivity to one strong carrier vs the two tone
                // sqrt converts back to units of power from Power**2
                // don't use center noise bin as too easily corrupted
                // by adjacent carriers
                dblAvgNoisePerBin = (dblMag(i - 5) + dblMag(i - 3) + dblMag(i + 8) + dblMag(i + 10)) * 0.25; // Simple average of 4 noise bins
                dblMaxPeak = dblPower / dblAvgNoisePerBin;
                if ( dblMaxPeak > dblMaxPeakSN )
                {
                    dblMaxPeakSN = dblMaxPeak;
                    dblCoarsePwrSN = 10*log10(dblMaxPeak);
                    intIatMaxPeak = i + 122;
                }
            } // Next i

            // Do the interpolation based on the two carriers at
            // nominal 1475 and 1525Hz
            if ( ((intIatMaxPeak - 123) >= intStartBin) &&
                 ((intIatMaxPeak - 118) <= intStopBin)
               )
            {
                // check to insure no index errors
                // Interpolate the adjacent bins using SpectralPeakLocator
                dblBinAdj1475 = SpectralPeakLocator(dblGoertzelReal(intIatMaxPeak - 123),
                                                    dblGoertzelImag(intIatMaxPeak - 123),
                                                    dblGoertzelReal(intIatMaxPeak - 122),
                                                    dblGoertzelImag(intIatMaxPeak - 122),
                                                    dblGoertzelReal(intIatMaxPeak - 121),
                                                    dblGoertzelImag(intIatMaxPeak - 121),
                                                    dblLeftMag
                                                   );

                if ( (dblBinAdj1475 < dblInterpretThreshold) &&
                     (dblBinAdj1475 > -dblInterpretThreshold)
                   )
                {
                    dblBinAdj = dblBinAdj1475;
                    ++intInterpCnt;
                }

                dblBinAdj1525 = SpectralPeakLocator(dblGoertzelReal(intIatMaxPeak - 118),
                                                    dblGoertzelImag(intIatMaxPeak - 118),
                                                    dblGoertzelReal(intIatMaxPeak - 117),
                                                    dblGoertzelImag(intIatMaxPeak - 117),
                                                    dblGoertzelReal(intIatMaxPeak - 116),
                                                    dblGoertzelImag(intIatMaxPeak - 116),
                                                    dblRightMag
                                                   );

                if ( (dblBinAdj1525 < dblInterpretThreshold) &&
                     (dblBinAdj1525 > -dblInterpretThreshold)
                   )
                {
                    dblBinAdj += dblBinAdj1525;
                    ++intInterpCnt;
                }
#ifdef DEBUG_DEMOD
                qDebug() << "Coarse: SPL 1475= "
                          << QString("#.00").arg(dblBinAdj1475)
                          << " SPL 1525= "
                          << QString("#.00").arg(dblBinAdj1525);
#endif
                if ( intInterpCnt == 0 )
                {
                    if ( MCB->SlowCPU )
                        intPtr += 480; // advance pointer 2 symbol (40 ms)
                    else
                        intPtr += 240; // advance pointer 1 symbol (20 ms)

                    blnLeaderDetected = false;
                    return false;
                }
                else
                {
                    dblBinAdj = dblBinAdj / intInterpCnt; // average the offsets that are within 1 bin
                    dblCoarseOffset = 10 * (intIatMaxPeak + dblBinAdj - 147); // compute the Coarse tuning offset in Hz
                }
            }
            else
            {
                if ( MCB->SlowCPU )
                    intPtr += 480; // advance pointer 2 symbol (40 ms)
                else
                    intPtr += 240; // advance pointer 1 symbol (20 ms)

                return false;
            }
        }


        // NarrowSearch: Used after a successful wide search or
        // if tuning = 0 or within 20 sec of a good fine tune
        if ( dblCoarseOffset < 999 )
            dblTrialOffset = dblCoarseOffset; // use the CoarseOffset calculation from above
        else
            dblTrialOffset = dblOffsetHz;     // use the prior computed offset value

        if ( (abs(dblTrialOffset) > MCB->TuningRange ) && (MCB->TuningRange > 0) )
        {
            if ( MCB->SlowCPU )
                intPtr += 480; // advance pointer 2 symbol (40 ms)
            else
                intPtr += 240; // advance pointer 1 symbol (20 ms)

            return false;
        }

        dblLeftCar = 147.5 + dblTrialOffset / 10; // the nominal positions of the two tone carriers based on the last computed dblOffsetHz
        dblRightCar = 152.5 + dblTrialOffset / 10;
        // Calculate 4 bins total for Noise values in
        // S/N computation (calculate average noise)
        // Simple average of noise bins
        GoertzelRealImagHamming(intNewSamples, intPtr, 1200, 142.5 + dblTrialOffset / 10, dblCtrR, dblCtrI); // nominal center -75 Hz
        dblAvgNoisePerBin = pow(dblCtrR,2) + pow(dblCtrI, 2);
        GoertzelRealImagHamming(intNewSamples, intPtr, 1200, 145.0 + dblTrialOffset / 10, dblCtrR, dblCtrI); // center - 50 Hz
        dblAvgNoisePerBin += pow(dblCtrR, 2) + pow(dblCtrI, 2);
        GoertzelRealImagHamming(intNewSamples, intPtr, 1200, 155.0 + dblTrialOffset / 10, dblCtrR, dblCtrI); // center + 50 Hz
        dblAvgNoisePerBin += pow(dblCtrR,2) + pow(dblCtrI, 2);
        GoertzelRealImagHamming(intNewSamples, intPtr, 1200, 157.5 + dblTrialOffset / 10, dblCtrR, dblCtrI); // center + 75
        dblAvgNoisePerBin += pow(dblCtrR,2) + pow(dblCtrI,2);
        dblAvgNoisePerBin = dblAvgNoisePerBin * 0.25; // simple average of 4 noise bins,  now units of power

        // Calculate the nominal carriers + one bin above and
        // below for Spectral Peak locator
        GoertzelRealImagHamming(intNewSamples, intPtr, 1200, dblLeftCar - 1, dblLeftR.at(0), dblLeftI.at(0));
        GoertzelRealImagHamming(intNewSamples, intPtr, 1200, dblLeftCar, dblLeftR.at(1), dblLeftI.at(1));
        dblLeftP = pow(dblLeftR.at(1),2) + pow(dblLeftI.at(1),2);
        GoertzelRealImagHamming(intNewSamples, intPtr, 1200, dblLeftCar + 1, dblLeftR.at(2), dblLeftI.at(2));
        GoertzelRealImagHamming(intNewSamples, intPtr, 1200, dblRightCar - 1, dblRightR.at(0), dblRightI.at(0));
        GoertzelRealImagHamming(intNewSamples, intPtr, 1200, dblRightCar, dblRightR.at(1), dblRightI.at(1));
        dblRightP = pow(dblRightR.at(1),2) + pow(dblRightI.at(1),2);
        GoertzelRealImagHamming(intNewSamples, intPtr, 1200, dblRightCar + 1, dblRightR.at(2), dblRightI.at(2));

        // Calculate the total power in the two tones
        // This mechanism designed to reject single carrier
        // but average both carriers if ratios is less than 4:1
        if ( dblLeftP > (4 * dblRightP) )
            dblPower = dblRightP;
        else if ( dblRightP > (4 * dblLeftP) )
            dblPower = dblLeftP;
        else
            // dblPower = (dblLeftP + dblRightP) * 0.5 ' simple average
            dblPower = sqrt(dblLeftP * dblRightP);

        dblSNdBPwr = 10 * log10(dblPower / dblAvgNoisePerBin);

        // Early leader detect code to calculate S:N on the first
        // 2 symbols). Concept is to allow more accurate framing
        // and synb detection and reduce false leader detects
        // (intNewSamples, intPtr, 480, 57 + dblTrialOffset / 25, dblCtrR, dblCtrI) // nominal center -75 Hz
        GoertzelRealImag(intNewSamples, intPtr, 480, 57 + dblTrialOffset / 25, dblCtrR, dblCtrI); // nominal center -75 Hz
        dblAvgNoisePerBin = pow(dblCtrR,2) + pow(dblCtrI,2);
        GoertzelRealImag(intNewSamples, intPtr, 480, 58 + dblTrialOffset / 25, dblCtrR, dblCtrI); // center - 50 Hz
        dblAvgNoisePerBin += pow(dblCtrR,2) + pow(dblCtrI,2);
        GoertzelRealImag(intNewSamples, intPtr, 480, 62 + dblTrialOffset / 25, dblCtrR, dblCtrI); // center + 50 Hz
        dblAvgNoisePerBin += pow(dblCtrR,2) + pow(dblCtrI,2);
        GoertzelRealImag(intNewSamples, intPtr, 480, 63 + dblTrialOffset / 25, dblCtrR, dblCtrI); // center + 75 Hz
        dblAvgNoisePerBin = 0.25 * (dblAvgNoisePerBin + pow(dblCtrR,2) + pow(dblCtrI,2)); // average of 4 noise bins
        dblLeftCar = 59 + dblTrialOffset / 25; // the nominal positions of the two tone carriers based on the last computerd dblOffsetHz
        dblRightCar = 61 + dblTrialOffset / 25;
        GoertzelRealImag(intNewSamples, intPtr, 480, dblLeftCar, dblCtrR, dblCtrI); // Left carrier
        dblLeftP = pow(dblCtrR,2) + pow(dblCtrI,2);
        GoertzelRealImag(intNewSamples, intPtr, 480, dblRightCar, dblCtrR, dblCtrI); // Right
        dblRightP = pow(dblCtrR,2) + pow(dblCtrI,2);
        // the following rejects a single tone carrier but
        // averages the two tones if ratio is < 4:1
        if ( dblLeftP > (4 * dblRightP) )
            dblPowerEarly = dblRightP;
        else if ( dblRightP > (4 * dblLeftP) )
            dblPowerEarly = dblLeftP;
        else
            //dblPowerEarly = (dblLeftP + dblRightP) * 0.5 // simple average if radio < 2:1
            dblPowerEarly = sqrt(dblLeftP * dblRightP);

        dblSNdBPwrEarly = 10 * log10(dblPowerEarly / dblAvgNoisePerBin);

        // End of Early leader detect test code //

        // The following thresholding requires the first 2 symbols
        // and the entire 10 symbol leader have a minium S:N for detection.
        // These calibration values yield a sensitivity of ~ - 10 dB S:N
        // (@ 3KHZ noise BW) with a Squelch=5, and 2 dB per squelch change
        // The threshold for early is 1 dB lower than the threshold for
        // the full 4 symbol leader. (compenseated for the 3 dB bandwidth
        // difference)
        if ( (dblSNdBPwr > (4 + MCB->RxSquelchLevel)) &&
             (dblSNdBPwrEarly > (MCB->RxSquelchLevel)) &&
             (dblAvgNoisePerBin > 100)
           )
        {
            // making early threshold = lower
            // (after 3 dB compensation for bandwidth)
#ifdef DEBUG_DEMOD
            qDebug() << "Fine Search S:N= " << QString("#.0").arg(dblSNdBPwr) << "dB     Early S:N= " << QString("#.0").arg(dblSNdBPwrEarly) << "dB";
#endif
            // Calculate the interpolation based on the left of the
            // two tones
            dblBinInterpLeft = SpectralPeakLocator(dblLeftR.at(0), dblLeftI.at(0), dblLeftR.at(1), dblLeftI.at(1), dblLeftR.at(2), dblLeftI.at(2), dblLeftMag);
            // And the right of the two tones
            dblBinInterpRight = SpectralPeakLocator(dblRightR.at(0), dblRightI.at(0), dblRightR.at(1), dblRightI(1), dblRightR.at(2), dblRightI.at(2), dblRightMag);
            // Weight the interpolated values in proportion to their magnitudes
            dblBinInterpLeft = dblBinInterpLeft * dblLeftMag / (dblLeftMag + dblRightMag);
            dblBinInterpRight = dblBinInterpRight * dblRightMag / (dblLeftMag + dblRightMag);
#ifdef DEBUG_DEMOD
            qDebug() << "Fine: SPL Left= " << QString("#.00").arg(dblBinInterpLeft) << " SPL Right= " << QString("#.00").arg(dblBinInterpRight);
#endif
            if ( abs(dblBinInterpLeft + dblBinInterpRight) < 1.0 )
            {
                // sanity check for the interpolators
                if ( (dblBinInterpLeft + dblBinInterpRight) > 0 )
                    dblOffsetHz = dblTrialOffset + qMin((dblBinInterpLeft + dblBinInterpRight) * 10, 3); // average left and right, adjustment bounded to +/- 3Hz max
                else
                    dblOffsetHz = dblTrialOffset + qMax((dblBinInterpLeft + dblBinInterpRight) * 10, -3);

                // Capture power for debugging ...
                // note: convert to 3 KHz noise bandwidth
                // from 25Hz or 10Hz for reporting consistancy.

                // Notice the addition of requiring a second detect
                //with small offset dramatically reduces false triggering even at Squelch values of 3
                // The following demonstrated good detection down to -10 dB S:N with squelch = 3 and minimal false triggering.
                if ( abs(dblPriorFineOffset - dblOffsetHz) < 2.9 )
                {
#ifdef DEBUG_DEMOD
                    qDebug() << "Prior-Offset= " << QString("#.0").arg(dblPriorFineOffset - dblOffsetHz);
#endif
                    strDecodeCapture = "Ldr; S:N(3KHz) Early=";
                    strDecodeCapture += QString("#.0").arg(dblSNdBPwrEarly - 20.8);
                    strDecodeCapture += "dB, Full=";
                    strDecodeCapture += QString("#.0").arg(dblSNdBPwr - 24.77);
                    strDecodeCapture += "dB, Offset=";
                    strDecodeCapture += QString("#0.0").arg(dblOffsetHz);
                    strDecodeCapture += "Hz ";
                    dttStartRmtLeaderMeasure = QDateTime::currentDateTime();
                    blnLeaderDetected = true;
                    if ( MCB->AccumulateStats )
                    {
                        stcTuningStats.dblLeaderSNAvg = ((stcTuningStats.dblLeaderSNAvg * stcTuningStats.intLeaderDetects) + dblSNdBPwr) / (1 + stcTuningStats.intLeaderDetects);
                        ++stcTuningStats.intLeaderDetects;
                    }
                    dblNCOFreq = 3000 + dblOffsetHz; // Set the NCO frequency and phase inc for mixing
                    dblNCOPhaseInc = dbl2Pi * dblNCOFreq / 12000;
                    // don't advance the pointer here
                    RxState = AcquireSymbolSync;
                    dttLastLeaderDetect = QDateTime::currentDateTime();
                    intSN = round(dblSNdBPwr - 24.77); // 24.77dB accomodates ratio of 3Kz BW:10 Hz BW (10Log 3000/10 = 24.77)
                    dttStartRmtLeaderMeasure = QDateTime::currentDateTime();
                    dblPriorFineOffset = 1000.0;
                    blnLeaderDetected = true;
                    emit signalDCD(true);
                    return true;
                }
                else
                {
                    dblPriorFineOffset = dblOffsetHz;
                    // always use 1 symbol inc when looking for
                    // next minimal offset
                    intPtr += 240; // advance pointer 1 symbol (20 ms)
                    blnLeaderDetected = false;
                    return false;
                }
            }
            else
            {
                if ( MCB->SlowCPU )
                    intPtr += 480; // advance pointer 2 symbol (40 ms)
                else
                    intPtr += 240; // advance pointer 1 symbol (20 ms)

                blnLeaderDetected  = false;
                dblPriorFineOffset = 1000.0;
                return false;
            }
        }
        else
        {
            if ( MCB->SlowCPU )
                intPtr += 480; // advance pointer 2 symbol (40 ms)
            else
                intPtr += 240; // advance pointer 1 symbol (20 ms)

            blnLeaderDetected = false;
            dblPriorFineOffset = 1000.0;
            return false;
        }
    }
    catch (exception Ex)
    {
        qCritical() << "Err:" << Ex.what();
        intPtr = intNewSamples->length(); // This will force a discard of all received samples
        return false;
    }
} // SearchFor2ToneLeader3()

double DemodulateDecode12K::QuinnSpectralPeakLocator(double XkM1Re, double XkM1Im, double XkRe, double XkIm, double XkP1Re, double XkP1Im)
{
    double dblDenom = pow(XkRe,2) + pow(XkIm,2);
    double dblAlpha1;
    double dblAlpha2;
    dblAlpha1 = ((XkM1Re * XkRe) + (XkM1Im * XkIm)) / dblDenom;
    dblAlpha2 = ((XkP1Re * XkRe) + (XkP1Im * XkIm)) / dblDenom;
    double dblDelta1 = dblAlpha1 / (1 - dblAlpha1);
    double dblDelta2 = dblAlpha2 / (1 - dblAlpha2);

    if ( (dblDelta1 > 0) && (dblDelta2 > 0) )
        return dblDelta2;
    else
        return dblDelta1;
}  // QuinnSpectralPeakLocator()

double DemodulateDecode12K::SpectralPeakLocator(double XkM1Re, double XkM1Im, double XkRe, double XkIm, double XkP1Re, double XkP1Im, double *dblCentMag)
{

    dblCentMag         = sqrt(pow(XkRe,2) + pow(XkIm,2));
    double dblLeftMag  = sqrt(pow(XkM1Re,2) + pow(XkM1Im,2));
    double dblRightMag = sqrt(pow(XkP1Re,2) + pow(XkP1Im,2));
    double ret = 1.22 * (dblRightMag - dblLeftMag) / (dblLeftMag + dblCentMag + dblRightMag); // Optimized for Hamming Window;
    return ret;
}  // SpectralPeakLocator()

bool DemodulateDecode12K::AcquireFrameSyncRSB()
{
    qint32 intLocalPtr = intMFSReadPtr;
    qint32 intAvailableSymbols = (intFilteredMixedSamples.length() - intMFSReadPtr) / 240;
    double dblPhaseSym1; // phase of the first symbol
    double dblPhaseSym2; // phase of the second symbol
    double dblPhaseSym3;  // phase of the third symbol
    double dblReal;
    double dblImag;
    double dblPhaseDiff12;
    double dblPhaseDiff23;

    try
    {
        if ( intAvailableSymbols < 3 ) return false; // must have at least 720 samples to search

        GoertzelRealImag(intFilteredMixedSamples, intLocalPtr, 240, 30, dblReal, dblImag); // Carrier at 1500 Hz nominal Positioning
        dblPhaseSym1 = atan2(dblImag, dblReal);
        intLocalPtr += 240; // advance one symbol
        GoertzelRealImag(intFilteredMixedSamples, intLocalPtr, 240, 30, dblReal, dblImag); // Carrier at 1500 Hz nominal Positioning
        dblPhaseSym2 = atan2(dblImag, dblReal);
        intLocalPtr += 240; // advance one symbol

        for ( i = 0; i < (intAvailableSymbols - 3); ++i )
        {
            // Compute the phase of the next symbol
            GoertzelRealImag(intFilteredMixedSamples, intLocalPtr, 240, 30, dblReal, dblImag); // Carrier at 1500 Hz nominal Positioning
            dblPhaseSym3 = atan2(dblImag, dblReal);

            // Compute the phase differences between sym1-sym2, sym2-sym3
            dblPhaseDiff12 = dblPhaseSym1 - dblPhaseSym2;
            if ( dblPhaseDiff12 > M_PI ) // bound phase diff to +/- Pi
                dblPhaseDiff12 -= dbl2Pi;
            else if ( dblPhaseDiff12 < -M_PI )
                dblPhaseDiff12 += dbl2Pi;

            dblPhaseDiff23 = dblPhaseSym2 - dblPhaseSym3;
            // bound phase diff to +/- Pi
            if ( dblPhaseDiff23 > M_PI )
                dblPhaseDiff23 -= dbl2Pi;
            else if ( dblPhaseDiff23 < -M_PI )
                dblPhaseDiff23 += dbl2Pi;

            if ( (abs(dblPhaseDiff12) > (0.6667 * M_PI)) &&
                 (abs(dblPhaseDiff23) < (0.3333 * M_PI))
               )
            {
                //Tighten the margin to 60 degrees
#ifdef DEBUG_DEMOD
                // *********************************
                // Debug code to look at filtered waveform
                //QVector<double> dblFilteredMixed(int75HzFilteredSamples.length() - 1);
                // dblAcquirePSKFrameSyncRSB.resize(intFilteredMixedSamples.Length - intMFSReadPtr - 1);
                //for ( k As Integer = 0; k < (dblFilteredMixed.Length - 1); ++k )
                //{
                //    dblFilteredMixed(k) = int75HzFilteredSamples(k)
                //} // Next k
                //objWT = New WaveTools
                //if ( IO.Directory.Exists(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav") = False )
                //{
                //    IO.Directory.CreateDirectory(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav")
                //}
                //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\AcquirePSKFrameSyncRSB.wav", 12000, 16, dblFilteredMixed)
                // End of debug code
                //************************************
#endif

                intLeaderRcvdMs = round((intLocalPtr - 30) / 12); // 30 is to accomodate offset of inital pointer for filter length.
                intMFSReadPtr = intLocalPtr + 240; // Position read pointer to start of the symbol following reference symbol
                if ( MCB->AccumulateStats ) ++stcTuningStats.intFrameSyncs; // accumulate tuning stats
                //strDecodeCapture &= "Sync; Ph1>2=" & Format(dblPhaseDiff12, "0.00") & " Ph2>3=" & Format(dblPhaseDiff23, "0.00") & ": "
#ifdef DEBUG_DEMOD
                qDebug() << "Phase 1 > 2 = " << QString("0.00").arg(dblPhaseDiff12) << " Phase2>3=" << QString("0.00").arg(dblPhaseDiff23);
#endif
                return true; // pointer is pointing to first 4FSK data symbol. (first symbol of frame type)
            }
            else
            {
                dblPhaseSym1 = dblPhaseSym2;
                dblPhaseSym2 = dblPhaseSym3;
                intLocalPtr += 240; // advance one symbol
            }
        } // Next i
        intMFSReadPtr = intLocalPtr - 480; // back up 2 symbols for next attempt (Current Sym2 will become new Sym1)
        return false;
    }
    catch (exception ex)
    {
        qCritical() << "Err: " << ex.what();
    }
    return false;
} // AcquireFrameSyncRSB()

bool DemodulateDecode12K::Acquire2ToneLeaderSymbolFraming()
{
    qint32 i = 0;
    qint32 intAbsPeak = 0;
    qint32 intJatPeak = 0;
    qint32 intIatMinErr = 0;
    qint32 intLocalPtr = intMFSReadPtr;  // try advancing one symbol to minimize initial startup errors
    double dblCarPh;
    double dblReal;
    double dblImag;
    double dblAbsPhErr;
    double dblMinAbsPhErr = 5000.0; // initialize to an excessive value
    double dblPhaseAtMinErr;

    // Use Phase of 1500 Hz leader  to establish symbol framing. Nominal phase is 0 or 180 degrees

    intLocalPtr = intMFSReadPtr + EnvelopeCorrelator(); // should position the pointer at the symbol boundary
    if ( intLocalPtr < intMFSReadPtr ) return false; // use negative value of EnvelopeCorrelator to indicate insufficient correlation.

    // Check 2 samples either side of the intLocalPtr for
    // minimum phase error.(closest to Pi or -Pi)
    // Could be as much as .4 Radians (~70 degrees)
    // depending on sampling positions.
    for (i = -2; i = 2; ++i )
    {
        // for 5 tries (normally is between -1 to +1)
        GoertzelRealImag(intFilteredMixedSamples, intLocalPtr + i, 240, 30, dblReal, dblImag); // Carrier at 1500 Hz nominal Positioning
        dblCarPh = atan2(dblImag, dblReal);
        dblAbsPhErr = abs(dblCarPh - (round(dblCarPh / M_PI) * M_PI));
        if ( dblAbsPhErr < dblMinAbsPhErr )
        {
            dblMinAbsPhErr = dblAbsPhErr;
            intIatMinErr = i;
            dblPhaseAtMinErr = dblCarPh;
        }
    } // Next i
    intMFSReadPtr = intLocalPtr + intIatMinErr;
#ifdef DEBUG_DEMOD
    qDebug() << "iatMinError="
             << QString("%1").arg(intIatMinErr)
             << "  Phase= "
             << QString("%0.00").arg(dblPhaseAtMinErr);
#endif
    RxState = AcquireFrameSync;
    if ( MCB->AccumulateStats )  ++stcTuningStats.intLeaderSyncs;
    //strDecodeCapture &= "Framing; i=" & intIatMinErr.ToString & ", Ptr=" & intMFSReadPtr.ToString & ", PhErr=" & Format(dblMinAbsPhErr, "#.00") & ": "

#ifdef DEBUG_DEMOD
    // *********************************
    // Debug code to look at filtered waveform
    //QVector<double> dblAcquire2ToneLeaderSymbolFraming(intFilteredMixedSamples.Length - intLocalPtr);
    //for ( k As Integer = 0; k < (dblAcquire2ToneLeaderSymbolFraming.length() - 1); ++k )
    //{
    //    dblAcquire2ToneLeaderSymbolFraming(k) = intFilteredMixedSamples(k)
    //} // Next k
    //objWT = new WaveTools;
    //if ( IO.Directory.Exists(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav") = False Then
    //    IO.Directory.CreateDirectory(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav")
    //}
    //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\Acquire2ToneLeaderSymbSync.wav", 12000, 16, dblFilteredMixed)
    // End of debug code
    //************************************
#endif
    return true;
}  // Acquire2ToneLeaderSymbolSync()

void DemodulateDecode12K::MixNCOFilter(QVector<qint32> *intNewSamples, qint32 *intReadPtr, double dblOffsetHz)
{
        if ( intNewSamples->length() == 0 ) return;

        qint32 i = 0;
        // Nominal NCO freq is 3000 Hz to downmix intNewSamples
        // (NCO - Fnew) to center of 1500 Hz (invertes the sideband too)
        dblNCOFreq = 3000.0 + dblOffsetHz;
        dblNCOPhaseInc = dblNCOFreq * dbl2Pi / 12000;
        intMixedSamples.resize(intNewSamples->length() - 1 - intReadPtr);
        for ( i = 0; i < (intNewSamples->length() - 1 - intReadPtr); ++ i )
        {
            intMixedSamples[i] = qint32(intNewSamples(intReadPtr + i) * cos(dblNCOPhase)); // later may want a lower "cost" implementation of "Cos"
            dblNCOPhase += dblNCOPhaseInc;
            if ( dblNCOPhase > dbl2Pi ) dblNCOPhase -= dbl2Pi;
        } // Next i
        intReadPtr = 0;
        intNewSamples->clear();
        // showed no significant difference if the 2000 Hz filer
        // used for all bandwidths.

        // filter through the FS filter
        // (required to reject image from Local oscillator)
        FSMixFilter2000Hz();
}  // MixNCOFilter()

void DemodulateDecode12K::FSMixFilter2000Hz()
{
    static qint32 intN  = 120;     // Length of filter 12000/100
    static qint32 intFilLen = qint32(intN / 2);
    static double dblR  = 0.9995;  // insures stability (must be < 1.0) (Value .9995 3/4/2014 gives good results)
    static double dblRn = pow(dblR,intN);
    static double dblR2 = pow(dblR,2);
    static double dblZin;
    static double dblZin_1;
    static double dblZin_2;
    static double dblZComb;  // Used in the comb generator
    static QVector<double> dblCoef(26); //the coefficients
    static QVector<double> dblZout_0(26);  // resonator outputs
    static QVector<double> dblZout_1(26);  // resonator outputs delayed one sample
    static QVector<double> dblZout_2(26);  // resonator outputs delayed two samples

    QVector<qint32> intFilteredSamples(intMixedSamples.length() - 1); //  Filtered samples

    // Initialize the coefficients
    if ( dblCoef.at(26) == 0 )
    {
        quint16 i = 0;
        for ( i = 4; i < 26; ++i )
        {
            // For Frequency = bin i
            dblCoef[i] = 2 * dblR * cos(2 * M_PI * i / intN);
        } // Next i
    }

    try
    {
        quint32 i = 0;
        quint32 j = 0;
        for (i = 0; i < (intMixedSamples.length() - 1); ++ i )
        {
            if ( i < intN )
                dblZin = intMixedSamples.at(i) - dblRn * intPriorMixedSamples.at(i);
            else
                dblZin = intMixedSamples.at(i) - dblRn * intMixedSamples.at(i - intN);

            // Compute the Comb
            dblZComb = dblZin - dblZin_2 * dblR2;
            dblZin_2 = dblZin_1;
            dblZin_1 = dblZin;

            // Now the resonators
            for ( j = 4; j < 26; ++k )
            {   // calculate output for 23 resonators
                dblZout_0[j] = dblZComb + dblCoef.at(j) * dblZout_1.at(j) - dblR2 * dblZout_2.at(j);
                    dblZout_2[j] = dblZout_1.at(j);
                    dblZout_1[j] = dblZout_0.at(j);
                    // scale each by transition coeff and +
                    //(Even) or - (Odd)
                    // Resonators 2 and 13 scaled by .389 get
                    // best shape and side lobe supression
                    // Scaling also accomodates for the filter
                    // "gain" of approx 60.
                    if ( (j == 4) || (j == 26) )
                        intFilteredSamples[i] += 0.389 * dblZout_0.at(j);
                    else if ( (j % 2) = 0 )
                        intFilteredSamples[i] += dblZout_0.at(j);
                    else
                        intFilteredSamples[i] -= dblZout_0.at(j);

                } // Next j
                intFilteredSamples[i] = intFilteredSamples.at(i) * 0.00833333333; // rescales for gain of filter
            } // Next i

            // update the prior intPriorMixedSamples array for the
            // next filter call
            ArrayCopy(intMixedSamples, intMixedSamples.Length - intN, intPriorMixedSamples, 0, intPriorMixedSamples.length());
            qint32 intCopyPtr = intFilteredMixedSamples.length();
            intFilteredMixedSamples.reserve(intFilteredMixedSamples.length() + intFilteredSamples.length() - 1);
            ArrayCopy(intFilteredSamples, 0, intFilteredMixedSamples, intCopyPtr, intFilteredSamples.length());
            intMixedSamples.clear();
            //********************************
            // Debug code to look at filter output
            //Dim dblFilteredMixed(intFilteredMixedSamples.Length - 1) As Double
            //For k As Integer = 0 To dblFilteredMixed.Length - 1
            //    dblFilteredMixed(k) = intFilteredMixedSamples(k)
            //Next
            //Dim objWT As New WaveTools
            //If IO.Directory.Exists(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav") = False Then
            //    IO.Directory.CreateDirectory(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav")
            //End If
            //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\FSMixFilter2000Hz.wav", 12000, 16, dblFilteredMixed)
            // End of debug code
            //************************************
    }
    catch (exception ex)
    {
        qCritical() << "Exception: " << ex.what();
    }
} // FSMixFilter2000Hz()


void DemodulateDecode12K::ClearAllMixedSamples()
{
    intFilteredMixedSamples.clear();
    intMixedSamples.clear();
    intPriorMixedSamples.resize(intPriorMixedSamples.length() - 1);
    intMFSReadPtr = 0;
} // ClearAllMixedSamples
