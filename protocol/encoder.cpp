#include "ardop_global.hpp"
#include "utils.hpp"

#include "modulator.hpp"

// -------------------------------------------------
//           PRIVATE METHODS AND FUNCTIONS
// -------------------------------------------------

QByteArray EncodeModulate::EncodeFSKData(qint32 bytFrameType, QByteArray *bytDataToSend, QString *strFrameName)
{
    Q_UNUSED(strFrameName)  // to suppress warnings in release build

    // First determine if bytDataToSend is
    // compatible with the requested modulation mode.
    bool    blnOdd         = false;
    bool    blnFrameTypeOK = false;
    quint16 intNumCar      = 0;
    quint32 intBaud        = 0;
    quint32 intDataLen     = 0;
    quint32 intRSLen       = 0;
    qint32  intDataToSendPtr  = 0;
    qint32  intEncodedDataPtr = 0;
    qint32  intCarDataCnt  = 0;
    qint32  intStartIndex  = 0;
    QString strType       = "";
    QString strMod        = "";
    QByteArray bytToRS;
    QByteArray bytEncodedData;

    Q_UNUSED(intStartIndex)   // to suppress warnings in release build

    blnFrameTypeOK = objFrameInfo->frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);

    if ( (intDataLen == 0) ||
         (bytDataToSend->length() == 0) ||
         !blnFrameTypeOK )
    {
        qCritical() << "Failure to update parameters for frame type H"
                    << bytFrameType
                    << "  DataToSend Len="
                    << QString("%1").arg(bytDataToSend->length());
        return NULL;
    }
    strFrameName = &strType;
    //  Need: (2 bytes for Frame Type) +
    // ( Data + RS + 1 byte byteCount +
    // 2 Byte CRC per carrier)
    bytEncodedData.resize(2 + intNumCar * (intDataLen + intRSLen + 1 + 2) - 1);
    bytToRS.resize(intDataLen + 3 - 1); // Data + Count + 2 byte CRC
    // Generate the 2 bytes for the frame type data:
    bytEncodedData[0] = bytFrameType;
    bytEncodedData[1] = bytFrameType ^ objProto->stcConnection.bytSessionID;
    intDataToSendPtr = 0;
    intEncodedDataPtr = 2;
    objRS8->setMaxCorrections(qint32(intRSLen / 2));  // RS length must be even

    if ( (intBaud < 600) || (intDataLen < 600) )
    {
        // Now compute the RS frame for each carrier
        // in sequence and move it to bytEncodedData
        for ( quint16 i = 0;  i < intNumCar; ++i )
        {
            // across all carriers
            intCarDataCnt = bytDataToSend->length() - intDataToSendPtr;
            if ( (quint32)intCarDataCnt >= intDataLen )
            {
                bytToRS.resize(intDataLen + 3 - 1);
                bytToRS[0] = intDataLen;
                intStartIndex = intEncodedDataPtr;
                ByteArrayCopy2(bytDataToSend, intDataToSendPtr, &bytToRS, 1, intDataLen);
                intDataToSendPtr += intDataLen;
            }
            else
            {
                bytToRS.resize(intDataLen + 3 - 1); // zero out bytToRS
                bytToRS[0] = intCarDataCnt;         // Could be 0 if insuffient data for # of carriers
                if ( intCarDataCnt > 0 ) ByteArrayCopy2(bytDataToSend, intDataToSendPtr, &bytToRS, 1, intCarDataCnt);
                intDataToSendPtr += intCarDataCnt;
            }
            GenCRC16FrameType(&bytToRS, 0, intDataLen, quint8(bytFrameType)); //calculate the CRC on the byte count  data bytes
            bytToRS = objRS8->RSEncode(bytToRS);
            ByteArrayCopy2(&bytToRS, 0, &bytEncodedData, intEncodedDataPtr, bytToRS.length());
            intEncodedDataPtr += bytToRS.length();
        } // Next i
        return bytEncodedData;
    }
    else
    {
        // special case for 600 baud 4FSK which
        // has 600 byte data field sent as three
        // sequencial (200 byte + 50 byte RS) groups
        // NOTE: Updated Oct 4, 2016 Rev 0.7.2.1 to
        //       fix problem if data length was less
        //       than 401 bytes  RM
        bytEncodedData.resize((2 + intDataLen + intRSLen + 9) - 1); // handles 3 groups of data with independed count and CRC
        bytEncodedData[0] = bytFrameType;
        bytEncodedData[1] = bytFrameType ^ objProto->stcConnection.bytSessionID;
        objRS8->setMaxCorrections(qint32(intRSLen / 6));

        for ( quint8 i = 0; i < 2; ++i )
        {
            // for three blocks of RS data
            bytToRS.resize(int(intDataLen / 3 + 3 - 1));
            intCarDataCnt = bytDataToSend->length() - intDataToSendPtr;
            if ( intCarDataCnt >= qint32(intDataLen / 3) )
            {
                bytToRS[0] = quint8(intDataLen / 3);
                ByteArrayCopy2(bytDataToSend, intDataToSendPtr, &bytToRS, 1, intDataLen / 3);
                intDataToSendPtr += qint32(intDataLen / 3);
            }
            else if (intCarDataCnt > 0 )
            {
                bytToRS[0] = bytDataToSend->length() - intDataToSendPtr;
                ByteArrayCopy2(bytDataToSend, intDataToSendPtr, &bytToRS, 1, bytDataToSend->length() - intDataToSendPtr);
                intDataToSendPtr = bytDataToSend->length();
            }
            else
            {
                bytToRS[0] = 0;
                intDataToSendPtr = bytDataToSend->length();
            }

            // If bytToRS(0) > 0 Then
            GenCRC16FrameType(&bytToRS, 0, intDataLen / 3, quint8(bytFrameType)); //calculate the CRC on the byte count  data bytes
            bytToRS = objRS8->RSEncode(bytToRS);
            ByteArrayCopy2(&bytToRS, 0, &bytEncodedData, intEncodedDataPtr, bytToRS.length());
            intEncodedDataPtr += bytToRS.length();
        } // Next i
        return bytEncodedData;
    }
} // Encode4FSKD()

QByteArray EncodeModulate::EncodePSK_QAM(quint8 bytFrameType, QByteArray *bytDataToSend, QString *strFrameName)
{
    Q_UNUSED(strFrameName)  // to suppress warnings in release build

    // First determine if bytDataToSend is compatible
    // with the requested modulation mode.
    bool    blnOdd            = false;
    bool    blnFrameTypeOK    = false;
    quint16 intNumCar         = 0;
    quint32 intRSLen          = 0;
    quint32 intBaud           = 0;
    quint32 intDataLen        = 0;
    qint32  intDataToSendPtr  = 0;
    qint32  intEncodedDataPtr = 0;
    quint32 intCarDataCnt     = 0;
    qint32  intStartIndex     = 0;
    QString strType           = "";
    QString strMod            = "";

    Q_UNUSED(intStartIndex)  // to suppress warnings in release build

    blnFrameTypeOK = objFrameInfo->frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);
    if ( (intDataLen == 0) ||
         (bytDataToSend->length() == 0) ||
         !blnFrameTypeOK
       )
    {
        qCritical() << "Failure to update parameters for frame type H"
                    << bytFrameType
                    << "  DataToSend Len="
                    << QString("%1").arg(bytDataToSend->length());
        return NULL;
    }

    if ( bytDataToSend->length() > int(intNumCar * intDataLen) )
    {
        qCritical() << "Data length exceeds frame capacity. Frame type=H"
                    << bytFrameType
                    << "  Data Length="
                    << QString("%1").arg(bytDataToSend->length());
        return NULL;
    }

    strFrameName = &strType;
    //  Need: (2 bytes for Frame Type) +(1 byte byteCount,  Data + 2 byteCRC + RS) per carrier
    QByteArray bytEncodedData;
    QByteArray bytToRS;
    bytEncodedData.reserve(2 + intNumCar * (intDataLen + intRSLen + 3) - 1);
    bytToRS.reserve(intDataLen + 3 - 1); // byte Count + Data  + 2 byte CRC

    // Generate the 2 bytes for the frame type data:
    bytEncodedData[0] = bytFrameType;
    bytEncodedData[1] = quint8(bytFrameType ^ objProto->stcConnection.bytSessionID);

    intDataToSendPtr = 0;
    intEncodedDataPtr = 2;
    objRS8->setMaxCorrections(qint32(intRSLen / 2));  // RS length must be even

    // Now compute the RS frame for each carrier
    // in sequence and move it to bytEncodedData
    for ( quint16 i = 0; i < (intNumCar - 1); ++i )
    {
        // across all carriers
        intCarDataCnt = bytDataToSend->length() - intDataToSendPtr;
        if ( intCarDataCnt >= intDataLen )
        {
            bytToRS.resize(intDataLen + 3 - 1);
            bytToRS[0] = intDataLen;
            intStartIndex = intEncodedDataPtr;
            ByteArrayCopy2(bytDataToSend, intDataToSendPtr, &bytToRS, 1, intDataLen);
            intDataToSendPtr += intDataLen;
        }
        else
        {
            bytToRS.resize(intDataLen + 3 - 1); // zero out bytToRS
            bytToRS[0] = intCarDataCnt; // Could be 0 if insuffient data for # of carriers
            if ( intCarDataCnt > 0 ) ByteArrayCopy2(bytDataToSend, intDataToSendPtr, &bytToRS, 1, intCarDataCnt);
            intDataToSendPtr += intCarDataCnt;
        }
        GenCRC16FrameType(&bytToRS, 0, intDataLen, bytFrameType);
        bytToRS = objRS8->RSEncode(bytToRS);
        ByteArrayCopy2(&bytToRS, 0, &bytEncodedData, intEncodedDataPtr, bytToRS.length());
        intEncodedDataPtr += bytToRS.length();
    } // Next i
    return bytEncodedData;
} // EncodePSK_QAM()

QVector<qint32> EncodeModulate::GenTwoToneLeaderWithSync(qint32 intSymLen)
{
    QVector<qint32> intLeader((240 * intSymLen) - 1);
    qint32 intPtr  = 0;
    qint32 intSign = 1;

    if ( (intSymLen % 2) == 1 ) intSign = -1;

    for ( qint32 i = 0; i < (intSymLen - 1); ++i )
    {
        // for the number of symbols needed
        // (two symbols less than total leader length)
        for (  qint32 j = 0; j < 239; ++j )
        {
            // for 240 samples per symbol (50 baud)
            if ( i != (intSymLen - 1) )
                intLeader[intPtr] = intSign * int50BaudTwoToneLeaderTemplate.at(j);
            else
                intLeader[intPtr] = -intSign * int50BaudTwoToneLeaderTemplate.at(j);

            ++intPtr;
        } // Next j
        intSign = -intSign;
    } // Next i
#ifdef DEBUG_MOD
    // *********************************
    // Debug code to look at Leader
    //Dim dblLeader(intLeader.Length - 1) As Double
    //For k As Integer = 0 To intLeader.Length - 1
    //    dblLeader(k) = intLeader(k)
    //Next k
    //Dim objWT As New WaveTools
    //If IO.Directory.Exists(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav") = False Then
    //    IO.Directory.CreateDirectory(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav")
    //End If
    //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\TwoToneLdr.wav", 12000, 16, dblLeader)
    // End of debug code
    //************************************
#endif

    return (intLeader);
}  // GenerateTwoToneLeaderWithSync()

QVector<qint32> EncodeModulate::Gen50BaudTwoToneLeaderWithSync(qint32 intSymLen)
{
    QVector<qint32> intLeader((240 * intSymLen) - 1);
    quint32 i       = 0;
    qint32  intPtr  = 0;
    qint32  intSign = 1;

    if ( (intSymLen % 2) == 1 ) intSign = -1;

    for ( i = 0; i < (quint32)(intSymLen - 1); ++i )
    {
        // for the number of symbols needed
        // (two symbols less than total leader length)
        for ( quint32 j = 0; j < 239; ++j )
        {
            // for 240 samples per symbol (50 baud)
            if ( i != (quint32)(intSymLen - 1) )
                intLeader[intPtr] = intSign * int50BaudTwoToneLeaderTemplate.at(j);
            else
                intLeader[intPtr] = -intSign * int50BaudTwoToneLeaderTemplate.at(j);

            ++intPtr;
        }  //  Next j
        intSign = -intSign;
    }  // Next i
#ifdef DEBUG_MOD
        // *********************************
        // Debug code to look at Leader
        //Dim dblLeader(intLeader.Length - 1) As Double
        //For k As Integer = 0 To intLeader.Length - 1
        //    dblLeader(k) = intLeader(k)
        //Next k
        //Dim objWT As New WaveTools
        //If IO.Directory.Exists(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav") = False Then
        //    IO.Directory.CreateDirectory(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav")
        //End If
        //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\50BaudTwoToneLdr.wav", 12000, 16, dblLeader)
        // End of debug code
        //************************************
#endif
        return (intLeader);
}  // Get50BaudTwoToneLeaderWithSync()

void EncodeModulate::Generate50BaudTwoToneLeaderTemplate()
{
    for (quint32 i = 0; i < 239; ++i )
    {
        int50BaudTwoToneLeaderTemplate[i] = intAmp * 0.55 * (sin(((1500 - 25) / 1500) * (i / 8 * 2 * M_PI)) - sin((1500 + 25) / 1500 * (i / 8 * 2 * M_PI)));
    } // Next i
} // Generate50BaudTwoToneLeaderTemplate()

QVector<qint32> EncodeModulate::ModTwoToneTest()
{
    return Gen50BaudTwoToneLeaderWithSync(250);
} // ModTwoToneTest()


void EncodeModulate::GeneratePSKTemplates()
{
    //Tone values
    //   the carrier frequencies in Hz
    QVector<double> dblCarFreq = {800.0, 1000.0, 1200.0, 1400.0, 1500.0, 1600.0, 1800.0, 2000.0, 2200.0};
    // for 1 carrier modes use index 4 (1500)
    // for 2 carrier modes use indexes 3, 5 (1400 and 1600 Hz)
    // for 4 carrier modes use indexes 2, 3, 5, 6 (1200, 1400, 1600, 1800Hz)
    // for 8 carrier modes use indexes 0,1,2,3,5,6,7,8 (800, 1000, 1200, 1400, 1600, 1800, 2000, 2200 Hz)
    QVector<double> dblCarPhaseInc(8); // the phase inc per sample

    double dblAngle;  // Angle in radians
    //Dim dblPeakAmp As Double = intAmp * 0.5 ' may need to adjust
    //Compute the phase inc per sample
    quint8 i = 0;
    for ( i = 0; i < 8; ++i )
    {
        dblCarPhaseInc[i] = 2 * M_PI * dblCarFreq.at(i) / 12000;
    } // Next i
    // Now compute the templates: (4320 32 bit values total)
    for ( i = 0; i < 8; ++i )
    {
        // across 9 tones
        for ( quint8 j = 0; i < 3; ++j )
        {
            // ( using only half the values and
            // sign compliment for the opposit phases)
            dblAngle = 2 * M_PI * j / 8;
            // 100 baud template
            for ( quint16 k = 0; k < 119; ++k )
            {
                // for 120 samples
                // (one 100 baud symbol,
                // 200 baud modes will just use half of the data)
                intPSK100bdCarTemplate[i][j][k] = intAmp * sin(dblAngle); // with no envelope control
                dblAngle += dblCarPhaseInc.at(i);
                if ( dblAngle >= (2 * M_PI) ) dblAngle -= (2 * M_PI);
            } // Next k
        } // Next j
    } // Next i
}  // GeneratePSKTemplates()

void EncodeModulate::GenerateFSKTemplates()
{
    // 50 baud Tone values
    // The possible carrier frequencies in Hz
    // NOTE: gaps for groups of 4 at 900, 1400, and 1900 Hz
    // improved isolation between simultaneous carriers
    QVector<double> dblCarFreq = {1425.0, 1475.0, 1525.0, 1575.0, 600.0, 700.0, 800.0, 900.0, 1100.0, 1200.0, 1300.0, 1400.0, 1600.0, 1700.0, 1800.0, 1900.0, 2100.0, 2200.0, 2300.0, 2400.0};
    QVector<double> dblCarPhaseInc(19);
    double dblAngle;  // Angle in radians

    // Compute the phase inc per sample
    quint8 i = 0;
    quint32 j = 0;
    for ( i = 0; i < 3; ++i )
    {
        dblCarPhaseInc[i] = double(2 * M_PI * dblCarFreq.at(i) / 12000);
    } // Next i

    // Now compute the templates: (960 32 bit values total)
    for ( i = 0; i < 3; ++i )
    {
        // across the 4 tones for 50 baud frequencies
        dblAngle = 0;
        //50 baud template
        for ( j = 0; j < 239; ++j )
        {
            // for 240 samples (one 50 baud symbol)
            // with no envelope control
            // (factor 1.1 chosen emperically to keep
            // FSK peak amplitude slightly below 2 tone peak)
            intFSK50bdCarTemplate[i][j] = qint32(intAmp * 1.1 * sin(dblAngle));
            dblAngle += dblCarPhaseInc.at(i);
            if ( dblAngle >= (2 * M_PI) ) dblAngle -= double(2 * M_PI);
        } // Next j
    } // Next i

    // 16 FSK templates (500 Hz BW, 25 baud)
    for ( i = 0; i < 15; ++i )
    {
        // across the 16 tones for 25 baud frequencies
        dblAngle = 0.0;
        // 25 baud template
        for ( j = 0; j < 480; ++j )
        {
            // for 480 samples (one 25 baud symbol)
            // with no envelope control (factor 1.1
            // chosen emperically to keep FSK peak
            // amplitude slightly below 2 tone peak)
            int25BaudCarTemplate[i][j] = qint32(intAmp * 1.1 * sin(dblAngle));
            dblAngle += double((2 * M_PI / 12000) * (1312.5 + i * 25));
            if ( dblAngle >= (2 * M_PI) ) dblAngle -= (2.0 * M_PI);
        } // Next k
    } // Next i

    // 4FSK templates for 600 baud (2 Khz bandwidth)
    for ( i = 0; i < 3; ++i )
    {
        // across the 4 tones for 600 baud frequencies
        dblAngle = 0.0;
        // 600 baud template
        for ( j = 0; j < 19; ++j )
        {
            // for 20 samples (one 600 baud symbol)
            // with no envelope control (factor 1.1
            // chosen emperically to keep FSK peak
            // amplitude slightly below 2 tone peak)
            intFSK600bdCarTemplate[i][j] = qint32(intAmp * 1.1 * sin(dblAngle));
            dblAngle += double((2 * M_PI / 12000) * (600 + i * 600));
            if ( dblAngle >= (2 * M_PI) ) dblAngle -= double(2.0 * M_PI);
        } // Next j
    } // Next i

    // 100 baud Tone values for a single carrier case
    // the 100 baud carrier frequencies in Hz
    dblCarFreq[0] = 1350;
    dblCarFreq[1] = 1450;
    dblCarFreq[2] = 1550;
    dblCarFreq[3] = 1650;
    // Values of dblCarFreq for index 4-19 as in Dim above
    // Compute the phase inc per sample
    for ( i = 0; i < 19; ++i )
    {
        dblCarPhaseInc[i] = double(2.0 * M_PI * dblCarFreq.at(i) / 12000.0);
    } // Next i

    // Now compute the templates: (2400 32 bit values total)
    for ( i = 0; i < 19; ++i )
    {
        //across 20 tones
        dblAngle = 0.0;
        // 100 baud template
        for ( j = 0; j < 119; ++j )
        {
            // for 120 samples (one 100 baud symbol)
            // with no envelope control (factor 1.1
            // chosen emperically to keep FSK peak
            // amplitude slightly below 2 tone peak)
            intFSK100bdCarTemplate[i][j] = qint32(intAmp * 1.1 * sin(dblAngle));
            dblAngle += dblCarPhaseInc.at(i);
            if ( dblAngle >= (2 * M_PI) ) dblAngle -= double(2.0 * M_PI);
        } // Next j
    } // Next i
}  // GenerateFSKTemplates()

void EncodeModulate::AddTrailer(QVector<qint32> *intSamples)
{
    quint32 intPtr          = (quint32)intSamples->length();
    qint32  intAddedSymbols = quint32(1 + MCB->TrailerLength / 10); // add 1 symbol + 1 per each 10 ms of MCB.Trailer

    intSamples->reserve(intSamples->length() + intAddedSymbols * 120 - 1);
    quint32 i = 0;
    quint32 j = 0;

    for ( i = 1; i < (quint32)intAddedSymbols; ++i )
    {
        for ( j = 0; j < 119; ++j )
        {
            intSamples->replace(intPtr,intPSK100bdCarTemplate[int(4)][int(0)].at(j));
            ++intPtr;
        } // Next j
    } // Next i
} // AddTrailer()

<<<<<<< HEAD
=======

>>>>>>> remotes/origin/ken
