#include "ardop_global.hpp"
#include "utils.hpp"
#include "frameinfo.hpp"
#include "reedsolomon.hpp"

#include "demodulator.hpp"

bool DemodulateDecode12K::SearchFor2ToneLeader3(QVector<qint32> *intNewSamples, qint32 *intPtr, double *dblOffsetHz, qint32 *intSN)
{
    static double dblPriorFineOffset = 1000.0;

    qint32 i = 0;
    qint32 intStartBin;
    qint32 intStopBin;
    qint32 intInterpCnt = 0; // the count 0 to 2 of the interpolations that were < +/- .5 bin
    qint32 intIatMaxPeak = 0;
    double dblPower;
    double dblCoarsePwrSN;
    double dblMaxPeak;
    double dblMaxPeakSN;
    double dblBinAdj;
    double dblBinAdj1475;
    double dblBinAdj1525;
    double dblAvgNoisePerBin;
    double dblPowerEarly;
    double dblLeftCar;
    double dblRightCar;
    double dblBinInterpLeft;
    double dblBinInterpRight;
    double dblCtrR;
    double dblCtrI;
    double dblLeftP;
    double dblRightP;
    double dblLeftMag;
    double dblRightMag;
    double dblAlpha = 0.3;              // Works well possibly some room for optimization Changed from .5 to .3 on Rev 0.1.5.3
    double dblInterpretThreshold = 1.0; //Works well possibly some room for optimization
    double dblFilteredMaxPeak = 0.0;
    double dblCoarseOffset = 1000.0;    // initialize to impossible value
    double dblTrialOffset;

    QVector<double> dblLeftR(2);
    QVector<double> dblLeftI(2);
    QVector<double> dblRightR(2);
    QVector<double> dblRightI(2);
    QVector<double> dblGoertzelReal(55);
    QVector<double> dblGoertzelImag(55);
    QVector<double> dblMag(55);          // This should allow tunning from nominal bins at 1425Hz to 1575Hz +/- 200 Hz tuning range
    // to eliminate the warning messages in release output
    Q_UNUSED(dblAlpha)
    Q_UNUSED(dblFilteredMaxPeak)
    Q_UNUSED(dblCoarsePwrSN)
    Q_UNUSED(intSN)

    try
    {
        // insure there are at least 1200 samples (5 symbols of 240 samples)
        if ( (intNewSamples->length() - int(intPtr)) < 1200 ) return false;

        if ( (QDateTime::currentDateTime().msecsTo(dttLastGoodFrameTypeDecode) > 20) &&
             (MCB->TuningRange > 0)
           )
        {
            qint32 param1 = 0;
            qint32 param2 = 0;
            double param3 = 0.0;
            double param4 = 0.0;
            double param5 = 0.0;
            // this is the full (coarse) search over the full
            // tuning range selected. Uses more CPU time and
            // with possibly larger deviation once connected.
            // Compute the start and stop bins based on the
            // tuning range Each bin is 12000/1200 or
            // 10.0 Hz/bin
            // limit bin calculations based on tuning range
            intStartBin = int(round((200 - MCB->TuningRange) / 10));
            intStopBin = 55 - intStartBin;
            dblMaxPeak = 0;
            // Generate the Power magnitudes for up to 56-10Hz
            // bins (a function of MCB.TuningRange)
            param1 = *intPtr;
            param2 = 1200;
            for ( i = intStartBin; i <= intStopBin; ++i )
            {
                // note hamming window reduces end effect
                // caused by 1200 samples (not an even
                // multiple of 240) but spreads response peaks
                param3 = i + 122.5;
                param4 = dblGoertzelReal.at(i);
                param5 = dblGoertzelImag.at(i);
                GoertzelRealImagHamming(intNewSamples, param1, param2, param3, &param4, &param5);
                dblMag[i] = pow(dblGoertzelReal.at(i),2.0) + pow(dblGoertzelImag.at(i),2.0); // dblMag(i) in units of power (V^2)
            } // Next i
            // Search the bins to locate the max S:N
            // in the two tone signal/avg noise.
            for ( i = (intStartBin + 5); i < (intStopBin - 10); ++i )
            {
                // +/- MCB.TuningRange from nominal for bins
                // spaced 50 Hz apart
                // sqrt converts back to units of power from
                // Power ^2
                // don't use center noise bin as too easily
                // corrupted by adjacent carriers
                dblPower = sqrt(dblMag.at(i) * dblMag.at(i + 5)); // using the product (geometric mean) to minimize sensitivity to one strong carrier vs the two tone

                // Simple average of 4 noise bins
                dblAvgNoisePerBin = (dblMag.at(i - 5) + dblMag.at(i - 3) + dblMag.at(i + 8) + dblMag.at(i + 10)) * 0.25;
                dblMaxPeak = dblPower / dblAvgNoisePerBin;
                if ( dblMaxPeak > dblMaxPeakSN )
                {
                    dblMaxPeakSN = dblMaxPeak;
                    dblCoarsePwrSN = 10 * log10(dblMaxPeak);
                    intIatMaxPeak = i + 122;
                }
            } // Next i

            // Do the interpolation based on the two
            // carriers at nominal 1475 and 1525Hz
            if ( ((intIatMaxPeak - 123) >= intStartBin) &&
                 ((intIatMaxPeak - 118) <= intStopBin)
               )
            {
                // check to insure no index errors
                // Interpolate the adjacent bins using
                // SpectralPeakLocator
                dblBinAdj1475 = SpectralPeakLocator(dblGoertzelReal.at(intIatMaxPeak - 123), dblGoertzelImag.at(intIatMaxPeak - 123),
                                                    dblGoertzelReal.at(intIatMaxPeak - 122), dblGoertzelImag.at(intIatMaxPeak - 122),
                                                    dblGoertzelReal.at(intIatMaxPeak - 121), dblGoertzelImag.at(intIatMaxPeak - 121),
                                                    &dblLeftMag);

                if ( (dblBinAdj1475 < dblInterpretThreshold) &&
                     (dblBinAdj1475 > -dblInterpretThreshold)
                   )
                {
                    dblBinAdj = dblBinAdj1475;
                    ++intInterpCnt;
                }

                dblBinAdj1525 = SpectralPeakLocator(dblGoertzelReal.at(intIatMaxPeak - 118), dblGoertzelImag.at(intIatMaxPeak - 118),
                                                    dblGoertzelReal.at(intIatMaxPeak - 117), dblGoertzelImag.at(intIatMaxPeak - 117),
                                                    dblGoertzelReal.at(intIatMaxPeak - 116), dblGoertzelImag.at(intIatMaxPeak - 116),
                                                    &dblRightMag);

                if ( (dblBinAdj1525 < dblInterpretThreshold) &&
                     (dblBinAdj1525 > -dblInterpretThreshold)
                   )
                {
                    dblBinAdj += dblBinAdj1525;
                    ++intInterpCnt;
                }

                // qDebug() << "Coarse: SPL 1475= " << QString("#.00").arg(dblBinAdj1475) << " SPL 1525= " << QString("#.00").arg(dblBinAdj1525);

                if ( intInterpCnt == 0 )
                {
                    if ( MCB->SlowCPU )
                        intPtr += 480; // advance pointer 2 symbol (40 ms)
                    else
                        intPtr += 240; // advance pointer 1 symbol (20 ms)

                    blnLeaderDetected = false;
                    dblPriorFineOffset = 1000; // Added 0.8.3.1
                    return false;
                }
                else
                {
                    dblBinAdj = double(dblBinAdj / intInterpCnt);                     // average the offsets that are within 1 bin
                    dblCoarseOffset = double(10 * (intIatMaxPeak + dblBinAdj - 147)); // compute the Coarse tuning offset in Hz
                }
            }
            else
            {
                if ( MCB->SlowCPU )
                    intPtr += 480; // advance pointer 2 symbol (40 ms)
                else
                    intPtr += 240; // advance pointer 1 symbol (20 ms)

                dblPriorFineOffset = 1000.0; // Added 0.8.3.1
                return false;
            }
        }


        //NarrowSearch:
        //   Used after a successful wide search or
        //   if tuning = 0 or within 20 sec of a good fine tune
        if ( dblCoarseOffset < 999 )
            dblTrialOffset = dblCoarseOffset; // use the CoarseOffset calculation from above
        else
            dblTrialOffset = *dblOffsetHz;     // use the prior computed offset value

        if ( (abs(dblTrialOffset) > MCB->TuningRange) &&
             (MCB->TuningRange > 0)
           )
        {
            dttLastGoodFrameTypeDecode = QDateTime::currentDateTime().addSecs(-30); // precondition to make next call of SearchFor2ToneLeader3 do full search
            if ( MCB->SlowCPU )
                intPtr += 480; // advance pointer 2 symbol (40 ms)
            else
                intPtr += 240; // advance pointer 1 symbol (20 ms)

            dblPriorFineOffset = 1000.0;
            return false;
        }

        dblLeftCar = double(147.5 + (dblTrialOffset / 10)); // the nominal positions of the two tone carriers based on the last computed dblOffsetHz
        dblRightCar = double(152.5 + (dblTrialOffset / 10));
        // Calculate 4 bins total for Noise values
        // in S/N computation (calculate average noise)
        // Simple average of noise bins
        qint32 p1 = 0;
        qint32 p2 = 0;
        double p3 = 142.5 + (dblTrialOffset / 10);
        double p4 = 0.0;
        double p5 = 0.0;
        p1 = *intPtr;
        p2 = 1200;

        GoertzelRealImagHamming(intNewSamples, p1, p2, p3, &dblCtrR, &dblCtrI); // nominal center -75 Hz
        dblAvgNoisePerBin = pow(dblCtrR,2.0) + pow(dblCtrI,2.0);
        p3 = 145.0 + (dblTrialOffset / 10);
        GoertzelRealImagHamming(intNewSamples, p1, p2, p3, &dblCtrR, &dblCtrI); // center - 50 Hz
        dblAvgNoisePerBin += pow(dblCtrR,2.0) + pow(dblCtrI,2.0);
        p3 = 155.0 + dblTrialOffset / 10;
        GoertzelRealImagHamming(intNewSamples, p1, p2, p3, &dblCtrR, &dblCtrI); // center + 50 Hz
        dblAvgNoisePerBin += pow(dblCtrR,2.0) + pow(dblCtrI,2.0);
        p3 = 157.5 + (dblTrialOffset / 10);
        GoertzelRealImagHamming(intNewSamples, p1, p2, p3, &dblCtrR, &dblCtrI); // center + 75 Hz
        dblAvgNoisePerBin = dblAvgNoisePerBin * 0.25;  // simple average of 4 noise bins,  now units of power

        // Calculate the nominal carriers + one bin
        // above and below for Spectral Peak locator
        p1 = *intPtr;
        p2 = 1200;
        p3 = dblLeftCar;
        p4 = dblLeftR.at(0);
        p5 = dblLeftI.at(0);
        GoertzelRealImagHamming(intNewSamples, p1, p2, p3 - 1, &p4, &p5);
        p4 = dblLeftR.at(1);
        p5 = dblLeftI.at(1);
        GoertzelRealImagHamming(intNewSamples, p1, p2, p3, &p4, &p5);
        dblLeftP = pow(dblLeftR.at(1),2.0) + pow(dblLeftI.at(1),2.0);
        p4 = dblLeftR.at(2);
        p5 = dblLeftI.at(2);
        GoertzelRealImagHamming(intNewSamples, p1, p2, p3 + 1, &p4, &p5);
        p3 = dblRightCar;
        p4 = dblRightR.at(0);
        p5 = dblRightI.at(0);
        GoertzelRealImagHamming(intNewSamples, p1, p2, p3 - 1, &p4, &p5);
        p4 = dblRightR.at(1);
        p5 = dblRightI.at(1);
        GoertzelRealImagHamming(intNewSamples, p1, p2, p3, &p4, &p5);
        dblRightP = pow(dblRightR.at(1),2.0) + pow(dblRightI.at(1),2.0);
        p4 = dblRightR.at(2);
        p5 = dblRightI.at(2);
        GoertzelRealImagHamming(intNewSamples, p1, p2, p3 + 1, &p4, &p5);

        // Calculate the total power in the two tones
        // This mechanism designed to reject single
        // carrier but average both carriers if ratios
        // is less than 4:1
        if ( dblLeftP > (4 * dblRightP) )
            dblPower = dblRightP;
        else if ( dblRightP > (4 * dblLeftP) )
            dblPower = dblLeftP;
        else
            dblPower = sqrt(dblLeftP * dblRightP); // geometric average

        // NOTE: Creating a floor of 1000 for
        // dblAvgNoisePerBin solves issues of
        // very low noise conditions when
        // running pure audio (no radio)
        dblSNdBPwr = 10 * log10(dblPower / qMax(dblAvgNoisePerBin, double(1000.0)));

        // Early leader detect code to calculate
        // S:N on the first 2 symbols)
        // The concept is to allow more accurate
        // framing and synb detection and reduce
        // false leader detects
        p1 = *intPtr;
        p2 = 480;
        p3 = dblTrialOffset / 25;
        p4 = dblCtrR;
        p5 = dblCtrI;
        GoertzelRealImag(intNewSamples, p1, p2, p3 + 57, &p4, &p5); // nominal center -75 Hz
        dblAvgNoisePerBin = pow(dblCtrR,2.0) + pow(dblCtrI,2.0);
        GoertzelRealImag(intNewSamples, p1, p2, p3 + 58, &p4, &p5); // center - 50 Hz
        dblAvgNoisePerBin += pow(dblCtrR,2.0) + pow(dblCtrI,2.0);
        GoertzelRealImag(intNewSamples, p1, p2, p3 + 62, &p4, &p5); // center + 50 Hz
        dblAvgNoisePerBin += pow(dblCtrR,2.0) + pow(dblCtrI,2.0);
        GoertzelRealImag(intNewSamples, p1, p2, p3 + 63, &p4, &p5); // center + 75 Hz

        // NOTE: Creating a floor of 1000 for
        // dblAvgNoisePerBin solves issues of
        // very low noise conditions when running
        // pure audio (no radio)
        double param1 = double(1000.0);
        double param2 = 0.25 * \
                       (dblAvgNoisePerBin + \
                       (pow(dblCtrR,2.0) + \
                        pow(dblCtrI,2.0)));
        dblAvgNoisePerBin = qMax(param1,param2); // average of 4 noise bins
        dblLeftCar = 59 + dblTrialOffset / 25; // the nominal positions of the two tone carriers based on the last computerd dblOffsetHz
        dblRightCar = 61 + dblTrialOffset / 25;
        p1 = *intPtr;
        p2 = 480;
        p3 = dblTrialOffset / 25;
        p4 = dblCtrR;
        p5 = dblCtrI;
        GoertzelRealImag(intNewSamples, p1, p2, dblLeftCar, &p4, &p5); // Left carrier
        dblLeftP = pow(dblCtrR,2.0) + pow(dblCtrI,2.0);
        GoertzelRealImag(intNewSamples, p1, p2, dblRightCar, &p4, &p5); // Right
        dblRightP = pow(dblCtrR,2.0) + pow(dblCtrI,2.0);

        // the following rejects a single tone
        // carrier but geometrically averages
        // the two tones if ratio is < 4:1
        if ( dblLeftP > (4 * dblRightP) )
            dblPowerEarly = dblRightP;
        else if ( dblRightP > (4 * dblLeftP) )
            dblPowerEarly = dblLeftP;
        else
            dblPowerEarly = sqrt(dblLeftP * dblRightP);

        dblSNdBPwrEarly = 10 * log10(dblPowerEarly / dblAvgNoisePerBin);
        // End of Early leader detect test code //

        // The following thresholding requires the
        // first 2 symbols and the first 5 symbols
        // of leader have a minimum S:N for detection.
        // These calibration values yield a sensitivity
        // of ~ - 10 dB S:N (@ 3KHZ noise BW) with a
        // Squelch=5
        // The threshold for early is the same threshold
        // as for the full 5 symbol leader. (compenseated
        // for the 3.97 dB bandwidth difference)
        if ( (dblSNdBPwr > (4 + MCB->RxSquelchLevel)) &&
             (dblSNdBPwrEarly > (MCB->RxSquelchLevel))
           )
        {
            // 4 compensation for bandwidth difference )
#ifdef DEBUG_DEMOD
            //qDebug() << "Fine Search S:N= " << QString("#.0").arg(dblSNdBPwr) << "dB     Early S:N= " << QString("#.0").arg(dblSNdBPwrEarly) << "dB";
#endif
            // Calculate the interpolation based on the
            // left of the two tones
            dblBinInterpLeft = SpectralPeakLocator(dblLeftR.at(0), dblLeftI.at(0), dblLeftR.at(1), dblLeftI.at(1), dblLeftR.at(2), dblLeftI.at(2), &dblLeftMag);
            // And the right of the two tones
            dblBinInterpRight = SpectralPeakLocator(dblRightR.at(0), dblRightI.at(0), dblRightR.at(1), dblRightI.at(1), dblRightR.at(2), dblRightI.at(2), &dblRightMag);
            // Weight the interpolated values in proportion
            // to their magnitudes
            dblBinInterpLeft = dblBinInterpLeft * dblLeftMag / (dblLeftMag + dblRightMag);
            dblBinInterpRight = dblBinInterpRight * dblRightMag / (dblLeftMag + dblRightMag);
#ifdef DEBUG_DEMOD
            //qDebug() << "Fine: SPL Left= " << QString("#.00").arg(dblBinInterpLeft) << " SPL Right= " << QString("#.00").arg(dblBinInterpRight);
#endif

            if ( abs(dblBinInterpLeft + dblBinInterpRight) < 1.0 )
            {
                // sanity check for the interpolators
                if ( (dblBinInterpLeft + dblBinInterpRight) > 0 )
                {
                    double param1 = (dblBinInterpLeft + dblBinInterpRight) * 10;
                    *dblOffsetHz = dblTrialOffset + qMin(param1, double(3.0)); // average left and right, adjustment bounded to +/- 3Hz max
                }
                else
                {
                    double param2 = (dblBinInterpLeft + dblBinInterpRight) * 10;
                    *dblOffsetHz = dblTrialOffset + qMax(param2, double(-3.0));
                }
                // Capture power for debugging ...
                // NOTE: convert to 3 KHz noise bandwidth
                // from 25Hz or 10Hz for reporting consistancy.
                // Note the addition of requiring a second
                // detect with small offset dramatically reduces
                // false triggering even at Squelch values of 3
                // The following demonstrated good detection
                // down to -10 dB S:N with squelch = 3 and
                // minimal false triggering.
                // Added rev 0.8.2.2 11/6/2016 RM
                double x = dblPriorFineOffset - *dblOffsetHz;
                if ( abs(x) < 2.9 )
                {
#ifdef DEBUG_DEMOD
                    qDebug() << "Prior-Offset= " << QString("#.0").arg(dblPriorFineOffset - dblOffsetHz);
#endif
                    objMain->strDecodeCapture = "Ldr; S:N(3KHz) Early=";
                    objMain->strDecodeCapture += QString("%1dB").arg(dblSNdBPwrEarly - 20.8);
                    objMain->strDecodeCapture += ", Full=";
                    objMain->strDecodeCapture += QString("%1dB").arg(dblSNdBPwr - 24.77);
                    objMain->strDecodeCapture += ", Offset=";
                    objMain->strDecodeCapture += QString("%1Hz ").arg(*dblOffsetHz);
                    dttStartRmtLeaderMeasure = QDateTime::currentDateTime();
                    blnLeaderDetected = true;
                    if ( MCB->AccumulateStats )
                    {
                        objMain->stcTuningStats.dblLeaderSNAvg = ((objMain->stcTuningStats.dblLeaderSNAvg * objMain->stcTuningStats.intLeaderDetects) + dblSNdBPwr) / (1 + objMain->stcTuningStats.intLeaderDetects);
                        ++objMain->stcTuningStats.intLeaderDetects;
                    }
                    dblNCOFreq = 3000.0 + *dblOffsetHz; // Set the NCO frequency and phase inc for mixing
                    dblNCOPhaseInc = dbl2Pi * dblNCOFreq / 12000.0;
                    // don't advance the pointer here
                    RxState = AcquireSymbolSync;
                    blnLeaderDetected = true;
                    dttLastLeaderDetect = QDateTime::currentDateTimeUtc();
                    // 24.77dB accomodates ratio of
                    // 3Kz BW:10 Hz BW (10Log 3000/10 = 24.77)
                    double y = round(dblSNdBPwr - 24.77);
                    qint32 z = int(y);
                    intSN = &z;
                    dttStartRmtLeaderMeasure = QDateTime::currentDateTime();
                    dblPriorFineOffset = 1000.0;  // Even reset this for a successful tuning as next attempt will require a clean start.
                    return true;
                }
                else
                {
                    dblPriorFineOffset = *dblOffsetHz;
                    // allways use 1 symbol inc when looking
                    // for next minimal offset
                    intPtr += 240; // advance pointer 1 symbol (20 ms)
                    blnLeaderDetected = false;
                    return false;
                }
            }
            else
            {
                if ( MCB->SlowCPU )
                    intPtr += 480; // advance pointer 2 symbol (40 ms)
                else
                    intPtr += 240; // advance pointer 1 symbol (20 ms)
                blnLeaderDetected = false;
                dblPriorFineOffset = 1000.0;
                return false;
            }
        }
        else
        {
            if ( MCB->SlowCPU )
                intPtr += 480; // advance pointer 2 symbol (40 ms)
            else
                intPtr += 240; // advance pointer 1 symbol (20 ms)

            blnLeaderDetected = false;
            dblPriorFineOffset = 1000.0;
            return false;
        }
    }
    catch (exception Ex)
    {
        qCritical() << "[DemodulateDecode12K.SearchFor2ToneLeader3]  Err:" << Ex.what();
        qint32 len = intNewSamples->length();
        intPtr = &len; // This will force a discard of all received samples
        return false;
    }
} // SearchFor2ToneLeader3()


qint32 DemodulateDecode12K::Acquire4FSKFrameType()
{
    qint32 ret = 0;
    // Check for 12 available 4FSK Symbols
    // (but only 10 are used)
    if ( (intFilteredMixedSamples.length() - intMFSReadPtr) < (240 * 10) )  return -2;

    QVector<qint32> intToneMags;
    intToneMags.clear();
    bool Ok = DemodFrameType4FSK(intMFSReadPtr, &intFilteredMixedSamples, &intToneMags);

    if ( !Ok )
    {
        objMain->Update4FSKConstellation(&intToneMags, &intLastRcvdFrameQuality);
        intMFSReadPtr += (240 * 10);
        return -1;
    }
    intRmtLeaderMeasure = QDateTime::currentDateTime().msecsTo(dttStartRmtLeaderMeasure);
    // Now do check received.  Tone array for testing
    // minimum distance decoder
    // If we have a pending connection (between the IRS
    // first decode of ConReq until it receives a ConAck
    // from the iSS)
    if ( objProtocol->isPending() )
    {
        // The pending session ID will become the session ID
        // once connected
        QByteArray ba;
        ba += objProtocol->bytPendingSessionID;
        ret = MinimalDistanceFrameType(intToneMags, ba);
    }
    else if ( objProtocol->isConnected() )
    {
        // If we are connected then just use the
        // stcConnection.bytSessionID
        QByteArray ba;
        ba += objProtocol->stcConnection.bytSessionID;
        ret = MinimalDistanceFrameType(intToneMags, ba);
    }
    else
    {
        // not connected and not pending so use
        // &FF (FEC or ARQ unconnected session ID
        ret = MinimalDistanceFrameType(intToneMags, QByteArray::fromHex("FF"));
    }

    if ( (ret > 0x30) && (ret < 0x39) ) objHI->SendCommandToHost("PENDING"); // early pending notice to stop scanners

    if ( (ret >= 0) && objFrameInfo->isShortControlFrame(ret) )
    {
         // update the constellation if a short frame
         // (no data to follow)
        objMain->Update4FSKConstellation(&intToneMags, &intLastRcvdFrameQuality);
    }

    if ( MCB->AccumulateStats )
    {
        if ( ret > 0 )
            ++objMain->stcTuningStats.intGoodFSKFrameTypes;
        else
            ++objMain->stcTuningStats.intFailedFSKFrameTypes;
    }

    // advance to read pointer to the next symbol
    // (if there is one)
    intMFSReadPtr += (240 * 10);
    return ret;
} // Acquire4FSKFrameType()

bool DemodulateDecode12K::DemodDecode4FSKConACK(quint8 bytFrameType, qint32 *intTiming)
{
    bool    blnOdd      = false;
    quint16 intNumCar  = 0;
    quint32 intRSLen   = 0;
    quint32 intBaud    = 0;
    quint32 intDataLen = 0;
    quint32 intCenterFreq = 0;
    quint32 intNumSymbolsPerCar = 0;
    QString strType    = "";
    QString strMod     = "";
    QVector<qint32> intToneMags;
    QByteArray bytRawData;
    QByteArray bytCall;
    bytCall.reserve(5);

    // Look up the details by Frame type
    objFrameInfo->frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);
    intTiming = 0;
    //  Timing info repeated three times
    intNumSymbolsPerCar = (intDataLen + intRSLen) * 4;
    intCenterFreq = 1500; // by default
    Demod1Car4FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, intCenterFreq, intNumSymbolsPerCar, &bytRawData, &intToneMags);
    objMain->Update4FSKConstellation(&intToneMags, &intLastRcvdFrameQuality);

    if ( bytRawData.at(0) == bytRawData.at(1) )
    {
        qint8 x = bytRawData.at(0);
        qint32 y = 10 * x;
        intTiming = &y;
    }
    else if ( bytRawData.at(0) == bytRawData.at(2) )
    {
        qint8 x = bytRawData.at(0);
        qint32 y = 10 * x;
        intTiming = &y;
    }
    else if ( bytRawData.at(1) == bytRawData.at(2) )
    {
        qint8 x = bytRawData.at(1);
        qint32 y = 10 * x;
        intTiming = &y;
    }

    qint32 j = *intTiming;
    if ( j >= 0 )
    {
        objMain->strRcvFrameTag = "_" + QString("%1").arg(*intTiming) + " ms";
        if ( MCB->DebugLog )
        {
            qDebug() << "Remote leader timing reported: "
                     << QString("%1").arg(*intTiming)
                     << " ms";
        }
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKFrameDataDecodes;
        ++objMain->intTestFrameCorrectCnt;
        objProtocol->stcConnection.intReceivedLeaderLen = intLeaderRcvdMs;
        // initialize the LastFrameType to an illegal Data frame
        bytLastDataFrameType = 0;
        return true;
    }
    else
    {
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFailedFSKFrameDataDecodes;
        return false;
    }
} // DemodDecode4FSKConACK()



bool DemodulateDecode12K::DemodDecode4FSKData(quint8 bytFrameType, QByteArray *bytData, qint32 *intQuality)
{
    // These static variables used to accumuate
    // correctly decoded carriers on simultaneous
    // multicarrier 4FSK modes.
    //Static bytLastFrameType As Byte = 0
    static qint32 intTestCounts = 0;
    static qint32 intPassCounts = 0;

    static QVector<qint32> intSumCounts;
    static QVector<QVector<qint16> > intToneMagHistory(3);

    static QByteArray bytCorrectedDataCar1;
    static QByteArray bytCorrectedDataCar2;
    static QByteArray bytCorrectedDataCar3;
    static QByteArray bytCorrectedDataCar4;
    //Static bytPass As Byte = 0

    bool    blnOdd     = false;
    bool    blnSuccess = false;
    quint8  bytPass    = 0;
    quint16 intNumCar  = 0;
    quint32 intBaud    = 0;
    quint32 intDataLen = 0;
    quint32 intRSLen   = 0;
    quint32 intNumSymbolsPerCar = 0;
    QVector<qint32> intToneMags;
    QVector<qint32> intToneMagsAllCar;
    intToneMagsAllCar.reserve(intNumCar * 4 * intNumSymbolsPerCar - 1);
    intToneMagsAllCar.clear();
    QString strType = "";
    QString strMod  = "";
    QByteArray bytCarData;
    QByteArray bytJustData;
    QByteArray bytNoRS;
    QByteArray bytRawData;

    Q_UNUSED(blnSuccess) // to stop warnings in release build

    ++intTestCounts;

    // Look up the details by Frame type
    objFrameInfo->frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);
    // initialise the static data if Accumulating
    // Stats or on a change in frame type
    // (e.g. even to odd, different # car, etc)
    intNumSymbolsPerCar = (1 + intDataLen + 2 + intRSLen) * 4; //  byte count, data, 2 byte CRC, RS
    if ( (bytLastDataFrameType != bytFrameType) ||
         (strExecutionDirectory.toLower().lastIndexOf("debug") == -1)
       )
    {
        bytPass = 0; // clear the passing flags (prior successful decodes of bytFrameType)
        bytLastDataFrameType = bytFrameType;
        bytCorrectedDataCar1.clear(); // Re initialize the bytCorrectedData array
        bytCorrectedDataCar2.clear(); // Re initialize the bytCorrectedData array
        bytCorrectedDataCar3.clear(); // Re initialize the bytCorrectedData array
        bytCorrectedDataCar4.clear(); // Re initialize the bytCorrectedData array
        int y = 4 * (intNumSymbolsPerCar - 1);
        for ( quint8 x = 0; x <= 3; ++x )
        {
            intToneMagHistory[x].resize(y); // clear avg ToneMagHistory used for analog Mem ARQ
        }
        intSumCounts.reserve(3); // zero out the counts in the averages
    }

    switch ( intNumCar )
    {
        // Note car frequences go from high to low
        //to accomodate sideband reversal after mixing
    case 1:
        if ( (bytPass & 0x80) == 0 )
        {
            quint32 param = (quint32)(1500);
            Demod1Car4FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, param, intNumSymbolsPerCar, &bytRawData, &intToneMags);
            VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 0, qint32(intToneMags.length()));
            bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
            if ( Ok )
            {
                bytCorrectedDataCar1.resize(bytCarData.length() - 1);
                ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar1, 0, quint32(bytCarData.length()));
                bytPass = (bytPass | 0x80);
            }
            else if ( intSumCounts.at(0) == 0 )
            {
                // initialize the TonMagHistory for the
                // first carrier
                for ( qint32 m = 0; m < (intToneMags.length() - 1); ++m )
                {
                    intToneMagHistory[0][m] = intToneMags.at(m);
                } // Next m
                intSumCounts[0] = 1;
            }
            else
            {
                // average the tone mags
                for ( qint32 m = 0; m < (intToneMags.length() - 1); ++m )
                {
                    quint32 val = intToneMagHistory[0].at(m);
                    val += int(intToneMags.at(m) / 2);
                    intToneMagHistory[0][m] = val;
                    intToneMags[m] = val;
                } // Next m
                Decode1Car4FSKFromTones(&bytRawData,&intToneMags);
                VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 0, intToneMags.length());
                bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
                if ( Ok )
                {
                    bytCorrectedDataCar1.resize(bytCarData.length() - 1);
                    ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar1, 0, bytCarData.length());
                    bytPass = (bytPass | 0x80);
                    if ( MCB->AccumulateStats )
                    {
                        ++objMain->stcTuningStats.intGoodFSKSummationDecodes;
                        intSumCounts[0] = 0;
                    }
                    else
                    {
                        intSumCounts[0] += 1;
                    }
                }
            }
        }
        break;

    case 2:
     {
        qint32 m = 0;
        if ( (bytPass & 0x80) == 0 )
        {
            quint32 param1 = 1750;
            Demod1Car4FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, param1, intNumSymbolsPerCar, &bytRawData, &intToneMags);
            VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 0, intToneMags.length());
            bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
            if ( Ok )
            {
                bytCorrectedDataCar1.resize(bytCarData.length() - 1);
                ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar1, 0, bytCarData.length());
                bytPass = (bytPass | 0x80);
            }
            else if ( intSumCounts.at(0) == 0 )
            {
                for ( m = 0; m < (intToneMags.length() - 1); ++m )
                {
                    intToneMagHistory[0][m] = intToneMags.at(m);
                } // Next m
                intSumCounts[0] = 1;
            }
            else
            {
                // average the tone mags
                for ( m = 0; m < (intToneMags.length() - 1); ++m )
                {
                    intToneMagHistory[0][m] = intToneMagHistory[0].at(m) + int(intToneMags.at(m) / 2);
                    intToneMags[m] = intToneMagHistory[0].at(m);
                } // Next m
                Decode1Car4FSKFromTones(&bytRawData, &intToneMags);

                VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 0, intToneMags.length());
                bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
                if ( Ok )
                {
                    bytCorrectedDataCar1.resize(bytCarData.length() - 1);
                    ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar1, 0, bytCarData.length());
                    bytPass = (bytPass | 0x80);
                    if ( MCB->AccumulateStats )
                    {
                        ++objMain->stcTuningStats.intGoodFSKSummationDecodes;
                        intSumCounts[0] = 0;
                    }
                    else
                    {
                        intSumCounts[0] += 1;
                    }
                }
            }


            if ( (bytPass & 0x40) == 0 )
            {
                Demod1Car4FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, (quint32)(1250), intNumSymbolsPerCar, &bytRawData, &intToneMags);
                VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 4 * intNumSymbolsPerCar, intToneMags.length());
                bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
                if ( Ok )
                {
                    bytCorrectedDataCar2.resize(bytCarData.length() - 1);
                    ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar2, 0, bytCarData.length());
                    bytPass = (bytPass | 0x40);
                }
                else if ( intSumCounts.at(1) == 0 )
                {
                    for ( m = 0; m < (intToneMags.length() - 1); ++m )
                    {
                        intToneMagHistory[1][m] = intToneMags.at(m);
                    } // Next m
                    intSumCounts[1] = 1;
                }
                else
                {
                    // average the tone mags
                    for ( m = 0; m < (intToneMags.length() - 1); ++m )
                    {
                        intToneMagHistory[1][m] = intToneMagHistory[1].at(m) + int(intToneMags.at(m) / 2);
                        intToneMags[m] = intToneMagHistory[1].at(m);
                    } // Next m
                    Decode1Car4FSKFromTones(&bytRawData, &intToneMags);
                    VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 4 * intNumSymbolsPerCar, intToneMags.length());
                    bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
                    if ( Ok )
                    {
                        bytCorrectedDataCar2.resize(bytCarData.length() - 1);
                        ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar2, bytCarData.length(), bytCarData.length());
                        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKSummationDecodes;
                        bytPass = (bytPass | 0x40);
                        intSumCounts[1] = 0;
                    }
                    else
                    {
                        intSumCounts[1] += 1;
                    }
                }
            }
        }
     }
     break;

    case 3:
        break;

    case 4:
     {
        if ( (bytPass & 0x80) == 0 )
        {
            quint32 m = 0;
            Demod1Car4FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, 2250, intNumSymbolsPerCar, &bytRawData, &intToneMags);
            VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 0, intToneMags.length());
            bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
            if ( Ok )
            {
                bytCorrectedDataCar1.resize(bytCarData.length() - 1);
                ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar1, 0, bytCarData.length());
                bytPass = (bytPass | 0x80);
            }
            else if ( intSumCounts.at(0) == 0 )
            {
                // capture tones for later averaging
                for ( m = 0; m < (quint32)(intToneMags.length() - 1); ++m )
                {
                    intToneMagHistory[0][m] = intToneMags.at(m);
                } // Next m
                intSumCounts[0] = 1;
            }
            else
            {
                // average the tone mags
                for ( m = 0; m < (quint32)(intToneMags.length() - 1); ++m )
                {
                    intToneMagHistory[0][m] = intToneMagHistory[0].at(m) + int(intToneMags.at(m) / 2);
                    intToneMags[m] = intToneMagHistory[0].at(m);
                }  // Next m
                //Demod1Car4FSK(intMFSReadPtr, intFilteredMixedSamples, intBaud, 2250, intNumSymbolsPerCar, bytRawData, intToneMags)
                Decode1Car4FSKFromTones(&bytRawData, &intToneMags);
                VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 0, intToneMags.length());
                bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
                if ( Ok )
                {
                    bytCorrectedDataCar1.resize(bytCarData.length() - 1);
                    ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar1, 0, bytCarData.length());
                    bytPass = (bytPass | 0x80);
                    if ( MCB->AccumulateStats )
                    {
                        ++objMain->stcTuningStats.intGoodFSKSummationDecodes;
                        intSumCounts[0] = 0;
                    }
                    else
                    {
                        intSumCounts[0] += 1;
                    }
                }
            }

            if ( (bytPass & 0x40) == 0 )
            {
                Demod1Car4FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, 1750, intNumSymbolsPerCar, &bytRawData, &intToneMags);
                quint32 param = 4 * intNumSymbolsPerCar;
                VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, param, intToneMags.length());
                bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
                if ( Ok )
                {
                    bytCorrectedDataCar2.resize(bytCarData.length() - 1);
                    ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar2, 0, bytCarData.length());
                    bytPass = (bytPass | 0x40);
                }
                else if ( intSumCounts.at(1) == 0 )
                {
                    for ( m = 0; m < (quint32)(intToneMags.length() - 1); ++m )
                    {
                        intToneMagHistory[1][m] = intToneMags.at(m);
                    } // Next m
                    intSumCounts[1] = 1;
                }
                else
                {
                    // average the tone mags
                    for ( m = 0; m < (quint32)(intToneMags.length() - 1); ++m )
                    {
                        intToneMagHistory[1][m] = intToneMagHistory[1].at(m) + int(intToneMags.at(m) / 2);
                        intToneMags[m] = intToneMagHistory[1].at(m);
                    } // Next m
                    Decode1Car4FSKFromTones(&bytRawData, &intToneMags);
                    quint32 param = 4 * intNumSymbolsPerCar;
                    VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, param, intToneMags.length());
                    bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
                    if ( Ok )
                    {
                        bytCorrectedDataCar2.resize(bytCarData.length() - 1);
                        ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar2, 0, bytCarData.length());
                        bytPass = (bytPass | 0x40);
                        if ( MCB->AccumulateStats )
                            ++objMain->stcTuningStats.intGoodFSKSummationDecodes;
                            intSumCounts[1] = 0;
                        }
                        else
                        {
                            intSumCounts[1] += 1;
                        }
                    }
                }
            }

            if ( (bytPass & 0x20) == 0 )
            {
                quint32 m = 0;
                Demod1Car4FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, 1250, intNumSymbolsPerCar, &bytRawData, &intToneMags);
                VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 8 * intNumSymbolsPerCar, intToneMags.length());
                bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
                if ( Ok )
                {
                    bytCorrectedDataCar3.resize(bytCarData.length() - 1);
                    ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar3, 0, bytCarData.length());
                    bytPass = (bytPass | 0x20);
                }
                else if ( intSumCounts.at(2) == 0 )
                {
                    for ( m = 0; m < (quint32)(intToneMags.length() - 1); ++m )
                    {
                        intToneMagHistory[2][m] = intToneMags.at(m);
                    } // Next m
                    intSumCounts[2] = 1;
                }
                else
                {
                    // average the tone mags
                    for ( m = 0; m < (quint32)(intToneMags.length() - 1); ++m )
                    {
                        intToneMagHistory[2][m] = intToneMagHistory[2].at(m) + int(intToneMags.at(m) / 2);
                        intToneMags[m] = intToneMagHistory[2].at(m);
                    } // Next m
                    Decode1Car4FSKFromTones(&bytRawData, &intToneMags);
                    VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 8 * intNumSymbolsPerCar, intToneMags.length());
                    bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
                    if ( Ok )
                    {
                        bytCorrectedDataCar3.resize(bytCarData.length() - 1);
                        ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar3, 0, bytCarData.length());
                        bytPass = (bytPass | 0x20);
                        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKSummationDecodes;
                        intSumCounts[2] = 0;
                    }
                    else
                    {
                        intSumCounts[2] += 1;
                    }
                }
            }

            if ( (bytPass & 0x10) == 0 )
            {
                quint32 m = 0;
                Demod1Car4FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, 750, intNumSymbolsPerCar, &bytRawData, &intToneMags);
                VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 12 * intNumSymbolsPerCar, intToneMags.length());
                bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
                if ( Ok )
                {
                    bytCorrectedDataCar4.resize(bytCarData.length() - 1);
                    ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar4, 0, bytCarData.length());
                    bytPass = (bytPass | 0x10);
                }
                else if ( intSumCounts.at(3) == 0 )
                {
                    for ( m = 0; m < (quint32)(intToneMags.length() - 1); ++m )
                    {
                        intToneMagHistory[3][m] = intToneMags.at(m);
                    } // Next m
                    intSumCounts[3] = 1;
                }
                else
                {
                    // average the tone mags
                    for ( m = 0; m < (quint32)(intToneMags.length() - 1); ++m )
                    {
                        intToneMagHistory[3][m] = intToneMagHistory[3].at(m) + int(intToneMags.at(m) / 2);
                        intToneMags[m] = intToneMagHistory[3].at(m);
                    } // Next m
                    Decode1Car4FSKFromTones(&bytRawData, &intToneMags);
                    VectorCopy2(&intToneMags, 0, &intToneMagsAllCar, 12 * intNumSymbolsPerCar, intToneMags.length());
                    bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
                    if ( Ok )
                    {
                        bytCorrectedDataCar4.resize(bytCarData.length() - 1);
                        ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar4, 0, bytCarData.length());
                        bytPass = (bytPass | 0x10);
                        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKSummationDecodes;
                        intSumCounts[3] = 0;
                    }
                    else
                    {
                        intSumCounts[3] += 1;
                    }
               }
            }
        }
        break;
    }

    objMain->Update4FSKConstellation(&intToneMagsAllCar, intQuality);

    switch ( intNumCar )
    {
    case 1:
        if ( bytPass == 0x80 )
        {
            if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKFrameDataDecodes;
            bytData = &bytCorrectedDataCar1;
            ++intPassCounts;
            //qDebug() << "4FSK Decode Success cnts = " << QString("%1").arg(intPassCounts) << "/" << QString("%1").arg(intTestCounts);
            return true;
        }
        else
        {
            if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFailedFSKFrameDataDecodes;
            // qDebug() << "4FSK Decode Success cnts = " << QString("%1").arg(intPassCounts) << "/" << QString("%1").arg(intTestCounts);
        }
        break;

    case 2:
        if ( bytPass == 0xC0 )
        {
            if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKFrameDataDecodes;
            bytData->resize(bytCorrectedDataCar1.length() + bytCorrectedDataCar2.length() - 1);
            ByteArrayCopy2(&bytCorrectedDataCar1, 0, bytData, 0, bytCorrectedDataCar1.length());
            ByteArrayCopy2(&bytCorrectedDataCar2, 0, bytData, bytCorrectedDataCar1.length(), bytCorrectedDataCar2.length());
            return true;
        }
        else
        {
            if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFailedFSKFrameDataDecodes;
        }
        break;

    case 4:
        if ( bytPass == 0xF0 )
        {
            if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKFrameDataDecodes;
            bytData->resize(bytCorrectedDataCar1.length() + bytCorrectedDataCar2.length() + bytCorrectedDataCar3.length() + bytCorrectedDataCar4.length() - 1);
            ByteArrayCopy2(&bytCorrectedDataCar1, 0, bytData, 0, bytCorrectedDataCar1.length());
            ByteArrayCopy2(&bytCorrectedDataCar2, 0, bytData, bytCorrectedDataCar1.length(), bytCorrectedDataCar2.length());
            ByteArrayCopy2(&bytCorrectedDataCar3, 0, bytData, bytCorrectedDataCar1.length() + bytCorrectedDataCar2.length(), bytCorrectedDataCar3.length());
            ByteArrayCopy2(&bytCorrectedDataCar4, 0, bytData, bytCorrectedDataCar1.length() + bytCorrectedDataCar2.length() + bytCorrectedDataCar3.length(), bytCorrectedDataCar4.length());
            return true;
        }
        else
        {
            if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFailedFSKFrameDataDecodes;
        }
        break;
    }

    return false;
} // DemodDecode4FSKData()

bool DemodulateDecode12K::DecodePSKData(quint8 bytFrameType, QVector<qint16> *intPhases, QVector<qint16> *intMags, QByteArray *bytData)
{
    // These static variables are used to
    // accumuate correctly decoded carriers
    // after repeats on multicarrier modes.
    static quint8 bytPass = 0x0;
    static QVector<QVector<qint16> > intCarPhaseAvg(7); // array to accumulate phases for averaging (Memory ARQ)
    static QVector<QVector<qint16> > intCarMagAvg(7);   // array to accumulate mags for averaging (Memory ARQ)
    static QVector<qint16> intSumCounts(7);
    static QByteArray bytCorrectedData0; // an array of correctedData one for carrier 0
    static QByteArray bytCorrectedData1; // an array of correctedData one for carrier 1
    static QByteArray bytCorrectedData2; // an array of correctedData one for carrier 2
    static QByteArray bytCorrectedData3; // an array of correctedData one for carrier 3
    static QByteArray bytCorrectedData4; // an array of correctedData one for carrier 4
    static QByteArray bytCorrectedData5; // an array of correctedData one for carrier 5
    static QByteArray bytCorrectedData6; // an array of correctedData one for carrier 6
    static QByteArray bytCorrectedData7; // an array of correctedData one for carrier 7



    bool    blnOdd        = false;
    bool    mDecodePSKData = false;
    quint8  bytCarMask    = 0x80;
    quint8  bytQualThresh = 100;
    quint16  intNumCar     = 0;
    quint32  intPSKMode    = 0;
    quint32  intBaud       = 0;
    quint32  intDataLen    = 0;
    quint32  intRSLen      = 0;
    quint32  intNumSymbolsPerCar = 0;
    QVector<qint16> intCarPhases;
    QVector<qint16> intCarMags;
    QString strType       = "";
    QString strMod        = "";
    QByteArray bytNoRS;
    QByteArray bytCorrectedCarData;
    QByteArray bytRawData;

    intCarPhases.clear();
    intCarMags.clear();
    bytNoRS.clear();
    bytCorrectedCarData.clear();

    Q_UNUSED(bytQualThresh)  // to stop warnings in release build
    Q_UNUSED(intNumSymbolsPerCar)

    // qDebug() << "DecodePSKData start time for " << QString("%1").arg(intNumCar) << " car " << QDateTime::currentDateTime().msecsTo(dttStartTimingMeasure).toString() << " ms";
    mDecodePSKData = false; // Preset for failure
    // Look up the details by Frame type
    objFrameInfo->frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);
    intPSKMode = strMod.mid(0, 1).toInt();
    intNumSymbolsPerCar = quint32(intPhases->length() / intNumCar);
    objRS8->setMaxCorrections(int(intRSLen / 2));
    bytRawData.resize(int((intPhases->length() / 4) / intNumCar) - 1); // for 4PSK modes
    if ( intPSKMode == 8 )
    {
        // Redim for 3 bits per phase value for 8PSK modes
        bytRawData.resize(int(((intPhases->length() * 3) / 8) / intNumCar) - 1);
    }
    intCarPhases.resize(int(intPhases->length() / intNumCar) - 1);
    intCarMags.resize(int(intMags->length() / intNumCar) - 1);
    // 1 byte byte Count, Data, 2 byte CRC
    bytNoRS.resize(1 + intDataLen + 2 - 1);
    // initialise the static data if accumulating stats or
    // on a change in frame type (e.g. even to odd, different
    // # car, etc)
    if ( (bytLastDataFrameType != bytFrameType) ||
         (objMain->strExecutionDirectory.toLower().lastIndexOf("debug") != -1)
       )
    {
        bytPass = 0; // clear the passing flags (prior successful decodes of bytFrameType)
        bytLastDataFrameType = bytFrameType;
        bytCorrectedData0.clear(); // Re initialize the bytCorrectedData array for carrier 0
        bytCorrectedData1.clear(); // Re initialize the bytCorrectedData array for carrier 1
        bytCorrectedData2.clear(); // Re initialize the bytCorrectedData array for carrier 2
        bytCorrectedData3.clear(); // Re initialize the bytCorrectedData array for carrier 3
        bytCorrectedData4.clear(); // Re initialize the bytCorrectedData array for carrier 4
        bytCorrectedData5.clear(); // Re initialize the bytCorrectedData array for carrier 5
        bytCorrectedData6.clear(); // Re initialize the bytCorrectedData array for carrier 6
        bytCorrectedData7.clear(); // Re initialize the bytCorrectedData array for carrier 7
        for ( int x = 0; x <= 7; ++x )
        {
            // clear avg phase of multiple frames
            // used for analog Mem ARQ
            quint32 y = intCarPhases.length() - 1;
            intCarPhaseAvg[x].resize(y);
        }

        // clear avg Mags of mulitple frames used
        // for Analog Mem ARQ (Added Rev 0.3.5.1)
        for ( int x = 0; x <= 7; ++x )
        {
            // clear avg phase of multiple frames
            // used for analog Mem ARQ
            quint32 y = intCarPhases.length() - 1;
            intCarMagAvg[x].resize(y);
        }

        // zero out the counts in the averages
        intSumCounts.resize(7);
    }

    // Across all carriers
    for ( quint16 i = 0; i < (quint32)(intNumCar - 1); ++i )
    {
        if ( (bytCarMask & bytPass) == 0 )
        {
            // decode only those carriers
            // not yet correctly decoded.
            quint32 param1 = i * intCarPhases.length();
            quint32 param2 = i * intCarMags.length();
            VectorCpy2(intPhases, param1, &intCarPhases, quint32(0), quint32(intCarPhases.length()));
            VectorCpy2(intMags,   param2, &intCarMags,   0, intCarMags.length());
            bool Ok = Decode1CarPSK(intCarPhases, intPSKMode, bytRawData, &bytCorrectedCarData, intRSLen, bytFrameType);
            if ( Ok )
            {
                switch ( i )
                {
                case 0: bytCorrectedData0.resize(bytCorrectedCarData.length() - 1); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData0, bytCorrectedCarData.length());
                case 1: bytCorrectedData1.resize(bytCorrectedCarData.length() - 1); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData1, bytCorrectedCarData.length());
                case 2: bytCorrectedData2.resize(bytCorrectedCarData.length() - 1); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData2, bytCorrectedCarData.length());
                case 3: bytCorrectedData3.resize(bytCorrectedCarData.length() - 1); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData3, bytCorrectedCarData.length());
                case 4: bytCorrectedData4.resize(bytCorrectedCarData.length() - 1); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData4, bytCorrectedCarData.length());
                case 5: bytCorrectedData5.resize(bytCorrectedCarData.length() - 1); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData5, bytCorrectedCarData.length());
                case 6: bytCorrectedData6.resize(bytCorrectedCarData.length() - 1); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData6, bytCorrectedCarData.length());
                case 7: bytCorrectedData7.resize(bytCorrectedCarData.length() - 1); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData7, bytCorrectedCarData.length());
                }
                bytPass = (bytCarMask | bytPass);
                intSumCounts[i] = 0;
            }
            else
            {
                // failure in correction so just add to
                // average and try to decode
                if ( intSumCounts.at(i) == 0 )
                {
                    // initialize Sum counts Phase average
                    // and Mag Average
                    for ( qint32 m = 0; m < (intCarPhases.length() - 1); ++m )
                    {
                        intCarPhaseAvg[i][m] = intCarPhases.at(m);
                        intCarMagAvg[i][m] = intCarMags.at(m);
                    } // Next m
                    intSumCounts[i] = 1;
                }
                else
                {
                    for ( qint32 k = 0; k < (intCarPhases.length() - 1); ++k )
                    {
                        intCarPhaseAvg[i][k] = WeightedAngleAvg(intCarPhaseAvg[i].at(k), intCarPhases.at(k), intCarMagAvg[i].at(k), intCarMags.at(k)); // Modified 0.3.5.1
                        intCarPhases[k]  = intCarPhaseAvg.at(i).at(k);
                        qint32 x = intCarMagAvg.at(i).at(k);
                        qint16 z = pow(x,2);
                        qint32 w = pow(intCarMags.at(k),2) / (intCarMagAvg[i].at(k) + intCarMags.at(k));
                        z += w;
                        intCarMagAvg[i][k] = z; // Added 0.3.5.1
                    } // Next k
                    ++intSumCounts[i];

                    // now try to decode based on
                    // the WeightedAveragePhases
                    bool Ok = Decode1CarPSK(intCarPhases, intPSKMode, bytRawData, &bytCorrectedCarData, intRSLen, bytFrameType);
                    if ( Ok )
                    {
                        quint32 v = bytCorrectedCarData.length();
                        quint32 w = bytCorrectedCarData.length() - 1;
                        switch ( i )
                        {
                        case 0: bytCorrectedData0.resize(w); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData0, v);
                        case 1: bytCorrectedData1.resize(w); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData1, v);
                        case 2: bytCorrectedData2.resize(w); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData2, v);
                        case 3: bytCorrectedData3.resize(w); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData3, v);
                        case 4: bytCorrectedData4.resize(w); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData4, v);
                        case 5: bytCorrectedData5.resize(w); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData5, v);
                        case 6: bytCorrectedData6.resize(w); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData6, v);
                        case 7: bytCorrectedData7.resize(w); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData7, v);
                        }
                        bytPass = (bytCarMask | bytPass);
                        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodPSKSummationDecodes;
                    }
                }
            }
        }
        bytCarMask = bytCarMask >> 1; // Shift the mask
    } // Next i

    switch ( intNumCar )
    {
    case 1:
        mDecodePSKData = (bytPass = 0x80);
    case 2:
        mDecodePSKData = (bytPass = 0xC0);
    case 4:
        mDecodePSKData = (bytPass = 0xF0);
    case 8:
        mDecodePSKData = (bytPass = 0xFF);
    }

    if ( mDecodePSKData )
    {
        qint32 intPtr = 0;
        bytData->clear();
        for ( quint8 i = 0; i < (quint8)(intNumCar - 1); ++i )
        {
            switch ( i )
            {
            case 0: bytData->reserve(intPtr + bytCorrectedData0.length() - 1); ByteArrayCopy2(&bytCorrectedData0, 0, bytData, intPtr, quint32(bytCorrectedData0.length())); intPtr += bytCorrectedData0.length();
            case 1: bytData->reserve(intPtr + bytCorrectedData1.length() - 1); ByteArrayCopy2(&bytCorrectedData1, 0, bytData, intPtr, bytCorrectedData1.length()); intPtr += bytCorrectedData1.length();
            case 2: bytData->reserve(intPtr + bytCorrectedData2.length() - 1); ByteArrayCopy2(&bytCorrectedData2, 0, bytData, intPtr, bytCorrectedData2.length()); intPtr += bytCorrectedData2.length();
            case 3: bytData->reserve(intPtr + bytCorrectedData3.length() - 1); ByteArrayCopy2(&bytCorrectedData3, 0, bytData, intPtr, bytCorrectedData3.length()); intPtr += bytCorrectedData3.length();
            case 4: bytData->reserve(intPtr + bytCorrectedData4.length() - 1); ByteArrayCopy2(&bytCorrectedData4, 0, bytData, intPtr, bytCorrectedData4.length()); intPtr += bytCorrectedData4.length();
            case 5: bytData->reserve(intPtr + bytCorrectedData5.length() - 1); ByteArrayCopy2(&bytCorrectedData5, 0, bytData, intPtr, bytCorrectedData5.length()); intPtr += bytCorrectedData5.length();
            case 6: bytData->reserve(intPtr + bytCorrectedData6.length() - 1); ByteArrayCopy2(&bytCorrectedData6, 0, bytData, intPtr, bytCorrectedData6.length()); intPtr += bytCorrectedData6.length();
            case 7: bytData->reserve(intPtr + bytCorrectedData7.length() - 1); ByteArrayCopy2(&bytCorrectedData7, 0, bytData, intPtr, bytCorrectedData7.length());
            }
        } // Next i

        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodPSKFrameDataDecodes;
        // qDebug() << "bytPass = " << bytPass;
    }
    else
    {
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFailedPSKFrameDataDecodes;
    }
    // qDebug() << "DecodePSKData end time for " & intNumCar.ToString & " car " & QDateTime::currentDateTime().msecsTo(dttStartTimingMeasure).toString("0.sss") << " ms";
    return mDecodePSKData;
}  // DecodePSKData()


bool DemodulateDecode12K::DecodeACKNAK(qint32 intFrameType, qint32 *intQuality)
{
    // mask off lower 5 bits - Range of 38 to 100 in steps of 2
    qint32 x = 38 + quint32(2 * (intFrameType & 0x1F));
    intQuality = &x;
    QString msg = "_Q" + QString("%1").arg(*intQuality);
    objMain->strRcvFrameTag = msg;
    return true;
} // DecodeACKNAK()

bool DemodulateDecode12K::DemodDecode4FSKConReq(quint8 bytFrameType, qint32 *intBW, QString *strCaller, QString *strTarget)
{
    Q_UNUSED(intBW)
    bool   blnOdd        = false;
    bool   blnRSOK       = false;
    quint8 k             = 0;
    quint16 intNumCar     = 0;
    quint32 intBaud       = 0;
    quint32 intDataLen    = 0;
    quint32 intRSLen      = 0;
    quint32 intCenterFreq = 0;
    quint32 intMatch      = 0;
    quint32 intNumSymbolsPerCar = 0;
    QString strType      = "";
    QString strMod       = "";
    QVector<qint32> intToneMags;
    QByteArray bytRawData;
    QByteArray bytCheck;
    QByteArray bytDecodedRS;
    QByteArray bytCall;
    bytCall.reserve(5);

    //Look up the details by Frame type
    objFrameInfo->frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);
    intNumSymbolsPerCar = (intDataLen + intRSLen) * 4; //  Two compressed call signs + 4 byte RS
    intCenterFreq = 1500;
    Demod1Car4FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, intCenterFreq, intNumSymbolsPerCar, &bytRawData, &intToneMags);
    objMain->Update4FSKConstellation(&intToneMags, &intLastRcvdFrameQuality);
    objMain->stcQualityStats.int4FSKQuality = intLastRcvdFrameQuality;
    objRS8->setMaxCorrections(int(intRSLen / 2));
    bytDecodedRS = objRS8->RSDecode(bytRawData, blnRSOK);
    ByteArrayCopy2(&bytDecodedRS, 0, &bytCall, 0, 6);
    QString tmp = DeCompressCallsign(bytCall);
    strCaller = &tmp;
    ByteArrayCopy2(&bytDecodedRS, 6, &bytCall, 0, 6);
    tmp = DeCompressCallsign(bytCall);
    strTarget = &tmp;
    QString strMsg = "_";
    strMsg += strCaller;
    strMsg += " > ";
    strMsg += strTarget;
    objMain->strRcvFrameTag = strMsg;
    bytCheck = objRS8->RSEncode(bytDecodedRS);

    for ( k = 12; k < 15; ++k )
    {
        if ( bytCheck.at(k) == bytRawData.at(k) ) ++intMatch;
    } // Next k

    if ( blnRSOK && (intMatch >= 2) )
    {
        qint32 bw = 0;
        switch ( bytFrameType )
        {
        case 31:
            bw = qint32(200);
            break;
        case 32:
            bw = qint32(500);
            break;
        case 33:
            bw = qint32(1000);
            break;
        case 34:
            bw = qint32(2000);
            break;
        }
        intBW = &bw;

        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKFrameDataDecodes;
        ++objMain->intTestFrameCorrectCnt;
        return true;
    }
    else
    {
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFailedFSKFrameDataDecodes;
        return false;
    }
} // DemodDecode4FSKConReq()

bool DemodulateDecode12K::DemodDecode4FSKID(quint8 bytFrameType, QString *strCallID, QString *strGridSquare)
{
    bool blnOdd          = false;
    bool blnRSOK         = false;
    quint8 k             = 0;
    quint16 intNumCar     = 0;
    quint32 intBaud       = 0;
    quint32 intDataLen    = 0;
    quint32 intRSLen      = 0;
    quint32 intCenterFreq = 0;
    quint32 intMatch      = 0;
    quint32 intNumSymbolsPerCar = 0;
    QString strType      = "";
    QString strMod       = "";
    QVector<qint32> intToneMags;
    QByteArray bytRawData;
    QByteArray bytDecodedRS;
    QByteArray bytCheck;
    QByteArray bytCall;
    bytCall.reserve(5);

    // Look up the details by Frame type
    objFrameInfo->frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);
    intNumSymbolsPerCar = (intDataLen + intRSLen) * 4; //  Compresssed Call + Compressed GS + 4 byte RS Parity
    intCenterFreq = 1500;
    Demod1Car4FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, intCenterFreq, intNumSymbolsPerCar, &bytRawData, &intToneMags);
    objMain->Update4FSKConstellation(&intToneMags, &intLastRcvdFrameQuality);
    objRS8->setMaxCorrections(int(intRSLen / 2));
    bytDecodedRS = objRS8->RSDecode(bytRawData, blnRSOK);
    ByteArrayCopy2(&bytDecodedRS, 0, &bytCall, 0, 6);
    QString str = DeCompressCallsign(bytCall);
    strCallID = &str;
    ByteArrayCopy2(&bytDecodedRS, 6, &bytCall, 0, 6);
    str = DeCompressGridSquare(bytCall).trimmed();
    strGridSquare = &str;

    if ( strGridSquare->length() == 6 )
    {
        QString str = "[" + strGridSquare->mid(0, 4).toUpper() + strGridSquare->mid(4, 2).toLower() + "]";
        strGridSquare = &str;
    }
    else if ( strGridSquare->length() == 8 )
    {
        QString str = "[" + strGridSquare->mid(0, 4).toUpper() + strGridSquare->mid(4, 2).toLower() + strGridSquare->mid(6, 2) + "]";
        strGridSquare = &str;
    }
    else
    {
        QString str = "[" + strGridSquare->toUpper() + "]";
        strGridSquare = &str;
    }

    QString msg = "_";
    msg += strCallID;
    msg += " ";
    msg += strGridSquare;
    objMain->strRcvFrameTag = msg;
    bytCheck = objRS8->RSEncode(bytDecodedRS);

    for ( k = 12; k <= 15; ++k )
    {
        if ( bytCheck.at(k) == bytRawData.at(k) ) ++intMatch;
    } // Next k

    if ( (intMatch >= 2) && blnRSOK )
    {
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKFrameDataDecodes;
        ++objMain->intTestFrameCorrectCnt;
        return true;
    }
    else
    {
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFailedFSKFrameDataDecodes;
        return false;
    }
}  // DemodDecode4FSKID()

bool DemodulateDecode12K::Decode4FSK600bdData(quint8 bytFrameType, QByteArray *bytData, qint32 *intQuality)
{
    // These static variables used to
    // accumuate correctly decoded carriers
    // on simultaneous multicarrier 4FSK modes.
    static QByteArray bytCorrectedDataBlock1;
    static QByteArray bytCorrectedDataBlock2;
    static QByteArray bytCorrectedDataBlock3;
    static quint8 bytPass = 0x0;   // not the same as numeric 0

    bool    blnOdd     = false;
    bool    blnSuccess = false;
    quint16 intNumCar  = 0;
    quint32 intBaud    = 0;
    quint32 intDataLen = 0;
    quint32 intRSLen   = 0;
    quint32 intNumSymbolsPerCar = 0;

    QString strType = "";
    QString strMod = "";

    QByteArray bytCarData;

    QVector<qint32> intToneMags;
    QByteArray bytJustData;
    QByteArray bytNoRS;
    QByteArray bytRawData;
    QByteArray bytBlockData;

    Q_UNUSED(blnSuccess)         // to suppress warnings in release build

    // Look up the details by Frame type
    objFrameInfo->frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);
    // initialise the static data if
    // accumulating stats or on a change
    // in frame type
    // (e.g. even to odd, different # car, etc)
    if ( intDataLen <= 200 )
    {
        //  byte count, data, 2 byte CRC, RS
        intNumSymbolsPerCar = (1 + intDataLen + 2 + intRSLen) * 4;
    }
    else
    {
        qint32 a = qint32(intDataLen / 3);
        qint32 b = qint32(intRSLen / 3);
        intNumSymbolsPerCar = (1 + a + 2 + b) * 12; //  byte count, data, 2 byte CRC, RS
    }

    if ( (bytLastDataFrameType != bytFrameType) ||
         (strExecutionDirectory.toLower().indexOf("debug") != -1)
       )
    {
        // clear the passing flags
        // (prior successful decodes of bytFrameType)
        bytPass = 0;
        bytLastDataFrameType = bytFrameType;
        bytCorrectedDataBlock1.clear(); // Re initialize the bytCorrectedData array
        bytCorrectedDataBlock2.clear(); // Re initialize the bytCorrectedData array
        bytCorrectedDataBlock3.clear(); // Re initialize the bytCorrectedData array
    }
    Demod1Car4FSK600bd(intMFSReadPtr, &intFilteredMixedSamples, intBaud, 1500, intNumSymbolsPerCar, &bytRawData, &intToneMags);
    if ( intDataLen > 200 )
        bytBlockData.resize(bytRawData.length() / 3 - 1);
    else
        bytBlockData.resize(bytRawData.length() - 1);

    if ( ((bytPass && 0x80) == 0) && (intDataLen <= 200) )
    {
        bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCorrectedDataBlock1, intDataLen, intRSLen, bytFrameType);
        if ( Ok ) bytPass = 0x80;
    }

    if ( ((bytPass && 0x80) == 0) && (intDataLen > 200) )
    {
        ByteArrayCopy2(&bytRawData, 0, &bytBlockData, 0, bytBlockData.length());
        bool Ok = CorrectRawDataWithRS(&bytBlockData, &bytCorrectedDataBlock1, qint32(intDataLen / 3), qint32(intRSLen / 3), bytFrameType);
        if ( Ok ) bytPass = (bytPass|0x80);
    }

    if ( ((bytPass && 0x40) == 0) && (intDataLen > 200) )
    {
        ByteArrayCopy2(&bytRawData, bytBlockData.length(), &bytBlockData, 0, bytBlockData.length());
        bool Ok  = CorrectRawDataWithRS(&bytBlockData, &bytCorrectedDataBlock2, qint32(intDataLen / 3), qint32(intRSLen / 3), bytFrameType);
        if ( Ok ) bytPass = (bytPass|0x40);
    }

    if ( ((bytPass && 0x20) == 0) && (intDataLen > 200) )
    {
        ByteArrayCopy2(&bytRawData, 2 * bytBlockData.length(), &bytBlockData, 0, bytBlockData.length());
        bool Ok = CorrectRawDataWithRS(&bytBlockData, &bytCorrectedDataBlock3, qint32(intDataLen / 3), qint32(intRSLen / 3), bytFrameType);
        if ( Ok ) bytPass = (bytPass|0x20);
    }

    if ( !intToneMags.isEmpty() ) objMain->Update4FSKConstellation(&intToneMags, intQuality);

    if ( (bytPass == 0x80) && (intDataLen <= 200) )
    {
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKFrameDataDecodes;
        bytData = &bytCorrectedDataBlock1;
        return true;
    }
    else if ( bytPass == 0xE0 )
    {
        bytData->resize(bytCorrectedDataBlock1.length() + bytCorrectedDataBlock2.length() + bytCorrectedDataBlock3.length() - 1);
        ByteArrayCopy2(&bytCorrectedDataBlock1, 0, bytData, 0, bytCorrectedDataBlock1.length());
        ByteArrayCopy2(&bytCorrectedDataBlock2, 0, bytData, bytCorrectedDataBlock1.length(), bytCorrectedDataBlock2.length());
        ByteArrayCopy2(&bytCorrectedDataBlock3, 0, bytData, bytCorrectedDataBlock1.length() + bytCorrectedDataBlock2.length(), bytCorrectedDataBlock3.length());
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFailedFSKFrameDataDecodes;
        return true;
    }
    return false;
} // Decode4FSK600bdData()


bool DemodulateDecode12K::Decode16FSKData(quint8 bytFrameType, QByteArray *bytData, qint32 *intQuality)
{
    // These static variables used to
    // accumuate correctly decoded carriers
    static quint8 bytPass = 0;
    static QVector<qint32> intToneMagHistory;
    static QVector<qint32> intSumCounts;
    static QByteArray bytCorrectedDataCar1;

    bool blnOdd       = false;
    bool blnSuccess   = false;
    quint16 intNumCar  = 0;
    quint32 intBaud    = 0;
    quint32 intDataLen = 0;
    quint32 intRSLen   = 0;
    quint32 intNumSymbolsPerCar = 0;
    QVector<qint32> intToneMags;
    QString strType = "";
    QString strMod  = "";
    QByteArray bytCarData;
    QByteArray bytJustData;
    QByteArray bytNoRS;
    QByteArray bytRawData;

    Q_UNUSED(blnSuccess)
    Q_UNUSED(bytData)

    // Look up the details by Frame type
    objFrameInfo->frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);

    // initialise the static data
    // if Accumulating Stats or on
    // a change in frame type
    // (e.g. even to odd, different # car, etc)

    //  byte count, data, 2 byte CRC, RS
    intNumSymbolsPerCar = (1 + intDataLen + 2 + intRSLen) * 2;
    if ( (bytLastDataFrameType != bytFrameType) ||
         (objMain->strExecutionDirectory.toLower().lastIndexOf("debug") != -1) )
    {
        bytPass = 0; // clear the passing flags (prior successful decodes of bytFrameType)
        bytLastDataFrameType = bytFrameType;
        // Re initialize the bytCorrectedData array
        bytCorrectedDataCar1.clear();
        // clear avg tones for analog Mem ARQ
        intToneMagHistory.resize(16 * intNumSymbolsPerCar - 1);
        intSumCounts.resize(3); // zero out the counts in the averages
    }

    if ( (bytPass & 0x80) == 0 )
    {
        Demod1Car16FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, 1500, intNumSymbolsPerCar, &bytRawData, &intToneMags);
        // VectorCpy2(&intToneMags, 0, &intToneMagsAllCar, 0, intToneMags.length());
        bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
        if ( Ok )
        {
            bytCorrectedDataCar1.resize(bytCarData.length() - 1);
            ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar1, 0, bytCarData.length());
            bytPass = (bytPass | 0x80);
            if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKFrameDataDecodes;
        }
        else if ( intSumCounts.at(0) == 0 )
        {
            for ( qint32 m = 0; m < (intToneMags.length() - 1); ++m )
            {
                intToneMagHistory[m] = intToneMags.at(m);
            } // Next m
            intSumCounts[0] = 1;
        }
        else
        {
            // average the tone mags
            for (qint32 m = 0; m < (intToneMags.length() - 1); ++m )
            {
                qint32 x = (intToneMagHistory.at(m) + intToneMags.at(m)) / 2;
                intToneMagHistory[m] = x;
                intToneMags[m] = intToneMagHistory.at(m);
            } // Next m
            Decode1Car16FSKFromTones(&bytRawData, &intToneMags);
            bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
            if ( Ok )
            {
                bytCorrectedDataCar1.resize(bytCarData.length() - 1);
                ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar1, 0, bytCarData.length());
                bytPass = (bytPass | 0x80);
                if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKSummationDecodes;
                intSumCounts[0] = 0;
            }
        }
    }

    if ( !intToneMags.isEmpty() )
        objMain->Update16FSKConstellation(&intToneMags, intQuality);

    if ( bytPass == 0x80 )
    {
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKFrameDataDecodes;
        bytData = &bytCorrectedDataCar1;
        return true;
    }
    else
    {
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFailedFSKFrameDataDecodes;
    }
    // End Select
    return false;
} // Decode16FSKData()

bool DemodulateDecode12K::Decode16QAMData(quint8 bytFrameType, QVector<qint16> *intPhases, QVector<qint16> *intMags, QVector<qint16> intRef, QByteArray *bytData)
{
    // These static variables are used to
    // accumuate correctly decoded carriers
    // after repeats on multicarrier modes.
    static quint8 bytPass = 0;

    static QVector<QVector<qint16> > intCarPhaseAvg(7); // array to accumulate phases for averaging (Memory ARQ)
    static QVector<QVector<qint16> > intCarMagAvg(7);   // array to accumulate magMods for averaging (Memory ARQ)
    static QVector<qint32> intSumCounts(7);

    static QByteArray bytCorrectedData0; // an array of correctedData one for carrier 0
    static QByteArray bytCorrectedData1; // an array of correctedData one for carrier 1
    static QByteArray bytCorrectedData2; // an array of correctedData one for carrier 2
    static QByteArray bytCorrectedData3; // an array of correctedData one for carrier 3
    static QByteArray bytCorrectedData4; // an array of correctedData one for carrier 4
    static QByteArray bytCorrectedData5; // an array of correctedData one for carrier Mod
    static QByteArray bytCorrectedData6; // an array of correctedData one for carrier 6
    static QByteArray bytCorrectedData7; // an array of correctedData one for carrier 7

    // these vars are all volitile in this function
    // and get initialized each time on entry
    bool blnOdd = false;
    bool thisDecode16QAMData = false; // Preset for failure
    quint8 bytCarMask = 0x80;
    quint8 bytQualThresh = 100;
    quint16 intNumCar  = 0;
    quint32 intPSKMode = 0;
    quint32 intBaud    = 0;
    quint32 intDataLen = 0;
    quint32 intRSLen   = 0;
    quint32 intNumSymbolsPerCar = 0;
    QString strType = "";
    QString strMod = "";
    QVector<qint16> intCarPhases;
    QVector<qint16> intCarMags;
    QByteArray bytNoRS;
    QByteArray bytCorrectedCarData;
    QByteArray bytRawData;
    // to prevent warnings in release build
    Q_UNUSED(bytQualThresh)
    Q_UNUSED(intPSKMode)
    Q_UNUSED(intNumSymbolsPerCar)

    // Look up the details by Frame type
    objFrameInfo->frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);

    if ( strMod != "16QAM" ) return thisDecode16QAMData;

    intPSKMode = 8; // Use 8 PSK clusters for 16QAM
    intNumSymbolsPerCar = int(intPhases->length() / intNumCar);
    objRS8->setMaxCorrections(int(intRSLen / 2));
    bytRawData.resize(int(int(intPhases->length() / 2) / intNumCar) - 1); // for 16QAM modes
    intCarPhases.resize(int(intPhases->length() / intNumCar) - 1);
    intCarMags.resize(int(intMags->length() / intNumCar) - 1);
    bytNoRS.resize(1 + intDataLen + 2 - 1); // 1 byte byte Count, Data, 2 byte CRC
    // initialise the static data if Accumulating Stats
    // or on a change in frame type (e.g. even to odd,
    // different # car, etc)
    if ( (bytLastDataFrameType != bytFrameType) ||
         (strExecutionDirectory.toLower().lastIndexOf("debug") != -1 )
       )
    {
        bytPass = 0; // clear the passing flags (prior successful decodes of bytFrameType)
        bytLastDataFrameType = bytFrameType;
        bytCorrectedData0.clear(); // Re initialize the bytCorrectedData array for carrier 0
        bytCorrectedData1.clear(); // Re initialize the bytCorrectedData array for carrier 1
        bytCorrectedData2.clear(); // Re initialize the bytCorrectedData array for carrier 2
        bytCorrectedData3.clear(); // Re initialize the bytCorrectedData array for carrier 3
        bytCorrectedData4.clear(); // Re initialize the bytCorrectedData array for carrier 4
        bytCorrectedData5.clear(); // Re initialize the bytCorrectedData array for carrier 5
        bytCorrectedData6.clear(); // Re initialize the bytCorrectedData array for carrier 6
        bytCorrectedData7.clear(); // Re initialize the bytCorrectedData array for carrier 7
        // 7x? array
        // clear avg phase of multiple frames used for
        // analog Mem ARQ
        for( int outer = 0; outer <= 7; ++outer )
        {
            intCarPhaseAvg[outer].resize(intCarPhases.length() - 1);
        }
        // 7x? array
        // clear avg Mags of mulitple frames used for
        // Analog Mem ARQ (Added Rev 0.3.5.1)
        for( int outer = 0; outer <= 7; ++outer )
        {
            intCarMagAvg[outer].resize(intCarPhases.length() - 1);
        }
        intSumCounts.resize(7);                              // zero out the counts in the averages
    }

    // Across all carriers
    for ( quint8 i = 0; i < (quint8)(intNumCar - 1); ++i )
    {
        if ( (bytCarMask & bytPass) == 0 )
        {
            //  decode only those carriers
            //  not yet correctly decoded.
            quint32 l = i * intCarPhases.length();
            quint32 m = i * intCarMags.length();
            quint32 n = intCarPhases.length();
            quint32 p = intCarMags.length();
            VectorCpy2(&intCarPhases, quint32(l), &intCarPhases, quint32(0), quint32(n));
            VectorCpy2(&intCarMags, m, &intCarMags, quint32(0), p);
            bool Ok = Decode1Car16QAM(intCarPhases, intCarMags, intRef.at(i), bytRawData, &bytCorrectedCarData, intRSLen, bytFrameType);
            if ( Ok )
            {
                quint32 x = bytCorrectedCarData.length();
                quint32 y = x - 1;
                switch ( i )
                {
                case 0: bytCorrectedData0.resize(y); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData0, x); break;
                case 1: bytCorrectedData1.resize(y); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData1, x); break;
                case 2: bytCorrectedData2.resize(y); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData2, x); break;
                case 3: bytCorrectedData3.resize(y); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData3, x); break;
                case 4: bytCorrectedData4.resize(y); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData4, x); break;
                case 5: bytCorrectedData5.resize(y); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData5, x); break;
                case 6: bytCorrectedData6.resize(y); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData6, x); break;
                case 7: bytCorrectedData7.resize(y); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData7, x); break;
                }

                bytPass = (bytCarMask | bytPass);
                intSumCounts[i] = 0;
            }
            else
            {
                // failure in RS correction so
                // just add to average and try to decode again
                if ( intSumCounts.at(i) == 0 )
                {
                    // initialize Sum counts Phase average and Mag Average
                    for ( qint32 m = 0; m < (intCarPhases.length() - 1); ++m )
                    {
                        intCarPhaseAvg[i][m] = intCarPhases.at(m);
                        intCarMagAvg[i][m] = intCarMags.at(m);
                    } // Next m
                    intSumCounts[i] = 1;
                }
            }
        }
        else
        {
            for ( qint32 k = 0; k < (intCarPhases.length() - 1); ++k )
            {
                quint16 p1 = intCarPhaseAvg[i][k];
                quint16 p2 = intCarPhases.at(k);
                quint16 p3 = intCarMagAvg[i][k];
                quint16 p4 = intCarMags.at(k);
                intCarPhaseAvg[i][k] = WeightedAngleAvg(p1, p2, p3, p4); // Modified 0.3.5.1
                intCarPhases[k] = intCarPhaseAvg[i][k];
                // Use simple weighted average for Mags
                intCarMagAvg[i][k] = qint16(intCarMagAvg[i][k] * intSumCounts.at(i) + intCarMags.at(k)) / (intSumCounts.at(i) + 1);
                intCarMags[k] = intCarMagAvg[i][k];
            } //  Next k
            ++intSumCounts[i];
            // now try to decode based on the
            // WeightedAveragePhases
            if ( Decode1Car16QAM(intCarPhases, intCarMags, intRef.at(i), bytRawData, &bytCorrectedCarData, intRSLen, bytFrameType) )
            {
                quint32 w = bytCorrectedCarData.length();
                quint32 x = bytCorrectedCarData.length() - 1;
                switch ( i )
                {
                case 0: bytCorrectedData0.resize(x); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData0, w); break;
                case 1: bytCorrectedData1.resize(x); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData1, w); break;
                case 2: bytCorrectedData2.resize(x); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData2, w); break;
                case 3: bytCorrectedData3.resize(x); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData3, w); break;
                case 4: bytCorrectedData4.resize(x); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData4, w); break;
                case 5: bytCorrectedData5.resize(x); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData5, w); break;
                case 6: bytCorrectedData6.resize(x); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData6, w); break;
                case 7: bytCorrectedData7.resize(x); ByteArrayCopy1(&bytCorrectedCarData, &bytCorrectedData7, w); break;
                }
                bytPass = (bytCarMask | bytPass);
                if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodQAMSummationDecodes;
            }
        }
    bytCarMask = bytCarMask >> 1; // Shift the mask
    } // Next i '

    switch ( intNumCar )
    {
    case 1:  thisDecode16QAMData = (bytPass = 0x80); break;
    case 2:  thisDecode16QAMData = (bytPass = 0xC0); break;
    }

    if ( thisDecode16QAMData )
    {
        qint32 intPtr = 0;
        bytData->clear();
        for ( quint8 i = 0; i < (quint8)(intNumCar - 1); ++i )
        {
            quint8 x = (quint8)(intPtr + (bytCorrectedData0.length() - 1));
            switch ( i )
            {
            case 0: bytData->reserve(x); ByteArrayCopy2(&bytCorrectedData0, 0, bytData, intPtr, bytCorrectedData0.length()); intPtr += bytCorrectedData0.length(); break;
            case 1: bytData->reserve(x); ByteArrayCopy2(&bytCorrectedData1, 0, bytData, intPtr, bytCorrectedData1.length()); intPtr += bytCorrectedData1.length(); break;
            case 2: bytData->reserve(x); ByteArrayCopy2(&bytCorrectedData2, 0, bytData, intPtr, bytCorrectedData2.length()); intPtr += bytCorrectedData2.length(); break;
            case 3: bytData->reserve(x); ByteArrayCopy2(&bytCorrectedData3, 0, bytData, intPtr, bytCorrectedData3.length()); intPtr += bytCorrectedData3.length(); break;
            case 4: bytData->reserve(x); ByteArrayCopy2(&bytCorrectedData4, 0, bytData, intPtr, bytCorrectedData4.length()); intPtr += bytCorrectedData4.length(); break;
            case 5: bytData->reserve(x); ByteArrayCopy2(&bytCorrectedData5, 0, bytData, intPtr, bytCorrectedData5.length()); intPtr += bytCorrectedData5.length(); break;
            case 6: bytData->reserve(x); ByteArrayCopy2(&bytCorrectedData6, 0, bytData, intPtr, bytCorrectedData6.length()); intPtr += bytCorrectedData6.length(); break;
            case 7: bytData->reserve(x); ByteArrayCopy2(&bytCorrectedData7, 0, bytData, intPtr, bytCorrectedData7.length()); break;
            }
        } // Next i
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodQAMFrameDataDecodes;
    }
    else
    {
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFailedQAMFrameDataDecodes;
    }
    return thisDecode16QAMData;
}  // Decode16QAMData()



bool DemodulateDecode12K::Decode1Car16FSKFromTones(QByteArray *bytData, QVector<qint32> *intToneMags)
{
    quint8 bytSym = 0;
    qint32 intMaxMag = 0;

    bytData->resize(int(intToneMags->length() / 32) - 1);

    for ( qint32 i = 0; i < (bytData->length() - 1); ++i )
    {
        quint32 x = 0;
        quint32 y = 0;
         // For each data byte
         for ( quint8 j = 0; j <= 1; ++j )
         {
             // for each 16FSK symbol (4 bits) in a byte
             intMaxMag = 0;
             for ( quint8 k = 0; k <= 15; ++k )
             {
                 x = i * 32 + 16 * j + k;
                 if ( intToneMags->at(x) > intMaxMag )
                 {
                     y = i * 32 + 16 * j + k;
                     intMaxMag = intToneMags->at(y);
                     bytSym = k;
                 }
             } // Next k
             quint32 z = bytData->at(i) << 4;
             bytData->insert(i, QString("%1").arg(z,16) + bytSym);
         } // Next j
    } // Next i
    return true;
}  //  Decode1Car16FSKFromTones

bool DemodulateDecode12K::Decode1Car16QAM(QVector<qint16> intCarPhases, QVector<qint16> intCarMags, qint16 intRef, QByteArray bytRawData, QByteArray *bytCorrectCarData, qint32 intRSLen, quint8 bytFrameType)
{
    qint32 intPhasePtr = 0;
    qint32 intData     = 0;

    QByteArray bytNoRS;
    bytNoRS.reserve(bytRawData.length() - intRSLen - 1);
    qint16 intCarMagThreshold = 0;
    // intMagPeak, intAvgMin, intAvgMax, intCountMin, intCountMax As Int32
    double dblAlpha = 0.1; // this determins how quickly the rolling average dblTrackingThreshold responds.
    // dblAlpha value of .1 seeps to work well...needs to be tested on fading channel (e.g. Multipath)
    double dblTrackingThreshold = 0;

    // Determine threshold using filtered tracking of
    // refernce phase amplitude (should be full amplitude value)
    // On WGN this appears to improve decoding threshold about
    // 1 dB 9/3/2016
    dblTrackingThreshold = 0.75 * intRef; // initialize the threshold based on the first ref symbol
    intCarMagThreshold = int(dblTrackingThreshold);

    for ( qint32 j = 0; j < (bytRawData.length() - 1); ++j )
    {
        // All 16QAM uses 8PSK phase decoding (3 bits) and 2 magnitudes (1 bit)
        for ( quint8 k = 0; k <= 1; ++k )
        {
            intPhasePtr = (j * 2) + k;
            if ( (intCarPhases.at(intPhasePtr) < 393) &&
                 (intCarPhases.at(intPhasePtr) > -393)
               )
            {
                intData = 0;
            }
            else if ( (intCarPhases.at(intPhasePtr) >= 393) &&
                      (intCarPhases.at(intPhasePtr) < 1179)
                    )
            {
                intData = 1;
            }
            else if ( (intCarPhases.at(intPhasePtr) >= 1179) &&
                      (intCarPhases.at(intPhasePtr) < 1965)
                    )
            {
                intData = 2;
            }
            else if ( (intCarPhases.at(intPhasePtr) >= 1965) &&
                      (intCarPhases.at(intPhasePtr) < 2751)
                    )
            {
                intData = 3;
            }
            else if ( (intCarPhases.at(intPhasePtr) >= 2751) ||
                      (intCarPhases.at(intPhasePtr) < -2751)
                    )
            {
                intData = 4;
            }
            else if ( (intCarPhases.at(intPhasePtr) >= -2751) &&
                      (intCarPhases.at(intPhasePtr) < -1965)
                    )
            {
                intData = 5;
            }
            else if ( (intCarPhases.at(intPhasePtr) >= -1965) &&
                      (intCarPhases.at(intPhasePtr) <= -1179)
                    )
            {
                intData = 6;
            }
            else
            {
                intData = 7;
            }

            // The following generates a low pass
            // (rolling average) threshold to allow
            // decoding the amplitude bit
            // dblAlpha values near .1 seem to work
            // well at least for WGN channels 9/3/2016
            if ( intCarMags.at(intPhasePtr) < intCarMagThreshold )
            {
                // if below the current threshold
                intData += 8; // add 8 to "inner circle" symbols.
                dblTrackingThreshold = ((1 - dblAlpha) * dblTrackingThreshold) + (dblAlpha * 1.5 * intCarMags.at(intPhasePtr));
            }
            else
            {
                // above the current threshold
                dblTrackingThreshold = ((1 - dblAlpha) * dblTrackingThreshold) + (dblAlpha * 0.75 * intCarMags.at(intPhasePtr));
            }

            intCarMagThreshold = int(dblTrackingThreshold);

             if ( k == 0 )
                 bytRawData[j] = intData << 4;
             else
                 bytRawData[j] = bytRawData.at(j) + intData;
        }  // Next k
    } // Next j

    ByteArrayCopy2(&bytRawData, 0, &bytNoRS, 0, bytNoRS.length());

    // Check to see if correct without RS parity
    bool Ok = CheckCRC16FrameType(&bytNoRS, bytFrameType);
    if ( Ok )
    {
         // No RS correction needed
         bytCorrectCarData->resize(bytNoRS.at(0) - 1);
         ByteArrayCopy2(&bytNoRS, 1, bytCorrectCarData, 0, bytNoRS.at(0));
         return true;
    }
    else
    {
        // Try correction with RS Parity
        QByteArray bytFailedNoRS;
        bytFailedNoRS.reserve(bytNoRS.length() - 1);
        ByteArrayCopy1(&bytNoRS, &bytFailedNoRS, bytNoRS.length());
        objRS8->setMaxCorrections(int(intRSLen / 2));
        bytNoRS = objRS8->RSDecode(bytRawData);
        bool yes = CheckCRC16FrameType(&bytNoRS, bytFrameType);
        if ( yes )
        {
            // No RS correction needed
            bytCorrectCarData->resize(bytNoRS.at(0) - 1);
            ByteArrayCopy2(&bytNoRS, 1, bytCorrectCarData, 0, bytNoRS.at(0));
            return true;
        }
        else
        {
            return false;
        }
    }
}  // Decode1Car16QAM()



bool DemodulateDecode12K::Decode1Car4FSKFromTones(QByteArray *bytData, QVector<qint32> *intToneMags)
{
    quint8 bytSym = 0;
    qint32 intIndex = 0;

    bytData->reserve(int(intToneMags->length() / 16) - 1);

    for ( qint32 i = 0; i < (bytData->length() - 1); ++i )
    {
        // For each data byte
        intIndex = 16 * i;
        for ( quint8 j = 0; j <= 3; ++j )
        {
             // for each 4FSK symbol (2 bits) in a byte
             if ( (intToneMags->at(intIndex) > intToneMags->at(intIndex + 1)) &&
                  (intToneMags->at(intIndex) > intToneMags->at(intIndex + 2)) &&
                  (intToneMags->at(intIndex) > intToneMags->at(intIndex + 3))
                )
             {
                 bytSym = 0;
             }
             else if ( (intToneMags->at(intIndex + 1) > intToneMags->at(intIndex)) &&
                       (intToneMags->at(intIndex + 1) > intToneMags->at(intIndex + 2)) &&
                       (intToneMags->at(intIndex + 1) > intToneMags->at(intIndex + 3))
                     )
             {
                 bytSym = 1;
             }
             else if ( (intToneMags->at(intIndex + 2) > intToneMags->at(intIndex)) &&
                       (intToneMags->at(intIndex + 2) > intToneMags->at(intIndex + 1)) &&
                       (intToneMags->at(intIndex + 2) > intToneMags->at(intIndex + 3))
                     )
             {
                 bytSym = 2;
             }
             else
             {
                 bytSym = 3;
             }
             bytData->replace(i,(bytData->at(i) << 2) + bytSym);
             intIndex += 4;
        } // Next j
    } // Next i
    return true;
}  //  Decode1Car4FSKFromTones()


bool DemodulateDecode12K::Decode1Car8FSKFromTones(QByteArray *bytData, QVector<qint32> *intToneMags)
{
    quint8 bytSym = 0;
    qint32 intThreeBytes = 0;
    qint32 intMaxMag = 0;

    bytData->resize((3 * int(intToneMags->length() / 64)) - 1);

    for ( qint32 i = 0; i < (int(bytData->length() / 3) - 1); ++i )
    {
         // For each group of 3 bytes data byte
         intThreeBytes = 0;
         for ( quint8 j = 0; j <= 7; ++j )
         {
             // for each group of 8 symbols (24 bits)
             intMaxMag = 0;
             for ( quint8 k = 0; k <= 7; ++k )
             {
                 // for each of 8 possible tones per symbol
                 if (intToneMags->at((i * 64) + 8 * j + k) > intMaxMag )
                 {
                     intMaxMag = intToneMags->at((i * 64) + 8 * j + k);
                     bytSym = k;
                 }
             } // Next k
             intThreeBytes = (intThreeBytes << 3) + bytSym;
         }  // Next j
         bytData->replace(3 * i,    (intThreeBytes & 0xFF0000) >> 16);
         bytData->replace(3 * i + 1,(intThreeBytes & 0xFF00) >> 8);
         bytData->replace(3 * i + 2,(intThreeBytes & 0xFF));
     }  // Next i
     return true;
}  //  Decode1Car8FSKFromTones

bool DemodulateDecode12K::Decode1CarPSK(QVector<qint16> intCarPhases, qint32 intPSKMode, QByteArray bytRawData, QByteArray *bytCorrectCarData, quint32 intRSLen, quint8 bytFrameType)
{
    qint32 intPhasePtr = 0;
    qint32 int24Bits   = 0;
    qint32 intData     = 0;

    QByteArray bytNoRS;
    bytNoRS.reserve((bytRawData.length() - intRSLen) - 1);

    for ( qint32 j = 0; j < (bytRawData.length() - 1); ++j )
    {
        switch ( intPSKMode )
        {
        case 4:
            // process 4 sequential phases per byte
            // (2 bits per phase)
            for ( quint8 k = 0; k <= 3; ++k )
            {
                if ( k == 0 )
                    bytRawData[j] = 0;
                else
                    bytRawData[j] = bytRawData.at(j) << 2;


                intPhasePtr = 4 * j + k;
                if ( (intCarPhases.at(intPhasePtr) < 786) &&
                     (intCarPhases.at(intPhasePtr) > -786)
                   )
                {
                    // do nothing
                }
                else if ( (intCarPhases.at(intPhasePtr) >= 786) &&
                          (intCarPhases.at(intPhasePtr) < 2356)
                        )
                {
                    quint8 newdata = bytRawData.at(j) + 1;
                    bytRawData.replace(bytRawData.at(j), newdata);
                }
                else if ( (intCarPhases.at(intPhasePtr) >= 2356) ||
                          (intCarPhases.at(intPhasePtr) <= -2356)
                        )
                {
                    quint8 newdata = bytRawData.at(j) + 2;
                    bytRawData[j] = newdata;
                }
                else
                {
                    quint8 newdata = bytRawData.at(j) + 3;
                    bytRawData[j] = newdata;
                }
            } // Next k

        case 8:
            // Process 8 sequential phases (3 bits per phase)
            // for 24 bits or 3 bytes
            // Status verified on 1 Carrier 8PSK with no
            // RS needed for High S/N
            if ((j + 1) % 3 == 0 )
            {
                // we have 3 unprocessed bytes
                // (24 bits) so process all 8 phases
                for ( qint32 k = 0; k <= 7; ++k )
                {
                    intPhasePtr = (int((j - 2) / 3) * 8 + k);
                    if ( (intCarPhases.at(intPhasePtr) < 393) &&
                         (intCarPhases.at(intPhasePtr) > -393)
                       )
                    {
                        intData = 0;
                    }
                    else if ( (intCarPhases.at(intPhasePtr) >= 393) &&
                              (intCarPhases.at(intPhasePtr) < 1179)
                            )
                    {
                        intData = 1;
                    }
                    else if ( (intCarPhases.at(intPhasePtr) >= 1179) &&
                              (intCarPhases.at(intPhasePtr) < 1965)
                            )
                    {
                        intData = 2;
                    }
                    else if ( (intCarPhases.at(intPhasePtr) >= 1965) &&
                              (intCarPhases.at(intPhasePtr) < 2751)
                            )
                    {
                        intData = 3;
                    }
                    else if ( (intCarPhases.at(intPhasePtr) >= 2751) ||
                              (intCarPhases.at(intPhasePtr) < -2751)
                            )
                    {
                        intData = 4;
                    }
                    else if ( (intCarPhases.at(intPhasePtr) >= -2751) &&
                              (intCarPhases.at(intPhasePtr) < -1965)
                            )
                    {
                        intData = 5;
                    }
                    else if ( (intCarPhases.at(intPhasePtr) >= -1965) &&
                              (intCarPhases.at(intPhasePtr) <= -1179)
                            )
                    {
                        intData = 6;
                    }
                    else
                    {
                        intData = 7;
                    }

                    if ( k == 0 )
                        int24Bits = intData << 21;
                    else
                        int24Bits += (intData << (21 - 3 * k));
                }
                bytRawData[j - 2] = (int24Bits & 0xFF0000) >> 16;
                bytRawData[j - 1] = (int24Bits & 0xFF00) >> 8;
                bytRawData[j] = (int24Bits & 0xFF);
            }
        }
    } // Next j

    ByteArrayCopy2(&bytRawData, 0, &bytNoRS, 0, bytNoRS.length());
    // Check to see if correct without RS parity
    bool Ok = CheckCRC16FrameType(&bytNoRS, bytFrameType);
    if ( Ok )
    {
        // No RS correction needed
        bytCorrectCarData->resize(bytNoRS.at(0) - 1);
        ByteArrayCopy2(&bytNoRS, 1, bytCorrectCarData, 0, bytNoRS.at(0));
        return true;
    }
    else
    {
        // Try correction with RS Parity
        QByteArray bytFailedNoRS;
        bytFailedNoRS.resize(bytNoRS.length() - 1);
        ByteArrayCopy1(&bytNoRS, &bytFailedNoRS, bytNoRS.length());
        objRS8->setMaxCorrections(int(intRSLen / 2));
        bytNoRS = objRS8->RSDecode(bytRawData);
        bool Ok = CheckCRC16FrameType(&bytNoRS, bytFrameType);
        if ( Ok )
        {
            // No RS correction needed
            bytCorrectCarData->resize(bytNoRS.at(0) - 1);
            ByteArrayCopy2(&bytNoRS, 1, bytCorrectCarData, 0, bytNoRS.at(0));
            return true;
        }
        else
        {
            return false;
        }
    }
}  // Decode1CarPSK()



bool DemodulateDecode12K::Decode8FSKData(quint8 bytFrameType, QByteArray *bytData, qint32 *intQuality)
{
    Q_UNUSED(bytData)
    // These static variables used to
    // accumuate correctly decoded carriers
    // on simultaneous multicarrier 4FSK modes.
    static quint8 bytPass = 0;
    static QVector<qint32> intToneMagHistory;
    static QVector<qint32> intSumCounts;
    static QByteArray bytCorrectedDataCar1;

    bool blnOdd = false;
    bool blnSuccess = false;
    quint16  intNumCar  = 0;
    quint32  intBaud    = 0;
    quint32  intDataLen = 0;
    quint32  intRSLen   = 0;
    quint32  intNumSymbolsPerCar = 0;
    QVector<qint32> intToneMags;
    QByteArray bytCarData;
    QByteArray bytJustData;
    QByteArray bytNoRS;
    QByteArray bytRawData;
    QString strType = "";
    QString strMod = "";

    bytCarData.clear();
    bytJustData.clear();
    bytRawData.clear();

    // to suppress warnings in release build
    Q_UNUSED(blnSuccess)

    // Look up the details by Frame type
    objFrameInfo->frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);
    // initialise the static data if
    // accumulating Stats or on a change
    // in frame type (e.g. even to odd,
    // different # car, etc)
    //
    // byte count, data, 2 byte CRC, RS (3 bits/symbol
    intNumSymbolsPerCar = (8 * (1 + intDataLen + 2 + intRSLen)) / 3;
    if ( (bytLastDataFrameType != bytFrameType) ||
         (objMain->strExecutionDirectory.toLower().lastIndexOf("debug") != -1)
       )
    {
        // clear the passing flags (prior
        // successful decodes of bytFrameType)
        bytPass = 0;
        bytLastDataFrameType = bytFrameType;
        // Re initialize the bytCorrectedData array
        bytCorrectedDataCar1.clear();
        // clear avg tones for analog Mem ARQ
        intToneMagHistory.resize(8 * intNumSymbolsPerCar - 1);
        intSumCounts.clear(); // zero out the counts in the averages
    }

    // NOTE: Car frequences go from high to low to
    //       accomodate sideband reversal after mixing
    if ( (bytPass && 0x80) == 0 )
    {
        Demod1Car8FSK(intMFSReadPtr, &intFilteredMixedSamples, intBaud, 1500, intNumSymbolsPerCar, &bytRawData, &intToneMags);
        bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
        if ( Ok )
        {
            bytCorrectedDataCar1.resize(bytCarData.length() - 1);
            ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar1, 0, bytCarData.length());
            bytPass = (bytPass | 0x80);
            if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKFrameDataDecodes;
        }
        else if ( intSumCounts.at(0) == 0 )
        {
            VectorCopy2(&intToneMags, 0, &intToneMagHistory, 0, intToneMags.length());
            intSumCounts[0] = 1;
        }
        else
        {
            // average the tone mags
            for ( qint32 m = 0; m < (intToneMags.length() - 1); ++m )
            {
                intToneMagHistory[m] = int((intToneMagHistory.at(m) + intToneMags.at(m)) / 2);
                intToneMags[m] = intToneMagHistory.at(m);
            } // Next m
            Decode1Car8FSKFromTones(&bytRawData, &intToneMags);
            bool Ok = CorrectRawDataWithRS(&bytRawData, &bytCarData, intDataLen, intRSLen, bytFrameType);
            if ( Ok )
            {
                bytCorrectedDataCar1.resize(bytCarData.length() - 1);
                ByteArrayCopy2(&bytCarData, 0, &bytCorrectedDataCar1, 0, bytCarData.length());
                bytPass = (bytPass | 0x80);
                if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKSummationDecodes;
                intSumCounts[0] = 0;
            }
        }
    }

    if ( !intToneMags.isEmpty() )
    {
        objMain->Update8FSKConstellation(&intToneMags, intQuality);
    }
    if ( bytPass == 0x80 )
    {
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intGoodFSKFrameDataDecodes;
        bytData = &bytCorrectedDataCar1;
        return true;
    }
    else
    {
        if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFailedFSKFrameDataDecodes;
    }
    return false;
} // Decode8FSKData()



bool DemodulateDecode12K::DecodeFrame(quint8 intFrameType, QByteArray *bytData)
{
    bool blnDecodeOK      = false;
    qint32 intBW          = 0;
    qint32 intTiming      = 0;
    qint32 intRcvdQuality = 0;
    qint32 intConstellationQuality = 0;
    QVector<qint16> intPhase;
    QVector<qint16> intMag;
    QVector<qint16> intRef; // used for QAM reference
    QByteArray bytFrameData;
    QString strIDCallSign = "";
    QString strGridSQ     = "";
    QString strCallerCallsign = "";
    QString strTargetCallsign = "";

    Q_UNUSED(bytFrameData)       // to suppress warnings in release build

    bytData->clear();
    objMain->strRcvFrameTag = "";
    stcStatus status;
    status.CtlName = "lblRxFrame";

    // DataACK/NAK and short control frames
    if ( (
          (intFrameType > 0) &&
          (intFrameType < 0x1F)
         ) || (intFrameType > 0xE0)
       )
    {
        // DataACK/NAK
        blnDecodeOK = DecodeACKNAK(intFrameType, &intRcvdQuality);
        status.Message = objFrameInfo->getName(intFrameType) + objMain->strRcvFrameTag;
    }
    else if (objFrameInfo->isShortControlFrame(intFrameType) )
    {
        // Short Control Frames
        blnDecodeOK = true;
        status.Message = objFrameInfo->getName(intFrameType);
    }

    // Special Frames
    switch ( intFrameType )
    {
    case 0x39:
        // Connect ACKs with Timing
        blnDecodeOK = DemodDecode4FSKConACK(intFrameType, &intTiming);
        if ( blnDecodeOK )
        {
            bytData->clear();
            bytData->replace(0,quint8(intTiming / 10));
            status.Message = objFrameInfo->getName(intFrameType) + objMain->strRcvFrameTag;
        }
        break;

    case 0x3A:
        // Connect ACKs with Timing
        blnDecodeOK = DemodDecode4FSKConACK(intFrameType, &intTiming);
        if ( blnDecodeOK )
        {
            bytData->clear();
            bytData->replace(0,quint8(intTiming / 10));
            status.Message = objFrameInfo->getName(intFrameType) + objMain->strRcvFrameTag;
        }
        break;

    case 0x3B:
        // Connect ACKs with Timing
        blnDecodeOK = DemodDecode4FSKConACK(intFrameType, &intTiming);
        if ( blnDecodeOK )
        {
            bytData->clear();
            quint8 byt = quint8(intTiming / 10);
            bytData->replace(0,byt);
            status.Message = objFrameInfo->getName(intFrameType) + objMain->strRcvFrameTag;
        }
        break;

    case 0x3C:
        // Connect ACKs with Timing
        blnDecodeOK = DemodDecode4FSKConACK(intFrameType, &intTiming);
        if ( blnDecodeOK )
        {
            bytData->clear();
            quint8 byt = quint8(intTiming / 10);
            bytData->replace(0,byt);
            status.Message = objFrameInfo->getName(intFrameType) + objMain->strRcvFrameTag;
        }
        break;

    case 0x30:
     {
        // ID Frame
        blnDecodeOK = DemodDecode4FSKID(0x30, &strIDCallSign, &strGridSQ);
        QString str = "ID:" + strIDCallSign + " " + strGridSQ + ": ";
        QByteArray ba = GetBytes(str);
        bytData = &ba;
        str = objFrameInfo->getName(intFrameType) + objMain->strRcvFrameTag;
        status.Message = str;
     }
        break;

    case 0x31:
     {
        // Connect Request
        blnDecodeOK = DemodDecode4FSKConReq(intFrameType, &intBW, &strCallerCallsign, &strTargetCallsign);
        status.Message = objFrameInfo->getName(intFrameType) + " " + strCallerCallsign + ">" + strTargetCallsign;
        QString str = strCallerCallsign + " " + strTargetCallsign;
        QByteArray ba = GetBytes(str);
        bytData = &ba;
        if ( blnDecodeOK ) bytData = &ba;
     }
     break;

    case 0x32:
     {
        // Connect Request
        blnDecodeOK = DemodDecode4FSKConReq(intFrameType, &intBW, &strCallerCallsign, &strTargetCallsign);
        status.Message = objFrameInfo->getName(intFrameType) + " " + strCallerCallsign + ">" + strTargetCallsign;
        QString str = strCallerCallsign;
        str += " ";
        str += strTargetCallsign;
        QByteArray ba = GetBytes(str);
        bytData = &ba;
        if ( blnDecodeOK ) bytData = &ba;
     }
     break;

    case 0x33:
     {
        // Connect Request
        blnDecodeOK = DemodDecode4FSKConReq(intFrameType, &intBW, &strCallerCallsign, &strTargetCallsign);
        status.Message = objFrameInfo->getName(intFrameType) + " " + strCallerCallsign + ">" + strTargetCallsign;
        QString str = strCallerCallsign;
        str += " ";
        str += strTargetCallsign;
        if ( blnDecodeOK )
        {
            QByteArray ba = GetBytes(str);
            bytData = &ba;
        }
    }
    break;

    case 0x34:
     {
        // Connect Request
        blnDecodeOK = DemodDecode4FSKConReq(intFrameType, &intBW, &strCallerCallsign, &strTargetCallsign);
        status.Message = objFrameInfo->getName(intFrameType) + " " + strCallerCallsign + ">" + strTargetCallsign;
        if ( blnDecodeOK )
        {
            QByteArray ba = GetBytes(strCallerCallsign + " " + strTargetCallsign);
            bytData = &ba;
        }
     }
     break;

    case 0x35:
     {
        // Connect Request
        blnDecodeOK = DemodDecode4FSKConReq(intFrameType, &intBW, &strCallerCallsign, &strTargetCallsign);
        status.Message = objFrameInfo->getName(intFrameType) + " " + strCallerCallsign + ">" + strTargetCallsign;
        if ( blnDecodeOK )
        {
            QByteArray ba = GetBytes(strCallerCallsign + " " + strTargetCallsign);
            bytData = &ba;
        }
     }
     break;

    case 0x36:
     {
        // Connect Request
        blnDecodeOK = DemodDecode4FSKConReq(intFrameType, &intBW, &strCallerCallsign, &strTargetCallsign);
        status.Message = objFrameInfo->getName(intFrameType) + " " + strCallerCallsign + ">" + strTargetCallsign;
        if ( blnDecodeOK )
        {
            QByteArray ba = GetBytes(strCallerCallsign + " " + strTargetCallsign);
            bytData = &ba;
        }
     }
     break;

    case 0x37:
     {
        // Connect Request
        blnDecodeOK = DemodDecode4FSKConReq(intFrameType, &intBW, &strCallerCallsign, &strTargetCallsign);
        status.Message = objFrameInfo->getName(intFrameType) + " " + strCallerCallsign + ">" + strTargetCallsign;
        if ( blnDecodeOK )
        {
            QByteArray ba = GetBytes(strCallerCallsign + " " + strTargetCallsign);
            bytData = &ba;
        }
     }
     break;

    case 0x38:
     {
        // Connect Request
        blnDecodeOK = DemodDecode4FSKConReq(intFrameType, &intBW, &strCallerCallsign, &strTargetCallsign);
        status.Message = objFrameInfo->getName(intFrameType) + " " + strCallerCallsign + ">" + strTargetCallsign;
        if ( blnDecodeOK )
        {
            QByteArray ba = GetBytes(strCallerCallsign + " " + strTargetCallsign);
            bytData = &ba;
        }
     }
     break;

    case 0x40:
     {
        // 1 Carrier Data frames -  PSK Data
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK )  blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
     }
     break;

    case 0x41:
     {
        // 1 Carrier Data frames -  PSK Data
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK )  blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
     }
     break;

    case 0x42:
        // 1 Carrier Data frames -  PSK Data
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK )  blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x43:
        // 1 Carrier Data frames -  PSK Data
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK )  blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x44:
        // 1 Carrier Data frames -  PSK Data
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK )  blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x45:
        // 1 Carrier Data frames -  PSK Data
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK )  blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    // QAM modes
    case 0x5C:
        blnDecodeOK = Demod16QAM(intFrameType, &intPhase, &intMag, &intRef, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = Decode16QAMData(intFrameType, &intPhase, &intMag, intRef, bytData);
        break;
    case 0x5D:
        blnDecodeOK = Demod16QAM(intFrameType, &intPhase, &intMag, &intRef, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = Decode16QAMData(intFrameType, &intPhase, &intMag, intRef, bytData);
        break;
    case 0x54:
        blnDecodeOK = Demod16QAM(intFrameType, &intPhase, &intMag, &intRef, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = Decode16QAMData(intFrameType, &intPhase, &intMag, intRef, bytData);
        break;
    case 0x55:
        blnDecodeOK = Demod16QAM(intFrameType, &intPhase, &intMag, &intRef, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = Decode16QAMData(intFrameType, &intPhase, &intMag, intRef, bytData);
        break;

    // 4FSK Data
    case 0x46:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x47:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x48:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x49:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x4A:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x4B:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x4C:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x4D:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    // 8FSK Data
    case 0x4E:
        blnDecodeOK = Decode8FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x4F:
        blnDecodeOK = Decode8FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    // 16FSK Data
    case 0x58:
        blnDecodeOK = Decode16FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x59:
        blnDecodeOK = Decode16FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x5A:
        blnDecodeOK = Decode16FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x5B:
        blnDecodeOK = Decode16FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    // 2 Carrier Data frames
    case 0x50:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x51:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x52:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x53: // Removed &H56. &H57 1/11/2016
        blnDecodeOK = DemodPSK(intFrameType,& intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    // 1000 Hz  Data frames
    case 0x60:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x61:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x62:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x63:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x64:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x65:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x66:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x67:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x68:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( MCB->DebugLog )
            qDebug() << "[DecodeFrame] "
                     << objFrameInfo->getName(intFrameType)
                     << " Decode " << blnDecodeOK
                     << "  Quality= " << QString("%1").arg(intLastRcvdFrameQuality);
        break;

    case 0x69:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( MCB->DebugLog )
            qDebug() << "[DecodeFrame] "
                     << objFrameInfo->getName(intFrameType)
                     << " Decode " << blnDecodeOK
                     << "  Quality= " << QString("%1").arg(intLastRcvdFrameQuality);
        break;

    // 2000 Hz Data frames
    case 0x70:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x71:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x72:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x73:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x74:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x75:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x76:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x77:
        blnDecodeOK = DemodPSK(intFrameType, &intPhase, &intMag, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        if ( blnDecodeOK ) blnDecodeOK = DecodePSKData(intFrameType, &intPhase, &intMag, bytData);
        break;

    case 0x78:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x79:
        blnDecodeOK = DemodDecode4FSKData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    // 4FSK Data (600 bd)
    case 0x7A:
        blnDecodeOK = Decode4FSK600bdData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x7B:
        blnDecodeOK = Decode4FSK600bdData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x7C:
        blnDecodeOK = Decode4FSK600bdData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0x7D:
        blnDecodeOK = Decode4FSK600bdData(intFrameType, bytData, &intConstellationQuality);
        status.Message = objFrameInfo->getName(intFrameType);
        break;

    case 0xD0:
        // Experimental Sounding frame
        DemodSounder(intMFSReadPtr, &intFilteredMixedSamples);
        blnDecodeOK = true;
    }

    if ( blnDecodeOK )
        status.BackColor = ColorLightGreen;
    else
        status.BackColor = ColorLightSalmon;

    // emit signal to TNCPanel to update display control
    emit signalUpdateDisplay(status.CtlName,status.BackColor,status.Message,QVariant(status.Value));

    if ( intConstellationQuality > 0 )
    {
        intLastRcvdFrameQuality = intConstellationQuality;
        if ( MCB->DebugLog && !blnDecodeOK )
            qDebug() << "[DecodeFrame] "
                     << objFrameInfo->getName(intFrameType)
                     << " FAIL, Quality= "
                     << QString("%1").arg(intLastRcvdFrameQuality);

        if ( MCB->DebugLog && blnDecodeOK )
            qDebug() << "[DecodeFrame] Frame: "
                     << objFrameInfo->getName(intFrameType)
                     << " PASS, Quality= "
                     << QString("%1").arg(intLastRcvdFrameQuality);
    }
    else
    {
        if ( MCB->DebugLog && !blnDecodeOK )
            qDebug() << "[DecodeFrame] "
                     << objFrameInfo->getName(intFrameType)
                     << " FAIL";

        if ( MCB->DebugLog && blnDecodeOK )
            qDebug() << "[DecodeFrame] "
                     << objFrameInfo->getName(intFrameType)
                     << " PASS";
    }
    return blnDecodeOK;
} // DecodeFrame()

bool DemodulateDecode12K::AcquireFrameSyncRSB()
{
    qint32 intLocalPtr = intMFSReadPtr;
    qint32 intAvailableSymbols = (intFilteredMixedSamples.length() - intMFSReadPtr) / 240;
    double dblPhaseSym1 = 0.0; // phase of the first symbol
    double dblPhaseSym2 = 0.0; // phase of the second symbol
    double dblPhaseSym3 = 0.0;  // phase of the third symbol
    double dblReal      = 0.0;
    double dblImag      = 0.0;
    double dblPhaseDiff12 = 0.0;
    double dblPhaseDiff23 = 0.0;

    try
    {
        quint32 i = 0;
        if ( intAvailableSymbols < 3 ) return false; // must have at least 720 samples to search

        GoertzelRealImag(&intFilteredMixedSamples, intLocalPtr, 240, 30, &dblReal, &dblImag); // Carrier at 1500 Hz nominal Positioning
        dblPhaseSym1 = atan2(dblImag, dblReal);
        intLocalPtr += 240; // advance one symbol
        GoertzelRealImag(&intFilteredMixedSamples, intLocalPtr, 240, 30, &dblReal, &dblImag); // Carrier at 1500 Hz nominal Positioning
        dblPhaseSym2 = atan2(dblImag, dblReal);
        intLocalPtr += 240; // advance one symbol

        for ( i = 0; i < (quint32)(intAvailableSymbols - 3); ++i )
        {
            // Compute the phase of the next symbol
            GoertzelRealImag(&intFilteredMixedSamples, intLocalPtr, 240, 30, &dblReal, &dblImag); // Carrier at 1500 Hz nominal Positioning
            dblPhaseSym3 = atan2(dblImag, dblReal);

            // Compute the phase differences between sym1-sym2, sym2-sym3
            dblPhaseDiff12 = dblPhaseSym1 - dblPhaseSym2;
            if ( dblPhaseDiff12 > M_PI ) // bound phase diff to +/- Pi
                dblPhaseDiff12 -= dbl2Pi;
            else if ( dblPhaseDiff12 < -M_PI )
                dblPhaseDiff12 += dbl2Pi;

            dblPhaseDiff23 = dblPhaseSym2 - dblPhaseSym3;
            // bound phase diff to +/- Pi
            if ( dblPhaseDiff23 > M_PI )
                dblPhaseDiff23 -= dbl2Pi;
            else if ( dblPhaseDiff23 < -M_PI )
                dblPhaseDiff23 += dbl2Pi;

            if ( (abs(dblPhaseDiff12) > (0.6667 * M_PI)) &&
                 (abs(dblPhaseDiff23) < (0.3333 * M_PI))
               )
            {
                //Tighten the margin to 60 degrees
#ifdef DEBUG_DEMOD
                // *********************************
                // Debug code to look at filtered waveform
                //QVector<double> dblFilteredMixed(int75HzFilteredSamples.length() - 1);
                // dblAcquirePSKFrameSyncRSB.resize(intFilteredMixedSamples.Length - intMFSReadPtr - 1);
                //for ( k As Integer = 0; k < (dblFilteredMixed.Length - 1); ++k )
                //{
                //    dblFilteredMixed(k) = int75HzFilteredSamples(k)
                //} // Next k
                //objWT = New WaveTools
                //if ( IO.Directory.Exists(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav") = False )
                //{
                //    IO.Directory.CreateDirectory(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav")
                //}
                //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\AcquirePSKFrameSyncRSB.wav", 12000, 16, dblFilteredMixed)
                // End of debug code
                //************************************
#endif

                // 30 is to accomodate offset of inital pointer
                // for filter length.
                intLeaderRcvdMs = round((intLocalPtr - 30) / 12);
                // Position read pointer to start of the symbol
                // following reference symbol
                intMFSReadPtr = intLocalPtr + 240;
                // accumulate tuning stats
                if ( MCB->AccumulateStats ) ++objMain->stcTuningStats.intFrameSyncs;
                //strDecodeCapture &= "Sync; Ph1>2=" & Format(dblPhaseDiff12, "0.00") & " Ph2>3=" & Format(dblPhaseDiff23, "0.00") & ": "
#ifdef DEBUG_DEMOD
                qDebug() << "Phase 1 > 2 = " << QString("0.00").arg(dblPhaseDiff12) << " Phase2>3=" << QString("0.00").arg(dblPhaseDiff23);
#endif
                return true; // pointer is pointing to first 4FSK data symbol. (first symbol of frame type)
            }
            else
            {
                dblPhaseSym1 = dblPhaseSym2;
                dblPhaseSym2 = dblPhaseSym3;
                intLocalPtr += 240; // advance one symbol
            }
        } // Next i
        intMFSReadPtr = intLocalPtr - 480; // back up 2 symbols for next attempt (Current Sym2 will become new Sym1)
        return false;
    }
    catch (exception ex)
    {
        qCritical() << "Err: " << ex.what();
    }
    return false;
} // AcquireFrameSyncRSB()

bool DemodulateDecode12K::Acquire2ToneLeaderSymbolFraming()
{
    qint32 i = 0;
    qint32 intAbsPeak = 0;
    qint32 intJatPeak = 0;
    qint32 intIatMinErr = 0;
    qint32 intLocalPtr = intMFSReadPtr;  // try advancing one symbol to minimize initial startup errors
    double dblCarPh       = 0.0;
    double dblReal        = 0.0;
    double dblImag        = 0.0;
    double dblAbsPhErr    = 0.0;
    double dblMinAbsPhErr = 5000.0; // initialize to an excessive value
    double dblPhaseAtMinErr = 0.0;

    Q_UNUSED(intAbsPeak)   // to suppress warnings in release build
    Q_UNUSED(intJatPeak)
    Q_UNUSED(dblPhaseAtMinErr)

    // Use Phase of 1500 Hz leader to establish
    // symbol framing. Nominal phase is 0 or 180 degrees
    intLocalPtr = intMFSReadPtr + EnvelopeCorrelator(); // should position the pointer at the symbol boundary
    if ( intLocalPtr < intMFSReadPtr ) return false; // use negative value of EnvelopeCorrelator to indicate insufficient correlation.

    // Check 2 samples either side of the intLocalPtr for
    // minimum phase error.(closest to Pi or -Pi)
    // Could be as much as .4 Radians (~70 degrees)
    // depending on sampling positions.
    for ( i = -2; i >= 2; ++i )
    {
        // for 5 tries (normally is between -1 to +1)
        // Carrier at 1500 Hz nominal Positioning
        GoertzelRealImag(&intFilteredMixedSamples, (intLocalPtr + i), 240, 30, &dblReal, &dblImag);
        dblCarPh = atan2(dblImag, dblReal);
        dblAbsPhErr = abs(dblCarPh - (round(dblCarPh / M_PI) * M_PI));
        if ( dblAbsPhErr < dblMinAbsPhErr )
        {
            dblMinAbsPhErr = dblAbsPhErr;
            intIatMinErr = i;
            dblPhaseAtMinErr = dblCarPh;
        }
    } // Next i
    intMFSReadPtr = intLocalPtr + intIatMinErr;
#ifdef DEBUG_DEMOD
    qDebug() << "iatMinError="
             << QString("%1").arg(intIatMinErr)
             << "  Phase= "
             << QString("%0.00").arg(dblPhaseAtMinErr);
#endif
    RxState = AcquireFrameSync;
    if ( MCB->AccumulateStats )  ++objMain->stcTuningStats.intLeaderSyncs;
    //strDecodeCapture &= "Framing; i=" & intIatMinErr.ToString & ", Ptr=" & intMFSReadPtr.ToString & ", PhErr=" & Format(dblMinAbsPhErr, "#.00") & ": "

#ifdef DEBUG_DEMOD
    // *********************************
    // Debug code to look at filtered waveform
    //QVector<double> dblAcquire2ToneLeaderSymbolFraming(intFilteredMixedSamples.Length - intLocalPtr);
    //for ( k As Integer = 0; k < (dblAcquire2ToneLeaderSymbolFraming.length() - 1); ++k )
    //{
    //    dblAcquire2ToneLeaderSymbolFraming(k) = intFilteredMixedSamples(k)
    //} // Next k
    //objWT = new WaveTools;
    //if ( IO.Directory.Exists(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav") = False Then
    //    IO.Directory.CreateDirectory(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav")
    //}
    //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\Acquire2ToneLeaderSymbSync.wav", 12000, 16, dblFilteredMixed)
    // End of debug code
    //************************************
#endif
    return true;
}  // Acquire2ToneLeaderSymbolSync()

void DemodulateDecode12K::MixNCOFilter(QVector<qint32> *intNewSamples, qint32 *intReadPtr, double dblOffsetHz)
{
        if ( intNewSamples->length() == 0 ) return;

        qint32 i = 0;
        qint32 j = 0;
        // Nominal NCO freq is 3000 Hz to downmix intNewSamples
        // (NCO - Fnew) to center of 1500 Hz (invertes the sideband too)
        dblNCOFreq = 3000.0 + dblOffsetHz;
        dblNCOPhaseInc = dblNCOFreq * dbl2Pi / 12000;
        j = qint32(intNewSamples->length()) - 1;
        j -= qint32(intReadPtr);
        intMixedSamples.resize(j);
        for ( i = 0; i < j; ++ i )
        {
            // later may want a lower "cost"
            // implementation of "cos"

            double jj = cos(dblNCOPhase);
            qint32 ww = intNewSamples->at(*intReadPtr + i);
            intMixedSamples[i] = qint32(ww * jj);
            dblNCOPhase += dblNCOPhaseInc;
            if ( dblNCOPhase > dbl2Pi ) dblNCOPhase -= dbl2Pi;
        } // Next i
        intReadPtr = 0;
        intNewSamples->clear();
        // showed no significant difference if the 2000 Hz filer
        // used for all bandwidths.

        // filter through the FS filter
        // (required to reject image from Local oscillator)
        FSMixFilter2000Hz();
}  // MixNCOFilter()

qint32 DemodulateDecode12K::MinimalDistanceFrameType(QVector<qint32> intToneMags, QByteArray bytSessionID)
{
    Q_UNUSED(bytSessionID)  // to suppress warnings in release build

    qint32 intIatMinDistance1 = 0;
    qint32 intIatMinDistance2 = 0;
    qint32 intIatMinDistance3 = 0;
    double dblDistance1    = 0.0;
    double dblDistance2    = 0.0;
    double dblDistance3    = 0.0;
    double dblMinDistance1 = 5.0; // minimal distance for the first byte initialize to large value
    double dblMinDistance2 = 5.0; // minimal distance for the second byte initialize to large value
    double dblMinDistance3 = 5.0; // minimal distance for the second byte under exceptional cases initialize to large value
    QByteArray bytValidFrameTypes;

    if ( (objProtocol->ARDOPState == objProtocol->ISS) ||
         (objProtocol->ARDOPState == objProtocol->IDLE)
       )
    {
        bytValidFrameTypes = objFrameInfo->bytValidISSFrameTypes; // ACKs, NAKs, END, DISC, BREAK, ConRejBusy, ConRejBW, and Con ACK
    }
    else
    {
        bytValidFrameTypes = objFrameInfo->bytValidFrameTypes; // all frame types
    }

    // Search through all the valid frame types
    // finding the minimal distance
    // This looks like a lot of computation but
    // measured < 1 ms for 135 iterations....RM 11/1/2016
    for ( qint32 i = 0; i < (bytValidFrameTypes.length() - 1); ++i )
    {
        dblDistance1 = ComputeDecodeDistance(0, intToneMags, bytValidFrameTypes.at(i), 0x0);
        dblDistance2 = ComputeDecodeDistance(20, intToneMags, bytValidFrameTypes.at(i), objProtocol->bytSessionID);
        if ( objProtocol->isPending() )
            dblDistance3 = ComputeDecodeDistance(20, intToneMags, bytValidFrameTypes.at(i), 0xFF);
        else
            dblDistance3 = ComputeDecodeDistance(20, intToneMags, bytValidFrameTypes.at(i), objProtocol->bytLastARQSessionID);

        if ( dblDistance1 < dblMinDistance1 )
        {
            dblMinDistance1 = dblDistance1;
            intIatMinDistance1 = bytValidFrameTypes.at(i);
        }

        if ( dblDistance2 < dblMinDistance2 )
        {
            dblMinDistance2 = dblDistance2;
            intIatMinDistance2 = bytValidFrameTypes.at(i);
        }

        if ( dblDistance3 < dblMinDistance3 )
        {
            dblMinDistance3 = dblDistance3;
            intIatMinDistance3 = bytValidFrameTypes.at(i);
        }
    } // Next i

    if ( objProtocol->bytSessionID == 0xFF )
    {
        // we are in a FEC QSO, monitoring an
        // ARQ session or have not yet reached
        // the ARQ Pending or Connected status

        // This handles the special case of a
        // DISC command received from the prior
        // session (where the station sending
        // DISC did not receive an END).
        if ( (intIatMinDistance1 == 0x29) &&
             (intIatMinDistance3 == 0x29) &&
             ((dblMinDistance1 < 0.3) ||
              (dblMinDistance3 < 0.3)
             )
           )
        {
            if ( MCB->DebugLog )
            {
                qDebug() << "[Frame Type OK]1 "
                                          << objFrameInfo->getName(intIatMinDistance1)
                                          << ", D1=" << QString("%1").arg(dblMinDistance1)
                                          << ", D3=" << QString("%1").arg(dblMinDistance3)
                                          << "  " << strDecodeCapture;
                return intIatMinDistance1;
            }
            else if ( (intIatMinDistance1 == intIatMinDistance2) &&
                      ((dblMinDistance1 < 0.4) ||
                       (dblMinDistance2 < 0.4))
                    )
            {
                // Relaxed decode test 0.7.2.3
                objMain->dblOffsetLastGoodDecode = objMain->dblOffsetHz;
                dttLastGoodFrameTypeDecode = QDateTime::currentDateTime();
                if ( MCB->DebugLog )
                {
                    qDebug() << "[Frame Type OK]2 "
                             << objFrameInfo->getName(intIatMinDistance1)
                             << ", D1=" << QString("%1").arg(dblMinDistance1)
                             << ", D2=" << QString("%1").arg(dblMinDistance2)
                             << "  " << strDecodeCapture;
                    return intIatMinDistance1;
                }
            }
            else if ( (dblMinDistance1 < 0.4) &&
                      (dblMinDistance1 < dblMinDistance2) &&
                      objFrameInfo->isDataFrame(intIatMinDistance1)
                    )
            {
                // this would handle the case of
                // monitoring an ARQ connection
                // where the SessionID is not &HFF
                // Relaxed decode test 0.7.2.4 (was .3)
                //strDecodeCapture &= "MDDecode3; ID=H" & Format(bytSessionID, "X") & ", Type=H" & Format(intIatMinDistance1, "X") & ":" & objFrameInfo.Name(intIatMinDistance1) & ", D1=" & Format(dblMinDistance1, "#.00") & ", D2=" & Format(dblMinDistance2, "#.00")
                // If MCB.DebugLog Then Logs.WriteDebug("[Frame Type OK  ] " & strDecodeCapture)
                if ( MCB->DebugLog )
                {
                    qDebug() << "[Frame Type OK]3 "
                             << objFrameInfo->getName(intIatMinDistance1)
                             << ", D1=" << QString("%1").arg(dblMinDistance1)
                             << ", D2=" << QString("%1").arg(dblMinDistance2)
                             << "  " << strDecodeCapture;
                    return intIatMinDistance1;
                }
                else if ( (dblMinDistance2 < 0.4) &&
                          (dblMinDistance2 < dblMinDistance1) &&
                          objFrameInfo->isDataFrame(intIatMinDistance2)
                        )
                {
                    // this would handle the case of
                    // monitoring an FEC transmission
                    // that failed above when the
                    // session ID is = &HFF
                    // Relaxed decode test 0.7.2.4 (was .3)
                    if ( MCB->DebugLog )
                    {
                        qDebug() << "[Frame Type OK]4 "
                                 << objFrameInfo->getName(intIatMinDistance1)
                                 << ", D1=" << QString("%1").arg(dblMinDistance1)
                                 << ", D2=" << QString("%1").arg(dblMinDistance2)
                                 << "  " << strDecodeCapture;
                        return intIatMinDistance2;
                    }
                    else
                    {
                        //If MCB.DebugLog Then Logs.WriteDebug("[Frame Type Fail] " & strDecodeCapture)
                        return -1;
                    }
                }
                else if ( objProtocol->isPending() )
                {
                    // We have a Pending ARQ connection
                    // This should be a Con Ack from the
                    // ISS if we are Pending
                    if (intIatMinDistance1 == intIatMinDistance2)
                    {
                        // matching indexes at minimal distances
                        // so high probablity of correct decode.
                        if ( (dblMinDistance1 < 0.4) ||
                             (dblMinDistance2 < 0.4)
                           )
                        {
                            // Relaxed decode test 0.7.2.3
                            objMain->dblOffsetLastGoodDecode = objMain->dblOffsetHz;
                            dttLastGoodFrameTypeDecode = QDateTime::currentDateTime(); // This allows restricting tuning changes to about +/- 4Hz from last dblOffsetHz
                            if ( MCB->DebugLog )
                            {
                                qDebug() << "[Frame Type OK]5 "
                                         << objFrameInfo->getName(intIatMinDistance1)
                                         << ", D1=" << QString("%1").arg(dblMinDistance1)
                                         << ", D2=" << QString("%1").arg(dblMinDistance2)
                                         << "  " << strDecodeCapture;
                                return intIatMinDistance1;
                            }
                        }
                        else
                        {
                            return -1; // indicates poor quality decode so  don't use
                        }
                    }
                    else if ( intIatMinDistance1 == intIatMinDistance3)
                    {
                        // matching indexes at minimal distances
                        // so high probablity of correct decode.
                        // handles the case of a received ConReq
                        // frame based on an ID of &HFF (ISS
                        // must have missed ConAck reply from
                        // IRS so repeated ConReq)
                        //strDecodeCapture &= "MDDecode7; ID=H" & Format(&HFF, "X") & ", type=H" & Format(intIatMinDistance1, "X") & ":" & objFrameInfo.Name(intIatMinDistance1) & ", D1=" & Format(dblMinDistance1, "#.00") & ", D3=" & Format(dblMinDistance3, "#.00")
                        if ( (intIatMinDistance1 >= 0x31) &&
                             (intIatMinDistance1 <= 0x38) &&
                             ((dblMinDistance1 < 0.4) ||
                              (dblMinDistance3 < 0.4)
                             )
                           )
                        {
                            // Check for ConReq (ISS must have
                            // missed previous ConAck
                            // Relaxed decode test 0.7.2.4
                            //If (intIatMinDistance1 >= &H31 And intIatMinDistance1 <= &H38) And ((dblMinDistance1 < 0.3) Or (dblMinDistance3 < 0.3)) Then ' Check for ConReq (ISS must have missed previous ConAck
                            objMain->dblOffsetLastGoodDecode = objMain->dblOffsetHz;
                            dttLastGoodFrameTypeDecode = QDateTime::currentDateTime(); // This allows restricting tuning changes to about +/- 4Hz from last dblOffsetHz
                            if ( MCB->DebugLog )
                            {
                                qDebug() << "[Frame Type OK]6 "
                                         << objFrameInfo->getName(intIatMinDistance1)
                                         << ", D1=" << QString("%1").arg(dblMinDistance1)
                                         << ", D3=" << QString("%1").arg(dblMinDistance3)
                                         << "  " << strDecodeCapture;
                                return intIatMinDistance1;
                            }
                        }
                        else
                        {
                            //If MCB.DebugLog Then Logs.WriteDebug("[Frame Type Fail] " & strDecodeCapture)
                            return -1; // indicates poor quality decode so  don't use
                        }
                    }
                    else if ( (dblMinDistance1 < dblDistance2) &&
                              (dblMinDistance1 < 0.3)
                            )
                    {
                        // non matching indexes
                        if ( MCB->DebugLog )
                        {
                            qDebug() << "[Frame Type OK]7 "
                                     << objFrameInfo->getName(intIatMinDistance1)
                                     << ", D1=" << QString("%1").arg(dblMinDistance1)
                                     << ", D2=" << QString("%1").arg(dblMinDistance2)
                                     << "  " << strDecodeCapture;
                            return intIatMinDistance1;
                        }
                    }
                    else if ( (dblMinDistance2 < dblDistance1) &&
                              (dblMinDistance2 < 0.3)
                            )
                    {
                        // non matching indexes
                        if ( MCB->DebugLog )
                        {
                            qDebug() << "[Frame Type OK]8 "
                                     << objFrameInfo->getName(intIatMinDistance1)
                                     << ", D1=" << QString("%1").arg(dblMinDistance1)
                                     << ", D2=" << QString("%1").arg(dblMinDistance2)
                                     << "  " << strDecodeCapture;
                            return intIatMinDistance2;
                        }
                    }
                    else
                    {
                        // If MCB.DebugLog Then Logs.WriteDebug("[Frame Type Fail]")
                        return -1;
                    }
                }
                else if ( objProtocol->isConnected() )
                {
                    // we have an ARQ connected session.
                    if ( MCB->AccumulateStats )
                    {
                        objMain->stcTuningStats.dblAvgDecodeDistance = \
                                (objMain->stcTuningStats.dblAvgDecodeDistance * \
                                 objMain->stcTuningStats.intDecodeDistanceCount + \
                                 0.5 * (dblMinDistance1 + dblMinDistance2)) / \
                                 (objMain->stcTuningStats.intDecodeDistanceCount + 1 \
                                );
                        objMain->stcTuningStats.intDecodeDistanceCount += 1;
                    }

                    if (intIatMinDistance1 == intIatMinDistance2)
                    {
                        // matching indexes so high probablity
                        // of correct decode.
                        if ( (intIatMinDistance1 >= 0xE0) &&
                             (
                              (intIatMinDistance1 <= 0xFF) ||
                              (intIatMinDistance1 == 0x23) ||
                              (intIatMinDistance1 == 0x2C) ||
                              (intIatMinDistance1 == 0x29)
                             )
                           )
                        {
                            // Check for critical
                            // ACK, BREAK, END, or DISC frames
                            if ( (dblMinDistance1 < 0.35) ||
                                 (dblMinDistance2 < 0.35)
                               )
                            {
                                // use tighter limits on these
                                // critical types
                                // Relaxed decode test 0.7.2.3 (.30 > .35)
                                objMain->dblOffsetLastGoodDecode = objMain->dblOffsetHz;
                                dttLastGoodFrameTypeDecode = QDateTime::currentDateTime(); // This allows restricting tuning changes to about +/- 4Hz from last dblOffsetHz
                                if ( MCB->DebugLog )
                                {
                                    qDebug() << "[Frame Type OK]9 "
                                             << objFrameInfo->getName(intIatMinDistance1)
                                             << ", D1=" << QString("%1").arg(dblMinDistance1)
                                             << ", D2=" << QString("%1").arg(dblMinDistance2)
                                             << "  " << strDecodeCapture;
                                    return intIatMinDistance1;
                                }
                            }
                            else
                            {
                                //If MCB.DebugLog Then Logs.WriteDebug("[Frame Type Fail] " & strDecodeCapture)
                                return -1; // indicates poor quality decode so  don't use
                            }
                        }
                        else
                        {
                            // non critical frames
                            // use looser limits here, there
                            // is no risk of protocol damage
                            // from these frames
                            if ( (dblMinDistance1 < 0.4) ||
                                 (dblMinDistance2 < 0.4)
                               )
                            {
                                if ( MCB->DebugLog )
                                {
                                    qDebug() << "[Frame Type OK]10 "
                                             << objFrameInfo->getName(intIatMinDistance1)
                                             << ", D1=" << QString("%1").arg(dblMinDistance1)
                                             << ", D2=" << QString("%1").arg(dblMinDistance2)
                                             << "  " << strDecodeCapture;
                                }
                                objMain->dblOffsetLastGoodDecode = objMain->dblOffsetHz;
                                dttLastGoodFrameTypeDecode = QDateTime::currentDateTime(); // This allows restricting tuning changes to about +/- 4Hz from last dblOffsetHz
                                return intIatMinDistance1;
                            }
                            else
                            {
                                //If MCB.DebugLog Then Logs.WriteDebug("[Frame Type Fail] " & strDecodeCapture)
                                return -1; // indicates poor quality decode so  don't use
                            }
                        }
                    }
                    else
                    {
                        // non matching indexes
                        // Relaxed decode test 0.7.2.3
                        // (accepted decode distance of .3
                        // that previously failed)
                        if ( (dblMinDistance1 < dblDistance2) &&
                             (dblMinDistance1 < 0.3) &&
                            !(intIatMinDistance1 >= 0xE0) &&
                             (
                              (intIatMinDistance1 <= 0xFF) ||
                              (intIatMinDistance1 == 0x23)    ||
                              (intIatMinDistance1 == 0x2C)    ||
                              (intIatMinDistance1 == 0x29)
                             )
                            )
                        {
                            if ( MCB->DebugLog )
                            {
                                qDebug() << "[Frame Type OK]11 "
                                         << objFrameInfo->getName(intIatMinDistance1)
                                         << ", D1=" << QString("%1").arg(dblMinDistance1)
                                         << ", D2=" << QString("%1").arg(dblMinDistance2)
                                         << "  " << strDecodeCapture;
                            }
                            objMain->dblOffsetLastGoodDecode = objMain->dblOffsetHz;
                            dttLastGoodFrameTypeDecode = QDateTime::currentDateTime(); // This allows restricting tuning changes to about +/- 4Hz from last dblOffsetHz
                            return intIatMinDistance1;
                        }
                        else if ( (dblMinDistance2 < dblDistance1) &&
                                  (dblDistance2 < 0.3) &&
                                 !((intIatMinDistance2 >= 0xE0) &&
                                  (
                                   (intIatMinDistance2 <= 0xFF) ||
                                   (intIatMinDistance2 == 0x23)  ||
                                   (intIatMinDistance2 == 0x2C)  ||
                                   (intIatMinDistance2 == 0x29)
                                  )
                                 )
                                )
                        {
                            if ( MCB->DebugLog )
                            {
                                qDebug() << "[Frame Type OK]12 "
                                         << objFrameInfo->getName(intIatMinDistance1)
                                         << ", D1=" << QString("%1").arg(dblMinDistance1)
                                         << ", D2=" << QString("%1").arg(dblMinDistance2)
                                         << "  " << strDecodeCapture;
                            }
                            objMain->dblOffsetLastGoodDecode = objMain->dblOffsetHz;
                            dttLastGoodFrameTypeDecode = QDateTime::currentDateTime(); // This allows restricting tuning changes to about +/- 4Hz from last dblOffsetHz
                            return intIatMinDistance2;
                        }
                        else
                        {
                            // If MCB.DebugLog Then Logs.WriteDebug("[Frame Type Fail] " & strDecodeCapture)
                            return -1; // indicates poor quality decode so  don't use
                        }
                    }
                }

                if ( MCB->DebugLog )
                {
                    qDebug("[Frame Type Fail]");
                }
                return -1; // indicates poor quality decode so  don't use
            }
        }
    }
    return -1; // indicates poor quality decode so  don't use
} // MinimalDistanceFrameType()
<<<<<<< HEAD
=======

>>>>>>> remotes/origin/ken
