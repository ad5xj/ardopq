#include <math.h>
using namespace std;

#include "utils.hpp"
#include "reedsolomon.hpp"
#include "frameinfo.hpp"

#include "modulator.hpp"

EncodeModulate::EncodeModulate(TNCPanel *parent) : QObject(parent)
{
    objMain      = parent;
    objProto     = parent->objProtocol;
    objRS8       = new ReedSolomon_8;
    objFrameInfo = new FrameInfo(this);

    intAmp = 26000;
    int50BaudTwoToneLeaderTemplate.clear();

    // reserve space for one-dimensional array
    int50BaudTwoToneLeaderTemplate.reserve(239);
    intFSK50bdCarTemplate.reserve(240);

    // reserve space for mulit-dimensional array
    int25BaudCarTemplate.reserve(16);
    for ( quint8 w = 0; w < 16; ++w )
    {
        int25BaudCarTemplate[w].reserve(480);
    }
    // reserve space for mulit-dimensional array
    intFSK50bdCarTemplate.reserve(4);
    for ( quint16 w = 0; w < 4; ++w )
    {
        intFSK50bdCarTemplate[w].reserve(240);
    }

    intFSK100bdCarTemplate.reserve(20);
    for ( quint8 w = 0; w < 21; ++w )
    {
        intFSK100bdCarTemplate[w].reserve(120);
    }

    intFSK600bdCarTemplate.reserve(4);
    for ( quint8 w = 0; w < 5; ++w )
    {
        intFSK600bdCarTemplate[w].reserve(20);
    }

    intPSK100bdCarTemplate.reserve(8);  // reserving width
    for ( int w = 0; w < 9; ++w )
    {
        intPSK100bdCarTemplate[w].resize(4);
        for ( int h = 0; h < 5; ++h)
        {
            intPSK100bdCarTemplate[w][h].resize(120);
        }
    }

    Generate50BaudTwoToneLeaderTemplate();
    GeneratePSKTemplates(); // Generate all PSK templates for 9 carriers,  8 phases, for  10 ms  symbols (also used for 16QAM)
    GenerateFSKTemplates(); // Generate all templates for 50 baud 4FSK and 25 baud 16FSK
}

EncodeModulate::~EncodeModulate()
{
    // free the local objects
    delete objRS8;
    delete objFrameInfo;
    delete objMain;
}

void EncodeModulate::CreateWaveStream(QVector<qint32> *intFilteredSamplesToSend)
{
    if ( intFilteredSamplesToSend->length() < 1000 )
    {
        qCritical() << "intFilteredSamplesToSend Length = " << QString("%1").arg(intFilteredSamplesToSend->length());
        return;
    }

    qint32 intTrace = 1;
    try
    {
        QByteArray aryWaveData;
        aryWaveData.resize(2 * intFilteredSamplesToSend->length() - 1);

        for ( quint32 n = 0;  n < (quint32)(intFilteredSamplesToSend->length() - 1); ++n )
        {
            aryWaveData[2 * n] = quint8(intFilteredSamplesToSend->at(n) && 0xFF);
            aryWaveData[2 * n + 1] = quint8((intFilteredSamplesToSend->at(n) && 0xFF00) / 256);
        } // Next n

        intTrace = 2;
#ifdef DEBUG_MOD
        // Debug code
        // Use WriteRiff to look at .wav file with ScopeDSP for analysis
        //objWave.WriteRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\Test.wav", 12000, 16, aryWaveData)
        objWave.WriteRIFFStream(memWaveStream, 12000, 16, aryWaveData)
#endif
    }
    catch (exception ex)
    {
        qCritical() << "intFilteredSamplesToSend Length = " << QString("%1").arg(intFilteredSamplesToSend->length()) << "  Trace=" << QString("%1").arg(intTrace) << "  Err: " << ex.what();
    }
}  // CreateWaveStream()


QVector<qint32> EncodeModulate::FSXmtFilter200_1500Hz(QVector<qint32> *intNewSamples)
{
    static qint32 intN  = 120;     // Length of filter 12000/100
    static qint32 intFilLen = qint32(intN / 2);
    static double dblR  = 0.9995;  // insures stability (must be < 1.0) (Value .9995 7/8/2013 gives good results)
    static double dblRn = pow(dblR, double(intN));
    static double dblR2 = pow(dblR, 2.0);
    static QVector<double> dblCoef(18); // the coefficients

    AddTrailer(intNewSamples); // add the trailer before filtering

    qint32 intPeakSample = 0;
    double dblZin   = 0.0;
    double dblZin_1 = 0.0;
    double dblZin_2 = 0.0;
    double dblZComb = 0.0;  // Used in the comb generator
    // The resonators
    QVector<double> dblZout_0(18); // resonator outputs
    QVector<double> dblZout_1(18); // resonator outputs delayed one sample
    QVector<double> dblZout_2(18); // resonator outputs delayed two samples
    QVector<qint32> intFilteredSamples(intNewSamples->length() - 1); //  Filtered samples

    Q_UNUSED(intPeakSample)  // to suppress warnings in release build

#ifdef DEBUG_MOD
    QVector<double> dblUnfilteredSamples(intNewSamples.length() - 1); // for debug wave plotting
    QVector<double> dblFilteredSamples(intNewSamples.length() - 1); // for debug wave plotting
#endif

    // Initialize the coefficients
    if ( dblCoef.at(15) == 0 )
    {
        for ( quint8 i = 14; i <= 16; ++ i )
        {
            dblCoef[i] = 2 * dblR * cos(2 * M_PI * i / intN); // For Frequency = bin i
        } // Next i
    }

    try
    {
        for ( qint32 i = 0; i < ((intNewSamples->length() + intFilLen) - 1); ++i )
        {
            if ( i < intN )
            {
                // dblUnfilteredSamples(i) = intNewSamples(i) ' debug code for waveform plotting.
                dblZin = intNewSamples->at(i);
            }
            else if ( i < intNewSamples->length() )
            {
                // dblUnfilteredSamples(i) = intNewSamples(i) ' debug code for waveform plotting.
                dblZin = intNewSamples->at(i) - dblRn * intNewSamples->at(i - intN);
            }
            else
            {
                dblZin = -dblRn * intNewSamples->at(i - intN);
            }

            // Compute the Comb
            dblZComb = dblZin - dblZin_2 * dblR2;
            dblZin_2 = dblZin_1;
            dblZin_1 = dblZin;

            // Now the resonators
            for ( quint8 j = 14; j <= 16; ++j )
            {
                // calculate output for 3 resonators
                dblZout_0[j] = dblZComb + dblCoef.at(j) * dblZout_1.at(j) - dblR2 * dblZout_2.at(j);
                dblZout_2[j] = dblZout_1.at(j);
                dblZout_1[j] = dblZout_0.at(j);
                // scale each by transition coeff and + (Even) or - (Odd)

                if ( i >= intFilLen )
                {
                    if ( (j == 14) || (j == 16) )
                        intFilteredSamples[i - intFilLen] += 0.7389 * dblZout_0.at(j);
                    else
                        intFilteredSamples[i - intFilLen] -= dblZout_0.at(j);
                }
            } // Next j

            if ( i >= intFilLen )
            {
                intFilteredSamples[i - intFilLen] = intFilteredSamples.at(i - intFilLen) * 0.00833333333; //  rescales for gain of filter
                if ( intFilteredSamples.at(i - intFilLen) > 32700 ) // Hard clip above 32700
                    intFilteredSamples[i - intFilLen] = 32700;
                else if ( intFilteredSamples.at(i - intFilLen) < -32700 )
                    intFilteredSamples[i - intFilLen] = -32700;

                //dblFilteredSamples(i - intFilLen) = intFilteredSamples(i - intFilLen) ' debug code
            }
        } // Next i
#ifdef DEBUG_MOD
        // *********************************
        //Debug code to look at filter output
        //Dim objWT As New WaveTools
        //If IO.Directory.Exists(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav") = False Then
        //    IO.Directory.CreateDirectory(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav")
        //End If
        //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\UnFiltered" & "_XmtFilter200.wav", 12000, 16, dblUnfilteredSamples)
        //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\Filtered" & "_XmtFilter200.wav", 12000, 16, dblFilteredSamples)
        // End of debug code
        // ************************************
#endif
    }
    catch (exception ex)
    {
        qCritical() << "Exception: " << ex.what();
        intFilteredSamples.clear();
    }
    return intFilteredSamples;
} // FSXmtFilter200_1500Hz()


QVector<qint32> EncodeModulate::FSXmtFilter500_1500Hz(QVector<qint32> *intNewSamples)
{
    static qint32 intN  = 120;     // Length of filter 12000/100
    static qint32 intFilLen = qint32(intN / 2);
    static double dblR  = 0.9995;  // insures stability (must be < 1.0) (Value .9995 7/8/2013 gives good results)
    static double dblRn = pow(dblR, double(intN));
    static double dblR2 = pow(dblR, 2.0);
    static QVector<double> dblCoef(18); // the coefficients

    AddTrailer(intNewSamples); // add the trailer before filtering

    qint32 intPeakSample = 0;
    double dblZin   = 0.0;
    double dblZin_1 = 0.0;
    double dblZin_2 = 0.0;
    double dblZComb = 0.0;  // Used in the comb generator
    // The resonators
    QVector<double> dblZout_0(18); // resonator outputs
    QVector<double> dblZout_1(18); // resonator outputs delayed one sample
    QVector<double> dblZout_2(18); // resonator outputs delayed two samples
    QVector<qint32> intFilteredSamples(intNewSamples->length() - 1); //  Filtered samples

#ifdef DEBUG_MOD
    QVector<double> dblUnfilteredSamples(intNewSamples.length() - 1); // for debug wave plotting
    QVector<double> dblFilteredSamples(intNewSamples.length() - 1); // for debug wave plotting
#endif

    // Initialize the coefficients
    if ( dblCoef.at(18) == 0 )
    {
        for ( quint8 i = 12; i < 18; ++i )  // For Frequency = bin i
        {
            dblCoef[i] = 2 * dblR * cos(2 * M_PI * i / intN);
        } // Next i
    }

    try
    {
        for ( qint32 i = 0; i < ((intNewSamples->length() + intFilLen) - 1); ++i )
        {
            if ( i < intN )
            {
                // dblUnfilteredSamples(i) = intNewSamples(i) ' debug code for waveform plotting.
                dblZin = intNewSamples->at(i);
            }
            else if ( i < intNewSamples->length() )
            {
                //dblUnfilteredSamples(i) = intNewSamples(i) ' debug code for waveform plotting.
                dblZin = intNewSamples->at(i) - dblRn * intNewSamples->at(i - intN);
            }
            else
            {
                dblZin = -dblRn * intNewSamples->at(i - intN);
            }

            // Compute the Comb
            dblZComb = dblZin - dblZin_2 * dblR2;
            dblZin_2 = dblZin_1;
            dblZin_1 = dblZin;

            // Now the resonators
            for ( quint8 j = 12; j < 18; ++j )
            {
                // calculate output for 7 resonators
                dblZout_0[j] = dblZComb + dblCoef.at(j) * dblZout_1.at(j) - dblR2 * dblZout_2.at(j);
                dblZout_2[j] = dblZout_1.at(j);
                dblZout_1[j] = dblZout_0.at(j);
                // scale each by transition coeff and + (Even)
                // or - (Odd)
                // Resonators 6 and 9 scaled by .15 to get
                // best shape and side lobe supression
                // to - 45 dB while keeping BW at
                // 500 Hz @ -26 dB
                // practical range of scaling .05 to .25
                // Scaling also accomodates for the
                // filter "gain" of approx 60.
                if ( i >= intFilLen )
                {
                    if ( (j == 12) || (j == 18) )
                        intFilteredSamples[i - intFilLen] += 0.10601 * dblZout_0.at(j);
                    else if ( (j == 13) || (j == 17) )
                        intFilteredSamples[i - intFilLen] -= 0.59383 * dblZout_0.at(j);
                    else if ( (j % 2) == 0 )
                        intFilteredSamples[i - intFilLen] += dblZout_0.at(j);
                    else
                        intFilteredSamples[i - intFilLen] -= dblZout_0.at(j);
                }
            } // Next j
            if ( i >= intFilLen )
            {
                //  rescales for gain of filter
                intFilteredSamples[i - intFilLen] = intFilteredSamples.at(i - intFilLen) * 0.00833333333;
                if ( intFilteredSamples.at(i - intFilLen) > 32700 ) // Hard clip above 32700
                    intFilteredSamples[i - intFilLen] = 32700;
                else if ( intFilteredSamples.at(i - intFilLen) < -32700 )
                    intFilteredSamples[i - intFilLen] = -32700;

                //dblFilteredSamples(i - intFilLen) = intFilteredSamples(i - intFilLen) ' debug code
                if ( abs(intFilteredSamples.at(i - intFilLen)) > abs(intPeakSample) ) intPeakSample = intFilteredSamples.at(i - intFilLen);
            }
        } // Next i
#ifdef DEBUG_MOD
        // *********************************
        // Debug code to look at filter output
        //Dim objWT As New WaveTools
        //If IO.Directory.Exists(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav") = False Then
        //    IO.Directory.CreateDirectory(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav")
        //End If
        //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\UnFiltered" & "_XmtFil500.wav", 12000, 16, dblUnfilteredSamples)
        //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\Filtered" & "_XmtFil500.wav", 12000, 16, dblFilteredSamples)
        //End of debug code
        // ************************************
#endif
    }
    catch (exception ex)
    {
        qCritical() << "Exception: " << ex.what();
        intFilteredSamples.clear();
    }
    return intFilteredSamples;
} // FSXmtFilter500_1500Hz()


QVector<qint32> EncodeModulate::FSXmtFilter1000_1500Hz(QVector<qint32> *intNewSamples)
{
    static qint32 intN  = 120;     // Length of filter 12000/100
    static qint32 intFilLen = qint32(intN / 2);
    static double dblR  = 0.9995;  // insures stability (must be < 1.0) (Value .9995 7/8/2013 gives good results)
    static double dblRn = pow(dblR, double(intN));
    static double dblR2 = pow(dblR, 2.0);
    static QVector<double> dblCoef(20); // the coefficients

    AddTrailer(intNewSamples); // add the trailer before filtering

    qint32 intPeakSample = 0;
    double dblZin   = 0.0;
    double dblZin_1 = 0.0;
    double dblZin_2 = 0.0;
    double dblZComb = 0.0;  // Used in the comb generator
    // The resonators
    QVector<double> dblZout_0(20); // resonator outputs
    QVector<double> dblZout_1(20); // resonator outputs delayed one sample
    QVector<double> dblZout_2(20); // resonator outputs delayed two samples
    QVector<qint32> intFilteredSamples(intNewSamples->length() - 1); //  Filtered samples

#ifdef DEBUG_MOD
    QVector<double> dblUnfilteredSamples(intNewSamples.length() - 1); // for debug wave plotting
    QVector<double> dblFilteredSamples(intNewSamples.length() - 1); // for debug wave plotting
#endif

    // Initialize the coefficients
    if ( dblCoef.at(20) == 0 )
    {
        for ( quint8 i = 10; i < 20; ++i )
        {
            // For Frequency = bin i
            dblCoef[i] = 2 * dblR * cos(2 * M_PI * i / intN);
        } // Next i
    }

    try
    {
        for ( qint32 i = 0; i <  ((intNewSamples->length() + intFilLen) - 1); ++i )
        {
            if ( i < intN )
            {
                // dblUnfilteredSamples(i) = intNewSamples(i) ' debug code for waveform plotting.
                dblZin = intNewSamples->at(i);
            }
            else if ( i < intNewSamples->length() )
            {
                //dblUnfilteredSamples(i) = intNewSamples(i) ' debug code for waveform plotting.
                dblZin = intNewSamples->at(i) - dblRn * intNewSamples->at(i - intN);
            }
            else
            {
                dblZin = -dblRn * intNewSamples->at(i - intN);
            }
            // Compute the Comb
            dblZComb = dblZin - dblZin_2 * dblR2;
            dblZin_2 = dblZin_1;
            dblZin_1 = dblZin;

            // Now the resonators
            for ( quint8 j = 10; j < 20; ++j )
            {
                // calculate output for 11 resonators
                dblZout_0[j] = dblZComb + dblCoef.at(j) * dblZout_1.at(j) - dblR2 * dblZout_2.at(j);
                dblZout_2[j] = dblZout_1.at(j);
                dblZout_1[j] = dblZout_0.at(j);
                // scale each by transition coeff and
                // + (Even) or - (Odd)
                // Resonators 6 and 9 scaled by .15 to
                // get best shape and side lobe supression
                // to - 45 dB while keeping BW at
                // 500 Hz @ -26 dB
                // practical range of scaling .05 to .25
                // Scaling also accomodates for the
                // filter "gain" of approx 60.
                if ( i >= intFilLen )
                {
                    if ( (j == 10) || (j == 20) )
                        intFilteredSamples[i - intFilLen] += 0.377 * dblZout_0.at(j);
                    else if ( (j % 2) == 0 )
                        intFilteredSamples[i - intFilLen] += dblZout_0.at(j);
                    else
                        intFilteredSamples[i - intFilLen] -= dblZout_0.at(j);
                }
            } // Next j

            if ( i >= intFilLen )
            {
                // rescales for gain of filter
                intFilteredSamples[i - intFilLen] = intFilteredSamples.at(i - intFilLen) * 0.00833333333;
                if ( intFilteredSamples.at(i - intFilLen) > 32700 ) // Hard clip above 32700
                    intFilteredSamples[i - intFilLen] = 32700;
                else if ( intFilteredSamples.at(i - intFilLen) < -32700 )
                    intFilteredSamples[i - intFilLen] = -32700;

                //dblFilteredSamples(i - intFilLen) = intFilteredSamples(i - intFilLen) ' debug code
                if ( abs(intFilteredSamples.at(i - intFilLen)) > abs(intPeakSample) ) intPeakSample = intFilteredSamples.at(i - intFilLen);
            }
        } // Next i
#ifdef DEBUG_MOD
        // *********************************
        // Debug code to look at filter output
        //Dim objWT As New WaveTools
        //If IO.Directory.Exists(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav") = False Then
        //    IO.Directory.CreateDirectory(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav")
        //End If
        //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\UnFiltered" & strFilename, 12000, 16, dblUnfilteredSamples)
        //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\Filtered" & strFilename, 12000, 16, dblFilteredSamples)
        // End of debug code
        //************************************
#endif
    }
    catch (exception ex)
    {
        qCritical() << "Exception: " << ex.what();
        intFilteredSamples.clear();
    }
    return intFilteredSamples;
} // FSXmtFilter1000_1500Hz()

QVector<qint32> EncodeModulate::FSXmtFilter2000_1500Hz(QVector<qint32> *intNewSamples)
{
    static qint32 intN  = 120;     // Length of filter 12000/100
    static qint32 intFilLen = qint32(intN / 2);
    static double dblR  = 0.9995;  // insures stability (must be < 1.0) (Value .9995 7/8/2013 gives good results)
    static double dblRn = pow(dblR, double(intN));
    static double dblR2 = pow(dblR, 2.0);
    static QVector<double> dblCoef(25); // the coefficients

    AddTrailer(intNewSamples); // add the trailer before filtering

    qint32 intPeakSample = 0;
    double dblZin   = 0.0;
    double dblZin_1 = 0.0;
    double dblZin_2 = 0.0;
    double dblZComb = 0.0;  // Used in the comb generator
    // The resonators
    QVector<double> dblZout_0(25); // resonator outputs
    QVector<double> dblZout_1(25); // resonator outputs delayed one sample
    QVector<double> dblZout_2(25); // resonator outputs delayed two samples
    QVector<qint32> intFilteredSamples(intNewSamples->length() - 1); //  Filtered samples

#ifdef DEBUG_MOD
    QVector<double> dblUnfilteredSamples(intNewSamples.length() - 1); // for debug wave plotting
    QVector<double> dblFilteredSamples(intNewSamples.length() - 1); // for debug wave plotting
#endif

    // Initialize the coefficients
    if ( dblCoef.at(25) == 0 )
    {
        for ( quint8 i = 5; i < 25; ++i )
        {
            dblCoef[i] = 2 * dblR * cos(2 * M_PI * i / intN); // For Frequency = bin i
        } // Next i
    }

    try
    {
        for ( qint32 i = 0; i < ((intNewSamples->length() + intFilLen) - 1); ++i )
        {
            if ( i < intN )
            {
                //dblUnfilteredSamples(i) = intNewSamples(i) ' debug code for waveform plotting.
                dblZin = intNewSamples->at(i);
            }
            else if ( i < intNewSamples->length() )
            {
                //dblUnfilteredSamples(i) = intNewSamples(i) ' debug code for waveform plotting.
                dblZin = intNewSamples->at(i) - dblRn * intNewSamples->at(i - intN);
            }
            else
            {
                dblZin = -dblRn * intNewSamples->at(i - intN);
            }

            // Compute the Comb
            dblZComb = dblZin - dblZin_2 * dblR2;
            dblZin_2 = dblZin_1;
            dblZin_1 = dblZin;

            // Now the resonators
            for ( quint8 j = 5; j < 25; ++j )
            {
                // calculate output for 23 resonators
                dblZout_0[j] = dblZComb + dblCoef.at(j) * dblZout_1.at(j) - dblR2 * dblZout_2.at(j);
                dblZout_2[j] = dblZout_1.at(j);
                dblZout_1[j] = dblZout_0.at(j);
                // scale each by transition coeff and + (Even) or - (Odd)
                // Resonators 6 and 9 scaled by .15 to get best shape
                // and side lobe supression to - 45 dB while
                // keeping BW at 500 Hz @ -26 dB
                // practical range of scaling .05 to .25
                // Scaling also accomodates for the filter "gain"
                // of approx 60.
                if ( i >= intFilLen )
                {
                    if ( (j == 5) || (j == 25) )
                        intFilteredSamples[i - intFilLen] -= 0.371 * dblZout_0.at(j);
                    else if ( (j % 2) == 0 )
                        intFilteredSamples[i - intFilLen] += dblZout_0.at(j);
                    else
                        intFilteredSamples[i - intFilLen] -= dblZout_0.at(j);
                }
            } // Next j
            if ( i >= intFilLen )
            {
                intFilteredSamples[i - intFilLen] = intFilteredSamples.at(i - intFilLen) * 0.00833333333;  // rescales for gain of filter
                if ( intFilteredSamples.at(i - intFilLen) > 32700 ) // Hard clip above 32700
                    intFilteredSamples[i - intFilLen] = 32700;
                else if ( intFilteredSamples.at(i - intFilLen) < -32700 )
                    intFilteredSamples[i - intFilLen] = -32700;

                //dblFilteredSamples(i - intFilLen) = intFilteredSamples(i - intFilLen) ' debug code
                if ( abs(intFilteredSamples.at(i - intFilLen)) > abs(intPeakSample) ) intPeakSample = intFilteredSamples.at(i - intFilLen);
            }
        } // Next i
#ifdef DEBUG_MOD
        // *********************************
        // Debug code to look at filter output
        //Dim objWT As New WaveTools
        //If IO.Directory.Exists(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav") = False Then
        //    IO.Directory.CreateDirectory(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav")
        //End If
        //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\UnFiltered2000Hz.wav", 12000, 16, dblUnfilteredSamples)
        //objWT.WriteFloatingRIFF(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")) & "\Wav\Filtered2000Hz.wav", 12000, 16, dblFilteredSamples)
        // End of debug code
        //************************************
#endif
    }
    catch (exception ex)
    {
        qCritical() << "Exception: " << ex.what();
        intFilteredSamples.clear();
    }
    return intFilteredSamples;
}  // FSXmtFilter2000_1500Hz()


QVector<qint32> EncodeModulate::ModQAM(quint8 bytFrame, QByteArray bytEncodedData, qint32 intLeaderLen)
{
    bool    blnOdd             = false;
    quint8  bytSym             = 0;
    quint8  bytSymToSend       = 0;
    quint8  bytMask            = 0;
    quint16 intNumCar         = 0;
    quint32 intBaud           = 0;
    quint32 intDataLen        = 0;
    quint32 intRSLen          = 0;
    quint32 intSampleLen      = 0;
    qint32  intSamplePtr       = 0;
    qint32  intDataPtr         = 0;
    qint32  intSampPerSym      = 0;
    qint32  intDataBytesPerCar = 0;
    qint32  intCarIndex        = 0;
    qint32  intCarStartIndex   = 0;
    qint32  intPeakAmp         = 0;
    qint32  intMask            = 0;
    qint32  intLeaderLenMS     = 0;
    double  dblCarScalingFactor = 0.0;
    QString strType           = "";
    QString strMod            = "";
    QByteArray bytLastSym;
    bytLastSym.reserve(8);  // Holds the last symbol sent (per carrier). bytLastSym(4) is 1500 Hz carrier (only used on 1 carrier modes)

    QVector<qint32> intSamples;
    QVector<qint32> intLeader;

    Q_UNUSED(intMask)      // to suppress warnings in release build
    Q_UNUSED(intPeakAmp)

    try
    {
        bool Ok = objFrameInfo->frameInfo(bytFrame, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);
        if ( !Ok )
        {
            qCritical() << "[ModQAM] no data frame info for frame type H"
                        << QString("%1").arg(bytFrame);
            intSamples.clear();
            return intSamples;
        }

        objMain->strLastWavStream = strType;
        if ( intLeaderLen == 0 )
            intLeaderLenMS = MCB->LeaderLength;
        else
            intLeaderLenMS = intLeaderLen;

        switch ( intBaud )
        {
        case 100:
            intSampPerSym = 120;
            if ( strMod == "16QAM" )
            {
                // add 10 50 baud 4FSK symbols for
                // Frame Type + one extra intSamplePerSym
                // for initial reference phase
                intSampleLen = intLeaderLenMS * 12 + 10 * 240 + intSampPerSym + intSampPerSym * (2 * (intDataLen + intRSLen + 3));
            }
        }

        switch ( intNumCar )
        {
        case 1:
            intCarStartIndex = 4;
            // Starting at 1500 Hz  (scaling factors
            // determined emperically to minimize crest factor)  TODO:  needs verification
            dblCarScalingFactor = 1.0;
            break;
        case 2:
            intCarStartIndex = 3;
            // Carriers at 1400 and 1600
            dblCarScalingFactor = 0.53;
            break;
        }

        intDataBytesPerCar = qint32((bytEncodedData.length() - 2) / intNumCar);
        intSamples.resize(intSampleLen);
        intLeader = GenTwoToneLeaderWithSync(intLeaderLenMS / 20);
        // copy the leader + sync
        VectorCopy2(&intLeader, 0, &intSamples, 0, intLeader.length());
        intSamplePtr = intLeader.length();
        // Create the 10 symbols (20 bit) 50 baud 4FSK
        // frame type with Implied SessionID and 2 symbol parity
        // No reference needed for 4FSK
        for ( quint8 j = 0; j < 2; ++j )
        {
            // for the 2 bytes of the frame type
            bytMask = 0xC0;
            for ( quint8 k = 0; k < 5; ++ k )
            {
                // for 5 symbols per byte (4 data + 1 parity)
                if ( k < 4 )
                    bytSymToSend = (bytMask & bytEncodedData.at(j)) >> (2 * (3 - k));
                else
                    bytSymToSend = objFrameInfo->ComputeTypeParity(bytEncodedData.at(0));

                for ( quint16 n = 0; n < 240; ++n )
                {
                    if ( ((5 * j + k) % 2) == 0 )
                    {
                        int x = intSamplePtr + n;
                        intSamples[x] = intFSK50bdCarTemplate[bytSymToSend].at(n);
                    }
                    else
                    {
                        // -sign insures no phase discontinuity
                        // at symbol boundaries
                        intSamples[intSamplePtr + n] = -intFSK50bdCarTemplate[bytSymToSend].at(n);
                    }
                } // Next n
                intSamplePtr += 240;
                bytMask = bytMask >> 2;
            } // Next k
        } // Next j

        // NPTE: 16QAM uses same PSK template as
        // 8PKS with amplitude scaling for symbol
        // values 8 through 15
        intPeakAmp = 0;
        // initialize to correct starting carrier
        intCarIndex = intCarStartIndex;
        // Now create a reference symbol for each carrier
        for ( quint8 i = 0; i < intNumCar; ++ i )
        {
            // across all carriers
            // using non 0 causes error on first
            // data byte 12/8/2014...Values 0-3
            // not important (carries no data).
            // (Possible chance for Crest Factor reduction?)
            bytSymToSend = 0;
            bytLastSym[i] = bytSymToSend;
            for ( quint16 n = 0; n < intSampPerSym; ++n )
            {
                // Sum for all the samples of a symbol
                if ( intBaud == 100 )
                {
                    // positive phase values template lookup
                    // for 8PSK.
                    int x = intSamplePtr + n;
                    intSamples[x] += intPSK100bdCarTemplate[intCarIndex][bytSymToSend].at(n);
                    if ( i == (quint8)(intNumCar - 1) )
                    {
                        // on the last carrier rescale value
                        // based on # of carriers to bound output
                        intSamples[x] = intSamples.at(intSamplePtr + n) * dblCarScalingFactor;
                    }
                }
            } // Next n
            ++intCarIndex;
            // skip over 1500 Hz for multi carrier modes
            // (multi carrier modes all use even
            // hundred Hz tones)
            if ( intCarIndex == 4 ) ++intCarIndex;
        } // Next i
        intSamplePtr += intSampPerSym;
        // End of reference phase generation

        // initialize pointer to start of data.
        intDataPtr = 2;
        if ( strMod == "16QAM" )
        {
            for ( quint32 m = 0; m < (quint32)intDataBytesPerCar; ++m )
            {
                // For each byte of input data (all carriers)
                bytMask = 0xF0; // Initialize mask each new data byte
                for ( quint8 k = 0; k < 2; ++k )
                {
                    // for 2 symbol values per byte of data
                    intCarIndex = intCarStartIndex; // initialize the carrrier index
                    for ( quint8 i = 0; i < intNumCar; ++i )
                    {
                        // across all carriers
                        bytSym = (bytMask & bytEncodedData.at(intDataPtr + i * intDataBytesPerCar)) >> (4 * (1 - k));
                        bytSymToSend = (bytLastSym.at(intCarIndex) + ((bytSym & 0x07)) % 8);   // Values 0-7
                        for ( quint32 n = 0; n < (quint32)intSampPerSym; ++n )
                        {
                            // Sum for all the samples of a symbols
                            if ( intBaud == 100 )
                            {
                                if ( bytSym < 8 )
                                {
                                    if ( bytSymToSend < 4 )
                                    {
                                        // This uses the symmetry of
                                        // the symbols to reduce the
                                        // table size by a factor of 2

                                        // positive phase values template lookup for 8PSK.
                                        intSamples[intSamplePtr + n] += intPSK100bdCarTemplate[intCarIndex][bytSymToSend].at(n);
                                    }
                                    else
                                    {
                                        // negative phase values,
                                        // subtract value of table
                                        intSamples[intSamplePtr + n] -= intPSK100bdCarTemplate[intCarIndex][bytSymToSend - 4].at(n);
                                    }
                                }
                                else
                                {
                                    if ( bytSymToSend < 4 )
                                    {
                                        // This uses the symmetry of the symbols to reduce the table size by a factor of 2
                                        // positive phase values template lookup for 8PSK.
                                        intSamples[intSamplePtr + n] += 0.5 * intPSK100bdCarTemplate[intCarIndex][bytSymToSend].at(n);
                                    }
                                    else
                                    {
                                        // negative phase values,
                                        // subtract value of table
                                        intSamples[intSamplePtr + n] -= 0.5 * intPSK100bdCarTemplate[intCarIndex][bytSymToSend - 4].at(n);
                                    }
                                }
                                if ( i == (intNumCar - 1) )
                                {
                                    // on the last carrier rescale value
                                    // based on # of carriers to bound output
                                    intSamples[intSamplePtr + n] = intSamples.at(intSamplePtr + n) * dblCarScalingFactor;
                                }
                            }
                        } // Next n
                        bytLastSym[intCarIndex] = bytSymToSend;
                        ++intCarIndex;
                        // skip over 1500 Hz for multi carrier
                        // modes (multi carrier modes all use
                        // even hundred Hz tones)
                        if ( intCarIndex == 4 ) ++intCarIndex;
                    } // Next i
                    bytMask = bytMask >> 4;
                    intSamplePtr += intSampPerSym;
                } // Next k
                ++intDataPtr;
            } // Next m
        }

        // Xmit filter per bandwidth
        switch ( intNumCar )
        {
        case 1:
            intSamples = FSXmtFilter200_1500Hz(&intSamples);
            break;
        case 2:
            intSamples = FSXmtFilter500_1500Hz(&intSamples);
            break;
        }
        return intSamples;
    }
    catch (exception ex)
    {
        qCritical() << "bytFrame= H" << bytFrame
                    << "  DataLen="  << QString().arg(bytEncodedData.length())
                    << "  Err: "     << ex.what();
        intSamples.clear();
    }
    return intSamples;
} // ModQAM()

// ------END OF PUBLIC METHODS AND FUNCTIONS -------


// -------------------------------------------------
//           PRIVATE METHODS AND FUNCTIONS
// -------------------------------------------------

quint8 EncodeModulate::GetSym8PSK(qint32 intDataPtr, qint32 k, qint32 intCar, QByteArray *bytEncodedData, qint32 intDataBytesPerCar)
{
    quint8 bytSym    = 0;
    qint32 int3Bytes = bytEncodedData->at(intDataPtr + intCar * intDataBytesPerCar);
    qint32 intMask = 0x07;

    int3Bytes = int3Bytes << 8;
    int3Bytes += bytEncodedData->at(intDataPtr + intCar * intDataBytesPerCar + 1);
    int3Bytes = int3Bytes << 8;
    // now have 3 bytes, 24 bits or 8 8PSK symbols
    int3Bytes += bytEncodedData->at(intDataPtr + intCar * intDataBytesPerCar + 2);
    intMask = intMask << (3 * (7 - k));
    bytSym = (intMask & int3Bytes) >> (3 * (7 - k));
    return bytSym;
} // GetSym8PSK


QVector<qint32> EncodeModulate::ModPSK(quint8 bytFrame, QByteArray bytEncodedData, qint32 intLeaderLen)
{
    bool    blnOdd        = false;
    quint8  bytSym        = 0;
    quint8  bytSymToSend  = 0;
    quint8  bytMask       = 0;
    quint16 intNumCar     = 0;
    quint32 intBaud       = 0;
    quint32 intDataLen    = 0;
    quint32 intRSLen      = 0;
    quint32 intSampleLen  = 0;
    qint32  intSamplePtr  = 0;
    qint32  intDataPtr    = 0;
    qint32  intSampPerSym = 0;
    qint32  intDataBytesPerCar = 0;
    qint32  intCarIndex   = 0;
    qint32  intCarStartIndex = 0;
    qint32  intPeakAmp    = 0;
    qint32  intMask       = 0;
    qint32  intLeaderLenMS = 0;
    QVector<qint32> intSamples;
    QVector<qint32> intLeader;
    double  dblCarScalingFactor = 0.0;
    QString strType       = "";
    QString strMod        = "";
    QByteArray bytLastSym;
    bytLastSym.resize(8); // Holds the last symbol sent (per carrier). bytLastSym(4) is 1500 Hz carrier (only used on 1 carrier modes)

    bytLastSym.clear();

    Q_UNUSED(intPeakAmp)   // to suppress warnings in release build

    try
    {
        if ( !objFrameInfo->frameInfo(bytFrame, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType) )
        {
            qCritical() << "No data frame info for frame type H" << bytFrame;
            intSamples.clear();
            return intSamples;
        }

        objMain->strLastWavStream = strType;
        if ( intLeaderLen == 0 )
            intLeaderLenMS = MCB->LeaderLength;
        else
            intLeaderLenMS = intLeaderLen;

        switch ( intBaud )
        {
        case 100:
            intSampPerSym = 120;
            if ( strMod == "4PSK" )
            {
                // add 10 50 baud 4FSK symbols for Frame Type +
                // one extra intSamplePerSym for initial
                // reference phase
                intSampleLen = intLeaderLenMS * 12 + 10 * 240 + intSampPerSym + intSampPerSym * (4 * (intDataLen + intRSLen + 3));
            }
            else if ( strMod == "8PSK" )
            {
                // add 10 50 baud 4FSK symbols for
                // Frame Type + one extra intSamplePerSym
                // reference phase
                intSampleLen = intLeaderLenMS * 12 + 10 * 240 + intSampPerSym + intSampPerSym * ((8 * (intDataLen + intRSLen + 3)) / 3);
            }

        case 167:
            intSampPerSym = 72; // the total number of samples per symbol
            if ( strMod == "4PSK" )
            {
                // add 10 50 baud 4FSK symbols for Frame Type +
                // one extra intSamplePerSym for initial
                // reference phase
                intSampleLen = intLeaderLenMS * 12 + 10 * 240 + intSampPerSym + intSampPerSym * (4 * (intDataLen + intRSLen + 3));
            }
            else if ( strMod == "8PSK" )
            {
                // add 10 50 baud 4FSK symbols for Frame Type +
                // one extra intSamplePerSym reference phase
                intSampleLen = intLeaderLenMS * 12 + 10 * 240 + intSampPerSym + +intSampPerSym * ((8 * (intDataLen + intRSLen + 3)) / 3);
            }
        }

        switch (intNumCar)
        {
        case 1:
            intCarStartIndex = 4;
            dblCarScalingFactor = 1.0; // Starting at 1500 Hz  (scaling factors determined emperically to minimize crest factor)  TODO:  needs verification
        case 2:
            intCarStartIndex = 3;
            dblCarScalingFactor = 0.53; // Starting at 1400 Hz
        case 4:
            intCarStartIndex = 2;
            dblCarScalingFactor = 0.29; // Starting at 1200 Hz
        case 8:
            intCarStartIndex = 0;
            dblCarScalingFactor = 0.17; // Starting at 800 Hz
        }

        intDataBytesPerCar = (bytEncodedData.length() - 2) / intNumCar;
        intSamples.resize(intSampleLen);
        intLeader = GenTwoToneLeaderWithSync(intLeaderLenMS / 20);
        VectorCopy2(&intLeader, 0, &intSamples, 0, intLeader.length()); //copy the leader + sync
        intSamplePtr = intLeader.length();

        // Create the 10 symbols (20 bit) 50 baud 4FSK frame type
        // with Implied SessionID and 2 symbol parity
        // No reference needed for 4FSK
        for ( quint8 j = 0; j < 1; ++j )
        {
            // for the 2 bytes of the frame type
            bytMask = 0xC0;
            for ( quint8 k = 0; k < 4; ++k )
            {
                // for 5 symbols per byte (4 data + 1 parity)
                if ( k < 4 )
                    bytSymToSend = (bytMask && bytEncodedData.at(j)) >> (2 * (3 - k));
                else
                    bytSymToSend = objFrameInfo->ComputeTypeParity(bytEncodedData.at(0));

                for ( quint32 n = 0; n < 239; ++n )
                {
                    if ( ((5 * j + k) % 2) == 0 )
                        intSamples[intSamplePtr + n] = intFSK50bdCarTemplate[bytSymToSend].at(n);
                    else
                        intSamples[intSamplePtr + n] = -intFSK50bdCarTemplate[bytSymToSend].at(n); // -sign insures no phase discontinuity at symbol boundaries
                }  // Next n
                intSamplePtr += 240;
                bytMask = bytMask >> 2;
            } // Next k
        } // Next j

        intPeakAmp = 0;
        // initialize to correct starting carrier
        intCarIndex = intCarStartIndex;
        // Now create a reference symbol for each carrier
        for ( quint8 i = 0; i < (intNumCar - 1); ++i )
        {
            // across all carriers
            bytSymToSend = 0;  //  using non 0 causes error on first data byte 12/8/2014   ...Values 0-3  not important (carries no data).   (Possible chance for Crest Factor reduction?)
            bytLastSym[i] = bytSymToSend;
            for ( qint32 n = 0; n < intSampPerSym; ++n )
            {
                // Sum for all the samples of a symbols
                if ( bytSymToSend < 2 )
                    intSamples[intSamplePtr + n] += intPSK100bdCarTemplate[intCarIndex][bytSymToSend * 2].at(n); // double the symbol value during template lookup for 4PSK. (skips over odd PSK 8 symbols)
                else
                    intSamples[intSamplePtr + n] -= intPSK100bdCarTemplate[intCarIndex][bytSymToSend * 2].at(n); // subtract 2 from the symbol value before doubling and subtract value of table

                if ( i == (intNumCar - 1) )
                    intSamples[intSamplePtr + n] = intSamples.at((intSamplePtr + n) * dblCarScalingFactor); // on the last carrier rescale value based on # of carriers to bound output

            } // Next n
            intCarIndex += 1;
            if ( intCarIndex == 4 ) intCarIndex += 1; // skip over 1500 Hz for multi carrier modes (multi carrier modes all use even hundred Hz tones)
        } // Next i
        intSamplePtr += intSampPerSym;
        // End of reference phase generation

        intDataPtr = 2; // initialize pointer to start of data.
        // NOTE: originally a switch case but not allowed for strings in Qt
        if ( strMod == "4PSK" )
        {
            for ( qint32 m = 0; m < intDataBytesPerCar; ++m )
            {
                // For each byte of input data (all carriers)
                bytMask = 0xC0; // Initialize mask each new data byte

                for ( quint8 k = 0; k < 3; ++k )
                {
                    // for 4 symbol values per byte of data
                    intCarIndex = intCarStartIndex; // initialize the carrrier index
                    for ( qint32 i = 0; i < intNumCar; ++i )
                    {
                        // across all carriers
                        bytSym = (bytMask & bytEncodedData.at(intDataPtr + i * intDataBytesPerCar)) >> (2 * (3 - k));
                        bytSymToSend = ((bytLastSym.at(intCarIndex) + bytSym) % 4); // Values 0-3

                        for (qint32 n = 0; n < intSampPerSym; ++n )
                        {
                            // Sum for all the samples of a symbols
                            if ( bytSymToSend < 2 )
                            {
                                // This uses the symmetry of the
                                // symbols to reduce the table
                                // size by a factor of 2

                                // double the symbol value during
                                // template lookup for 4PSK.
                                // (skips over odd PSK 8 symbols)
                                intSamples[intSamplePtr + n] += intPSK100bdCarTemplate[intCarIndex][2 * bytSymToSend].at(n);
                            }
                            else
                            {
                                // subtract 2 from the symbol value
                                // before doubling and subtract
                                // value of table
                                intSamples[intSamplePtr + n] -= intPSK100bdCarTemplate[intCarIndex][2 * (bytSymToSend - 2)].at(n);
                            }

                            // Need to examine clipping
                            // and scaling here
                            if ( i == (intNumCar - 1) ) intSamples[intSamplePtr + n] = intSamples.at(intSamplePtr + n) * dblCarScalingFactor;
                        } // Next n

                        bytLastSym[intCarIndex] = bytSymToSend;
                        intCarIndex += 1;
                        if (intCarIndex == 4 ) ++intCarIndex; // skip over 1500 Hz for multi carrier modes (multi carrier modes all use even hundred Hz tones)
                    } // Next i
                    bytMask = bytMask >> 2;
                    intSamplePtr += intSampPerSym;
                } // Next k
                ++intDataPtr;
            } // Next m
        }
        else if ( strMod == "8PSK")
        {
            // More complex ...must go through data
            // in 3 byte chunks creating 8 Three bit
            // symbols for each 3 bytes of data.
            for ( qint32 m = 0; m < ((intDataBytesPerCar / 3) - 1); ++ m )
            {
                for ( quint8 k = 0; k < 7; ++k )
                {
                    // for 8 symbols in 24 bits of int3Bytes
                    intCarIndex = intCarStartIndex; // initialize the carrrier index
                    for ( qint32 i = 0; i < (intNumCar - 1); ++i )
                    {
                        bytSym = GetSym8PSK(intDataPtr, k, i, &bytEncodedData, intDataBytesPerCar);
                        bytSymToSend = ((bytLastSym.at(intCarIndex) + bytSym) % 8);

                        for ( qint32 n = 0; n < intSampPerSym; ++n )
                        {
                            // Sum for all the samples of a symbol
                            if ( bytSymToSend < 4 )
                            {
                                //This uses the symmetry of the symbols to reduce the table size by a factor of 2
                                // positive phase values template lookup for 8PSK.
                                intSamples[intSamplePtr + n] += intPSK100bdCarTemplate[intCarIndex][bytSymToSend].at(n);
                            }
                            else
                            {
                                intSamples[intSamplePtr + n] -= intPSK100bdCarTemplate[intCarIndex][bytSymToSend - 4].at(n); // negative phase values,  subtract value of table
                            }

                            if ( i == (intNumCar - 1) )
                                intSamples[intSamplePtr + n] = intSamples.at(intSamplePtr + n) * dblCarScalingFactor; // Need to examine clipping and scaling here

                        } // Next n
                        bytLastSym[intCarIndex] = bytSymToSend;
                        ++intCarIndex;
                        if ( intCarIndex == 4 ) ++intCarIndex; // skip over 1500 Hz for multi carrier modes (multi carrier modes all use even hundred Hz tones)
                    } // Next i
                    intMask = intMask >> 3;
                    intSamplePtr += intSampPerSym;
                } // Next k
                intDataPtr += 3;
           } // Next m
        }

        // Xmit filter per bandwidth
        switch (intNumCar)
        {
        case 1:
            intSamples = FSXmtFilter200_1500Hz(&intSamples);
        case 2:
            intSamples = FSXmtFilter500_1500Hz(&intSamples);
        case 4:
            intSamples = FSXmtFilter1000_1500Hz(&intSamples);
        case 8:
            intSamples = FSXmtFilter2000_1500Hz(&intSamples);
        }
        return intSamples;
    }
    catch (exception ex)
    {
        qCritical() << "bytFrame= H"
                    << bytFrame
                    << "  DataLen="
                    << QString("%1").arg(bytEncodedData.length())
                    << "  Err: " << ex.what();
        intSamples.clear();
    }
    return intSamples;
} // ModPSK()


QVector<qint32> EncodeModulate::Mod4FSKData(quint8 bytFrame, QByteArray bytEncodedData, qint32 intLeaderLen)
{
    bool    blnOdd             = false;
    quint8  bytSymToSend       = 0;
    quint8  bytMask            = 0;
    quint16 intNumCar          = 0;
    quint32 intBaud            = 0;
    quint32 intDataLen         = 0;
    quint32 intRSLen           = 0;
    quint32 intSampleLen       = 0;
    qint32  intMask            = 0;
    qint32  intSamplePtr       = 0;
    qint32  intDataPtr         = 0;
    qint32  intSampPerSym      = 0;
    qint32  intDataBytesPerCar = 0;
    qint32  intLeaderLenMS     = 0;
    double  dblCarScalingFactor = 0.0;
    QString strType = "";
    QString strMod = "";
    QByteArray bytLastSym;
    bytLastSym.resize(8); // Holds the last symbol sent (per carrier). bytLastSym(4) is 1500 Hz carrier (only used on 1 carrier modes)
    QVector<qint32> intLeader;
    QVector<qint32> intSamples;

    Q_UNUSED(intMask)   // to suppress warnings in release build


    try
    {
        if ( !objFrameInfo->frameInfo(bytFrame, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType) )
        {
            qCritical() << "No data for frame type H" << bytFrame;
            intSamples.clear();
            return intSamples;
        }

        if ( strMod.trimmed() != "4FSK" )
        {
            qCritical() << "Frame Type: H" << bytFrame << " is not 4FSK";
            intSamples.clear();
            return intSamples;
        }

        if ( !(strType.trimmed() == "DataACK") ||
              (strType.trimmed() == "DataNAK") ||
              (strType.trimmed() == "IDFrame") ||
              (strType.startsWith("ConReq"))   ||
              (strType.startsWith("ConAck")) )
        {
            objMain->strLastWavStream = strType;
        }

        if ( intLeaderLen == 0 )
            intLeaderLenMS = MCB->LeaderLength;
        else
            intLeaderLenMS = intLeaderLen;

        intLeader.clear();

        switch (intBaud)
        {
        case 50:
            intSampPerSym = 240;
            // For FEC transmission the computed
            // leader length = MCB.Leader length
            intSampleLen = intLeaderLenMS * 12 + 240 * 10 + intSampPerSym * (4 * (bytEncodedData.length() - 2) / intNumCar);
            intLeader = GenTwoToneLeaderWithSync(intLeaderLenMS / 20);
            break;
        case 100:
            intSampPerSym = 120;
            intSampleLen = intLeaderLenMS * 12 + 240 * 10 + intSampPerSym * (4 * (bytEncodedData.length() - 2) / intNumCar);
            intLeader = GenTwoToneLeaderWithSync(intLeaderLenMS / 20);
            break;
        }

        // Create the leader
        intSamples.resize(intSampleLen - 1);
        intDataBytesPerCar = (bytEncodedData.length() - 2) / intNumCar;
        VectorCopy2(&intLeader, 0, &intSamples, 0, intLeader.length()); //copy the leader + sync
        intSamplePtr = intLeader.length();
        // Create the 8 symbols (16 bit) 50 baud 4FSK
        // frame type with Implied SessionID
        // No reference needed for 4FSK

        // NOTE: revised To accomodate 1 parity symbol per byte
        // (10 symbols total)
        for ( quint8 j = 0; j <= 1; ++j )
        {
            // for the 2 bytes of the frame type
            bytMask = 0xC0;
            for ( quint8 k = 0; k <= 4; ++k )
            {
                // for 5 symbols per byte (4 data + 1 parity)
                if ( k < 4 )
                    bytSymToSend = (bytMask & bytEncodedData.at(j)) >> (2 * (3 - k));
                else
                    bytSymToSend = objFrameInfo->ComputeTypeParity(bytEncodedData.at(0));

                for ( quint16 n = 0; n <= 239; ++n )
                {
                    if ( ((5 * j + k) % 2) == 0 )
                        intSamples[intSamplePtr + n] = intFSK50bdCarTemplate[bytSymToSend].at(n);
                    else
                        intSamples[intSamplePtr + n] = -intFSK50bdCarTemplate[bytSymToSend].at(n); // -sign insures no phase discontinuity at symbol boundaries
                }  // Next n
                intSamplePtr += 240;
                bytMask = bytMask >> 2;
            } // Next k
        } // Next j

        // No reference needed for 4FSK
        intDataPtr = 2;
        switch ( intNumCar )
        {
        case 1: // use carriers 0-4
            dblCarScalingFactor = 1.0; //  (scaling factors determined emperically to minimize crest factor)
            for ( qint32 m = 0; m < (intDataBytesPerCar - 1); ++m )
            {
                // For each byte of input data
                bytMask = 0xC0; // Initialize mask each new data byte
                for ( quint8 k = 0; k <= 3; ++k )
                {
                    // for 4 symbol values per byte of data
                    bytSymToSend = (bytMask & bytEncodedData.at(intDataPtr)) >> (2 * (3 - k)); // Values 0-3
                    for ( qint32 n = 0; n < (intSampPerSym - 1); ++n )
                    {
                        // Sum for all the samples of a symbols
                        if ((k % 2) == 0)
                        {
                            if ( intBaud == 50 )
                                intSamples[intSamplePtr + n] = intFSK50bdCarTemplate[bytSymToSend].at(n);
                            else
                                intSamples[intSamplePtr + n] = intFSK100bdCarTemplate[bytSymToSend].at(n);
                        }
                        else
                        {
                            if ( intBaud == 50 )
                                intSamples[intSamplePtr + n] = -intFSK50bdCarTemplate[bytSymToSend].at(n);
                            else
                                intSamples[intSamplePtr + n] = -intFSK100bdCarTemplate[bytSymToSend].at(n);
                        }
                    } // Next n
                    bytMask = bytMask >> 2;
                    intSamplePtr += intSampPerSym;
                } // Next k
                ++intDataPtr;
            } // Next m

            if ( intBaud == 50 )
                intSamples = FSXmtFilter200_1500Hz(&intSamples);
            else if ( intBaud == 100 )
                intSamples = FSXmtFilter500_1500Hz(&intSamples);
            break;

        case 2:
            // use carriers 8-15 (100 baud only)
            dblCarScalingFactor = 0.51; //  (scaling factors determined emperically to minimize crest factor)
            for ( quint32 m = 0; m < (quint32)intDataBytesPerCar; ++m )
            {
                // For each byte of input data
                bytMask = 0xC0; // Initialize mask each new data byte
                for ( quint8 k = 0; k < 4; ++k )
                {
                    // for 4 symbol values per byte of data
                    for ( quint32 n = 0; n < (quint32)intSampPerSym; ++n )
                    {
                        // for all the samples of a symbol
                        // for 2 carriers
                        //    First carrier
                        //      Values 0-3
                        bytSymToSend = (bytMask & bytEncodedData.at(intDataPtr)) >> (2 * (3 - k));
                        intSamples[intSamplePtr + n] = intFSK100bdCarTemplate[8 + bytSymToSend].at(n);
                        //    Second carrier
                        //      Values 0-3
                        bytSymToSend = (bytMask & bytEncodedData.at(intDataPtr + intDataBytesPerCar)) >> (2 * (3 - k));
                        intSamples[intSamplePtr + n] = dblCarScalingFactor * (intSamples.at(intSamplePtr + n) + intFSK100bdCarTemplate[12 + bytSymToSend].at(n));
                    } // Next n
                    bytMask = bytMask >> 2;
                    intSamplePtr += intSampPerSym;
                } // Next k
                ++intDataPtr;
            } // Next m
            intSamples = FSXmtFilter1000_1500Hz(&intSamples);
            break;

        case 4:
            // use carriers 4-19 (100 baud only)
            dblCarScalingFactor = 0.27; //  (scaling factors determined emperically to minimize crest factor)
            for ( quint32 m = 0; m < (quint32)intDataBytesPerCar; ++m )
            {
                // For each byte of input data
                bytMask = 0xC0; // Initialize mask each new data byte
                for ( quint8 k = 0; k < 4; ++k )
                {
                    // for 4 symbol values per byte of data
                    for ( quint32 n = 0; n < (quint32)intSampPerSym; ++n )
                    {
                        // for all the samples of a symbol
                        // for 4 carriers
                        //    First carrier
                        bytSymToSend = (bytMask & bytEncodedData.at(intDataPtr)) >> (2 * (3 - k)); // Values 0-3
                        intSamples[intSamplePtr + n] = intFSK100bdCarTemplate[4 + bytSymToSend].at(n);
                        //    Second carrier
                        bytSymToSend = (bytMask & bytEncodedData.at(intDataPtr + intDataBytesPerCar)) >> (2 * (3 - k)); // Values 0-3
                        intSamples[intSamplePtr + n] = intSamples.at(intSamplePtr + n) + intFSK100bdCarTemplate[8 + bytSymToSend].at(n);
                        //   Third carrier
                        bytSymToSend = (bytMask & bytEncodedData.at(intDataPtr + (2 * intDataBytesPerCar))) >> (2 * (3 - k)); // Values 0-3
                        intSamples[intSamplePtr + n] = intSamples.at(intSamplePtr + n) + intFSK100bdCarTemplate[12 + bytSymToSend].at(n);
                        //   Fourth carrier
                        bytSymToSend = (bytMask & bytEncodedData.at(intDataPtr + (3 * intDataBytesPerCar))) >> (2 * (3 - k)); // Values 0-3
                        intSamples[intSamplePtr + n] = dblCarScalingFactor * (intSamples.at(intSamplePtr + n) + intFSK100bdCarTemplate[16 + bytSymToSend].at(n));
                    } // Next n
                    bytMask = bytMask >> 2;
                    intSamplePtr += intSampPerSym;
                } // Next k
                ++intDataPtr;
            } // Next m
            intSamples = FSXmtFilter2000_1500Hz(&intSamples);
        }
        return intSamples;
    }
    catch (exception ex)
    {
        qCritical() << "bytFrame= H" << bytFrame
                    << "  DataLen="  << QString("%1").arg(bytEncodedData.length())
                    << "  Err: " << ex.what();
        intSamples.clear();
    }
    return intSamples;
} // Mod4FSKData()


QVector<qint32> EncodeModulate::Mod4FSK600BdData(quint8 bytFrame, QByteArray bytEncodedData, qint32 intLeaderLen)
{
    bool    blnOdd             = false;
    quint8  bytSymToSend       = 0;
    quint8  bytMask            = 0;
    quint16 intNumCar          = 0;
    quint32 intBaud            = 0;
    quint32 intDataLen         = 0;
    quint32 intRSLen           = 0;
    qint32  intSampleLen       = 0;
    qint32  intSamplePtr       = 0;
    qint32  intDataPtr         = 0;
    qint32  intSampPerSym      = 0;
    qint32  intDataBytesPerCar = 0;
    qint32  intMask            = 0;
    qint32  intLeaderLenMS     = 0;
    QString strType            = "";
    QString strMod             = "";
    QVector<qint32> intLeader;
    QVector<qint32> intSamples;

    Q_UNUSED(intMask) // to suppress warnings in release build

    try
    {
        if ( !objFrameInfo->frameInfo(bytFrame, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType) )
        {
            qCritical() << "No data for frame type H" << bytFrame;
            intSamples.clear();
            return intSamples;
        }
        objMain->strLastWavStream = strType;
        if ( intLeaderLen == 0 )
            intLeaderLenMS = MCB->LeaderLength;
        else
            intLeaderLenMS = intLeaderLen;

        intLeader.clear();

        intSampPerSym = 12000 / intBaud;
        // For FEC transmission the computed
        // leader length = MCB.Leader length
        intSampleLen = intLeaderLenMS * 12 + 240 * 10 + intSampPerSym * (4 * (bytEncodedData.length() - 2));
        intLeader = GenTwoToneLeaderWithSync(qint32(intLeaderLenMS / 20));

        // Create the leader
        intDataBytesPerCar = qint32((bytEncodedData.length() - 2) / intNumCar);
        intSamples.resize(intSampleLen - 1);
        VectorCopy2(&intLeader, 0, &intSamples, 0, intLeader.length()); //copy the leader + sync
        intSamplePtr = intLeader.length();

        // Create the 10symbols (16 bit) 50 baud 4FSK frame type
        // with Implied SessionID
        // No reference needed for 4FSK
        // NOTE: revised To accomodate 1 parity symbol per byte
        // (10 symbols total)
        for ( quint8 j = 0; j <= 1; ++ j )
        {
            // for the 2 bytes of the frame type
            bytMask = 0xC0;
            for ( quint8 k = 0; k <= 4; ++k )
            {
                // for 5 symbols per byte (4 data + 1 parity)
                if ( k < 4 )
                    bytSymToSend = (bytMask & bytEncodedData.at(j)) >> (2 * (3 - k));
                else
                    bytSymToSend = objFrameInfo->ComputeTypeParity(bytEncodedData.at(0));

                for ( quint16 n = 0; n < 239; ++n )
                {
                    if ( ((5 * j + k) % 2) == 0)
                        intSamples[intSamplePtr + n] = intFSK50bdCarTemplate[bytSymToSend].at(n);
                    else
                        intSamples[intSamplePtr + n] = -intFSK50bdCarTemplate[bytSymToSend].at(n); // -sign insures no phase discontinuity at symbol boundaries
                } // Next n
                intSamplePtr += 240;
                bytMask = bytMask >> 2;
            } // Next k
        } // Next j

        //  No reference needed for 4FSK (will have to expand
        // to hande 3 sets of 200 data bytes each (to accomodate
        // RS limitation of 254 max bytes)
        intDataPtr = 2;
        for ( qint32 m = 0; m < (intDataBytesPerCar - 1); ++m )
        {
            // For each byte of input data
            bytMask = 0xC0; // Initialize mask each new data byte
            for ( quint8 k = 0; k <= 3; ++k )
            {
                // for 4 symbol values per byte of data
                bytSymToSend = (bytMask & bytEncodedData.at(intDataPtr)) >> (2 * (3 - k)); // Values 0 - 15
                for ( qint32 n = 0; n < intSampPerSym; ++n )
                {
                    // Sum for all the samples of a symbols
                    intSamples[intSamplePtr + n] = intFSK600bdCarTemplate[bytSymToSend].at(n);
                } //Next n
                bytMask = bytMask >> 2;
                intSamplePtr += intSampPerSym;
            } // Next k
            ++intDataPtr;
        } // Next m
        intSamples = FSXmtFilter2000_1500Hz(&intSamples);
        return intSamples;
    }
    catch (exception ex)
    {
        qCritical() << "bytFrame= H" << bytFrame
                    << "  DataLen="  << QString().arg(bytEncodedData.length())
                    << "  Err: " << ex.what();
        intSamples.clear();
    }
    return intSamples;
} // Mod4FSK600BdData()


QVector<qint32> EncodeModulate::Mod16FSKData(quint8 bytFrame, QByteArray bytEncodedData, qint32 intLeaderLen)
{
    bool    blnOdd = false;
    quint8  bytSymToSend       = 0;
    quint8  bytMask            = 0;
    quint16 intNumCar          = 0;
    quint32 intBaud            = 0;
    quint32 intDataLen         = 0;
    quint32 intRSLen           = 0;
    quint32 intSampleLen       = 0;
    qint32  intSamplePtr       = 0;
    qint32  intDataPtr         = 0;
    qint32  intSampPerSym      = 0;
    qint32  intDataBytesPerCar = 0;
    qint32  intMask            = 0;
    qint32  intLeaderLenMS     = 0;
    QVector<qint32> intLeader;
    QVector<qint32> intSamples;
    QString strType           = "";
    QString strMod            = "";

    Q_UNUSED(intMask)  // to suppress warning in release build

    try
    {
        if ( !objFrameInfo->frameInfo(bytFrame, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType) )
        {
            qCritical() << "No data for frame type H"
                        << bytFrame;
            intSamples.clear();
            return intSamples;
        }

        if ( strMod != "16FSK" )
        {
            qCritical() << "Frame Type: H"
                        << bytFrame << " is not 16FSK";
            intSamples.clear();
            return intSamples;
        }
        objMain->strLastWavStream = strType;
        if ( intLeaderLen == 0 )
            intLeaderLenMS = MCB->LeaderLength;
        else
            intLeaderLenMS = intLeaderLen;

        intLeader.clear();
        intSamples.clear();
        intSampPerSym = 480;
        // For FEC transmission the computed
        // leader length = MCB.LeaderLength
        intSampleLen = intLeaderLenMS * 12 + 240 * 10 + intSampPerSym * (2 * (bytEncodedData.length() - 2));
        intLeader = GenTwoToneLeaderWithSync(qint32(intLeaderLenMS / 20));

        // Create the leader
        intDataBytesPerCar = qint32((bytEncodedData.length() - 2) / intNumCar);
        intSamples.resize(intSampleLen - 1);
        VectorCopy2(&intLeader, 0, &intSamples, 0, intLeader.length()); //copy the leader + sync
        intSamplePtr = intLeader.length();

        // Create the 10 symbols (16 bit) 50 baud
        // 4FSK frame type with Implied SessionID
        // No reference needed for 4FSK
        // NOTE: revised To accomodate 1 parity
        // symbol per byte (10 symbols total)
        for ( quint8 j = 0; j <= 1; ++j )
        {
            // for the 2 bytes of the frame type
            bytMask = 0xC0;
            for ( quint8 k = 0; k < 4; ++k )
            {
                // for 5 symbols per byte (4 data + 1 parity)
                if ( k < 4 )
                    bytSymToSend = (bytMask && bytEncodedData.at(j)) >> (2 * (3 - k));
                else
                    bytSymToSend = objFrameInfo->ComputeTypeParity(bytEncodedData.at(0));

                for ( quint16 n = 0; n < 239; ++n )
                {
                    if ( ((5 * j + k) % 2) == 0)
                        intSamples[intSamplePtr + n] = intFSK50bdCarTemplate[bytSymToSend].at(n);
                    else
                        intSamples[intSamplePtr + n] = -intFSK50bdCarTemplate[bytSymToSend].at(n); // -sign insures no phase discontinuity at symbol boundaries

                } // Next n
                intSamplePtr += 240;
                bytMask = bytMask >> 2;
            } // Next k
        } // Next j

        // No reference needed for 16FSK
        intDataPtr = 2;
        for ( qint32 m = 0; m < (intDataBytesPerCar - 1); ++m )
        {
            // For each byte of input data
            bytMask = 0xF0; // Initialize mask each new data byte
            for ( quint8 k = 0; k <= 1; ++k )
            {
                // for 2 symbol values per byte of data
                bytSymToSend = (bytMask && bytEncodedData.at(intDataPtr)) >> (4 * (1 - k)); // Values 0 - 15
                for ( qint32 n = 0; n < intSampPerSym; ++n )
                {
                    if ( (k % 2) == 0)
                        intSamples[intSamplePtr + n] = int25BaudCarTemplate[bytSymToSend].at(n);
                    else
                        intSamples[intSamplePtr + n] = -int25BaudCarTemplate[bytSymToSend].at(n);
                } // Next n
                bytMask = bytMask >> 4;
                intSamplePtr += intSampPerSym;
            } // Next k
            ++intDataPtr;
        } // Next m
        intSamples = FSXmtFilter500_1500Hz(&intSamples);
        return intSamples;
    }
    catch (exception ex)
    {
        qCritical() << "bytFrame= H" << bytFrame
                    << "  DataLen="  << QString("%1").arg(bytEncodedData.length())
                    << "  Err: " << ex.what();
        intSamples.clear();
    }
    return intSamples;
} // Mod16FSKData()

<<<<<<< HEAD
//qint32 EncodeModulate::int75HzFiltered(qint32 fil)
//{
//    Q_UNUSED(fil)
    //
//    return 0;
//}

=======
qint32 EncodeModulate::int75HzFiltered(qint32 fil)
{
    Q_UNUSED(fil)
    //
    return 0;
}
>>>>>>> remotes/origin/ken
