#include <exception>
using namespace std;

#include <QtCore/QDebug>  // so we can write messages to log file

#include "reedsolomon.hpp"

ReedSolomon_8::ReedSolomon_8 ()
{
    // set initial values into local vars
    mm = 8;
    nn = 255;
    tt = 0;
    kk = nn - 2 * tt;
    pp4 = {1, 1, 0, 0, 1};
    pp8 = {1, 0, 1, 1, 1, 0, 0, 0, 1};
    pp  = pp8;
    alpha_to.resize(nn);
    index_of.resize(nn);
    gg.resize(nn - kk);
    recd.resize(nn - 1);
    data.clear();
    bb.resize((nn - kk) - 1);
}

// -----------------------------------------------------
//  PROPERTY SETS AND GETS
// -----------------------------------------------------
quint32 ReedSolomon_8::getMaxCorrections() { return tt; }

void ReedSolomon_8::setMaxCorrections(quint32 value)
{
    if ( (value >= 1) && (value <= quint32((nn - 1) / 2)) )
    {
        tt = value;
        kk = nn - 2 * tt;
        gg.resize(nn - kk);
        bb.resize((nn - kk) - 1);
        data.resize((nn - (2 * tt)) - 1); // the max size of the encoding array
        generate_gf();
        gen_poly();
    }
}


// ------------------------------------------------------
// PUBLIC METHODS AND FUNCTIONS
// ------------------------------------------------------
QByteArray ReedSolomon_8::RSEncode(QByteArray bytData)
{
    if ( bytData.length() > data.length() ) return NULL; // too long of data

    quint32 i;

    qint32 intStartIndex = data.length() - bytData.length(); // set the start point for shortened RS codes

    QByteArray bytTemp;

    for ( i = 0; i < (quint32)(bytData.length() - 1); ++ i )
    {
        data[i + intStartIndex] = bytData.at(i);
    } // Next i
    encode_rs(); // create the parity symbols
    bytTemp.resize(bytData.length() + bb.length() - 1);

    for ( i = 0; i <  (quint32)(bytTemp.length() - 1); ++ i )
    {
        if ( i < (quint32)bytData.length() )
            bytTemp[i] = quint8(data.at(i + intStartIndex)); // copy the data including stuffed leading zeros
        else
            bytTemp[i] = quint8(bb.at(i - (data.length() - intStartIndex))); // append the parity symbols
    } // Next i
    return bytTemp;
} // RSEncode()

QByteArray ReedSolomon_8::RSDecode(QByteArray bytRcv, bool blnCorrected)
{
    Q_UNUSED(blnCorrected)   // to suppress warnings in release build

    quint32 i;
    quint32 j;
    quint32 intTrace = 0;
    quint32 intIndexSave = 0;
    quint32 intIsave = 0;
    QVarLengthArray<qint32> intTemp;

    Q_UNUSED(intTrace)

    intTemp.reserve(nn - 1);

    try
    {
        bool blnDecodeFailed = false;
        Q_UNUSED(blnDecodeFailed)

        blnErrorsCorrected = false;

        if ( ((quint32)bytRcv.length() > nn) ||
             ((quint32)bytRcv.length() < (1 + 2 * tt)) )
        {
            blnCorrected = false;
            qCritical() << "Length Error Rcv:"
                        << QString("%1").arg(bytRcv.length())
                        << " nn:" << QString("%1").arg(nn)
                        << " tt:" << QString("%1").arg(tt)
                        << "  Return empty array.";
            QByteArray bytNull;
            bytNull.clear();
            return bytNull;
        }
        intTrace = 1;
        quint32 intStartIndex = nn - bytRcv.length(); // set the start point for shortened RS codes

        for ( j = 0; j < (quint32)(nn - 1); ++j )
        {
            if ( j < intStartIndex )
                intTemp[j] = 0;
            else
                intTemp[j] = bytRcv.at(j - intStartIndex);
        } // Next j

        intTrace = 2;
        // convert to indexed form

        for ( i = 0; i < (quint32)(recd.length() - 1); ++i )
        {
            intIsave = i;
//            intIndexSave = index_of(intTemp.at(i));
//            recd[i] = index_of(intTemp.at(i));
        } // Next i

        intTrace = 3;
        decode_rs();
        intTrace = 4;
        blnCorrected = &blnErrorsCorrected;

        if ( blnErrorsCorrected )
        {
            QByteArray bytTemp;
            bytTemp.reserve(bytRcv.length() - 2 * tt - 1);

            for ( i = 0; i < (quint32)(bytTemp.length() - 1); ++i )
            {
                bytTemp[i] = quint8(recd.at(i + intStartIndex));
            } // Next i
            intTrace = 5;
            blnCorrected = &blnErrorsCorrected;
            return bytTemp;
        }
        else
        {
            bytRcv.resize(bytRcv.length() - 2 * tt - 1);
            return bytRcv;
        }
    }
    catch (exception ex)
    {
        qCritical() << "Exception Return input.  i: "
                    << QString("%1").arg(intIsave)
                    << " Index: "
                    << QString("%1").arg(intIndexSave)
                    << " Err: " << ex.what();
        blnCorrected = false;
        bytRcv.resize(bytRcv.length() - 2 * tt - 1);
        return bytRcv;
    }
}  // RSDecode()


// -------------------------------------------------
// PRIVATE METHODS AND FUNCTIONS
// -------------------------------------------------
void ReedSolomon_8::generate_gf()
{
    quint32 i;
    quint32 mask = 1; //  mask = 1
    alpha_to[mm] = 0;
    for ( i = 0; i < mm; ++i )
    {
        alpha_to[i] = mask;
        index_of[alpha_to[i]] = i;
        if ( pp[i] != 0 ) alpha_to[mm] ^= mask;
        mask <<= 1 ;
    } // Next i

    index_of[alpha_to[mm]] = mm ;
    mask >>= 1 ;
    for ( i = mm + 1; i < nn; ++i )
    {
        if ( alpha_to[i-1] >= mask )
            alpha_to[i] = alpha_to[mm] ^ ((alpha_to[i-1] ^ mask) << 1);
        else
            alpha_to[i] = alpha_to[i - 1] << 1;

        index_of[alpha_to[i]] = i;
    } // Next i
    index_of[0] = -1;
} // generate_gf()


void ReedSolomon_8::gen_poly()
{
    quint32 i;
    quint32 j;

    gg[0] = 2;    // primitive element alpha = 2  for GF(2**mm)
    gg[1] = 1;    // g(x) = (X+alpha) initially

    for ( i = 2; i <= (nn-kk); ++i )
    {
        gg[i] = 1;
        for ( j = i-1; j > 0; --j )
        {
            if ( gg[j] != 0 )
                gg[j] = gg[j-1] ^ alpha_to[(index_of[gg[j]] + i) % nn] ;
            else
                gg[j] = gg[j-1];
        } // Next j
        gg[0] = alpha_to[(index_of[gg[0]] + i) % nn]; // gg[0] can never be zero
    } // Next i
    // convert gg[] to index form for quicker encoding
    for ( i = 0; i <= (nn-kk); ++i )
    {
        gg[i] = index_of[gg[i]] ;
    } // Next i
} // gen_poly()


void ReedSolomon_8::decode_rs()
{
    quint32 i = 0;
    quint32 j = 0;
    quint32 u = 0;
    quint32 q = 0;
    quint32 count = 0;
    qint32 syn_error = 0;

    QVector<QVector<qint32> > elp(nn - kk + 1);
    elp.reserve(nn - kk - 1);
    QVector<qint32> d;
    QVector<qint32> l;
    QVector<qint32> u_lu;
    QVector<qint32> s;
    QVector<qint32> root;
    QVector<qint32> loc;
    QVector<qint32> z;
    QVector<qint32> err;
    QVector<qint32> reg;


    d.resize(nn - kk + 1);
    l.resize(nn - kk + 1);
    u_lu.resize(nn - kk + 1);
    s.resize(nn - kk + 1);
    root.resize(tt - 1);
    loc.resize(tt - 1);
    z.resize(tt);
    err.resize(nn - 1);
    reg.resize(tt);

    try
    {
        // first form the syndromes

        for ( i = 1; i <= (nn-kk); ++i )
        {
            s[i] = 0;
            for ( j = 0; j < nn; ++j )
            {
                if ( (qint32)recd.at(j) != -1 ) s[i] ^= alpha_to[(recd.at(j) + i * j) % nn]; // recd[j] in index form
            } // Next j
            // convert syndrome from polynomial form to index form
            if ( s[i] != 0 ) syn_error = 1;    // set flag if non-zero syndrome => error
            s[i] = index_of[s[i]];
        } // Next i

        if ( syn_error )
        {
            // if errors, try and correct
            // compute the error location polynomial via the
            // Berlekamp iterative algorithm, following the
            // terminology of Lin and Costello: d[u] is the
            // 'mu'th discrepancy, where u='mu'+1 and 'mu'
            // (the Greek letter!) is the step number
            // ranging from -1 to 2*tt (see L&C), l[u] is the
            // degree of the elp at that step, and u_l[u] is
            // the difference between the step number and the
            // degree of the elp.
            //
            // initialise table entries
            d[0] = 0;           // index form
            d[1] = s[1];        // index form
            elp[0][0] = 0;      // index form
            elp[1][0] = 1 ;     // polynomial form

            for ( i = 1; i < (nn-kk); ++i )
            {
                elp[0][i] = -1;   // index form
                elp[1][i] = 0;    // polynomial form
            } // Next i
            l[0] = 0 ;
            l[1] = 0 ;
            u_lu[0] = -1 ;
            u_lu[1] = 0 ;
            u = 0;
            do
            {
                ++u;
                if ( d[u] == -1 )
                {
                    l[u+1] = l[u];
                    for ( i = 0; i <= (quint32)l.at(u); ++i )
                    {
                        elp[u+1][i] = elp[u][i];
                        elp[u][i] = index_of[elp[u][i]];
                    } //Next i
                }
                else
                {
                    // search for words with greatest
                    // u_lu[q] for which d[q]!=0
                    q = u-1;
                    while ( (d[q]==-1) && (q>0) ) --q;
                    // have found first non-zero d[q]
                    if ( q > 0 )
                    {
                        j = q;
                        do
                        {
                            --j;
                            if ( (d[j] != -1) && (u_lu[q] < u_lu[j]) )   q = j;
                        } while (j > 0);
                    }

                    // have now found q such that d[u]!=0 and u_lu[q]
                    // is maximum
                    // store degree of new elp polynomial
                    if ( (qint32)l.at(u) > (qint32)(l.at(q) + u - q) )
                        l[u + 1] = l[u] ;
                    else
                        l[u + 1] = l[q] + u - q;


                    // form new elp(x)
                    for ( i = 0; i < (nn-kk); ++i )
                    {
                        elp[u+1][i] = 0;
                    } // Next i

                    for ( i = 0; i <= (quint32)l.at(q); ++i )
                    {
                        if ( elp[q][i] != -1 )  elp[u + 1][i + u - q] = alpha_to[(d[u] + nn - d[q] + elp[q][i]) % nn];
                    } // Next i

                    for ( i = 0; i <= (quint32)l.at(u); ++i )
                    {
                        elp[u+1][i] ^= elp[u][i];
                        elp[u][i] = index_of[elp[u][i]];  //convert old elp value to index*/
                    } // Next i
                }
                u_lu[u + 1] = u - l[u + 1];

                // form (u+1)th discrepancy
                if ( u < (quint32)(nn-kk) )
                {
                    // no discrepancy computed on last iteration
                    if ( (qint32)s.at(u + 1) != -1 )
                        d[u + 1] = alpha_to[s[u + 1]];
                    else
                        d[u + 1] = 0 ;

                    for ( i = 1; i <= (quint32)l.at(u+1); ++i )
                    {
                        if ( (s[u + 1 - i] != -1 ) && (elp[u + 1][i] != 0) )
                            d[u + 1] ^= alpha_to[(s[u + 1 - i] + index_of[elp[u + 1][i]]) % nn];
                    } // Next i
                    d[u + 1] = index_of[d[u + 1]];    // put d[u+1] into index form */
                }
            } while ( (u < (quint32)(nn-kk)) && ((quint32)l.at(u + 1) <= tt) );
            ++u;
            if ( (quint32)l.at(u) <= tt )
            {
                // can correct error */
                // put elp into index form
                for ( i = 0; i <= (quint32)l.at(u); ++i )
                {
                    elp[u][i] = index_of[elp[u][i]];
                } // Next i

                // find roots of the error location polynomial
                for ( i = 1; i <= (quint32)l.at(u); ++i )
                {
                    reg[i] = elp[u][i];
                } // Next i
                count = 0;
                for ( i = 1; i <= nn; ++i )
                {
                    q = 1;
                    for ( j = 1; j <= (quint32)l.at(u); ++j )
                    {
                        if ( (qint32)reg.at(j) != -1 )
                        {
                            reg[j] = (reg[j] + j) % nn;
                            q ^= alpha_to[reg[j]];
                        } ;
                    } // Next j
                    if ( !q )
                    {
                        // store root and error location number indices
                        root[count] = i;
                        loc[count]  = nn - i ;
                        ++count;
                    };
                } // Next i

                if ( count == (quint32)l.at(u) )
                {
                    // no. roots = degree of elp hence <= tt errors
                    blnErrorsCorrected = true;
                    // form polynomial z(x)
                    for ( i = 1; i <= (quint32)l.at(u); ++i )
                    {
                        // Z[0] = 1 always - do not need
                        if ( (s[i] != -1) && (elp[u][i] != -1) )
                            z[i] = alpha_to[s[i]] ^ alpha_to[elp[u][i]];
                        else if ( (s[i] != -1) && (elp[u][i] == -1) )
                            z[i] = alpha_to[s[i]] ;
                        else if ( (s[i] == -1) && (elp[u][i] != -1) )
                            z[i] = alpha_to[elp[u][i]] ;
                        else
                            z[i] = 0;

                        for ( j = 1; j < i; ++j )
                        {
                            if ( (s[j] != -1) && (elp[u][i - j] != -1))
                                z[i] ^= alpha_to[(elp[u][i - j] + s[j]) % nn];
                        } // Next j
                        z[i] = index_of[z[i]];  // put into index form
                    } // Next i

                    // evaluate errors at locations given by error
                    // location numbers loc[i]
                    for ( i = 0; i < nn; ++i )
                    {
                        err[i] = 0;
                        if ( (qint32)recd[i] != -1 )        /// convert recd[] to polynomial form */
                            recd[i] = alpha_to[recd[i]];
                        else
                            recd[i] = 0;
                    }  // Next i

                    for ( i = 0; i < (quint32)l.at(u); ++i )
                    {
                        // compute numerator of error term first
                        err[loc[i]] = 1;       // accounts for z[0]
                        for ( j = 1; j <= (quint32)l.at(u); ++j )
                        {
                            if ( z[j] != -1 )  err[loc[i]] ^= alpha_to[(z[j]+j*root[i])%nn] ;
                        } // Next j

                        if ( err[loc[i]] != 0 )
                        {
                            err[loc[i]] = index_of[err[loc[i]]];
                            q = 0;     // form denominator of error term
                            for ( j = 0; j < (quint32)l.at(u); ++j )
                            {
                                if ( j != i) q += index_of[1 ^ alpha_to[(loc[j] + root[i]) % nn]];
                            } // Next j

                            q = q % nn;
                            err[loc[i]] = alpha_to[(err[loc[i]]  -q + nn) % nn];
                            recd[loc[i]] ^= err[loc[i]];   //recd[i] must be in polynomial form */
                        }
                    } // Next i
                }
                else
                {
                    // no. roots != degree of elp => >tt errors and
                    // cannot solve
                    blnErrorsCorrected = false;
                    for ( i = 0; i < nn; ++i )
                    {
                        // could return error flag if desired
                        if ( (qint32)recd.at(i) != -1 )  // convert recd[] to polynomial form */
                            recd[i] = alpha_to[recd[i]];
                        else
                            recd[i] = 0 ;     /* just output received codeword as is */
                    } // Next i
                }
            }
            else
            {
                // elp has degree has degree >tt hence cannot solve
                blnErrorsCorrected = false;
                for ( i = 0; i < nn; ++i )
                {
                    // could return error flag if desired
                    if ( (qint32)recd.at(i) != -1 )    // convert recd[] to polynomial form */
                        recd[i] = alpha_to[recd[i]];
                    else
                        recd[i] = 0;     // just output received codeword as is */
                } // Next i
            }
        }
        else
        {
            // no non-zero syndromes => no errors: output received codeword
            blnErrorsCorrected = true;
            for ( i = 0; i < nn; ++i )
            {
                if ( (qint32)recd.at(i) != -1 )  // convert recd[] to polynomial form
                    recd[i] = alpha_to[recd[i]];
                else
                    recd[i] = 0;

            } // Next i
        }
    }
    catch (exception ex)
    {
        qCritical() << "[decode_rs] Err: " << ex.what();
    }
} // decode_rs()



void ReedSolomon_8::encode_rs()
{
    quint32 i;
    quint32 j;
    qint32 feedback;


    for ( i = 0; i < (nn-kk); ++i )
    {
        bb[i] = 0 ;
    } // Next i

    for ( i = (quint32)(kk - 1); i > 0; --i )
    {
        feedback = index_of[data[i] ^ bb[nn-kk-1]];
        if ( feedback != -1 )
        {
            for ( j = nn - kk - 1; j > 0; --j )
            {
                if ( (qint32)gg.at(j) != -1 )
                    bb[j] = bb[j-1] ^ alpha_to[(gg[j] + feedback) % nn];
                else
                    bb[j] = bb[j-1];
            } // Next j
            bb[0] = alpha_to[(gg[0] + feedback) % nn];
        }
        else
        {
            for ( j = nn - kk - 1; j > 0; --j )
            {
                bb[j] = bb[j-1];
            } // Next j
            bb[0] = 0 ;
        }
    } // Next i
} // encode_rs()

<<<<<<< HEAD

=======
>>>>>>> remotes/origin/ken
