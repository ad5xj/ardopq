#include <QtCore/QDebug>
#include <QtCore/QStringList>

#include "frameinfo.hpp"

FrameInfo::FrameInfo(QObject *parent) : QObject(parent)
{
    initFrameNames();
    initValidFrameTypes();
}

// ----------------------------------------------------
//               PROPERTY SETS AND GETS
// ----------------------------------------------------
quint32 FrameInfo::getSamplesToComplete()
{
    quint8 bytID = m_name; // make the header byte local
    if ( (bytID > 0) )
    {
        // DataACK and DataNAK
        if ( (bytID <= 0x1F) || (bytID >= 0xE0) ) return 0;
    }
    if ( (bytID = 0x23) || (bytID = 0x24) || (bytID = 0x29) || (bytID = 0x2C) || (bytID = 0x2D) || (bytID = 0x2E) ) return 0; // Short control frames
    if ( (bytID >= 0x30) && (bytID <= 0x38) ) return quint32(1.005 * (6 + 6 + 4) * 4 * 240);                                   // ID Frame Call sign + Grid Square, Connect request frames Modified 0.7.2.5 increase by 2 bytes
    if ( (bytID >= 0x39) && (bytID <= 0x3C) ) return quint32(1.005 * 240 * 3 * 4);                                             // Con ACK with timing data

    if ( (bytID == 0x40) || (bytID == 0x41) ) return quint32(1.005 * (120 + (1 + 64 + 2 + 32) * 4 * 120));                     // 1 carrier 100 baud 4PSK
    if ( (bytID == 0x42) || (bytID == 0x43) ) return quint32(1.005 * (120 + (1 + 16 + 2 + 8) * 4 * 120));                      // 1 carrier 100 baud 4PSK Short
    if ( (bytID == 0x44) || (bytID == 0x45) ) return quint32(1.005 * (120 + (120 * (8 * (1 + 108 + 2 + 36)) / 3)));            // 1 carrier 100 baud 8PSK
    if ( (bytID == 0x46) || (bytID == 0x47) ) return quint32(1.005 * (240 * 4 * (1 + 32 + 2 + 8)));                            // 1 carrier 50 baud 4FSK
    if ( (bytID == 0x48) || (bytID == 0x49) ) return quint32(1.005 * (240 * 4 * (1 + 16 + 2 + 4)));                            // 1 carrier 50 baud 4FSK short
    if ( (bytID == 0x4A) || (bytID == 0x4B) ) return quint32(1.005 * (120 * 4 * (1 + 64 + 2 + 16)));                           // 1 carrier 100 baud 4FSK
    if ( (bytID == 0x4C) || (bytID == 0x4D) ) return quint32(1.005 * (120 * 4 * (1 + 32 + 2 + 8)));                            // 1 carrier 100 baud 4FSK short
    if ( (bytID == 0x4E) || (bytID == 0x4F) ) return quint32(1.005 * (480 * (8 * (1 + 24 + 2 + 6)) / 3));                      // 1 carrier 25 baud 8FSK

    if ( (bytID == 0x50) || (bytID == 0x51) ) return quint32(1.005 * (120 + (1 + 64 + 2 + 32) * 4 * 120));                     // 2 carrier 100 baud 4PSK
    if ( (bytID == 0x52) || (bytID == 0x53) ) return quint32(1.005 * (120 + (120 * (8 * (1 + 108 + 2 + 36)) / 3)));            // 2 carrier 100 baud 8PSK
    if ( (bytID == 0x54) || (bytID == 0x55) ) return quint32(1.005 * (120 + (1 + 128 + 2 + 64) * 2 * 120));                    // 2 carrier 100 baud 16QAM
    if ( (bytID == 0x58) || (bytID == 0x59) ) return quint32(1.005 * (480 * 2 * (1 + 32 + 2 + 8)));                            // 1 carrier 25 baud 16FSK (in testing)
    if ( (bytID == 0x5A) || (bytID == 0x5B) ) return quint32(1.005 * (480 * 2 * (1 + 16 + 2 + 4)));                            // 1 carrier 25 baud 16FSK Short (in testing)
    if ( (bytID == 0x5C) || (bytID == 0x5D) ) return quint32(1.005 * (120 + (1 + 128 + 2 + 64) * 2 * 120));                    // 1 carrier 100 baud 16QAM
    if ( (bytID == 0x60) || (bytID == 0x61) ) return quint32(1.005 * (120 + (1 + 64 + 2 + 32) * 4 * 120));                     // 4 carrier 100 baud 4PSK
    if ( (bytID == 0x62) || (bytID == 0x63) ) return quint32(1.005 * (120 + (120 * (8 * (1 + 108 + 2 + 36)) / 3)));            // 4 carrier 100 baud 8PSK
    if ( (bytID == 0x64) || (bytID == 0x65) ) return quint32(1.005 * (72 + (1 + 120 + 2 + 40) * 4 * 72));                      // 4 carrier 167 baud 4PSK
    if ( (bytID == 0x66) || (bytID == 0x67) ) return quint32(1.005 * (72 + (72 * (8 * (1 + 159 + 2 + 60)) / 3)));              // 4 carrier 167 baud 8PSK
    if ( (bytID == 0x68) || (bytID == 0x69) ) return quint32(1.005 * (120 * 4 * (1 + 64 + 2 + 16)));                           // 2 carrier 100 baud 4FSK

    if ( (bytID == 0x70) || (bytID == 0x71) ) return quint32(1.005 * (120 + (1 + 64 + 2 + 32) * 4 * 120));                     // 8 carrier 100 baud 4PSK
    if ( (bytID == 0x72) || (bytID == 0x73) ) return quint32(1.005 * (120 + (120 * (8 * (1 + 108 + 2 + 36)) / 3)));            // 8 carrier 100 baud 8PSK
    if ( (bytID == 0x74) || (bytID == 0x75) ) return quint32(1.005 * (72 + (1 + 120 + 2 + 40) * 4 * 72));                      // 8 carrier 167 baud 4PSK
    if ( (bytID == 0x76) || (bytID == 0x77) ) return quint32(1.005 * (72 + (72 * (8 * (1 + 159 + 2 + 60)) / 3)));              // 2 carrier 167 baud 8PSK ' 8 carrier 167 baud 4PSK
    if ( (bytID == 0x78) || (bytID == 0x79) ) return quint32(1.005 * (120 * 4 * (1 + 64 + 2 + 16)));                           // 4 carrier 100 baud 4FSK
    // experimental 600 baud for VHF/UHF FM
    if ( (bytID == 0x7A) || (bytID == 0x7B) ) return quint32(1.005 * (20 * 4 * 3 * (1 + 200 + 2 + 50)));                       // 1 carrier 600 baud 4FSK (3 groups of 200 bytes each for RS compatibility)
    if ( (bytID == 0x7C) || (bytID == 0x7D) ) return quint32(1.005 * (20 * 4 * (1 + 200 + 2 + 50)));                           // 1 carrier 600 baud 4FSK short
    // experimental SOUNDINGs
    if ( (bytID == 0xD0) ) return quint32(1.005 * 60 * 18 * 40);
    return -1; // No frame type match
}

// -------------------------------------------------
//               PUBLIC FUNCTIONS AND METHODS
// -------------------------------------------------
QString FrameInfo::getName(quint8 bytID)
{
    if ( bytID < 0x20 )
    {
        return strFrameType.at(0);
    }
    else if ( bytID >= 0xE0 )
    {
        return strFrameType.at(0xE0);
    }
    else
    {
        return strFrameType.at(bytID);
    }
}

bool FrameInfo::isValidFrameType(quint8 bytType)
{
    if ( (bytType > 0) && (bytType <= 0x1F) ) return true;
    if ( (bytType = 0x23)  ||
         (bytType == 0x24) ||
         (bytType == 0x29) ||
         (bytType == 0x2C) ||
         (bytType == 0x2D) ||
         (bytType == 0x2E) ) return true;
    if ( (bytType >= 0x30) && (bytType <= 0x3C) ) return true;
    if ( (bytType >= 0x40) && (bytType <= 0x4F) ) return true;
    if ( (bytType >= 0x50) && (bytType <= 0x55) ) return true;
    if ( (bytType >= 0x58) && (bytType <= 0x5D) ) return true;
    if ( (bytType >= 0x60) && (bytType <= 0x69) ) return true;
    if ( (bytType >= 0x70) && (bytType <= 0x7D) ) return true;
    if ( (bytType > 0xE0)  && (bytType < 0xFF) ) return true;
    if ( bytType == 0xD0 ) return true;

    return false;
}  // IsValidFrameType()

bool FrameInfo::isDataFrame(quint8 intFrameType)
{
    QString strFrame = getName(intFrameType);
    if ( strFrame.isEmpty() ) return false;
    return ( strFrame.endsWith(".E") || strFrame.endsWith(".O") );
} // IsDataFrame()


bool FrameInfo::isValidISSFrameType(quint8 bytType)
{
    if ( (bytType > 0) && (bytType <= 0x1F) ) return true;    // NAK
    if ( (bytType == 0x23) ||
         (bytType == 0x29) ||
         (bytType == 0x2C) ||
         (bytType == 0x2D) ||
         (bytType == 0x2E) ) return true;                      // BREAK, DISC, END, ConRejBusy, or ConRejBW
    if ( (bytType >= 0x31) && (bytType <= 0x3C) ) return true; // Con req and Con ACK
    if ( (bytType > 0xE0) && (bytType < 0xFF) ) return true; // ACK
    return false;
}  // IsValidFrameType()

bool FrameInfo::isShortControlFrame(quint8 bytType)
{
    if ( bytType <= 0x1F ) return true; // NAK
    if ( (bytType == 0x23) ||
         (bytType == 0x24) ||
         (bytType == 0x29) ||
         (bytType == 0x2C) ||
         (bytType == 0x2D) ||
         (bytType == 0x2E) ) return true; // BREAK,  DISC, END, ConRejBusy, ConRejBW
    if ( bytType >= 0xE0 ) return true; // ACK
    return false;
} // IsShortControlFrame()

QByteArray FrameInfo::getDataModes(qint32 intBW)
{
    QByteArray byt200;
    QByteArray byt500;
    QByteArray byt1000;
    QByteArray byt2000;

    byt200.clear();
    byt500.clear();
    byt1000.clear();
    byt2000.clear();

    switch (intBW)
    {
    case 200:
        //8FSK.200.25, 4FSK.200.50, 4PSK.200.100, 8PSK.200.100, 16QAM.200.100
        //  (288, 429, 768, 1296, 1512 byte/min)
        byt200.append(0x4E);
        byt200.append(0x46);
        byt200.append(0x40);
        byt200.append(0x44);
        byt200.append(0x5C);
        return byt200;
    case 500:
        //16FSK.500.25S, 16FSK.500.25, 4FSK.500.100, 4PSK.500.100, 8PSK.500.100, 16QAM.500.100)
        // (329, 429, 881, 1536, 2592, 4305 bytes/min)
        byt500.append(0x5A);
        byt500.append(0x58);
        byt500.append(0x4A);
        byt500.append(0x50);
        byt500.append(0x52);
        byt500.append(0x54);
        return byt500;
    case 1000: // Streamlined 0.3.1.6
        //16FSK.500.25S, 16FSK.500.25, 4FSK.500.100, 4FSK.1000.100, 4PSK.1000.100, 8PSK.1000.100, 8PSK.1000.167
        //(329, 429, 881, 1762, 3072, 5184, 8610 bytes/min)
        byt1000.append(0x5A);
        byt1000.append(0x58);
        byt1000.append(0x4A);
        byt1000.append(0x68);
        byt1000.append(0x60);
        byt1000.append(0x62);
        byt1000.append(0x66);
        return byt1000;
    case 2000:
        if ( MCB->TuningRange > 0 ) // Streamlined 0.3.1.6
        {
            // These do not include the 600 baud modes for
            // FM only.
            // 16FSK.500.25S, 16FSK.500.25, 4FSK.500.100,
            // 4FSK.1000.100, 4FSK.2000.100, 4PSK.2000.100,
            // 8PSK.2000.100, 8PSK.2000.167)
            //(329, 429, 881, 1762, 3624, 6144, 10386,
            // 17220 bytes/min)
            byt2000.append(0x5A);
            byt2000.append(0x58);
            byt2000.append(0x4A);
            byt2000.append(0x68);
            byt2000.append(0x78);
            byt2000.append(0x70);
            byt2000.append(0x72);
            byt2000.append(0x76);
            return byt2000;
        }
        else
        {
            // These include the 600 baud modes for FM only.
            // The following is temporary, Plan to replace
            // 8PSK 8 carrier modes with high baud 4PSK and 8PSK.
            //
            // 4FSK.500.100S, 4FSK.500.100, 4FSK.2000.600S,
            // 4FSK.2000.600, 4PSK.2000.100, 8PSK.2000.100)
            // (696, 881, 4338, 5863, 6144, 10386 bytes/min)
            byt2000.append(0x4C);
            byt2000.append(0x4A);
            byt2000.append(0x7C);
            byt2000.append(0x7A);
            byt2000.append(0x70);
            byt2000.append(0x72);
            return byt2000;
        }
    }
    return NULL;
} // getDataModes()


qint32 FrameInfo::getNumCarriers(quint8 bytFrameType)
{
    bool blnOdd = false;
    bool blnFrameTypeOK = false;

    quint16 intNumCar  = 0;
    quint32 intBaud    = 0;
    quint32 intDataLen = 0;
    quint32 intRSLen   = 0;

    QString strType = "";
    QString strMod  = "";

    blnFrameTypeOK = frameInfo(bytFrameType, &blnOdd, &intNumCar, &strMod, &intBaud, &intDataLen, &intRSLen, &strType);

    if ( blnFrameTypeOK )
        return intNumCar;
    else
        return 0;
} // getNumCarriers()

QByteArray FrameInfo::getShiftUpThresholds(qint32 intBW)
{
    QByteArray byt200;
    QByteArray byt500;
    QByteArray byt1000;
    QByteArray byt2000;

    byt200.clear();
    byt500.clear();
    byt1000.clear();
    byt2000.clear();

    switch (intBW)
    {
    case 200:
        byt200.append(82);
        byt200.append(85);
        byt200.append(83);
        byt200.append(76);
        return byt200;
    case 500:
        byt500.append(82);
        byt500.append(82);
        byt500.append(82);
        byt500.append(84);
        byt500.append(75);
        return byt500;
    case 1000:
        // Threshold for 8PSK 167 baud changed
        // from 75 to 80 on rev 0.7.2.3
        byt1000.append(82);
        byt1000.append(82);
        byt1000.append(82);
        byt1000.append(77);
        byt1000.append(82);
        byt1000.append(80);
        return byt1000;
    case 2000:
        if (MCB->TuningRange > 0 )
        {
            // These do not include the 600 baud modes
            // for FM only.
            // Threshold for 8PSK 167 baud changed
            // from 73 to 80 on rev 0.7.2.3
            byt2000.append(82);
            byt2000.append(82);
            byt2000.append(82);
            byt2000.append(85);
            byt2000.append(86);
            byt2000.append(87);
            byt2000.append(80);
            return byt2000;
        }
        else
        {
            // These include the 600 baud modes for FM only.
            byt2000.append(90);
            byt2000.append(90);
            byt2000.append(75);
            byt2000.append(75);
            byt2000.append(73);
            return byt2000;
        }
    }
    return NULL;
} // getDataModes()


bool FrameInfo::frameInfo(quint8 bytFrameType, bool *blnOdd, quint16 *intNumCar, QString *strMod, quint32 *intBaud, quint32 *intDataLen, quint32 *intRSLen, QString *strType)
{
    // to suppress warnings in relase build
    Q_UNUSED(blnOdd)
    Q_UNUSED(intNumCar)
    Q_UNUSED(intBaud)
    Q_UNUSED(intDataLen)
    Q_UNUSED(intRSLen)
    Q_UNUSED(strMod)
    Q_UNUSED(strType)

    if ( bytFrameType <= 0x1F)
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 50;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = 0;
        intRSLen = 0;
        strMod = &strLocal;
        intBaud = &intLocal2;
    }
    else if ( (bytFrameType == 0x23) ||
              (bytFrameType == 0x24) ||
              (bytFrameType == 0x29) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 50;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = 0;
        intRSLen = 0;
        strMod = &strLocal;
        intBaud = &intLocal2;
    }
    else if ( (bytFrameType == 0x2C) ||
              (bytFrameType == 0x2D) ||
              (bytFrameType == 0x2E) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 50;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = 0;
        intRSLen = 0;
        strMod = &strLocal;
        intBaud = &intLocal2;
    }
    else if ( (bytFrameType >= 0x30) && (bytFrameType <= 0x38) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 12;
        quint32 intLocal3 = 4;
        quint32 intLocal4 = 50;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4; // Modified 0.7.2.5 to increase RS
    }
    else if ( (bytFrameType >= 0x39) && (bytFrameType <= 0x3C) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 3;
        quint32 intLocal4 = 50;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = 0;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( bytFrameType >= 0xE0 )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal4 = 50;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = 0;
        intRSLen = 0;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x40) || (bytFrameType == 0x41) )
    {  // 1 Carrier Data modes
       // 100 baud PSK (200 baud not compatible with 200 Hz bandwidth)  (Note 1 carrier modes Qual Threshold reduced to 30 (was 50) for testing April 20, 2015
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 64;
        quint32 intLocal3 = 32;
        quint32 intLocal4 = 100;
        QString strLocal = "4PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x42) || (bytFrameType == 0x43) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 16;
        quint32 intLocal3 = 8;
        quint32 intLocal4 = 100;
        QString strLocal = "4PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x44) || (bytFrameType == 0x45) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 108;
        quint32 intLocal3 = 36;
        quint32 intLocal4 = 100;
        QString strLocal = "8PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x5C) || (bytFrameType == 0x5D) )
    {   // 100 baud 16QAM
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 128;
        quint32 intLocal3 = 64;
        quint32 intLocal4 = 100;
        QString strLocal = "16QAM";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x46) || (bytFrameType == 0x47) )
    {   // 50 baud 4FSK 200 Hz bandwidth
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 32;
        quint32 intLocal3 = 8;
        quint32 intLocal4 = 50;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x48) || (bytFrameType == 0x49) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 16;
        quint32 intLocal3 = 4;
        quint32 intLocal4 = 50;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x4E) || (bytFrameType == 0x4F) )
    {     // 25 baud 8FSK 200 Hz bandwidth (in testing)
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 24;
        quint32 intLocal3 = 6;
        quint32 intLocal4 = 25;
        QString strLocal = "8FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x4A) || (bytFrameType == 0x4B) )
    {    // 100 baud 4FSK 500 Hz bandwidth
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 64;
        quint32 intLocal3 = 16;
        quint32 intLocal4 = 100;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x4C) || (bytFrameType == 0x4D) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 32;
        quint32 intLocal3 = 8;
        quint32 intLocal4 = 100;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x58) || (bytFrameType == 0x59) )
    {   // 25 baud 16FSK 500 Hz bandwidth
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 32;
        quint32 intLocal3 = 8;
        quint32 intLocal4 = 25;
        QString strLocal = "16FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x5A) || (bytFrameType = 0x5B) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 16;
        quint32 intLocal3 = 4;
        quint32 intLocal4 = 25;
        QString strLocal = "16FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x7A) || (bytFrameType == 0x7B) )
    {    // 600 baud 4FSK 2000 Hz bandwidth
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 600;
        quint32 intLocal3 = 150;
        quint32 intLocal4 = 600;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x7C) || (bytFrameType == 0x7D) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 1;
        quint32 intLocal2 = 200;
        quint32 intLocal3 = 50;
        quint32 intLocal4 = 600;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x50) || (bytFrameType == 0x51) )
    {   // 2 Carrier Data Modes
        // 100 baud
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 2;
        quint32 intLocal2 = 64;
        quint32 intLocal3 = 32;
        quint32 intLocal4 = 100;
        QString strLocal = "4PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x52) || (bytFrameType == 0x53) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 2;
        quint32 intLocal2 = 108;
        quint32 intLocal3 = 36;
        quint32 intLocal4 = 100;
        QString strLocal = "8PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x54) || (bytFrameType == 0x55) )
    {   // 100 baud 16QAM
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 2;
        quint32 intLocal2 = 128;
        quint32 intLocal3 = 64;
        quint32 intLocal4 = 100;
        QString strLocal = "16QAM";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x60) || (bytFrameType == 0x61) )
    {   // 4 Carrier Data Modes
        // 100 baud
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 4;
        quint32 intLocal2 = 64;
        quint32 intLocal3 = 32;
        quint32 intLocal4 = 100;
        QString strLocal = "4PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x62) || (bytFrameType == 0x63) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 4;
        quint32 intLocal2 = 108;
        quint32 intLocal3 = 36;
        quint32 intLocal4 = 100;
        QString strLocal = "8PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x64) || (bytFrameType == 0x65) )
    {   //167 Baud
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 4;
        quint32 intLocal2 = 120;
        quint32 intLocal3 = 40;
        quint32 intLocal4 = 167;
        QString strLocal = "4PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x66) || (bytFrameType == 0x67) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 4;
        quint32 intLocal2 = 159;
        quint32 intLocal3 = 60;
        quint32 intLocal4 = 167;
        QString strLocal = "8PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x68) || (bytFrameType == 0x69) )
    {   // 100 baud 2 carrier 4FSK
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 2;
        quint32 intLocal2 = 64;
        quint32 intLocal3 = 16;
        quint32 intLocal4 = 100;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x70) || (bytFrameType == 0x71) )
    {   // 8 Carrier Data modes
        // 100 baud
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 8;
        quint32 intLocal2 = 64;
        quint32 intLocal3 = 32;
        quint32 intLocal4 = 100;
        QString strLocal = "4PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x72) || (bytFrameType == 0x73) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 8;
        quint32 intLocal2 = 108;
        quint32 intLocal3 = 36;
        quint32 intLocal4 = 100;
        QString strLocal = "8PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x74) || (bytFrameType == 0x75) )
    {   // 167 baud
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 8;
        quint32 intLocal2 = 120;
        quint32 intLocal3 = 40;
        quint32 intLocal4 = 167;
        QString strLocal = "4PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x76) || (bytFrameType == 0x77) )
    {
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 8;
        quint32 intLocal2 = 159;
        quint32 intLocal3 = 60;
        quint32 intLocal4 = 167;
        QString strLocal = "8PSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else if ( (bytFrameType == 0x78) || (bytFrameType == 0x79) )
    {   // 100 baud 4 carrier 4FSK
        bool blnLocal = ((0x1 & bytFrameType) != 0);
        quint16 intLocal1 = 4;
        quint32 intLocal2 = 64;
        quint32 intLocal3 = 16;
        quint32 intLocal4 = 100;
        QString strLocal = "4FSK";
        blnOdd = &blnLocal;
        intNumCar = &intLocal1;
        intDataLen = &intLocal2;
        intRSLen = &intLocal3;
        strMod = &strLocal;
        intBaud = &intLocal4;
    }
    else
    {
        qCritical() << "[PSKDataInfo] No data for frame type= H" << QString("%1").arg(bytFrameType);
        return false;
    }

//        if ( (bytFrameType >= 0) && (bytFrameType <= 0x1F) )
//            strType = &strFrameType.at(0);
//        else if ( bytFrameType >= 0xE0 )
//            strType = &strFrameType.at(0xE0);
//        else
//            strType = &strFrameType.at(bytFrameType);

    return true;
} //  FrameInfo()

QStringList FrameInfo::getAllDataModes()
{
QStringList strAllDataModes;

for ( int x = 0; x < AllModes.count(); ++x )
{
    strAllDataModes.append(AllModes.at(x));
}
return strAllDataModes;
}

quint8 FrameInfo::FrameCode(QString strFrameName)
{
    for ( quint16 i = 0; i < 255; ++i )
    {
        if ( strFrameType.at(i) == strFrameName )  return quint8(i);
    }
    return 0;
} // FrameCode()

quint8 FrameInfo::ComputeTypeParity(quint8 bytFrameType)
{
    quint8 bytMask = 0xC0;
    quint8 bytParitySum = 0x01;
    quint8 bytSym = 0x00;

    for ( quint8 k = 0; k < 3; ++k )
    {
        bytSym = (bytMask & bytFrameType) >> (2 * (3 - k));
        bytParitySum = bytParitySum ^ bytSym;
        bytMask = bytMask >> 2;
    }
    return bytParitySum & 0x03;
} // ComputeTypeParity()





// -------------------------------------------------
//              PRIVATE FUNCTIONS AND METHODS
// -------------------------------------------------
void FrameInfo::initFrameNames()
{
    // names stored in ardop globals AllModes
    strFrameType.clear();
    for ( int x = 0; x < AllModes.count(); ++x )
    {
        strFrameType[x] = AllModes.at(x);
    }
}

void FrameInfo::initValidFrameTypes()
{
    quint32 intValidFrameCtr = 0;
    quint32 intValidISSFrameCtr = 0;

    bytValidFrameTypes.resize(255);
    bytValidISSFrameTypes.resize(255);

    for ( quint16 i = 0; i < 255; ++i )
    {
        if ( isValidFrameType(i) )
        {
            bytValidFrameTypes[intValidFrameCtr] = i;
            ++intValidFrameCtr;
        }

        if ( isValidISSFrameType(i) )
        {
            bytValidISSFrameTypes[intValidISSFrameCtr] = i;
            ++intValidISSFrameCtr;
        }
    } // Next i
    bytValidFrameTypes.reserve(intValidFrameCtr - 1);
    bytValidISSFrameTypes.reserve(intValidISSFrameCtr - 1);
}  //   initValidFrameTypes()

