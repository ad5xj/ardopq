#include "helpabout.hpp"

HelpAbout::HelpAbout(QWidget *parent) : QDialog(parent)
{
    ui = new Ui::HelpAbout;
    ui->setupUi(this);
    setStyleSheet("QDialog {color: white; background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #aaa, stop: 1 #222);}");

    QString msg;

    msg += tr("<b>ARDOP Open Source Modem Project</b><br /><br />"
              "This application is an abstract conversion "
              "of the ARDOP modem project originally written<br />"
              "in VB.NET or 'C' for Windows by <br />"
              "  Rick Muething, KN6KB and <br />"
              "  John Wiseman,  G8BPQ.<br /><br />"
              "There has been a concious attempt to leverage existing "
              "code and is working well in other applications. It is designed "
              "to be platform independent and transportable. It was "
              "completely developed in C++ using the Digia Open Source Qt libraries "
              "version 5.7.n &copy; 2016 Digia Corp.<br />"
              "This ARDOP Modem software project is released under the GPL v3 or "
              "later, license.<br />"
              "&copy; Copyright 2016 Ken Standard AD5XJ<br />"
              "ad5xj@arrl.net<br />"
              "See the copyright notice in the header portion of the source code for"
              "more detail.<br /><br />"
              "Qt is licensed under the GNU Lesser General Public License (LGPL) "
              "version 3 and is appropriate for the development of Qt applications. "
              "This application wil comply with the terms and conditions of the "
              "GNU LGPL version 3 (or GNU GPL version 3). The License was included "
              "in the source code files of this project. If you did not receive a "
              "copy of the license, please got to http://fsf.org for a current "
              "version or write to:<br />"
              "Free Software Foundation, Inc.<br />"
              "59 Temple Place - Suite 330,<br />"
              "Boston, MA  02111-1307, USA.<br /><br />"
              "Parts of this project were derived from the Qt SDK Examples. "
              "Many thanks are due to the Digia Corp. perveyors of the Qt Framework "
              "and SDK. All applicable copyrights by Digia apply.<br /><br />"
              "Also included in this project is the Open Source libraries of "
              "the MIT Fast Fourier Transform project FFTW v3 by <br />"
              "  Copyright &copy; 2003, 2007-14 Matteo Frigo<br />"
              "  Copyright &copy; 2003, 2007-14 Massachusetts Institute of Technology<br />"
              "released under the GLP v3 or later license. See the included license.<br /><br />"
              "Also from QSSTV Copyright &copy; 2000-2012 <br />"
              "  by Johan Maes, ON4QZ <br />"
              "  on4qz@telenet.be <br />"
              "  http://users.telenet.be/on4qz<br />"
              "released as a GPL v3 project. "
              "Thanks are due to Johan Maes for his help and the basis for some of the "
              "code in this project modified from the QSSTV project. Thanks "
              "for your help and consultation Johan.<br /><br />"
              "Much help and support also came from ham radio friends and "
              "fellow programmers like:<br />"
              "  Doug Collinge, VE3GNU for his contributions and support <br />"
              "as well as:<br />"
              "  Patrick Deschastres, F1JSC for his help and expertise.<br /><br /><br /><br />"
              );
    ui->btnOk->setStyleSheet("QPushButton {color: white; background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ccc, stop: 1 #555);}");
    ui->lblMsg->document()->setHtml(msg);
    ui->lblMsg->setReadOnly(true);
    setModal(true);
    setWindowFlags(Qt::FramelessWindowHint);
    setParent(0); // Create TopLevel-Widget
    setAttribute(Qt::WA_NoSystemBackground, true);
    connect(ui->btnOk,SIGNAL(clicked(bool)),this,SLOT(close()));
}

void HelpAbout::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
