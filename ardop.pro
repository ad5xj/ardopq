include(ardop.pri)
TEMPLATE = app
TARGET = ardopd
#TARGET = ardop
DESTDIR = bin
DESTDIR_TARGET = bin

QT += \
    widgets \
    charts \
    multimedia \
    multimediawidgets \
    xml \
    network \
    serialport

CONFIG += qt resources thread C++11 silent
CONFIG += exceptions debug  # comment out for release


MOC_DIR     = .moc
RCC_DIR     = .rcc
OBJECTS_DIR = .obj
UI_DIR      = .ui
UI_HEADERS_DIR = .ui

INCLUDEPATH += \
    images \
    inc \
    inc/Utils \
    inc/Sound \
    inc/Widgets \
    inc/Protocol \
    inc/WavPlayer

win32: {
INCLUDEPATH += C:\\FFTW3
}

win32: {
DEPENDPATH  += C:\\FFTW3
}

HEADERS += inc/ardop_global.hpp

HEADERS += inc/Utils/utils.hpp

HEADERS += \
    inc/Sound/audioinputio.hpp \
    inc/Sound/audiooutputio.hpp \
    inc/Sound/bufferobject.hpp

HEADERS += \
    inc/Protocol/demodulator.hpp \
    inc/Protocol/modulator.hpp \
    inc/Protocol/reedsolomon.hpp \
    inc/Protocol/frameinfo.hpp

HEADERS += \
    inc/Widgets/vumeterwidget.hpp \
    inc/Widgets/waterfallwidget.hpp \
    inc/Widgets/constellationwidget.hpp

HEADERS += \
    inc/WavPlayer/videowidget.hpp \
    inc/WavPlayer/playlistmodel.hpp \
    inc/WavPlayer/wavplayer.hpp \
    inc/WavPlayer/histogramwidget.hpp

HEADERS += \
    inc/Protocol/demodulator.hpp \
    inc/Protocol/modulator.hpp \
    inc/Protocol/reedsolomon.hpp \
    inc/Protocol/frameinfo.hpp

HEADERS += \
    inc/helpabout.hpp \
    inc/tncpanel.hpp \
    inc/tncsetup.hpp \
    inc/modem.hpp


SOURCES += utils/utils.cpp

SOURCES += \
    sound/bufferobject.cpp \
    sound/audioinputio.cpp \
    sound/audiooutputio.cpp

SOURCES += \
    widgets/constellationwidget.cpp \
    widgets/waterfallwidget.cpp \
    widgets/vumeterwidget.cpp

SOURCES += \
    protocol/demodulator.cpp \
    protocol/modulator.cpp \
    protocol/decoder.cpp \
    protocol/encoder.cpp \
    protocol/reedsolomon.cpp \
    protocol/protocol.cpp \
    protocol/frameinfo.cpp

SOURCES += \
    wavplayer/videowidget.cpp \
    wavplayer/playlistmodel.cpp \
    wavplayer/histogramwidget.cpp \
    wavplayer/wavplayer.cpp

SOURCES += \
    src/helpabout.cpp \
    src/tncpanel.cpp \
    src/tncsetup.cpp

SOURCES += \
    modem.cpp \
    main.cpp

FORMS += \
    ui/calibration.ui \
    ui/helpabout.ui \
    ui/radiosetup.ui \
    ui/soundconfig.ui \
    ui/tncpanel.ui \
    ui/tncsetup.ui \
    ui/wavplayer.ui

RESOURCES += \
    images/ardopres.qrc


LIBS += -LC:\\FFTW3\ -lfftw3

