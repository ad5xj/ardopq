#include <QtWidgets/QApplication>

#include "tncpanel.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    TNCPanel t;
    t.show();

    return a.exec();
}
