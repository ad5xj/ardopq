#ifndef STATETESTER_HPP
#define STATETESTER_HPP
#include <QtWidgets/QDialog>

#include "ui_statetester.h"

namespace Ui {
class StateTester;
}

class StateTester : public QDialog
{
    Q_OBJECT

public:
    explicit StateTester(QWidget *parent = 0);
    ~StateTester();

signals:
    void signalDCD();
    void signalNODCD();

protected:
    void changeEvent(QEvent *e);

private slots:
    void slotEmitDCD();

private:
    bool LEDonoff;

    Ui::StateTester *ui;

};

#endif // STATETESTER_HPP
