#include <QtCore/QDebug>

#include "statetester.hpp"

StateTester::StateTester(QWidget *parent) : QDialog(parent),
    ui(new Ui::StateTester)
{
    ui->setupUi(this);
    LEDonoff = false;
    connect(ui->btnDCD,SIGNAL(clicked(bool)),this,SLOT(slotEmitDCD()));
}

StateTester::~StateTester()
{
    delete ui;
}

void StateTester::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void StateTester::slotEmitDCD()
{
    if (ui->radGood->isChecked())
        emit signalDCD();
    else
        emit signalNODCD();

    qDebug() << "Signaling Conn";
}
