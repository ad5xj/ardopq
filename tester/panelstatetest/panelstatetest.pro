#DEFINES += DEBUG_FFT

TEMPLATE = app
TARGET   = guitest
DESTDIR  = ../../bin
DESTDIR_TARGET = ../../bin


QT      += core gui widgets multimedia serialport
CONFIG  += qt thread resources
debug:CONFIG += exceptions debug

MOC_DIR   = .moc
RCC_DIR   = .rcc
OBJECTS_DIR = .obj
UI_HEADERS_DIR = .ui
UI_SOURCES_DIR = .

win32: {
INCLUDEPATH += C:\\FFTW3
}
INCLUDEPATH += \
    ../../inc \
    ../../inc/Sound \
    ../../inc/Utils \
#    ../../inc/Protocol \
    ../../inc/Widgets

win32: {
DEPENDPATH += C:\\FFTW3\
}

HEADERS += ../../inc/ardop_global.hpp

HEADERS += \
    ../../inc/Sound/audioinputio.hpp \
    ../../inc/Sound/bufferobject.hpp

HEADERS += \
    ../../inc/Utils/fftw3wrapper.hpp \
    ../../inc/Utils/utils.hpp

HEADERS += \
    ../../inc/Widgets/vumeterwidget.hpp \
    ../../inc/Widgets/fftdisplay.hpp \
    ../../inc/Widgets/markerwidget.hpp \
    ../../inc/Widgets/spectrumwidget.hpp

#HEADERS += \
#    ../../inc/Protocol/demodulator.hpp

HEADERS += \
    ../../inc/helpabout.hpp \
    ../../inc/tncsetup.hpp \
    ../../inc/tncpanel.hpp \
    statetester.hpp



SOURCES += \
    ../../sound/audioinputio.cpp \
    ../../sound/bufferobject.cpp

SOURCES += \
    ../../utils/utils.cpp \
    ../../utils/fftw3wrapper.cpp

SOURCES += \
    ../../widgets/vumeterwidget.cpp \
    ../../widgets/fftdisplay.cpp \
    ../../widgets/markerwidget.cpp \
    ../../widgets/spectrumwidget.cpp

#SOURCES += \
#    ../../protocol/demodulator.cpp

SOURCES += \
    ../../src/tncsetup.cpp \
    ../../src/tncpanel.cpp \
    ../../src/helpabout.cpp \
    main.cpp \
    statetester.cpp

RESOURCES += ../../images/ardopres.qrc

FORMS   += \
    ../../ui/spectrumwidget.ui \
    ../../ui/tncpanel.ui \
    ../../ui/tncsetup.ui \
    ../../ui/helpabout.ui \
    statetester.ui


win32: {
LIBS += -LC:\\FFTW3\ -lfftw3-3
}

unix:!mac {
LIBS += -L/usr/lib/ -lfftw3
}

