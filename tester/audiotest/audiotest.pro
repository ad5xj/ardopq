#DEFINES += DEBUG_AUDIO
TEMPLATE = app
TARGET = audiotest
DESTDIR = ../../bin
DESTDIR_TARGET = ../../

QT     += core gui widgets multimedia
CONFIG += qt thread exceptions
debug:CONFIG += debug

MOC_DIR  = .moc
RCC_DIR  = .rcc
UI_DIR   = .ui
OBJECTS_DIR = .obj

INCLUDEPATH += \
    ../../inc \
    ../../inc/Sound \
    ../../inc/Widgets \
    ../../inc/Protocol \
    ../../inc/Ui

HEADERS  += ../../inc/Widgets/vumeterwidget.hpp

HEADERS  += \
    ../../inc/Sound/bufferobject.hpp \
    ../../inc/Sound/audioio.hpp

HEADERS  += ../../inc/Ui/dlgsndconfig.hpp

HEADERS  += audiotest.hpp


SOURCES  += \
    ../../sound/bufferobject.cpp \
    ../../sound/audioio.cpp


SOURCES  += ../../widgets/vumeterwidget.cpp

SOURCES  += ../../src/dlgsndconfig.cpp

SOURCES += \
    main.cpp \
    audiotest.cpp

FORMS    += \
    ../../ui/dlgsndconfig.ui \
    audiotest.ui

RESOURCES += ../../images/ardopres.qrc

