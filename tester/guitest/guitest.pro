DEFINES += DEBUG_FFT
include(localconfig.pri)

TEMPLATE = app
TARGET   = guitest
DESTDIR  = ../../bin
DESTDIR_TARGET = ../../bin

QT      += core gui widgets multimedia
CONFIG  += qt thread
debug:CONFIG += exceptions debug

MOC_DIR += .moc 
RCC_DIR += .rcc 
OBJECTSDIR += .obj
UI_DIR  += .ui

INCLUDEPATH += \
    ../../inc \
    ../../inc/Sound \
    ../../inc/Utils \
    ../../inc/Widgets


HEADERS += ../../inc/ardop_global.hpp \
    ../../inc/Widgets/markerwidget.hpp

HEADERS += \
    ../../inc/Sound/audioinputio.hpp \
    ../../inc/Sound/bufferobject.hpp \
    ../../inc/Utils/fftw3wrapper.hpp

HEADERS += ../../inc/Utils/utils.hpp

HEADERS += \
    ../../inc/Widgets/vumeterwidget.hpp \
    ../../inc/Widgets/fftdisplay.hpp \
    ../../inc/Widgets/spectrumwidget.hpp

HEADERS += guitest.hpp


SOURCES += \
    ../../sound/audioinputio.cpp \
    ../../sound/bufferobject.cpp \
    ../../widgets/markerwidget.cpp

SOURCES += \
    ../../utils/utils.cpp \
    ../../utils/fftw3wrapper.cpp

SOURCES += \
    ../../widgets/vumeterwidget.cpp \
    ../../widgets/fftdisplay.cpp \
    ../../widgets/spectrumwidget.cpp

SOURCES += \
    main.cpp \
    guitest.cpp

FORMS   += \
    guitest.ui \
    ../../ui/spectrumwidget.ui


DISTFILES += \
    localconfig.pri \
    localconfig.pri.doug \
    localconfig.pri.ken

