# This local configs file included into the main .pro file
# for local configs such as paths.

win32: {
INCLUDEPATH += C:/FFTW3
DEPENDPATH  += C:/FFTW3
LIBS += -LC:/FFTW3/ -lfftw3-3
}

unix:!mac {
LIBS += -L/usr/lib/ -lfftw3
}
