/*!
 * \copyright 2016 AD5XJ
 * contact: Ken ad5xj@arrl.net
 *
 * The GPL License was included in the source code files
 * of this project. If you did not receive a copy of the
 * license, please got to http://fsf.org for a current
 * version or write to:<br />
 * Free Software Foundation, Inc.<br />
 * 59 Temple Place - Suite 330,<br />
 * Boston, MA  02111-1307, USA.<br /><br />
 *
 * $QT_BEGIN_LICENSE:GPL$
 * You may use this file under the terms of the GPL v3
 * or later license as follows:
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * notice is included with the source code:
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $QT_END_LICENSE$   */
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QIODevice>
#include <QtCore/QTranslator>
#include <QtCore/QSettings>
#include <QtCore/QDateTime>
#include <QtCore/QTextStream>
#include <QtCore/QMessageLogger>
#include <QtCore/QMessageLogContext>
#include <QtWidgets/QApplication>

#include "ardop_global.hpp"

#include "modem.hpp"

// -------------------------------------
// log file
// -------------------------------------
void debugMessageOutput(QtMsgType type,const QMessageLogContext &context,const QString &msg)
{
    bool    dbg2file = false;
    QDir    dir;
    QFile   logfile;
    QString txt = "";
    QString slogfile = "";
    QString logname = "";
    QDateTime dttTimestamp = QDateTime::currentDateTimeUtc();

    dbg2file = false;
    logname  = QString(dir.canonicalPath().toLatin1());
    logname = QDir::fromNativeSeparators(logname);
    logname += "/../Logs/ARDOP_TNC_";
    logname += dttTimestamp.toString("yyyyMMdd");
    logname += ".log";
    logname = QDir::fromNativeSeparators(logname);

    QSettings settings(logname.toLatin1(),QSettings::IniFormat);
    settings.beginGroup("CONFIG");
     dbg2file = settings.value("Dbg2File",true).toBool();
     slogfile  = settings.value("DbgFile").toString().trimmed();
    settings.endGroup();
    if ( slogfile == "" )
    {
        // do nothing
    }
    else
    {
        logname = "";
        logname = slogfile;
    }
    qDebug() << "Log file using " << logname;
    // Switch structure left to be converted to write
    // into the file in the future
    QString debugdate = QDateTime::currentDateTimeUtc().toString("dd-MM-yyyy hh:mm:ss.zzz t ");
    QString message(msg);
    txt += debugdate + " ";
    // extract the priority of the message
    int priority = message.left(1).toInt();
    if (priority >= 0) message.remove(0, 1);

    switch ( type )
    {
    case QtInfoMsg:
        txt += "INFO [";
        txt += QString(context.function);
        txt += ":";
        txt += QString("%1").arg(context.line);
        txt += "]";
        txt += " -- ";
        txt += QString(" %1").arg(msg);
        break;
    case QtWarningMsg:
        txt += "WARNING [";
        txt += QString(context.function);
        txt += ":";
        txt += QString("%1").arg(context.line);
        txt += "]";
        txt += " -- ";
        txt += QString(" %1").arg(msg);
        break;
    case QtCriticalMsg:
        txt += "!!!CRITICAL!!! [";
        txt += QString(context.function);
        txt += ":";
        txt += QString("%1").arg(context.line);
        txt += "]";
        txt += "--";
        txt += QString(" %1").arg(msg);
        break;
    case QtFatalMsg:
        txt += "<<<Fatal>>> [";
        txt += QString(context.function);
        txt += ":";
        txt += QString("%1").arg(context.line);
        txt += "]";
        txt += " -- ";
        txt += QString(" %1").arg(msg);
        txt += "\n";
        txt += "%{backtrace [depth=7] [separator='...']}";
        break;
    case QtDebugMsg:
        txt += "DEBUG [";
        txt += QString(context.function);
        txt += ":";
        txt += QString("%1").arg(context.line);
        txt += "]";
        txt += "--";
        txt += QString(" %1").arg(msg);
        break;
    }

    if ( dbg2file )
    {
        if ( logname == "" )
        {
            qDebug() << "Opening logger file " << logname;
            QFile logfile(logname.toLatin1());
            bool ok = logfile.open(QIODevice::WriteOnly|QIODevice::Append);
            if ( !ok )
            {
                qDebug() << "Error on open " << logfile.errorString();
                qDebug() << logfile.errorString();
            }
        }
        else
        {
            qDebug() << "Writing to logger file " << logname;
            QFile logfile(logname.toLatin1());
            bool ok = logfile.open(QIODevice::WriteOnly|QIODevice::Append);
            if ( !ok )
            {
                qDebug() << "Error on open " << logfile.errorString();
                qDebug() << logfile.errorString();
            }
            else
            {
                qDebug() << "Opening Logfile " << logfile.fileName();
                logfile.open(QIODevice::WriteOnly | QIODevice::Append);
            }
            QTextStream ts(&logfile);
            ts << txt << endl;
            logfile.close();
        }
    }
}



int main(int argc, char *argv[])
{
    qDebug() << "starting application";
    QApplication a(argc, argv);

    qDebug() << "setting local app info";
    a.setApplicationVersion(APP_VERSION);
    a.setApplicationName(APP_NAME);

    qDebug() << "installing translator";
    QTranslator translator;
    a.installTranslator(&translator);

    QSettings settings("ardop_tnc.ini",QSettings::IniFormat);
    settings.beginGroup("TNC");
      bool logit = settings.value("DebugLog",false).toBool();
    settings.endGroup();
    if ( logit )
    {
      qDebug() << "installing custom message handler";
      QT_MESSAGELOGCONTEXT
      qInstallMessageHandler(debugMessageOutput);
    }
#ifdef DEBUG_MODEM
    qDebug() << "Starting MODEM";
#endif
    ARDOPModem m;
    m.setNickName("ARDOPMODEM");

    int ret = a.exec();
    qInstallMessageHandler(0);
    return ret;
}
