#include <QtCore/QDebug>
#include <QtCore/qmath.h>
#include <QtCore/qendian.h>
#include <QtCore/QThread>
//#include <QtWidgets/QApplication>
#include <QtMultimedia/QAudioDeviceInfo>
#include <QtMultimedia/QAudioOutput>

#include "audiooutputio.hpp"

// constants for testing
const int DurationSeconds = 1;
const int ToneSampleRateHz = 600;
const int DataSampleRateHz = 44100;
const int BufferSize      = 32768;
//


AudioOutputIO::AudioOutputIO(QObject *parent) : QIODevice(parent)
{
    blnFormatSet   = false;
    numUsedBytes   = 0;
    m_samplerate   = 0;
    m_pos          = 0;
    m_maxAmplitude = 0.0;
    m_level        = 0.0;

    m_format = QAudioDeviceInfo::defaultInputDevice().preferredFormat();
    TxBuffer = new BufferObject;
}

AudioOutputIO::~AudioOutputIO()
{
    delete TxBuffer;
}

void AudioOutputIO::setAudioFormat(QAudioFormat fmt)
{
#ifdef DEBUG_AUDIO
    qDebug() << "Setting Audio Format from params";
    qDebug() << "Audio Format:";
    if ( fmt.byteOrder() == QAudioFormat::BigEndian )
      qDebug() << "   Byte: BIG ENDIAN";
    else
        qDebug() << "   Byte: LITTLE ENDIAN";
    qDebug() << "   Ch:" << QString("%1").arg(fmt.channelCount());
    qDebug() << "Codec:" << QString("%1").arg(fmt.codec());
    qDebug() << "   Sz:" << QString("%1").arg(fmt.sampleSize());
    qDebug() << " Rate:" << QString("%1").arg(fmt.sampleRate());
    if ( fmt.sampleType() == QAudioFormat::SignedInt )
      qDebug() << "  Typ: SignedInt";
    else if ( fmt.sampleType() == QAudioFormat::UnSignedInt )
        qDebug() << "  Typ: UnSignedInt";
    else if ( fmt.sampleType() == QAudioFormat::Float )
        qDebug() << "  Typ: Float";
#endif
    m_format.setByteOrder(fmt.byteOrder());
    m_format.setChannelCount(fmt.channelCount());
    m_format.setCodec(fmt.codec());
    m_format.setSampleRate(fmt.sampleRate());
    m_format.setSampleSize(fmt.sampleSize());
    m_format.setSampleType(fmt.sampleType());

    switch ( m_format.sampleSize() )
    {
    case 8:
        switch (m_format.sampleType())
        {
        case QAudioFormat::UnSignedInt:
            m_maxAmplitude = 255;
            break;
        case QAudioFormat::SignedInt:
            m_maxAmplitude = 127;
            break;
        default:
            break;
        }
        break;
    case 16:
        switch (m_format.sampleType())
        {
        case QAudioFormat::UnSignedInt:
            m_maxAmplitude = 65535;
            break;
        case QAudioFormat::SignedInt:
            m_maxAmplitude = 32767;
            break;
        default:
            break;
        }
        break;

    case 32:
        switch (m_format.sampleType())
        {
        case QAudioFormat::UnSignedInt:
            m_maxAmplitude = 0xffffffff;
            break;
        case QAudioFormat::SignedInt:
            m_maxAmplitude = 0x7fffffff;
            break;
        case QAudioFormat::Float:
            m_maxAmplitude = 0x7fffffff; // Kind of
        default:
            break;
        }
        break;

    default:
        break;
    }
    blnFormatSet = true; // Now audioIO is ready to use
#ifdef DEBUG_AUDIO
    qDebug() << "Audio Format Set....exiting";
#endif
}

void AudioOutputIO::start()
{
    if ( !blnFormatSet )
    { // check to see the format has been set before using
#ifdef DEBUG_AUDIO
        qCritical() << "Trying to start without format set...";
#endif
        return;
    }
    errorString() = "";
    // open for capture

    if ( !open(QIODevice::ReadOnly) )
    {
#ifdef DEBUG_AUDIO
    qDebug() << "Audio IODevice open error..." << errorString();
#endif
        return;
    }
#ifdef DEBUG_AUDIO
    qDebug() << "Audio IODevice open...exiting";
#endif
    emit signalAudioOutputOK();
}

void AudioOutputIO::stop()  { m_pos = 0; close(); }

qint64 AudioOutputIO::readData(char *data, qint64 len)
{
    qint64 total = 0;
    qint64 bufsiz = TxBuffer->getBufSize();

    // data must be pre-positioned
    // in TxBuffer before calling here
    // and preferrablly position is at 0
    if ( TxBuffer->bufferNotEmpty )
    {
        while ( len - total > 0 )
        {
            const qint64 chunk = qMin((bufsiz - m_pos), len - total);
            memcpy(data + total, TxBuffer->constData() + m_pos, chunk);
            m_pos = (m_pos + chunk) % bufsiz;
            total += chunk;
            if ( m_pos >= bufsiz )
            {
                TxBuffer->buffer->clear();
                break;
            }
        }
    }
    return total;
}

qint64 AudioOutputIO::writeData(const char *data, qint64 len)
{
    // not used here
    Q_UNUSED(data)
    Q_UNUSED(len)
    return 0;
}


