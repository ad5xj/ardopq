#include <QtCore/QStandardPaths>
#include <QtCore/QTime>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtMultimedia/QMediaMetaData>

#include "playlistmodel.hpp"

#include "wavplayer.hpp"

WavPlayer::WavPlayer(QWidget *parent) : QDialog(parent)
{
    ui = new Ui::WavPlayer;
    ui->setupUi(this);

    initPlayer();
}
WavPlayer::~WavPlayer() { delete ui; }

// ----------------------------------------------------
// LOCAL EVENT HANDLERS
// ----------------------------------------------------
void WavPlayer::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch ( e->type() )
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

// ----------------------------------------------------
// LOCAL SLOT HANDLERS
// ----------------------------------------------------
void WavPlayer::jump(const QModelIndex &index)
{
    if ( index.isValid() )
    {
        playlist->setCurrentIndex(index.row());
        player->play();
    }
}

void WavPlayer::seek(qint32 seconds)  { player->setPosition(seconds * 1000); }

void WavPlayer::openwav()
{
      QFileDialog fileDialog(this);
      fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
      fileDialog.setWindowTitle(tr("Open WAV Files"));
      QStringList supportedMimeTypes = player->supportedMimeTypes();
      if ( !supportedMimeTypes.isEmpty() )
      {
          supportedMimeTypes.append("audio/vnd.wav"); // .wav playlists
          fileDialog.setMimeTypeFilters(supportedMimeTypes);
      }
      fileDialog.setDirectory(QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).value(0, QDir::homePath()));
      if ( fileDialog.exec() == QDialog::Accepted )  addToPlaylist(fileDialog.selectedUrls());
}

void WavPlayer::statusChanged(QMediaPlayer::MediaStatus status)
{
    handleCursor(status);

    // handle status message
    switch (status) {
    case QMediaPlayer::UnknownMediaStatus:
        break;
    case QMediaPlayer::NoMedia:
        break;
    case QMediaPlayer::LoadedMedia:
        break;
    case QMediaPlayer::BufferingMedia:
        break;
    case QMediaPlayer::BufferedMedia:
        setStatusInfo(QString());
        break;
    case QMediaPlayer::LoadingMedia:
        setStatusInfo(tr("Loading..."));
        break;
    case QMediaPlayer::StalledMedia:
        setStatusInfo(tr("Media Stalled"));
        break;
    case QMediaPlayer::EndOfMedia:
        QApplication::alert(this);
        break;
    case QMediaPlayer::InvalidMedia:
        slotDisplyErrMsg();
        break;
    }
}

void WavPlayer::handleCursor(QMediaPlayer::MediaStatus status)
{
#ifndef QT_NO_CURSOR
    if (status == QMediaPlayer::LoadingMedia ||
        status == QMediaPlayer::BufferingMedia ||
        status == QMediaPlayer::StalledMedia
       )
        setCursor(QCursor(Qt::BusyCursor));
    else
        unsetCursor();
#endif
}

void WavPlayer::durationChanged(qint64 duration)
{
    m_duration = duration/1000;
    ui->horizontalSlider_2->setMaximum(duration / 1000);
}

void WavPlayer::positionChanged(qint64 progress)
{
    if ( !ui->horizontalSlider_2->isSliderDown() )
    {
        ui->horizontalSlider_2->setValue(progress / 1000);
    }
    updateDurationInfo(progress / 1000);
}

void WavPlayer::playlistPositionChanged(quint32 currentItem)
{
    ui->playlistView->setCurrentIndex(playlistModel->index(currentItem, 0));
}

void WavPlayer::bufferingProgress(int prog)
{
    setStatusInfo(tr("Buffering %4%").arg(prog));
}


void WavPlayer::metaDataChanged()
{
    if ( player->isMetaDataAvailable() )
    {
        setTrackInfo(QString("%1 - %2")
                     .arg(player->metaData(QMediaMetaData::AlbumArtist).toString())
                     .arg(player->metaData(QMediaMetaData::Title).toString()));

        if ( coverLabel )
        {
            QUrl url = player->metaData(QMediaMetaData::CoverArtUrlLarge).value<QUrl>();
            coverLabel->setPixmap(!url.isEmpty() ? QPixmap(url.toString()) : QPixmap());
        }
    }
}



void WavPlayer::setVolume(quint16 vol) { ui->volume->setValue(vol); }

void WavPlayer::setMuted(bool muted)
{
    if ( muted != m_muted )
    {
        m_muted = muted;
        ui->btnMute->setIcon(style()->standardIcon(muted ? QStyle::SP_MediaVolumeMuted : QStyle::SP_MediaVolume));
    }
}

void WavPlayer::setState(QMediaPlayer::State pbstate)
{
    if ( pbstate != m_pbstate )
    {
        m_pbstate = pbstate;

        switch (pbstate)
        {
        case QMediaPlayer::StoppedState:
            ui->btnStop->setEnabled(false);
            ui->btnOpen->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
            break;
        case QMediaPlayer::PlayingState:
            ui->btnStop->setEnabled(true);
            ui->btnPause->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
            break;
        case QMediaPlayer::PausedState:
            ui->btnStop->setEnabled(true);
            ui->btnPause->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
            break;
        }
    }
}

void WavPlayer::playClicked()
{
    switch ( m_pbstate )
    {
    case QMediaPlayer::StoppedState:
        break;
    case QMediaPlayer::PausedState:
        emit play();
        break;
    case QMediaPlayer::PlayingState:
        emit pause();
        break;
    }
}


void WavPlayer::muteClicked()  { emit changeMuting(!m_muted); }

void WavPlayer::setPlaybackRate(qreal pbrate)
{
    for ( int i = 0; i < ui->cboRateBox->count(); ++i )
    {
        if ( qFuzzyCompare(pbrate, qreal(ui->cboRateBox->itemData(i).toReal())))
        {
            ui->cboRateBox->setCurrentIndex(i);
        }
        else
        {
            ui->cboRateBox->addItem(QString("%1x").arg(pbrate), QVariant(pbrate));
            ui->cboRateBox->setCurrentIndex(ui->cboRateBox->count() - 1);
        }
    }
}

// ----------------------------------------------------
// FRIENDLY METHODS AND FUNCTIONS
// ----------------------------------------------------
void WavPlayer::updateRate() { emit changeRate(playbackRate()); }

void WavPlayer::updateDurationInfo(qint64 currentInfo)
{
    QString tStr;
    if ( currentInfo || m_duration )
    {
        QTime currentTime((currentInfo / 3600) % 60, (currentInfo / 60) % 60, currentInfo % 60, (currentInfo * 1000) % 1000);
        QTime totalTime((m_duration / 3600) % 60, (m_duration / 60) % 60, m_duration % 60, (m_duration * 1000) % 1000);
        QString format = "mm:ss";
        if ( m_duration > 3600 ) format = "hh:mm:ss";
        tStr = currentTime.toString(format) + " / " + totalTime.toString(format);
    }
    ui->lblRunTme->setText(tStr);
}

void WavPlayer::setStatusInfo(QString info)
{
    statusInfo = info;
    if ( !statusInfo.isEmpty() )
        setWindowTitle(QString("%1 | %2").arg(trackInfo).arg(statusInfo));
    else
        setWindowTitle(trackInfo);
}

void WavPlayer::setTrackInfo(QString info)
{
    trackInfo = info;
    if ( !statusInfo.isEmpty() )
        setWindowTitle(QString("%1 | %2").arg(trackInfo).arg(statusInfo));
    else
        setWindowTitle(trackInfo);
}

void WavPlayer::addToPlaylist(const QList<QUrl> urls)
{
    foreach (const QUrl &url, urls)
    {
        if ( isPlaylist(url) )
            playlist->load(url);
        else
            playlist->addMedia(url);
    }
}

void WavPlayer::initPlayer()
{
    m_muted = false;
    m_pbstate = QMediaPlayer::StoppedState;
    m_vol = 80;

    ui->btnFirst->setIcon(style()->standardIcon(QStyle::SP_MediaSeekBackward));
    ui->btnPause->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    ui->btnLast->setIcon(style()->standardIcon(QStyle::SP_MediaSkipForward));
    ui->btnMute->setIcon(style()->standardIcon(QStyle::SP_MediaVolume));

    connect(ui->btnOpen, SIGNAL(clicked()), this, SLOT(openwav()));
    connect(ui->btnFirst, SIGNAL(clicked()), this, SIGNAL(previous()));
    connect(ui->btnPause, SIGNAL(clicked()), this, SIGNAL(stop()));
    connect(ui->btnLast, SIGNAL(clicked()), this, SIGNAL(next()));
    connect(ui->btnMute, SIGNAL(clicked()), this, SLOT(muteClicked()));

    player = new QMediaPlayer(this);
    // owned by PlaylistModel
    playlist = new QMediaPlaylist();
    player->setPlaylist(playlist);

    connect(player, SIGNAL(durationChanged(qint64)), this, SLOT(durationChanged(qint64)));
    connect(player, SIGNAL(positionChanged(qint64)), this, SLOT(positionChanged(qint64)));
    connect(player, SIGNAL(metaDataChanged()),       this, SLOT(metaDataChanged()));
    connect(player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(statusChanged(QMediaPlayer::MediaStatus)));
    connect(player, SIGNAL(bufferStatusChanged(int)), this, SLOT(bufferingProgress(int)));
    connect(player, SIGNAL(error(QMediaPlayer::Error)), this, SLOT(slotDisplyErrMsg()));
    connect(playlist, SIGNAL(currentIndexChanged(int)), this, SLOT(playlistPositionChanged(quint32)));

    playlistModel = new PlaylistModel(this);
    playlistModel->setPlaylist(playlist);

    ui->playlistView->setModel(playlistModel);
    ui->playlistView->setCurrentIndex(playlistModel->index(playlist->currentIndex(), 0));

    connect(ui->playlistView, SIGNAL(activated(QModelIndex)), this, SLOT(jump(QModelIndex)));

    connect(ui->horizontalSlider_2, SIGNAL(sliderMoved(int)), this, SLOT(seek(int)));
}

#ifndef PLAYER_NO_COLOROPTIONS
void WavPlayer::showColorDialog()
{
    if ( !colorDialog )
    {
        QSlider *brightnessSlider = new QSlider(Qt::Horizontal);
        brightnessSlider->setRange(-100, 100);
        brightnessSlider->setValue(ui->video->brightness());
        connect(brightnessSlider, SIGNAL(sliderMoved(int)), ui->video, SLOT(setBrightness(int)));
        connect(ui->video, SIGNAL(brightnessChanged(int)), brightnessSlider, SLOT(setValue(int)));

        QSlider *contrastSlider = new QSlider(Qt::Horizontal);
        contrastSlider->setRange(-100, 100);
        contrastSlider->setValue(ui->video->contrast());
        connect(contrastSlider, SIGNAL(sliderMoved(int)), ui->video, SLOT(setContrast(int)));
        connect(ui->video, SIGNAL(contrastChanged(int)), contrastSlider, SLOT(setValue(int)));

        QSlider *hueSlider = new QSlider(Qt::Horizontal);
        hueSlider->setRange(-100, 100);
        hueSlider->setValue(ui->video->hue());
        connect(hueSlider, SIGNAL(sliderMoved(int)), ui->video, SLOT(setHue(int)));
        connect(ui->video, SIGNAL(hueChanged(int)), hueSlider, SLOT(setValue(int)));

        QSlider *saturationSlider = new QSlider(Qt::Horizontal);
        saturationSlider->setRange(-100, 100);
        saturationSlider->setValue(ui->video->saturation());
        connect(saturationSlider, SIGNAL(sliderMoved(int)), ui->video, SLOT(setSaturation(int)));
        connect(ui->video, SIGNAL(saturationChanged(int)), saturationSlider, SLOT(setValue(int)));

        QFormLayout *layout = new QFormLayout;
        layout->addRow(tr("Brightness"), brightnessSlider);
        layout->addRow(tr("Contrast"), contrastSlider);
        layout->addRow(tr("Hue"), hueSlider);
        layout->addRow(tr("Saturation"), saturationSlider);

        QPushButton *button = new QPushButton(tr("Close"));
        layout->addRow(button);

        colorDialog = new QDialog(this);
        colorDialog->setWindowTitle(tr("Color Options"));
        colorDialog->setLayout(layout);

        connect(button, SIGNAL(clicked()), colorDialog, SLOT(close()));
    }
    colorDialog->show();
}
#endif

