#ifndef MODULATOR_HPP
#define MODULATOR_HPP
/*! \file */
#include <QtCore/QObject>
#include <QtCore/QVector>

class ReedSolomon_8;
class FrameInfo;
class ARDOPprotocol;
class TNCPanel;

#include "tncpanel.hpp"


/*! \ingroup PROTOCOL
 * \class EncodeModulate
 * \brief
 * \details
 * This class provides for all Encoding (including FEC) and
 * Modulation for the ARDOP protocol.
 *
 * It is designed to work with the TNCPanel (as parent) and
 * the AudioIO module to send audio from the selected device. */
class EncodeModulate : public QObject
{
    Q_OBJECT

public:
    EncodeModulate(TNCPanel *parent=0);
    ~EncodeModulate();

    qint32 intAmp;             //!< Selected to have some margin in calculations with 16 bit values (< 32767) this must apply to all filters as well.

    // (only positive Phase values are in the table,
    // sign reversal is used to get the negative phase
    // values) This reduces the table size from 7680 to 3840
    // integers
    QVector<qint32> int50BaudTwoToneLeaderTemplate;         //!< holds just 1 symbol (20 ms) of the 50 Baud leader

    void setMCB(ModemControlBlock *mcb)     { MCB = mcb; }

    /*! \details
     * Function to creat a playable wave stream
     * (more efficent than playing a .wav file from Disk)  */
    void CreateWaveStream(QVector<qint32> *intFilteredSamplesToSend);

<<<<<<< HEAD
//    /*! \details
//     * Function used for setting up 75 Hz filter   */
//    qint32 int75HzFiltered(qint32);
=======
    /*! \details
     * Function used for setting up 75 Hz filter   */
    qint32 int75HzFiltered(qint32);
>>>>>>> remotes/origin/ken


    /*! \details
     * Function to generate a 5 second burst of a
     * 50 baud two tone (1475 and 1525 Hz) used for
     * setting up drive level   */
    QVector<qint32> ModTwoToneTest();

    /*! \details
     * Function to accept the output of EncodePSK_QAM
     * and create the integer array of 32 bit samples
     * suitable for playing                       */
    QVector<qint32> ModPSK(quint8 bytFrame, QByteArray bytEncodedData, qint32 intLeaderLen = 0);

    /*! \details
     * Function to accept the output of EncodePSK_QAM
     * and create the integer array of 32 bit samples
     * suitable for playing                      */
    QVector<qint32> ModQAM(quint8 bytFrame, QByteArray bytEncodedData, qint32 intLeaderLen = 0);

    /*! \details
     * Function to Modulate data encoded for 4FSK and
     * create the integer array of 32 bit samples suitable
     * for playing. Function works for 1, 2 or 4
     * simultaneous carriers                     */
    QVector<qint32> Mod4FSKData(quint8 bytFrame, QByteArray bytEncodedData, qint32 intLeaderLen = 0);

    /*! \details
     * Function to Modulate data encoded for 4FSK
     * High baud rate and create the integer array
     * of 32 bit samples suitable for playing */
    QVector<qint32> Mod4FSK600BdData(quint8 bytFrame, QByteArray bytEncodedData, qint32 intLeaderLen = 0);

    /*! \details
     * Function to encoded data  for 16FSK modes and
     * create the integer array of 32 bit samples suitable
     * for playing.    */
    QVector<qint32> Mod16FSKData(quint8 bytFrame, QByteArray bytEncodedData, qint32 intLeaderLen = 0);

    /*! \details
     * Function to apply 200 Hz filter for transmit<br />
     *
     * Used for PSK 200 Hz modulation XMIT filter<br />
     *   Assumes sample rate of 12000<br />
     *   Implements 3 100 Hz wide sections centered on 1500 Hz
     *     (~200 Hz wide @ - 30dB centered on 1500 Hz)<br />
     *   FSF (Frequency Selective Filter) variables    */
    QVector<qint32> FSXmtFilter200_1500Hz(QVector<qint32> *intNewSamples);

    /*! \details
     * Function to apply 500 Hz filter for transmit<br />
     * Used for FSK modulation XMIT filter<br />
     *   Assumes sample rate of 12000<br />
     *   Implements 7-100Hz wide sections centered on 1500 Hz
     *   (~500 Hz wide @ - 30dB centered on 1500 Hz)<br />
     *   FSF (Frequency Selective Filter) variables       */
    QVector<qint32> FSXmtFilter500_1500Hz(QVector<qint32> *intNewSamples);

    /*! \details
     * Function to apply 1000 Hz filter for transmit<br />
     *
     * Used for FSK modulation XMIT filter<br />
     *  Assumes sample rate of 12000<br />
     *  Implements 11-100Hz wide sections
     *   centered on 1500 Hz
     *   (~1000 Hz wide @ - 30dB centered on 1500 Hz)<br />
     *  FSF (Frequency Selective Filter) variables     */
    QVector<qint32> FSXmtFilter1000_1500Hz(QVector<qint32> *intNewSamples);

    /*! \details
     * Function to apply 2000 Hz filter for transmit<br />
     *  Used for FSK modulation XMIT filter<br />
     *   Assumes sample rate of 12000<br />
     *   Implements 21-100 Hz wide sections centered on 1500 Hz
     *     (~2000 Hz wide @ - 30dB centered on 1500 Hz)<br />
     *   FSF (Frequency Selective Filter) variables   */
    QVector<qint32> FSXmtFilter2000_1500Hz(QVector<qint32> *intNewSamples);

    /*! \details
     * Function to encode data for all PSK and QAM frame types.<br />
     * Objective is to use this to send all PSK data frames.<br />
     * Output is a byte array which includes:<br />
     * \li A 2 byte Header which includes the Frame ID. The frame
     * ID will be sent using 4FSK 50 baud (1500 Hz center).
     * It will include the Frame ID and the Frame ID XOR with
     * bytSession ID.
     * \li n sections one for each carrier that will inlcude all
     * data (with FEC appended) for the entire frame. Each block
     * will be identical in length.
     *
     * Ininitial implementation:<br />
     * intNum Car may be 1, 2, 4 or 8<br />
     * intBaud may be 100, 167<br />
     * intPSKMode may be 4 (4PSK) or 8 (8PSK) or 16QAM<br />
     * bytDataToSend must be equal to or less than max
     * data supported or a exception will be logged and an
     * empty array returned.                              */
    QByteArray EncodePSK_QAM(quint8 bytFrameType, QByteArray *bytDataToSend, QString *strFrameName);

    /*! \details
     * Function to encode data for all FSK frame types<br />
     * Objective is to use this to send all 4FSK data frames <br />
     * Output is a byte array which includes:<br />
     * \li A 2 byte Header which include the Frame ID.
     * This will be sent using 4FSK at 50 baud. It will
     * include the Frame ID and ID Xored by the Session bytID.
     * \li n sections one for each carrier that will inlcude
     * all data (with FEC appended) for the entire frame. Each
     * block will be identical in length.
     *
     * Ininitial implementation:<br />
     * intNum Car may be 1, 2, 4 or 8<br />
     * intBaud may be 50, 100<br />
     * strMod is 4FSK)<br />
     * bytDataToSend must be equal to or less than max
     * data supported by the frame or a exception will
     * be logged and an empty array returned      */
    QByteArray EncodeFSKData(qint32 bytFrameType, QByteArray *bytDataToSend, QString *strFrameName);


private:
    QVector<QVector<qint32> > int25BaudCarTemplate;         //!< Template for 16FSK carriers spaced at 25 Hz, 25 baud
    QVector<QVector<qint32> > intFSK50bdCarTemplate;        //!< Template for 4FSK carriers spaced at 50 Hz, 50 baud
    QVector<QVector<qint32> > intFSK100bdCarTemplate;       //!< Template for 4FSK carriers spaced at 100 Hz, 100 baud
    QVector<QVector<qint32> > intFSK600bdCarTemplate;       //!< Template for 4FSK carriers spaced at 600 Hz, 600 baud  (used for FM only)
    QVector<QVector<QVector<qint32> > > intPSK100bdCarTemplate;         //!< The actual templates over 9 carriers for 4 phase values and 120 samples

    TNCPanel      *objMain;                         // parent object is TNC Panel always
    ReedSolomon_8 *objRS8;                          // Reed Soloman Encoder used in RS FEC modes.
    FrameInfo     *objFrameInfo;                    // the FrameInfo module
    ARDOPprotocol *objProto;                        // the protocol object in TNC Panel

    ModemControlBlock *MCB;

    /*! \details
     * Function to extract an 8PSK symbol from an
     * encoded data array       */
    quint8 GetSym8PSK(qint32 intDataPtr, qint32 k, qint32 intCar, QByteArray *bytEncodedData, qint32 intDataBytesPerCar);

    /*! \details
     * Function to generate the Two-tone leader and
     * Frame Sync (used in all frame types) <br />
     * Generate a 50 baud (20 ms symbol time) 2 tone leader
     * leader tones used are 1475 and 1525 Hz.   */
    QVector<qint32> GenTwoToneLeaderWithSync(qint32 intSymLen);

    /*! \details
     *  Function to generate the Two-tone leader and Frame Sync (used in all
     * frame types).<br />
     * Generate a 50 baud (20 ms symbol time) 2 tone leader.<br />
     * Leader tones used are 1475 and 1525 Hz.  */
    QVector<qint32> Gen50BaudTwoToneLeaderWithSync(qint32 intSymLen);

    /*! \details Function to generate 1 symbol of leader  */
    void Generate50BaudTwoToneLeaderTemplate();


    /*! \details
     * Function to create the PSK symbol templates for
     * 8 tones and 8 phases at 200 baud.<br />
     * Generate templates of 120 samples (each template = 10 ms)
     * for each of the 9 possible carriers used in PSK and
     * 16QAM modulation.
     *
     * Used to speed up computation of PSK frames and reduce
     * use of sin() functions.
     *
     * Amplitude values will have to be scaled based on the
     * number of Active Carriers (1, 2, 4 or 8) initial values
     * should be OK for 1 carrier.   */
    void GeneratePSKTemplates();

    /*! \details
     * Function to create the FSK symbol templates<br />
     * Generate templates of 240 samples
     * (each symbol template = 20 ms) for each of the
     * 4 possible carriers used in 200 Hz BW FSK modulation.<br />
     * Generate templates of 120 samples
     * (each symbol template = 10 ms) for each of the
     * 20 possible carriers used in 500, 1000 and 2000 Hz BW 4FSK
     * modulation.
     *
     * Used to speed up computation of FSK frames and
     * reduce use of sin() functions.    */
    void GenerateFSKTemplates();

    /*! \details Function to add trailer before filtering  */
    void AddTrailer(QVector<qint32> *intSamples);
};

#endif // MODULATOR_HPP
