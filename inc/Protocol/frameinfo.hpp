#ifndef FRAMEINFO_HPP
#define FRAMEINFO_HPP
/*! \file */
#include <QtCore/QObject>

#include "ardop_global.hpp"

/*! \ingroup ARDOP
 * \class FrameInfo
 * \brief Class to decode and handle the various data frame types
 * \details
 */
class FrameInfo : QObject
{
    Q_OBJECT
    Q_PROPERTY(quint8 m_smplsToComplete READ getSamplesToComplete)

public:
    explicit FrameInfo(QObject *parent=0);

    QByteArray bytValidFrameTypes;
    QByteArray bytValidISSFrameTypes;

    // PROPERTY SETS AND GETS //

    /*! \details
     * Gets the number of samples (@12 KHz) needed to complete
     * the frame after the Frame ID is detected.
     * Value is increased by factor of 1.005 (5000 ppm) to
     * accomodate sample rate offsets in Transmitter and Receiver.
     *
     * \note
     * These samples DO NOT include the PSK reference symbols
     * which are accomodated in DemodPSK         */
    quint32 getSamplesToComplete();
    //                        //

    // ---------------------------------------------------
    // PUBLIC METHODS AND FUNCTIONS
    // ---------------------------------------------------
    void setMCB(ModemControlBlock *mcb)   { MCB = mcb; }

    /*! \details
     * Function to look up frame info from bytFrameType.
     * Used to "lookup" all parameters by frame Type.<br />
     * \return bool
     *
     * Returns TRUE if all fields updated otherwise <br />
     * FALSE (improper bytFrameType)
     * Removed bytQualThreshold Sept 1, 2016 (not used or needed) <br />
     * 1 Carrier 4FSK control frames    */
    bool frameInfo(quint8 bytFrameType, bool *blnOdd, quint16 *intNumCar, QString *strMod, quint32 *intBaud, quint32 *intDataLen, quint32 *intRSLen, QString *strType);

    /*! \details
     * Function to determine if a valid frame type if in
     * ISS or IDLE state used in the minimum distance
     * decoder (update if frames added or removed)
     * ACKs, NAKs, END, DISC, BREAK, ConRejBusy,
     * ConRejBW, and Con ACK */
    bool isValidISSFrameType(quint8 bytType);
    bool isShortControlFrame(quint8 bytType);  //!< \details Function to determine if frame type is short control frame
    bool isDataFrame(quint8 intFrameType);     //!< \details Function to determine if it is a data frame (Even OR Odd)
    bool isValidFrameType(quint8 bytType);     //!< Function to determine if a valide frame type used in the minimum distance decoder (update if frames added or removed)


    quint8 FrameCode(QString strFrameName);        //!< Function to look up the byte value from the frame string name
    quint8 ComputeTypeParity(quint8 bytFrameType);  //!< A function to compute the parity symbol used in the frame type encoding

    qint32 getNumCarriers(quint8 bytFrameType);    //!< \details This function is used to get the number of carriers from the frame type

    /*! \details
     * Function to get base (even) data modes by bandwidth
     * for ARQ sessions <br />
     * Revised version 0.3.5<br />
     * Idea is to use this list in the gear shift algorithm
     * to select modulation mode based on bandwidth and
     * robustness. Sequence modes in approximate order of
     * robustness ...most robust first, shorter frames of
     * same modulation first                   */
    QByteArray getDataModes(qint32 intBW);

    /*! \details
     * Function to get Shift up thresholds by bandwidth for
     * ARQ sessions<br />
     * Initial values determined by finding the following process:<br />
     * (all using Pink Gaussian Noise channel 0 to 3 KHz) <br />
     * 1) Find Min S:N that will give reliable (at least 4/5 tries)
     * decoding at the fastest mode for the bandwidth.<br />
     * 2) At that SAME S:N use the next fastest (more robust mode
     * for the bandwidth)<br />
     * 3) Over several frames average the Quality of the decoded
     * frame in 2) above That quality value is the one that
     * is then used as the shift up threshold for that mode.<br />
     *
     * \note
     * The top mode will never use a value to shift up. This might
     * be adjusted some but should along with a requirement for
     * two successive ACKs make a good algorithm */
    QByteArray getShiftUpThresholds(qint32 intBW);

    QString getName(quint8 value);                   //!< converts m_name - byte data to string.

    QStringList getAllDataModes();               //!< Function to get the string array of all data modes known (from globals AllModes Vector list)

    // ---------------------------------------------------

private:
    // PROPERTY LOCAL VARS  //
    quint8 m_name;
    quint8 m_smplsToComplete;
    //                     //

    QStringList strFrameType;

    ModemControlBlock *MCB;

    void initFrameNames();
    void initValidFrameTypes();
};

#endif // FRAMEINFO_HPP
