#ifndef REEDSOLOMON_HPP
#define REEDSOLOMON_HPP
#include <QtCore/QObject>
#include <QtCore/QVector>

/*! \ingroup ARDOP
 * \class ReedSolomon_8
 * \brief Reed Solomon Encode - Decode class
 * \details
 */
class ReedSolomon_8 : public QObject
{
    Q_OBJECT
    Q_PROPERTY(quint32 tt READ getMaxCorrections WRITE setMaxCorrections) //!< Property to set max # of corrections and initialize all arrays

public:
    explicit ReedSolomon_8();

    // -------------------------------------------
    // PROPERTY SETS AND GETS
    // -------------------------------------------
    void setMaxCorrections(quint32 max);

    quint32 getMaxCorrections();
    // -----END PROPERTY SETS AND GETS -----------



    // -------------------------------------------
    // PUBLIC METHODS AND FUNCTIONS
    // -------------------------------------------
    /*! \details
     * Function to do the RS encode.<br />
     * For full length and shortend RS codes up to 8 bit symbols (mm = 8)
     * bytData is the symbol array to encode. If less than full RS length
     * it is padded with leading 0's (shortened RS code)  */
    QByteArray RSEncode(QByteArray bytData);

    /*! \ details
     * Main RS decode function<br />
     * Variable bytRcv is the received array of symbols (up to 8 bits/symbol)
     * including systematic data + 2 * tt parity symbols length of bytRcv may
     * be shorter than full length (nn) but must be agreed upon a priori by
     * both encoder and decoder (Shortened code)<br />
     * Shortening simply appends leading 0's (before encoding and before decoding)
     * and then discarding the same 0's at the end.
     *
     * This verified with several thousand automated random test cases for
     * both 239,255 and 223, 255 RS codes approximate encode + decode time
     * (with max num of errors) is on the order of 1 ms (Pentium 1700 MHz)
     *
     * \note
     * On some intensive test done July 5, 2009 by RM (> 20 Million RS Corrections)
     * there are rare (but possible) conditions where RSDecode reports
     * blnCorrected = true but there are still bytes with errors. The exact
     * mechanism for this is unknown so this now requires a Sum Check confirmation
     * even if the blnCorrected is reported True. In over 20 Million test cases there
     * was no condition that blnCorrected was reported true, there was an error,
     * and the 16 bit Sum check did not detect the error.      */
    QByteArray RSDecode(QByteArray bytRcv, bool blnCorrected=false);




private:
    // PROPERTY LOCAL VARS  //
    quint32 tt;                   //!< number of errors that can be corrected
    //  END PROPERTY VARS   //


    bool blnErrorsCorrected;

    quint32 mm;                   //!< RS code over GF(2**4) - change to suit
    quint32 nn;                   //!< nn=2**mm -1   length of codeword
    quint32 kk;                   //!< the number of information characters  kk = nn-2*tt

    QVector<quint32> pp4; //!< specify irreducible polynomial coeffts */
    QVector<quint32> pp8; //!< specify irreducible polynomial coeffts */
    QVector<quint32> pp;
    QVector<quint32> alpha_to;
    QVector<quint32> index_of;
    QVector<quint32> gg;
    QVector<quint32> recd;
    QVector<quint32> data;
    QVector<quint32> bb;


    /*! \details
     * Function to generate the GF<br />
     * Generate GF(2**mm) from the irreducible polynomial p(X) in
     * pp[0]..pp[mm]<br />
     * lookup tables: index->polynomial form   alpha_to[] contains j=alpha**i;<br />
     *                polynomial form -> index form  index_of[j=alpha**i] = i<br />
     *                alpha=2 is the primitive element of GF(2**mm)<br />    */
    void generate_gf();

    /*! \details
     * Obtain the generator polynomial of the tt-error correcting,
     * length nn = (2**mm -1) Reed Solomon code from the product of
     * (X+alpha**i), i=1..2*tt */
    void gen_poly();

    /*! \details
     * Function to Decode RS<br />
     * Assume we have received bits grouped into mm-bit symbols in
     * recd[i], i=0..(nn-1), and recd[i] is index form (ie as
     * powers of alpha).<br />
     * We first compute the 2*tt syndromes by substituting
     * alpha**i into rec(X) and evaluating, storing the syndromes
     * in s[i], i=1..2tt (leave s[0] zero).
     *
     * Then we use the Berlekamp iteration to find the error
     * location polynomial elp[i]. If the degree of the elp
     * is >tt, we cannot correct all the errors and hence just
     * put out the information symbols uncorrected. If the
     * degree of elp is <=tt, we substitute alpha**i , i=1..n
     * into the elp to get the roots, hence the inverse roots,
     * the error location numbers. If the number of errors
     * located does not equal the degree of the elp, we have
     * more than tt errors and cannot correct them.
     *
     * Otherwise, we then solve for the error value at the error
     * location and correct the error. The procedure is that
     * found in Lin and Costello. For the cases where the number
     * of errors is known to be too large to correct, the
     * information symbols as received are output (the advantage
     * of systematic encoding is that hopefully some of the
     * information symbols will be okay and that if we are in
     * luck, the errors are in the parity part of the transmitted
     * codeword). Of course, these insoluble cases can be
     * returned as error flags to the calling routine if desired. */
    void decode_rs();

    /*! \details
     * Function to Encode Reed Solomon<br />
     * take the string of symbols in data[i], i=0..(k-1) and
     * encode systematically to produce 2*tt parity symbols
     * in bb[0]..bb[2*tt-1]
     * data[] is input and bb[] is output in polynomial form.<br />
     * Encoding is done by using a feedback shift register with
     * appropriate connections specified by the elements of gg[],
     * which was generated above.<br />
     * Codeword is: c(X) = data(X)*X**(nn-kk)+ b(X) */
    void encode_rs();
<<<<<<< HEAD
=======


>>>>>>> remotes/origin/ken
};

#endif // REEDSOLOMON_HPP
