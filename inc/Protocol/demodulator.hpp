#ifndef DEMODULATOR_HPP
#define DEMODULATOR_HPP
#include <iostream>

#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtWidgets/QDialog>

#include "reedsolomon.hpp"
#include "tncpanel.hpp"

/*! \class DemodulateDecode12K
 * \brief A class to handle all Demodulation and Decoding
 * for Sound Cards
 * \details
 * By default the demodulator is using a 12000 Hz sample
 * rate. This may change as the code matures. */
class DemodulateDecode12K : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDateTime dttLastGoodFrameTypeDecode READ getLastGoodFrameTypeDecode WRITE setLastGoodFrameTypeDecode)

public:
    explicit DemodulateDecode12K(QDialog *parent=0);
    virtual ~DemodulateDecode12K();

    bool   blnLeaderDetected = false; //!< Setup upon a leader detect....cleared after frame decode or failure to detect leader

    qint32 intMFSReadPtr;             //!< pointer to the current next to use sample of intMixedFilteredSamples
    qint32 intPriorTone = 0;          //!< the prior tone since this is incremental FSK
    qint32 intLeaderRcvdMs = 1000;    //!<
    qint32 intRmtLeaderMeasure = 0;   //!<
    qint32 intLastRcvdFrameQuality;   //!<

    QDateTime dttLastLeaderDetect = QDateTime::currentDateTimeUtc();

    // arrays
    // vector objects are used here instead of arrays becase of the
    // static nature of arrays in C++. The QVector class allows a
    // dynamic resize on demand.
    QVector<qint32> intFilteredMixedSamples;
    QVector<qint32> intMixedSamples;

    // PROPERTY SETS AND GETS   //
    void setLastGoodFrameTypeDecode(QDateTime val)  { dttLastGoodFrameTypeDecode = val; }

    QDateTime getLastGoodFrameTypeDecode()          { return dttLastGoodFrameTypeDecode; }
    //                          //

    /*! \details Set the pointer to the only copy of the MCB from the TNC Panel. */
    void setMCB(ModemControlBlock *mcb)             { MCB = mcb; }


    void ClearAllMixedSamples();      //!< Subroutine to clear all mixed samples
    /*!
     * \details Subroutine to compute Goertzel algorithm and
     * return Real and Imag components for a single frequency bin.
     *
     * intRealIn is a buffer at least intPtr + N in length
     * N need not be a power of 2
     * m need not be an integer
     * Computes the Real and Imaginary Freq values for bin m
     * Verified to = FFT results for at least 10 significant digits
     * Timings for 1024 Point on Laptop (64 bit Core Duo 2.2 Ghz)<br />
     *    GoertzelRealImag .015 ms   Normal FFT (.5 ms)<br />
     * assuming Goertzel is proportional to N and
     * FFT time proportional to Nlog2N<br />
     * FFT:Goertzel time  ratio ~ 3.3 Log2(N)
     *  */

    void GoertzelRealImag(QVector<qint32> *, qint32, qint32, double, double *, double *);
    /*! \details
     * Function to compute Goertzel algorithm and return Real and
     * Imag components for a single frequency bin with a Hanning
     * Window function.
     *
     * intRealIn is a buffer at least intPtr + N in length
     * N need not be a power of 2
     * m need not be an integer
     * Computes the Real and Imaginary Freq values for bin m
     * Verified to = FFT results for at least 10 significant digits
     * Timings for 1024 Point on Laptop (64 bit Core Duo 2.2 Ghz)<br />
     *    GoertzelRealImag .015 ms   Normal FFT (.5 ms)<br />
     * assuming Goertzel is proportional to N and FFT time
     * proportional to Nlog2N<br />
     * FFT:Goertzel time  ratio ~ 3.3 Log2(N)
     *
     * EXPERIMENTAL */
    void GoertzelRealImagHanning(QVector<qint32> *, qint32, qint32, double, double *, double *);

    /*! \details
     * Function to compute Goertzel algorithm and return Real and
     * Imag components for a single frequency bin with a Hamming
     * Window function.
     *
     * intRealIn is a buffer at least intPtr + N in length<br />
     * N need not be a power of 2<br />
     * m need not be an integer<br />
     * Computes the Real and Imaginary Freq values for bin m<br />
     * Verified to = FFT results for at least 10 significant digits.<br />
     * Timings for 1024 Point on Laptop (64 bit Core Duo 2.2 Ghz)<br />
     *    GoertzelRealImag .015 ms   Normal FFT (.5 ms)<br />
     * assuming Goertzel is proportional to N and FFT time proportional to Nlog2N<br />
     * FFT:Goertzel time  ratio ~ 3.3 Log2(N)
     *
     * EXPERIMENTAL */
    void GoertzelRealImagHamming(QVector<qint32> *, qint32, qint32, double, double *, double *);

    /*! \details
     * Function to Mix new samples with NCO to tune to
     * nominal 1500 Hz center with reversed sideband and filter.
     *
     * Correct the dimension of intPriorMixedSamples if needed
     * (should only happen after a bandwidth setting change). */
    void MixNCOFilter(QVector<qint32> *, qint32 *, double);

    /*! \details
     * Function to detect and tune the 50 baud 2 tone leader
     * (for all bandwidths) Updated version of SearchFor2ToneLeader2()
     * This version uses 10Hz bin spacing. Hamming window on Goertzel,
     * and simple spectral peak interpolator.
     *
     * It requires about 50% more CPU time when running but produces
     * more sensive leader detection and more accurate tuning search
     * through the samples looking for the telltail 50 baud 2 tone
     * pattern (nominal tones 1475, 1525 Hz).
     *
     * Find the offset in Hz (due to missmatch in transmitter - receiver
     * tuning.
     *
     * Finds the S:N (power ratio of the tones 1475 and 1525 ratioed
     * to "noise" averaged from bins at 1425, 1450, 1550, and 1575Hz) */
    bool SearchFor2ToneLeader3(QVector<qint32> *, qint32 *, double *, qint32 *);

    /*! \details
     * Function to acquire the Frame Sync for all Frames
     *
     * This is for acquiring FSKFrameSync After Mixing Tones
     * Mirrored around 1500 Hz. e.g. Reversed Sideband
     * Frequency offset should be near 0 (normally within +/- 1 Hz)
     * Locate the sync Symbol which has no phase change from
     * the prior symbol (50 baud BPSK leader @ 1500 Hz) */
     bool AcquireFrameSyncRSB();

     /*! \details
      * Function to look at the 2 tone leader and establishes the
      * Symbol framing using envelope correlation and minimal phase error.
      */
     bool Acquire2ToneLeaderSymbolFraming();

     /*! \details
     * Function to interpolate spectrum peak using Quinn algorithm
     * Only used with SearchFor2ToneLeader2 -- currently not used.
     *
     * \note This does not work well with windowed samples...use
     * SpectralPeakLocator for those.
     *
     * Theory is based on the Quinn algorithm in Streamlining
     * Digital Processing page 139<br />
     * Alpha1 = Re(Xk-1/Xk)<br />
     * Alpha2 = Re(Xk+1/Xk)<br />
     * Delta1 = Alpha1/(1 - Alpha1)<br />
     * Delta2 = Alpha2/(1 - Alpha2)<br />
     *
     * if Delta1 > 0 and Delta2 > 0 then <br />
     * &nbsp;&nbsp;&nbsp;&nbsp;Delta = Delta2<br />
     * else<br />
     * &nbsp;&nbsp;&nbsp;&nbsp;Delta = Delta1<br />
     * ...should be within .1 bin for S:N > 2 dB  */
    double QuinnSpectralPeakLocator(double, double, double, double, double, double);

    /*! \details
     * Function to interpolate spectrum peak using simple interpolation.
     * Use this for Windowed samples instead of QuinnSpectralPeakLocator
     *
     * \returns double ret
     * Empirically determines optimum for
     * For Hamming  Window use factor of 1.22.
     * For Hanning  Window use factor of 1.36.
     * For Blackman Window use factor of 1.75.  */
    double SpectralPeakLocator(double, double, double, double, double, double, double *);


signals:
    void signalConnRcvd();     //!< Signal that a CONN REQ has been received.
    void signalDiscRcvd();     //!< Signal that a DISC REQ has been received.
    void signalDCD(bool val);  //!< Signal that a good leader has been received. True = leader is good. False = not good.

private:
    // used for initial receive testing...later put in correct protocol states
    enum ReceiveState
    {
        SearchingForLeader,
        AcquireSymbolSync,
        AcquireFrameSync,
        AcquireFrameType,
        DecodeFrameType,
        AcquireFrame,
        DecodeFrame
    };
    static ReceiveState RxState; // used for initial testing

    // Bytes
    quint8 bytLastDataFrameType = 0;

    // doubles
    double dblNCOFreq; //!< nominal NC frequency
    double dblNCOPhase;
    double dblSNdBPwr;
    double dblSNdBPwrEarly; //!< the SN Power in the entire 4 symbol leader and the first 2 symbols.
    double dblNCOPhaseInc;
    double dbl2Pi;

    QVector<qint32> intPriorMixedSamples; //!< a buffer of 120 samples to hold the prior samples used in the filter
    QVector<double> dblFilEnvelopeOut;
    QVector<double> dblFilEnvelopeIn;
    QVector<double> dblAcquire2ToneLeaderSymbolFraming; //!< For plotting used for debugging for waveform capture and analysis
    QVector<double> dblAcquirePSKFrameSyncRSB;
    QVector<double> dblAcquire4FSKFrameType;

    // Date/Time
    QDateTime dttStartRmtLeaderMeasure;
    QDateTime dttLastGoodFrameTypeDecode = QDateTime::currentDateTime().addSecs(-(60*60)); // -1hr
    QDateTime dttStartTimingMeasure;

            // objects
    ReedSoloman8 *objRS8;
    FrameInfo    *objFrameInfo;
    TNCPanel     *objMain;

    ModemControlBlock *MCB;

    /*! \details
     * Local function to apply 2000 Hz filter to mixed samples.
     * Assumes sample rate of 12000.<br />
     * Implements 23 100 Hz wide sections
     * (~2000 Hz wide @ - 30dB centered on 1500 Hz). <br />
     * FSF (Frequency Selective Filter) variables  */
    void FSMixFilter2000Hz();
};

#endif // DEMODULATOR_HPP
