#ifndef WAVPLAYER_HPP
#define WAVPLAYER_HPP
#include <QtCore/QList>
#include <QtCore/QUrl>
#include <QtCore/QFileInfo>
#include <QtMultimedia/QMediaPlayer>
#include <QtMultimedia/QMediaPlaylist>
#include <QtMultimedia/QVideoProbe>

#include "ui_wavplayer.h"

class PlaylistModel;
class VideoWidget;

namespace Ui {
class WavPlayer;
}

class WavPlayer : public QDialog
{
    Q_OBJECT
    // READ ONLY PROPERTIES             //
    Q_PROPERTY(bool m_muted READ isMuted)
    Q_PROPERTY(quint16 m_vol READ volume)
    Q_PROPERTY(QMediaPlayer::State m_state READ getPBState)
    Q_PROPERTY(qreal m_pbRate READ playbackRate)
    //                                  //

public:
    explicit WavPlayer(QWidget *parent = 0);
    virtual ~WavPlayer();

    // PROPERTY SETS AND GETS    //
    bool isMuted() const             { return m_muted; }
    quint16 volume() const           { return ui->volume->value(); }
    qreal playbackRate() const       { return ui->cboRateBox->itemData(ui->cboRateBox->currentIndex()).toReal(); }
    QMediaPlayer::State getPBState() { return m_pbstate; }
    //                           //

signals:
    void stop();
    void pause();
    void play();
    void next();
    void previous();
    void changeVolume(quint16 vol);
    void changeMuting(bool muting);
    void changeRate(qreal pbrate);

protected:
    void changeEvent(QEvent *e);

public slots:
    void setState(QMediaPlayer::State state);
    void setVolume(quint16 vol);
    void setMuted(bool mute);
    void setPlaybackRate(qreal pbrate);

private slots:
    void playClicked();
    void muteClicked();
    void updateRate();
    void openwav();
    void seek(qint32 seconds);
    void jump(const QModelIndex &index);
    void handleCursor(QMediaPlayer::MediaStatus status);
    void slotDisplyErrMsg()      { setStatusInfo(player->errorString()); }
    void bufferingProgress(int prog);
    void durationChanged(qint64 duration);
    void positionChanged(qint64 prog);
    void playlistPositionChanged(quint32 pos);
    void metaDataChanged();
    void statusChanged(QMediaPlayer::MediaStatus status);

#ifndef PLAYER_NO_COLOROPTIONS
    void showColorDialog();
#endif
private:
    static bool isPlaylist(const QUrl &url) // Check for ".m3u" playlists.
    {
        if ( !url.isLocalFile() ) return false;
        const QFileInfo fileInfo(url.toLocalFile());
        return fileInfo.exists() && !fileInfo.suffix().compare(QLatin1String("wav"), Qt::CaseInsensitive);
    }

    // Property Loc var defs  //
    bool m_muted;

    quint16 m_vol;

    qreal m_pbRate;

    QMediaPlayer::State m_pbstate;
    //                        //

    qint64 m_duration;

    QString trackInfo;
    QString statusInfo;

    QLabel          *coverLabel;

#ifndef PLAYER_NO_COLOROPTIONS
    QPushButton     *colorButton;
    QDialog         *colorDialog;
  #endif

    Ui::WavPlayer   *ui;

    QVideoProbe     *probe;
    QMediaPlayer    *player;
    QMediaPlaylist  *playlist;

    PlaylistModel   *playlistModel;
    HistogramWidget *histogram;

    void initPlayer();
    void addToPlaylist(const QList<QUrl> urls);
    void setTrackInfo(QString info);
    void setStatusInfo(QString infomsg);
    void updateDurationInfo(qint64 currentInfo);
};

#endif // WAVPLAYER_HPP
