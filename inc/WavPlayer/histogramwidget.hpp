#ifndef HISTOGRAMWIDGET_HPP
#define HISTOGRAMWIDGET_HPP
#define HISTOGRAMWIDGET_H
#include <QtCore/QObject>
#include <QtCore/QThread>
#include <QtWidgets/QWidget>
#include <QtMultimedia/QVideoFrame>

class FrameProcessor: public QObject
{
    Q_OBJECT

public slots:
    void processFrame(QVideoFrame frame, quint32 levels);

signals:
    void histogramReady(QVector<qreal> histogram);
};

class HistogramWidget : public QWidget
{
    Q_OBJECT

public:
    explicit HistogramWidget(QWidget *parent = 0);
    ~HistogramWidget();

    void setLevels(quint32 levels) { m_levels = levels; }

public slots:
    void processFrame(QVideoFrame frame);
    void setHistogram(QVector<qreal> histogram);

protected:
    void paintEvent(QPaintEvent *event);

private:
    bool m_isBusy;

    quint32 m_levels;

    QVector<qreal> m_histogram;

    QThread m_processorThread;

    FrameProcessor m_processor;
};

#endif // HISTOGRAMWIDGET_HPP
