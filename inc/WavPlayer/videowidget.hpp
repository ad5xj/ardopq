#ifndef VIDEOWIDGET_HPP
#define VIDEOWIDGET_HPP
#include <QtCore/QEvent>
#include <QtWidgets/QWidget>
#include <QtGui/QKeyEvent>
#include <QtGui/QMouseEvent>
#include <QtMultimediaWidgets/QVideoWidget>

class VideoWidget : public QVideoWidget
{
    Q_OBJECT

public:
    VideoWidget(QWidget *parent = 0);

protected:
    void keyPressEvent(QKeyEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
};

#endif // VIDEOWIDGET_HPP
