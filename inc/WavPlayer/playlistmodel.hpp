#ifndef PLAYLISTMODEL_HPP
#define PLAYLISTMODEL_HPP
#include <QAbstractItemModel>

class QMediaPlaylist;

class PlaylistModel : public QAbstractItemModel
{
    Q_OBJECT
    Q_PROPERTY(QMediaPlaylist *playlist READ getPlayList)

public:
    enum Column
    {
        Title = 0,
        ColumnCount
    };

    PlaylistModel(QObject *parent = 0);

    // PROPERTY SETS AND GETS     //
    QMediaPlaylist *getPlayList() const;

    void setPlaylist(QMediaPlaylist *playlist);
    //                            //

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;


    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::DisplayRole);

private slots:
    void beginInsertItems(int start, int end);
    void endInsertItems();
    void beginRemoveItems(int start, int end);
    void endRemoveItems();
    void changeItems(int start, int end);

private:
    QMap<QModelIndex, QVariant> m_data;

    QMediaPlaylist  *m_playlist;
};

#endif // PLAYLISTMODEL_HPP
