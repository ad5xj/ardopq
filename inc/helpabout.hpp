#ifndef HELPABOUT_HPP
#define HELPABOUT_HPP
#include <QtWidgets/QDialog>

#include "ui_helpabout.h"

class HelpAbout : public QDialog
{
    Q_OBJECT

public:
    explicit HelpAbout(QWidget *parent = 0);

protected:
    void changeEvent(QEvent *e);

private:
    Ui::HelpAbout *ui;
};

#endif // HELPABOUT_HPP
