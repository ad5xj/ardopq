#ifndef FFTSPECTURMWIDGET_HPP
#define FFTSPECTURMWIDGET_HPP
#include <QtWidgets/QWidget>
#include <QtCharts/QChartGlobal>

QT_CHARTS_BEGIN_NAMESPACE
class QLineSeries;
class QChart;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

class AudioInputIO;

class FFTSpectrumWidget : public QWidget
{
    Q_OBJECT

public:
    explicit FFTSpectrumWidget(QWidget *parent = 0);
    virtual ~FFTSpectrumWidget();

private:
    QChart           *m_chart;
    QLineSeries      *m_series;
    AudioInputIO     *m_device;
};

#endif // FFTSpectrumWidget_HPP
