#ifndef VUMETERWIDGET_HPP
#define VUMETERWIDGET_HPP
/*! \file */
#include <QtWidgets/QWidget>
#include <QtWidgets/QProgressBar>
#include <QtGui/QPaintEvent>
#include <QtGui/QLinearGradient>

/*! \ingroup TNCPANEL
 * \class VUMeterWidget
 * \brief A sub-class of the QProgressBar w/enhancements
 * \details
 * This sub-class of QProgressBar enhances the standard
 * behavior and look of the default. The background of
 * the chunks are gradient shaded and color changes
 * depending on the level set.                  */
class VUMeterWidget : public QProgressBar
{
    Q_OBJECT

public:
    explicit VUMeterWidget(QWidget *parent = 0);
    ~VUMeterWidget();

protected:
    void paintEvent(QPaintEvent*) Q_DECL_OVERRIDE;

signals:

public slots:

private:
    int pos;
    int pos70;
    int pos85;
};

#endif // VUMETERWIDGET_HPP
