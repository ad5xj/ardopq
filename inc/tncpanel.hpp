#ifndef TNCPANEL_HPP
#define TNCPANEL_HPP
/*! \file */
#include <math.h>

#include <QtCore/QTimer>
#include <QtCore/QEvent>
#include <QtCore/QIODevice>
#include <QtCore/QVarLengthArray>
#include <QtGui/QShowEvent>
#include <QtGui/QCloseEvent>
#include <QtWidgets/QDialog>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QMenu>
#include <QtWidgets/QAction>
#include <QtCore/QStateMachine>
#include <QtCore/QState>
#include <QtCore/QEventTransition>
#include <QtMultimedia/QAudio>
#include <QtMultimedia/QAudioInput>
#include <QtMultimedia/QAudioOutput>
#include <QtMultimedia/QAudioFormat>
#include <QtMultimedia/QAudioDeviceInfo>
//#include <QtSerialPort/QSerialPort>

#include "ardop_global.hpp"
#include "audioinputio.hpp"
#include "demodulator.hpp"
#include "modulator.hpp"
#include "protocol.hpp"
#include "modem.hpp"

#include "ui_tncpanel.h"

namespace Ui {
class TNCPanel;
}

class ARDOPprotocol;
class MainPrivate;
class TNCSetup;
class DemodulateDecode12K;
class EncodeModulate;

/*! \struct TuningStats
 * \details
 * Structure used for statistics used to evaluate
 * and measure performance */
struct TuningStats
{
    qint32 intLeaderDetects;
    qint32 intLeaderSyncs;
    qint32 intAccumLeaderTracking;
    qint32 intAccumQAMTracking;
    qint32 intGoodFSKFrameTypes;
    qint32 intFailedFSKFrameTypes;
    qint32 intAccumFSKTracking;
    qint32 intFSKSymbolCnt;
    qint32 intQAMSymbolCnt;
    qint32 intGoodPSKFrameDataDecodes;
    qint32 intGoodFSKFrameDataDecodes;
    qint32 intGoodQAMFrameDataDecodes;
    qint32 intFailedFSKFrameDataDecodes;
    qint32 intFailedQAMFrameDataDecodes;
    qint32 intAvgFSKQuality;
    qint32 intFrameSyncs;
    qint32 intGoodPSKSummationDecodes;
    qint32 intGoodFSKSummationDecodes;
    qint32 intGoodQAMSummationDecodes;
    qint32 intAccumPSKLeaderTracking;
    qint32 intPSKTrackAttempts;
    qint32 intQAMTrackAttempts;
    qint32 intAccumPSKTracking;
    qint32 intPSKSymbolCnt;
    qint32 intFailedPSKFrameDataDecodes;
    qint32 intAvgPSKQuality;
    qint32 intDecodeDistanceCount;
    qint32 intShiftUPs;
    qint32 intShiftDNs;
    qint32 intLinkTurnovers;
    qint32 intEnvelopeCors;
    double dblAvgCorMaxToMaxProduct;
    double dblFSKTuningSNAvg;
    double dblLeaderSNAvg;
    double dblAvgPSKRefErr;
    double dblAvgDecodeDistance;
};  // TuningStats

/*! \struct QualityStats
 * \details Structure used for Quality stats */
struct QualityStats
{
    qint32 intQAMQuality;
    qint32 intQAMQualityCnts;
    qint32 intQAMSymbolsDecoded;
    qint32 int4FSKQuality;
    qint32 int4FSKQualityCnts;
    qint32 int8FSKQuality;
    qint32 int8FSKQualityCnts;
    qint32 int16FSKQuality;
    qint32 int16FSKQualityCnts;
    qint32 intFSKSymbolsDecoded;
    qint32 intPSKSymbolsDecoded;
    QVarLengthArray<qint32,1> intPSKQuality;
    QVarLengthArray<qint32,1> intPSKQualityCnts;
};  // QualityStats

/*! \struct SessionStats
 * \details  Structure for Session Statistics */
struct SessionStats
{
    bool      blnStatsValid;
    qint32    intTotalBytesSent;
    qint32    intTotalBytesReceived;
    qint32    intFrameTypeDecodes;
    qint32    intMax1MinThruput;
    qint32    intGearShifts;
    double    dblSearchForLeaderAvg;
    double    modeQuality[];
    QDateTime dttSessionStart;
}; //  SessionStats



/*! \ingroup VirtualTNC
 * \class TNCPanel
 * \brief The TNC Display Panel and TNC state machine
 * \details
 * This module is the graphical user interface (GUI) to
 * the ARDOP protocol and modem.
 *
 * The TNC displays many pieces of information while
 * audio is being either captured or played back, and
 * the ARDOP Protocol changes state or operation during
 * send or receive.
 *
 * <ul>
 *  <li>The current RMS level of the audio stream, and the recent
 * 'high watermark' level.</li>
 *  <li>The state of the TNC and protocol</li>
 *  <li>The demodulation of data to datascope display</li>
 * </ul><br />
 * just to name a few.
 *
 * Spectrum analysis is performed by calculating the
 * Fast Fourier Transform (FFT) of a segment of audio data.
 * An open-source library, FFTW3, is called to do the
 * transform analysis, against which this application
 * is dynamically linked on Windows, Linux, and Mac.
 *
 * \note
 * The ModemControlBlock (MCB) and the RadioContrlBlock (RCB)
 * are created in the MODEM module and used here. Do Not Replicate
 * to local vars except for temporary volatile use. MODEM(parent)
 * is the only copy of the control blocks. Pass the pointer to
 * them for use elsewhere.                  */
class TNCPanel : public QDialog
{
    Q_OBJECT
    Q_PROPERTY(bool m_soundIsPlaying READ soundIsPlaying WRITE setSoundIsPlaying)

public:
    explicit TNCPanel(QWidget *parent=0);
    ~TNCPanel();

    // ------------------------------------
    // Public Data VARS
    // ------------------------------------
    bool blnCodecStarted;
    bool blnInTestMode;
    bool blnClosing;
    bool blnFramePending;
    bool blnLastPTT;
    bool blnRadioMenuEnabled;

    quint8 bytLastACKedDataFrameType;  //!< added to accomodate once a data frame was AKEed to return ACK even if failed decode.


    qint32 intRepeatCnt;
    qint32 intTestFrameCorrectCnt;

    double dblOffsetHz;
    double dblMaxLeaderSN;
    double dblOffsetLastGoodDecode;

    QString strExecutionDirectory;
    QString strProductVersion;
    QString strWavDirectory;
    QString strLastWavStream;
    QString strRcvFrameTag;
    QString strDecodeCapture;

    ReceiveState RxState;

    TuningStats  stcTuningStats;
    QualityStats stcQualityStats;
    SessionStats stcSessionStats;

    QByteArray     *bytDataToSend;       // This is the only copy
    QByteArray     *bytRecvdDataQueue;

    // --------------------------------------
    // PUBLIC OBJECTS
    // --------------------------------------
    Ui::TNCPanel *ui;

    // Demodulator object for TNC
    DemodulateDecode12K *objDemod;
    // Modulator object for TNC
    EncodeModulate      *objMod;
    // Modem parent object - host interface
    ARDOPModem          *objMain;
    // ARDOP Protocol object
    ARDOPprotocol       *objProtocol;
    // Setup form for TNC
    TNCSetup            *objTNCSetup;


    // PROPERTY SETS AND GETS       //
    void setSoundIsPlaying(bool p)      { m_soundIsPlaying = p; }

    bool soundIsPlaying()               { return m_soundIsPlaying; }
    //                              //


    // --------------------------------------
    // PUBLIC METHODS AND FUNCTIONS
    // --------------------------------------
    void Update16FSKConstellation(QVector<qint32> *intToneMags, qint32 *intQuality);
    void Update8FSKConstellation(QVector<qint32> *intToneMags, qint32 *intQuality);
    void Update4FSKConstellation(QVector<qint32> *intToneMags, qint32 *intQuality);    //!< \details Subroutine to update the 4FSK Constellation. Subroutine to update bmpConstellation plot for 4FSK modes...
    void UpdatePhaseConstellation(QVector<qint16> *intPhases, QVector<qint16> *intMag, QString strMod, qint32 *intQuality, bool blnQAM = false);   //!< Subroutine to Update the PhaseConstellation. Subroutine to update bmpConstellation plot for PSK modes...Skip plotting and calulations of intPSKPhase(0) as this is a reference phase (9/30/2014)
    void UpdateRxFrameText(QString);
    void UpdateRxFrameColor(QColor);

    // --------------------------------------
    // GET PARENT OBJECT (modem)
    // --------------------------------------
    void setParentObject(ARDOPModem *obj)    { objMain = obj; connectAndSetupUi(); }

    // --------------------------------------
    //  CONTROL BLOCK FROM MODEM AS PARENT
    // --------------------------------------
    void setMCB(ModemControlBlock *MCB)      { mcb = MCB; }
//    void setRCB(RadioControlBlock *RCB)      { rcb = RCB; }

protected:
    void changeEvent(QEvent *e);
    void showEvent(QShowEvent *);
    void closeEvent(QCloseEvent *);


public slots:
    void setRadioMenuEnabled(bool b);

private slots:
    void slotClose();
    void slotHelpAbout();
    void slotSetupTNC(bool);
    void slotSetupAudio(bool);
    void slotReadMore();  //!< Connected to the AudioInputIO object to get the internally buffered data.
    void slotUpdateVU();  //!< Connected to the update signal of the AudioInputIO Object - update the VU Meter using the AudioInputIO.level() function.
    void slotUpdateDisplay(QString name,QColor color, QString txt, QVariant val);  //!< Connected to signalUpdateDisplay from other modules
    void slotAudioErr(QString,QString);  //!< Connected to the AudioInput Object to display errors.
    void slotDispTypeChgd();
    void slotVolChgd(int);   //!< Connected to the volume control on the TNC Panel. A change in setting will update the AudioInput object.
    void slotAudioInputOK(); //!< Connected to the AudioInputOK() signal of the AudioInputIO Object - signals the TNC to change the status display.
    void slotSetCAR();       //!< Set the CAR led status.

private:
    // property local vars   //
    bool m_soundIsPlaying;
    //                       //

    QByteArray       m_buffer;

//    RxState          m_state;
    /*! \details
     * A QStateMachine runs its own event loop.
     * For signal transitions (QSignalTransition objects),
     * QStateMachine automatically posts a QStateMachine::SignalEvent
     * to itself when it intercepts the corresponding signal; similarly,
     * for QObject event transitions (QEventTransition objects) a
     * QStateMachine::WrappedEvent is posted.
     *
     * QStateMachine is a subclass of QState. This allows for a
     * state machine to be a child state of another machine.
     * QStateMachine reimplements QState::onEntry() and calls
     * QStateMachine::start(), so that when the child state machine
     * is entered, it will automatically start running.
     *
     * The parent state machine treats the child machine as an
     * atomic state in the state machine algorithm. The child
     * state machine is self-contained; it maintains its own
     * event queue and configuration. In particular, note that
     * the configuration() of the child machine is not part of
     * the parent machine's configuration (only the child machine
     * itself is).
     *
     * States of the child state machine cannot be specified as
     * targets of transitions in the parent state machine; only
     * the child state machine itself can. Conversely, states of
     * the parent state machine cannot be specified as targets of
     * transitions in the child state machine. The child state
     * machine's finished() signal can be used to trigger a
     * transition in the parent machine.
     *
     * You can post your own events to the state machine using
     * QStateMachine::postEvent().
     *
     * When posting a custom event to the state machine, you typically
     * also have one or more custom transitions that can be triggered
     * from events of that type. To create such a transition, you
     * subclass QAbstractTransition and reimplement
     * QAbstractTransition::eventTest(), where you check if an event
     * matches your event type (and optionally other criteria,
     * e.g. attributes of the event object).
     *
     * In the eventTest() reimplementation, we first check if the
     * event type is the desired one; if so, we cast the event to
     * a StringEvent and perform the string comparison.
     *
     * An event that is not handled by any relevant transition
     * will be silently consumed by the state machine. It can be
     * useful to group states and provide a default handling of
     * such events.
     *
     * In some state machines it can be useful to focus the
     * attention on assigning properties in states, not on
     * restoring them when the state is no longer active. If you
     * know that a property should always be restored to its
     * initial value when the machine enters a state that does
     * not explicitly give the property a value, you can set the
     * global restore policy to QStateMachine::RestoreProperties.
     *
     * When this restore policy is set, the machine will
     * automatically restore all properties. If it enters a state
     * where a given property is not set, it will first search
     * the hierarchy of ancestors to see if the property is
     * defined there. If it is, the property will be restored to
     * the value defined by the closest ancestor. If not, it will
     * be restored to its initial value (i.e. the value of the
     * property before any property assignments in states were
     * executed.)
     *
     * Also, for more detailed information, see the documentation
     * on the QStateMachine Framework in the Qt Official Documentation
     * that comes with the SDK or online at http://qt.io.
     *
     */
    QStateMachine    tncsm;

    QAudioFormat     stcSCFormat;
    QAudioFormat     m_format;
    QAudioDeviceInfo devCaptureDevice;
    // Objects on the heap   //
    class PanelPrivate;
    PanelPrivate  *mwd;

    QState        *state_DISC;
    QState        *state_Idle;
    QState        *state_ISS;
    QState        *state_IRS;
    QState        *state_DCD;
    QState        *state_ERR;
    QState        *state_NODCD;
    QState        *state_ModeChg;
    QState        *state_SHUTDOWN;

    // controls to display on the dialog that are not native as
    // in QMainWindow
    QMenuBar     *menubar;
    QMenu        *fileMenu;
    QMenu        *setupMenu;
    QMenu        *helpMenu;
    QAction      *actionFileExit;
    QAction      *actionSetupRadio;
    QAction      *actionSetupAudio;
    QAction      *actionSetupTNC;
    QAction      *actionHelpAbout;
    QAction      *actionHelpIndex;

    // Qt Library Audio Objects
    QAudioInput  *audioInput;
    QAudioOutput *audioOutput;
    QIODevice    *m_input;

    // Sub-classed local object
    AudioInputIO        *audioIOObject;  //!< \sa AudioInputIO


    // Local Control Block objects used by reference elsewhere
    ModemControlBlock *mcb;
    RadioControlBlock *rcb;


    void initializeLocalVars();
    void initializeFromIni();
    void initializeAudio();
    void clearPanel();
    void createMenus();
    void createStateMachine();
    void createLocalObjects();
    void finishGUI();
    /*! \details
     * The format and capture device must have been set
     * before calling this function since both are needed
     * to create the input object   */
    void createAudioInput();

    void connectAndSetupUi();
    void connectSignals();
    void readSettings();
    void writeSettings();
    void audioInputError(QAudio::Error);
    double getVolumeDb();
};

#endif // TNCPANEL_HPP
