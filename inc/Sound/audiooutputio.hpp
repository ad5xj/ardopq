#ifndef AUDIOOUTPUTIO_HPP
#define AUDIOOUTPUTIO_HPP
/*! \file audioio.hpp */
#include <QtCore/QIODevice>
#include <QtCore/QByteArray>
#include <QtMultimedia/QAudioFormat>

#include "ardop_global.hpp"
#include "bufferobject.hpp"

#define PUSH_MODE_LABEL "Enable push mode"
#define PULL_MODE_LABEL "Enable pull mode"
#define SUSPEND_LABEL   "Suspend recording"
#define RESUME_LABEL    "Resume recording"

const qint32 BufferSize = 4096;

/*! \ingroup SOUND
 * \class AudioOutputIO
 * \brief The Audio Output IO Object
 * \details
 * Qt uses this sub-class of QIODevice to write data to
 * the Audio output object selected. Because QtMultimedia
 * allows the application to access the raw audio stream,
 * the data can be modified by the application before
 * arriving here.
 *
 * The application has data to be written created by the
 * encoder / modulator DSP operations after MCB.PTT is enabled.
 */
class AudioOutputIO : public QIODevice
{
    Q_OBJECT
    Q_PROPERTY(bool blnFormatSet READ isFormatSet)          //!< read only
    Q_PROPERTY(quint32      m_samplerate READ getSampleRate WRITE setSampleRate)
    Q_PROPERTY(QAudioFormat m_format     READ format        WRITE setAudioFormat)

public:
    AudioOutputIO(QObject *parent=0);
    ~AudioOutputIO();

    // PROPERTY SETS AND GETS  //
    void setAudioFormat(QAudioFormat fmt);
    void setSampleRate(quint32 rate)       { m_samplerate = rate; m_format.sampleRate = rate; }

    bool isFormatSet()         { return blnFormatSet; }
    QAudioFormat format()      { return m_format;  }
    //                         //

    qint64 bytesAvailable() const;

    //--------------------------------------
    // data buffer objects
    //--------------------------------------
    BufferObject   *TxBuffer;  //!< static buffer definition for audioOutput

    qint64 readData(char *data, qint64 maxlen);     //!< \details dummy procedure to satisfy virtual purity
    qint64 writeData(const char *data, qint64 len); //!< \details write captured data to buffer \returns len of data

    void start();
    void stop();
    qreal level() const { return m_level; }

signals:
    void update();
    void signalAudioOutputOK();

public slots:

private:
    // Property Local Var Defs  //
    bool         blnFormatSet;

    quint32      m_samplerate;

    QAudioFormat m_format;
    //                          //


    quint32      m_pos;
    quint32      numUsedBytes;
    quint32      m_maxAmplitude;

    qreal        m_level; // 0.0 <= m_level <= 1.0 or percent of max 1.0 = 100%

    QMutex       mutex;

    ModemControlBlock *MCB;  //!< local version of object passed to it
};


#endif // AUDIOOUTPUTIO_HPP
