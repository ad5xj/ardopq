#ifndef BUFFEROBJECT_HPP
#define BUFFEROBJECT_HPP
#include <QtCore/QObject>
#include <QtCore/QVector>
#include <QtCore/QMutex>
#include <QtCore/QWaitCondition>
#include <QtCore/QByteArray>

enum DataType_t
{
    Int,
    UInt,
    Float,
    Double
};
typedef DataType_t DataType;

/*! \ingroup SOUND
 * \class BufferObject
 * \brief central circular buffer for tx and rx audio
 * \details
 * This is not a template but an object that represents
 * either input or output data (one for each). It is a
 * circular buffer whose size may be set by the setBufSize
 * property function. The size defaults to 1024 but the
 * current size may be retrived with getBufSize property
 * function.
 *
 * Modified to be more flexible and to vary buffer type
 * depending on data type from audio sample.<br />
 * Choices are: Integer (default), UnSigned Int, Float, Double
 *
 * There are several static elements of this object:
 *
 * static QWaitCondition bufferNotEmpty;<br />
 * static QWaitCondition bufferNotFull;<br />
 * static QMutex         Mutex              -  mutex locking for buffer<br />
 * static int            NumUsedBytes = 0;<br />
 * static QByteArray    *Buffer; <br />      */
class BufferObject : public QObject
{
    Q_OBJECT
//    Q_PROPERTY(quint8  m_datatype   READ getDataType WRITE setDataType)
    Q_PROPERTY(quint32 m_bufferSize READ getBufSize  WRITE setBufSize)
    Q_PROPERTY(quint32 m_buflen     READ length) // same thing as getBufSize but provided for backward compatibility.

public:
    BufferObject(QObject *parent=0);
    virtual ~BufferObject();

    volatile int          NumUsedBytes;

    QWaitCondition        bufferNotEmpty;     //!< Wait condition for circular buffer use
    QWaitCondition        bufferNotFull;      //!< Wait condition for circular buffer use
    QMutex                mutex;              //!< Platform Mutex locking for circular buffer use

    QByteArray           *bufferchar;         // not sure if we need this here
    QVector<qint32>      *bufferint;
    QVector<quint32>     *bufferuint;
    QVector<float>       *bufferfl;

    // PROPERTY SETS AND GETS     //
    void setBufSize(quint32 bz)                 { m_bufferSize = bz; }
//    void setDataType(int typ)                   { m_datatype = (DataType)typ;  }


//    QString getDataType();
    quint32 getBufSize()                        { return m_bufferSize; }
    quint32 length()                            { return m_bufferSize; } // same thing as getBufSize but provided for backward compatibility
    // end property sets and gets //

    void initialize();                        //!< reset buffer to zero and get ready to start use
    QByteArray       *cdata()                            { return bufferchar; }
    QVector<qint32>  *idata()                            { return bufferint; }
    QVector<quint32> * udata()                           { return bufferuint; }
    QVector<float>   * fdata()                            { return bufferfl; }
    void writeBuffer(QByteArray *, quint32);


private:
    // PROPERTY VAR LOCAL DEFINITIONS //
    quint32  m_bufferSize;
    quint32  m_buflen;
//    DataType m_datatype;
    //                                //

    static const int DataSize = 100000;
    static quint32 stwByte;
    static quint32 strByte;
};

#endif // BUFFEROBJECT_HPP
