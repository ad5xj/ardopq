#ifndef DLGSNDCONFIG_HPP
#define DLGSNDCONFIG_HPP
#include <QtCore/QEvent>
#include <QtGui/QCloseEvent>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDialog>

#include "ardop_global.hpp"
#include "tncpanel.hpp"

#include "ui_dlgsndconfig.h"

class DlgSndConfig : public QDialog
{
    Q_OBJECT

public:
    explicit DlgSndConfig(TNCPanel *parent = 0);
    virtual ~DlgSndConfig();

    void setMCB(ModemControlBlock *mcb);

protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *);

private slots:
    void slotSaveSettings();
    void slotSetDirty();

private:
    class SndConfigPrivate;

    Ui::DlgSndConfig  *ui;
    SndConfigPrivate  *cd;     //!< *d-ptr to private local vars
    ModemControlBlock *MCB;

    void findDevices();
    void displaySettings();
    void readSettings();
    void saveSettings();
};

#endif // DLGSNDCONFIG_HPP
