#include "helpabout.hpp"

HelpAbout::HelpAbout(QWidget *parent) : QDialog(parent)
{
    ui = new Ui::HelpAbout;
    ui->setupUi(this);
    setStyleSheet("QDialog {color: white; background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #aaa, stop: 1 #222);}");

    QString msg;

    msg += tr("<b>ARDOP Open Source Modem Project</b><br />"
              "copyright &copy; 2016 Ken Standard AD5XJ<br />"
              "contact: Ken ad5xj@arrl.net<br /><br />"
              "This ARDOP Modem software project is released under the GPL v3 or later, license.<br />"
              "See the copyright notice in the header portion of the source code for more detail.<br /><br />"
              "This application is an abstract conversion of the ARDOP modem project originally written in VB.NET" 
              "or 'C' for Windows by Rick Muething, KN6KB and John Wiseman, G8BPQ.<br /><br />"
              "There has been a concious attempt to leverage existing code and is working well in other applications. "
              "It is designed to be platform independent and transportable. It was completely developed in C++ " 
              "using the Digia Open Source Qt libraries version 5.7.n &copy; 2016 Digia Corp.<br /><br />"
              "Qt is licensed under the GNU Lesser General Public License (LGPL) version 3 and is appropriate " 
              "for the development of Qt applications. This application wil comply with the terms and conditions " 
              "of the GNU LGPL version 3 (or GNU GPL version 3). The License was included in the source code " 
              "files of this project. If you did not receive a copy of the license, please got to http://fsf.org " 
              "for a current version or write to:<br /><br />"
              "&nbsp;&nbsp;&nbsp;Free Software Foundation, Inc.<br />"
              "&nbsp;&nbsp;&nbsp;59 Temple Place - Suite 330,<br />"
              "&nbsp;&nbsp;&nbsp;Boston, MA  02111-1307, USA.<br /><br />"
              "Parts of this project were derived from the Qt SDK Examples. Many thanks are due to the " 
              "Digia Corp. perveyors of the Qt Framework and SDK. All applicable copyrights by Digia apply.<br /><br />"
              "Also included in this project is the Open Source libraries of the MIT Fast Fourier Transform project " 
              "FFTW v3 by<br />"
              "&nbsp;&nbsp;&nbsp;Copyright &copy; 2003, 2007-14 Matteo Frigo<br />"
              "&nbsp;&nbsp;&nbsp;Copyright &copy; 2003, 2007-14 Massachusetts Institute of Technology<br />"
              "&nbsp;&nbsp;&nbsp;released under the GLP v3 or later license. See the included license.<br /><br />"
              "Also from QSSTV Copyright &copy; 2000-2012 <br />"
              "&nbsp;&nbsp;&nbsp;by Johan Maes, ON4QZ <br />"
              "&nbsp;&nbsp;&nbsp;on4qz@telenet.be <br />"
              "&nbsp;&nbsp;&nbsp;http://users.telenet.be/on4qz<br />"
              "released as a GPL v3 project.<br /><br />"
              "Thanks are due to Johan Maes for his help and the basis for some of the<br />"
              "code in this project modified from the QSSTV project. Thanks for your help <br />"
              "and consultation Johan.<br /><br />"
              "Much help and support also came from ham radio friends and fellow programmers like:<br />"
              "&nbsp;&nbsp;&nbsp;Doug Collinge, VE3GNU  - for his contributions and support <br />"
              "as well as:<br /><br />"
              "&nbsp;&nbsp;&nbsp;Patrick Deschastres, F1JSC  - for his help and expertise.<br /><br /><br /><br />"
              "The GPL License was included in the source code files of this project. If you did not receive "
              "a copy of the license, please got to http://fsf.org for a current version or write to:<br />"
              "&nbsp;&nbsp;&nbsp;Free Software Foundation, Inc.<br />"
              "&nbsp;&nbsp;&nbsp;59 Temple Place - Suite 330,<br />"
              "&nbsp;&nbsp;&nbsp;Boston, MA  02111-1307, USA.<br /><br />"
              "You may source code or binaries from this project under the terms of the GPL v3 or later "
              "license as follows:<br /><br />"
              "Redistribution and use in source and binary forms, with or without modification,<br />"
              "are permitted provided that the following notice is included with the source code:<br /><br />"
              "THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS IS' AND <br />"
              "ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED  <br />"
              "WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE <br />"
              "DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR  <br />"
              "ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES  <br />"
              "(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; <br />"
              "LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON  <br />"
              "ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT <br />"
              "(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS <br />"
              "SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
              );
    ui->btnOk->setStyleSheet("QPushButton {color: white; background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ccc, stop: 1 #555);}");
    ui->lblMsg->document()->setHtml(msg);
    ui->lblMsg->setReadOnly(true);
    setModal(true);
    setWindowFlags(Qt::FramelessWindowHint);
    setParent(0); // Create TopLevel-Widget
    setAttribute(Qt::WA_NoSystemBackground, true);
    connect(ui->btnOk,SIGNAL(clicked(bool)),this,SLOT(close()));
}

void HelpAbout::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
