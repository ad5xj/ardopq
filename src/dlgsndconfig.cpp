#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtMultimedia/QAudioFormat>
#include <QtMultimedia/QAudioInput>
#include <QtMultimedia/QAudioOutput>
#include <QtMultimedia/QAudioDeviceInfo>

#include "dlgsndconfig.hpp"

class DlgSndConfig::SndConfigPrivate
{
public:
    bool    blnDataDirty;
    bool    blnStereo;
    bool    blnUseDefaults;

    quint16 intChannels;
    quint16 intSqLevel;
    quint16 intRxVol;
    quint16 intTxDrive;

    quint32 intRxsmplrate;
    quint32 intTxsmplrate;

    QString strCaptureDev;
    QString strPlayDev;

    QByteArray mygeo;

    QAudioDeviceInfo m_device;
    QAudioFormat     m_format;

    QList<QAudioDeviceInfo> captureDevices;
    QList<QAudioDeviceInfo> playbackDevices;
};

DlgSndConfig::DlgSndConfig(TNCPanel *parent) : QDialog(parent)
{
    cd = new SndConfigPrivate;

    ui = new Ui::DlgSndConfig;
    ui->setupUi(this);

}

DlgSndConfig::~DlgSndConfig()
{
    delete ui;
    delete cd;
}

void DlgSndConfig::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void DlgSndConfig::closeEvent(QCloseEvent *)
{
    cd->mygeo = saveGeometry();
    MCB->Stereo = ui->chkStereo;
    MCB->PlaybackDevice = ui->cboTxPlaybackDevice->currentText().trimmed();
    MCB->CaptureDevice  = ui->cboRxCaptureDeivce->currentText().trimmed();
    MCB->RxVolume = ui->spinVol->value();
    MCB->TxDriveLevel = ui->spinDrive->value();
    saveSettings();
}

void DlgSndConfig::setMCB(ModemControlBlock *mcb)
{
    MCB = mcb;
    findDevices();
    readSettings();
    displaySettings();

    connect(ui->btnCancel,SIGNAL(clicked(bool)),this,SLOT(close()));
    connect(ui->btnSave,SIGNAL(clicked(bool)),this,SLOT(slotSaveSettings()));
    connect(ui->cboRxCaptureDeivce,SIGNAL(currentIndexChanged(int)),this,SLOT(slotSetDirty()));
    connect(ui->cboTxPlaybackDevice,SIGNAL(currentIndexChanged(int)),this,SLOT(slotSetDirty()));
}

void DlgSndConfig::slotSetDirty()
{
    cd->blnDataDirty = true;
    ui->btnSave->setEnabled(true);
}

void DlgSndConfig::slotSaveSettings()
{
    cd->mygeo = saveGeometry();
    MCB->Stereo = ui->chkStereo;
    MCB->PlaybackDevice = ui->cboTxPlaybackDevice->currentText().trimmed();
    MCB->CaptureDevice  = ui->cboRxCaptureDeivce->currentText().trimmed();
    MCB->RxVolume = ui->spinVol->value();
    MCB->TxDriveLevel = ui->spinDrive->value();
    saveSettings();
}

void DlgSndConfig::displaySettings()
{
    restoreGeometry(cd->mygeo);

#ifdef Q_OS_WIN
    ui->pulseRadioButton->setVisible(false);
    ui->alsaRadioButton->setVisible(false);

    ui->directXRadioButton->setVisible(true);
    ui->wmmRadioButton->setVisible(true);
    ui->portaudRadioButton->setVisible(true);
#endif

#ifdef Q_OS_LINUX
    ui->pulseRadioButton->setEnabled(true);
    ui->alsaRadioButton->setEnabled(true);
    ui->portaudRadioButton->setEnabled(true);

    ui->directXRadioButton->setEnabled(false);
    ui->wmmRadioButton->setEnabled(false);
#endif

    // set Stereo check from saved settings
    ui->chkStereo->setEnabled(MCB->Stereo);
    ui->spinChan->setValue(cd->intChannels);

    // find capture device saved in device list
    // find play device saved in device list
    // set saved sample rate for Rx
    // set saved sample rate for Tx
    // display Rx Vol
    ui->spinVol->setValue(MCB->RxVolume);
    // display Drive lvl
    ui->spinDrive->setValue(MCB->TxDriveLevel);
    // display Sq lvl
    ui->spinSq->setValue(MCB->RxSquelchLevel);
    // disable save until some change
    ui->btnSave->setEnabled(false);
}

void DlgSndConfig::findDevices()
{
    quint16 x = 0;
    // load the available devices into a list for reference
    cd->captureDevices.clear();
    cd->playbackDevices.clear();
    cd->captureDevices = QAudioDeviceInfo::availableDevices(QAudio::AudioInput);
    cd->playbackDevices = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
    // put device names in the combobox for selection
    for ( x = 0; x < (cd->captureDevices.count() - 1); ++x )
    {
        ui->cboRxCaptureDeivce->addItem(cd->captureDevices.at(x).deviceName());
    }

    for ( x = 0; x < (cd->playbackDevices.count() - 1); ++x )
    {
        ui->cboTxPlaybackDevice->addItem(cd->playbackDevices.at(x).deviceName());
    }
}

void DlgSndConfig::readSettings()
{
#ifdef DEBUG_AUDIO
    qDebug() << "Reading Settings from ardop_tnc.ini...";
#endif
    QSettings settings("ardop_tnc.ini",QSettings::IniFormat);
    settings.beginGroup("SNDCONFG");
     cd->mygeo = settings.value("WindowGeometry","").toByteArray();
    settings.endGroup();

    settings.beginGroup("SOUND");
     MCB->CaptureDevice  = settings.value("CaptureDevice",  "default").toString();
     MCB->PlaybackDevice = settings.value("PlaybackDevice", "default").toString();
     MCB->RxVolume       = settings.value("RxVolume",             70).toInt();
     MCB->TxDriveLevel   = settings.value("DriveLevel",         80).toInt();
     MCB->RxSquelchLevel = settings.value("Squelch",            20).toInt();
     MCB->Stereo         = settings.value("Stereo",             false).toBool();
     MCB->RxAudioSampleRate = settings.value("RxSampleRate", 8000).toInt();
     MCB->TxAudioSampleRate = settings.value("TxSampleRate", 8000).toInt();
     if (MCB->Stereo)
         cd->intChannels    = settings.value("RxChannels", 2).toInt();
     else
         cd->intChannels = 1;
    settings.endGroup();
    // update local vars with control block values
    cd->strCaptureDev   = MCB->CaptureDevice;
    cd->strPlayDev      = MCB->PlaybackDevice;
    cd->intRxsmplrate   = MCB->RxAudioSampleRate;

    settings.beginGroup("TNC");
#ifdef DEBUG_AUDIO
    qDebug() << "Reading Settings complete...returning";
#endif
}

void DlgSndConfig::saveSettings()
{
#ifdef DEBUG_AUDIO
    qDebug() << "Saving Audio Settings to ardop_tnc.ini...";
#endif
    QSettings settings("ardop_tnc.ini",QSettings::IniFormat);
    settings.beginGroup("SNDCONFG");
     settings.setValue("WindowGeometry", cd->mygeo);
    settings.endGroup();

    settings.beginGroup("SOUND");
     settings.setValue("CaptureDevice",  MCB->CaptureDevice);
     settings.setValue("PlaybackDevice", MCB->PlaybackDevice);
     settings.setValue("DriveLevel",     MCB->TxDriveLevel);
     settings.setValue("Squelch",        MCB->RxSquelchLevel);
     settings.setValue("Stereo",         MCB->Stereo);
     settings.setValue("CaptureClock",   MCB->CaptureClock);
     settings.setValue("PlaybackClock",  MCB->PlaybackClock);
    settings.endGroup();

    settings.sync();
#ifdef DEBUG_AUDIO
    qDebug() << "Audio Settings complete...returning";
#endif
}

